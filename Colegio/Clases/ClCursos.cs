﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Colegio.ET;
using System.Transactions;
using System.Data.SqlClient;

namespace Colegio.Clases
{
    public class ClCursos
    {
        public DataTable GetCursos()
        {
            try
            {
                string sql = "SELECT CS.Sec, CS.NomCurso, CS.IdTitular FROM ADCursos CS";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public DataTable GetCursosByAño(int idAño)
        {
            try
            {
                string sql = "SELECT CS.Sec, CS.NomCurso FROM ADCursos CS " +
                " INNER JOIN ADAñoLectivo AL ON CS.IdAñoLectivo = AL.Sec" +
                " WHERE AL.Sec = "+ idAño.ToString();
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<ADCursosETVista> GetCursosVista()
        {
            try
            {
                string sql = "SELECT CS.Sec AS IdCurso, CS.NomCurso, TI.IdTercero AS DocTitular " +
                " , CONCAT(TI.Pnombre, ' ', TI.Snombre, ' ', TI.Papellido, ' ', TI.Sapellido) AS NomTitular " +
                " , GD.Grado, GD.Nombre AS NomGrado, CS.IdAñoLectivo, AL.Año FROM ADCursos CS " +
                " INNER JOIN ADTerceros TI ON CS.IdTitular = TI.IdTercero " +
                " INNER JOIN ADGrados GD ON CS.IdGrado = GD.Sec " +
                " INNER JOIN ADAñoLectivo AL ON CS.IdAñoLectivo = AL.Sec";
                List<ADCursosETVista> res = ClFunciones.DataReaderMapToList<ADCursosETVista>(sql);
                return res;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool InsertCurso(ADCursosET curso)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        int Sec = ClFunciones.TraeSiguienteSecuencial("ADCursos", "Sec", con);                        
                        SqlCommand comando = new SqlCommand("SP_ADCursos_I", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pSec", Sec);
                        comando.Parameters.AddWithValue("@pNomCurso", curso.NomCurso);
                        comando.Parameters.AddWithValue("@pIdTitular", curso.IdTitular);
                        comando.Parameters.AddWithValue("@pIdAñoLectivo", curso.IdAñoLectivo);
                        comando.Parameters.AddWithValue("@pIdGrado", curso.IdGrado);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        string Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return false;
                        }
                    }
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public bool UpdateCurso(ADCursosET curso)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_ADCursos_U", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pSec", curso.Sec);
                        comando.Parameters.AddWithValue("@pNomCurso", curso.NomCurso);
                        comando.Parameters.AddWithValue("@pIdTitular", curso.IdTitular);
                        comando.Parameters.AddWithValue("@pIdAñoLectivo", curso.IdAñoLectivo);
                        comando.Parameters.AddWithValue("@pIdGrado", curso.IdGrado);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        string Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return false;
                        }
                    }
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CursoTieneMatriculas(int SecCurso)
        {
            try
            {
                string sql = "SELECT * FROM ADMatriculas MT WHERE IdCurso = "+ SecCurso;
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count<=0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public bool InsertMateriasCurso(ADMateriasCursosET ADMateriaCurso)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_ADMateriasCursos_I  ", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pIdCurso", ADMateriaCurso.IdCurso);
                        comando.Parameters.AddWithValue("@pIdMateria", ADMateriaCurso.IdMateria);
                        comando.Parameters.AddWithValue("@pIdMaestro", ADMateriaCurso.IdMaestro);
                        comando.Parameters.AddWithValue("@pDesempeños", ADMateriaCurso.Desempeños);
                        comando.Parameters.AddWithValue("@pIH", ADMateriaCurso.IH);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        string Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return false;
                        }
                    }
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public bool UpdateMateriasCurso(ADMateriasCursosET ADMateriaCurso)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_ADMateriasCursos_U", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pSec", ADMateriaCurso.Sec);
                        comando.Parameters.AddWithValue("@pIdMaestro", ADMateriaCurso.IdMaestro);
                        comando.Parameters.AddWithValue("@pDesempeños", ADMateriaCurso.Desempeños);
                        comando.Parameters.AddWithValue("@pIH", ADMateriaCurso.IH);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        string Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return false;
                        }
                    }
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
