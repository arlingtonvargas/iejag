﻿namespace Colegio
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.Imagenes16X16 = new System.Windows.Forms.ImageList(this.components);
            this.mdiPrincipal = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnEstudiantes = new DevExpress.XtraBars.BarButtonItem();
            this.btnMatriculas = new DevExpress.XtraBars.BarButtonItem();
            this.btnHorarios = new DevExpress.XtraBars.BarButtonItem();
            this.btnCursos = new DevExpress.XtraBars.BarButtonItem();
            this.mnuInformes = new DevExpress.XtraBars.BarSubItem();
            this.btnRptNotas = new DevExpress.XtraBars.BarButtonItem();
            this.btnRptNotasPorEstudiente = new DevExpress.XtraBars.BarButtonItem();
            this.btnRptEstActivosInacti = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem13 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem14 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem15 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem16 = new DevExpress.XtraBars.BarButtonItem();
            this.btnNotasPorCurso = new DevExpress.XtraBars.BarButtonItem();
            this.mnuNotas = new DevExpress.XtraBars.BarSubItem();
            this.btnReportarNotas = new DevExpress.XtraBars.BarButtonItem();
            this.btnSalir = new DevExpress.XtraBars.BarButtonItem();
            this.btnNuevMaestro = new DevExpress.XtraBars.BarButtonItem();
            this.btnEditMaestro = new DevExpress.XtraBars.BarButtonItem();
            this.btnListarMaestros = new DevExpress.XtraBars.BarButtonItem();
            this.btnRoles = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.btnCambiarContra = new DevExpress.XtraBars.BarButtonItem();
            this.btnListUsuarios = new DevExpress.XtraBars.BarButtonItem();
            this.btnUsuariosRol = new DevExpress.XtraBars.BarButtonItem();
            this.btnPermisosXrol = new DevExpress.XtraBars.BarButtonItem();
            this.btnSalirSeg = new DevExpress.XtraBars.BarButtonItem();
            this.btnMaetros = new DevExpress.XtraBars.BarButtonItem();
            this.btnDesConectar = new DevExpress.XtraBars.BarButtonItem();
            this.btnAdministrativos = new DevExpress.XtraBars.BarButtonItem();
            this.btnEst = new DevExpress.XtraBars.BarButtonItem();
            this.btnGenNotas = new DevExpress.XtraBars.BarButtonItem();
            this.btnMaestroMat = new DevExpress.XtraBars.BarButtonItem();
            this.btnGenBoletines = new DevExpress.XtraBars.BarButtonItem();
            this.btnCargarNotas = new DevExpress.XtraBars.BarButtonItem();
            this.mnuCursos = new DevExpress.XtraBars.BarSubItem();
            this.btnAddMatCurso = new DevExpress.XtraBars.BarButtonItem();
            this.btnProyectos = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.btnMaterias = new DevExpress.XtraBars.BarButtonItem();
            this.btnSedes = new DevExpress.XtraBars.BarButtonItem();
            this.btnAñoLectivo = new DevExpress.XtraBars.BarButtonItem();
            this.btnJornadas = new DevExpress.XtraBars.BarButtonItem();
            this.btnConfigGeneral = new DevExpress.XtraBars.BarButtonItem();
            this.mnuEstudiantes = new DevExpress.XtraBars.BarSubItem();
            this.btnObservador = new DevExpress.XtraBars.BarButtonItem();
            this.btnGenCarnet = new DevExpress.XtraBars.BarButtonItem();
            this.rpPrincipal = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpConexion = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpTerceros = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgInicio = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpConfiguracion = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpSeguridad = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpboletin = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpPPP = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgPPP = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.msgAyuda = new DevExpress.XtraBars.BarStaticItem();
            this.skinBarSubItem1 = new DevExpress.XtraBars.SkinBarSubItem();
            this.lblFechaIngreso = new DevExpress.XtraBars.BarStaticItem();
            this.lblUser = new DevExpress.XtraBars.BarStaticItem();
            this.lblVersion = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.Imagenes32X32 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mdiPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imagenes32X32)).BeginInit();
            this.SuspendLayout();
            // 
            // Imagenes16X16
            // 
            this.Imagenes16X16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Imagenes16X16.ImageStream")));
            this.Imagenes16X16.TransparentColor = System.Drawing.Color.Transparent;
            this.Imagenes16X16.Images.SetKeyName(0, "PrintSortAsc_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(1, "PrintSortDesc_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(2, "Close_16x16.png");
            this.Imagenes16X16.Images.SetKeyName(3, "Team_16x16.png");
            // 
            // mdiPrincipal
            // 
            this.mdiPrincipal.AppearancePage.Header.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mdiPrincipal.AppearancePage.Header.Options.UseFont = true;
            this.mdiPrincipal.AppearancePage.HeaderActive.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdiPrincipal.AppearancePage.HeaderActive.Options.UseFont = true;
            this.mdiPrincipal.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageAndTabControlHeader;
            this.mdiPrincipal.MdiParent = null;
            this.mdiPrincipal.TabPageWidth = 180;
            this.mdiPrincipal.PageAdded += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.mdiPrincipal_PageAdded);
            this.mdiPrincipal.PageRemoved += new DevExpress.XtraTabbedMdi.MdiTabPageEventHandler(this.mdiPrincipal_PageRemoved);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.btnEstudiantes,
            this.btnMatriculas,
            this.btnHorarios,
            this.btnCursos,
            this.mnuInformes,
            this.mnuNotas,
            this.btnReportarNotas,
            this.btnSalir,
            this.btnNuevMaestro,
            this.btnEditMaestro,
            this.btnListarMaestros,
            this.btnRoles,
            this.barSubItem3,
            this.btnCambiarContra,
            this.btnUsuariosRol,
            this.btnPermisosXrol,
            this.btnSalirSeg,
            this.btnRptNotas,
            this.btnRptEstActivosInacti,
            this.barButtonItem13,
            this.barButtonItem14,
            this.barButtonItem15,
            this.barButtonItem16,
            this.btnNotasPorCurso,
            this.btnMaetros,
            this.btnDesConectar,
            this.btnListUsuarios,
            this.btnAdministrativos,
            this.btnEst,
            this.btnGenNotas,
            this.btnRptNotasPorEstudiente,
            this.btnMaestroMat,
            this.btnGenBoletines,
            this.btnCargarNotas,
            this.mnuCursos,
            this.btnAddMatCurso,
            this.btnProyectos,
            this.barSubItem1,
            this.btnMaterias,
            this.btnSedes,
            this.btnAñoLectivo,
            this.btnJornadas,
            this.btnConfigGeneral,
            this.mnuEstudiantes,
            this.btnObservador,
            this.btnGenCarnet});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 66;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpPrincipal,
            this.rpSeguridad,
            this.rpboletin,
            this.rpPPP});
            this.ribbonControl1.Size = new System.Drawing.Size(992, 145);
            // 
            // btnEstudiantes
            // 
            this.btnEstudiantes.Caption = "Estudiantes";
            this.btnEstudiantes.Id = 15;
            this.btnEstudiantes.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnEstudiantes.ImageOptions.LargeImage")));
            this.btnEstudiantes.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEstudiantes.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnEstudiantes.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEstudiantes.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnEstudiantes.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstudiantes.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEstudiantes.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEstudiantes.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnEstudiantes.Name = "btnEstudiantes";
            this.btnEstudiantes.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnEstudiantes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEstudiantes_ItemClick);
            // 
            // btnMatriculas
            // 
            this.btnMatriculas.Caption = "Matriculas";
            this.btnMatriculas.Id = 16;
            this.btnMatriculas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMatriculas.ImageOptions.Image")));
            this.btnMatriculas.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnMatriculas.ImageOptions.LargeImage")));
            this.btnMatriculas.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMatriculas.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnMatriculas.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMatriculas.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnMatriculas.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMatriculas.ItemAppearance.Normal.Options.UseFont = true;
            this.btnMatriculas.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMatriculas.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnMatriculas.Name = "btnMatriculas";
            this.btnMatriculas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMatriculas_ItemClick);
            // 
            // btnHorarios
            // 
            this.btnHorarios.Caption = "Horarios";
            this.btnHorarios.Id = 17;
            this.btnHorarios.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnHorarios.ImageOptions.Image")));
            this.btnHorarios.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnHorarios.ImageOptions.LargeImage")));
            this.btnHorarios.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnHorarios.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnHorarios.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnHorarios.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnHorarios.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnHorarios.ItemAppearance.Normal.Options.UseFont = true;
            this.btnHorarios.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnHorarios.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnHorarios.Name = "btnHorarios";
            this.btnHorarios.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnCursos
            // 
            this.btnCursos.Caption = "Nuevo curso";
            this.btnCursos.Id = 18;
            this.btnCursos.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCursos.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCursos.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCursos.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCursos.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCursos.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCursos.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCursos.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCursos.Name = "btnCursos";
            this.btnCursos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCursos_ItemClick);
            // 
            // mnuInformes
            // 
            this.mnuInformes.Caption = "Informes";
            this.mnuInformes.Id = 20;
            this.mnuInformes.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuInformes.ImageOptions.Image")));
            this.mnuInformes.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuInformes.ImageOptions.LargeImage")));
            this.mnuInformes.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuInformes.ItemAppearance.Disabled.Options.UseFont = true;
            this.mnuInformes.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuInformes.ItemAppearance.Hovered.Options.UseFont = true;
            this.mnuInformes.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuInformes.ItemAppearance.Normal.Options.UseFont = true;
            this.mnuInformes.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuInformes.ItemAppearance.Pressed.Options.UseFont = true;
            this.mnuInformes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRptNotas),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRptNotasPorEstudiente),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRptEstActivosInacti),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem13),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem14),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem15),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem16),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNotasPorCurso)});
            this.mnuInformes.Name = "mnuInformes";
            // 
            // btnRptNotas
            // 
            this.btnRptNotas.Caption = "Generar Boletines";
            this.btnRptNotas.Id = 36;
            this.btnRptNotas.Name = "btnRptNotas";
            this.btnRptNotas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRptNotas_ItemClick);
            // 
            // btnRptNotasPorEstudiente
            // 
            this.btnRptNotasPorEstudiente.Caption = "Reporte de notas por estudiante";
            this.btnRptNotasPorEstudiente.Id = 50;
            this.btnRptNotasPorEstudiente.Name = "btnRptNotasPorEstudiente";
            this.btnRptNotasPorEstudiente.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRptNotasPorEstudiente_ItemClick);
            // 
            // btnRptEstActivosInacti
            // 
            this.btnRptEstActivosInacti.Caption = "Reporte de estudiantes Activos e Inactivos";
            this.btnRptEstActivosInacti.Id = 37;
            this.btnRptEstActivosInacti.Name = "btnRptEstActivosInacti";
            this.btnRptEstActivosInacti.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem13
            // 
            this.barButtonItem13.Caption = "Reporte maestros activos";
            this.barButtonItem13.Id = 38;
            this.barButtonItem13.Name = "barButtonItem13";
            this.barButtonItem13.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem14
            // 
            this.barButtonItem14.Caption = "Reporte maestros inactivos";
            this.barButtonItem14.Id = 39;
            this.barButtonItem14.Name = "barButtonItem14";
            this.barButtonItem14.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem15
            // 
            this.barButtonItem15.Caption = "Reporte de estudiantes por curso";
            this.barButtonItem15.Id = 40;
            this.barButtonItem15.Name = "barButtonItem15";
            this.barButtonItem15.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem16
            // 
            this.barButtonItem16.Caption = "Reporte estudiantes perdiendo por curso";
            this.barButtonItem16.Id = 41;
            this.barButtonItem16.Name = "barButtonItem16";
            this.barButtonItem16.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnNotasPorCurso
            // 
            this.btnNotasPorCurso.Caption = "Notas Estudiantes por curso";
            this.btnNotasPorCurso.Id = 42;
            this.btnNotasPorCurso.Name = "btnNotasPorCurso";
            this.btnNotasPorCurso.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNotasPorCurso_ItemClick);
            // 
            // mnuNotas
            // 
            this.mnuNotas.Caption = "Notas";
            this.mnuNotas.Id = 22;
            this.mnuNotas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuNotas.ImageOptions.Image")));
            this.mnuNotas.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuNotas.ImageOptions.LargeImage")));
            this.mnuNotas.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuNotas.ItemAppearance.Disabled.Options.UseFont = true;
            this.mnuNotas.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuNotas.ItemAppearance.Hovered.Options.UseFont = true;
            this.mnuNotas.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuNotas.ItemAppearance.Normal.Options.UseFont = true;
            this.mnuNotas.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuNotas.ItemAppearance.Pressed.Options.UseFont = true;
            this.mnuNotas.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnReportarNotas)});
            this.mnuNotas.Name = "mnuNotas";
            // 
            // btnReportarNotas
            // 
            this.btnReportarNotas.Caption = "Reportar notas";
            this.btnReportarNotas.Id = 23;
            this.btnReportarNotas.Name = "btnReportarNotas";
            this.btnReportarNotas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReportarNotas_ItemClick);
            // 
            // btnSalir
            // 
            this.btnSalir.Caption = "Salir";
            this.btnSalir.Id = 24;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.LargeImage")));
            this.btnSalir.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalir.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnSalir.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalir.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnSalir.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalir.ItemAppearance.Normal.Options.UseFont = true;
            this.btnSalir.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalir.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSalir_ItemClick);
            // 
            // btnNuevMaestro
            // 
            this.btnNuevMaestro.Caption = "Nuevo";
            this.btnNuevMaestro.Id = 26;
            this.btnNuevMaestro.Name = "btnNuevMaestro";
            this.btnNuevMaestro.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNuevMaestro_ItemClick);
            // 
            // btnEditMaestro
            // 
            this.btnEditMaestro.Caption = "Editar";
            this.btnEditMaestro.Id = 27;
            this.btnEditMaestro.Name = "btnEditMaestro";
            this.btnEditMaestro.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnListarMaestros_ItemClick);
            // 
            // btnListarMaestros
            // 
            this.btnListarMaestros.Caption = "Lista de Maestros";
            this.btnListarMaestros.Id = 28;
            this.btnListarMaestros.Name = "btnListarMaestros";
            this.btnListarMaestros.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnListarMaestros_ItemClick);
            // 
            // btnRoles
            // 
            this.btnRoles.Caption = "Roles";
            this.btnRoles.Id = 29;
            this.btnRoles.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRoles.ImageOptions.Image")));
            this.btnRoles.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnRoles.ImageOptions.LargeImage")));
            this.btnRoles.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnRoles.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnRoles.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnRoles.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnRoles.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnRoles.ItemAppearance.Normal.Options.UseFont = true;
            this.btnRoles.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnRoles.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnRoles.Name = "btnRoles";
            this.btnRoles.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRoles_ItemClick);
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Usuarios";
            this.barSubItem3.Id = 30;
            this.barSubItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.Image")));
            this.barSubItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barSubItem3.ImageOptions.LargeImage")));
            this.barSubItem3.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem3.ItemAppearance.Disabled.Options.UseFont = true;
            this.barSubItem3.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem3.ItemAppearance.Hovered.Options.UseFont = true;
            this.barSubItem3.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem3.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem3.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem3.ItemAppearance.Pressed.Options.UseFont = true;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCambiarContra),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnListUsuarios)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // btnCambiarContra
            // 
            this.btnCambiarContra.Caption = "Cambiar contraseña";
            this.btnCambiarContra.Id = 31;
            this.btnCambiarContra.Name = "btnCambiarContra";
            this.btnCambiarContra.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCambiarContra_ItemClick);
            // 
            // btnListUsuarios
            // 
            this.btnListUsuarios.Caption = "Lista Usuarios";
            this.btnListUsuarios.Id = 45;
            this.btnListUsuarios.Name = "btnListUsuarios";
            this.btnListUsuarios.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnListUsuarios_ItemClick);
            // 
            // btnUsuariosRol
            // 
            this.btnUsuariosRol.Caption = "Roles por usuario";
            this.btnUsuariosRol.Id = 32;
            this.btnUsuariosRol.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnUsuariosRol.ImageOptions.Image")));
            this.btnUsuariosRol.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnUsuariosRol.ImageOptions.LargeImage")));
            this.btnUsuariosRol.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnUsuariosRol.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnUsuariosRol.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnUsuariosRol.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnUsuariosRol.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnUsuariosRol.ItemAppearance.Normal.Options.UseFont = true;
            this.btnUsuariosRol.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnUsuariosRol.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnUsuariosRol.Name = "btnUsuariosRol";
            this.btnUsuariosRol.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUsuariosRol_ItemClick);
            // 
            // btnPermisosXrol
            // 
            this.btnPermisosXrol.Caption = "Permisos por rol";
            this.btnPermisosXrol.Id = 33;
            this.btnPermisosXrol.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPermisosXrol.ImageOptions.Image")));
            this.btnPermisosXrol.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnPermisosXrol.ImageOptions.LargeImage")));
            this.btnPermisosXrol.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnPermisosXrol.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnPermisosXrol.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnPermisosXrol.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnPermisosXrol.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnPermisosXrol.ItemAppearance.Normal.Options.UseFont = true;
            this.btnPermisosXrol.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnPermisosXrol.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnPermisosXrol.Name = "btnPermisosXrol";
            this.btnPermisosXrol.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPermisosXrol_ItemClick);
            // 
            // btnSalirSeg
            // 
            this.btnSalirSeg.Caption = "Salir";
            this.btnSalirSeg.Id = 35;
            this.btnSalirSeg.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalirSeg.ImageOptions.Image")));
            this.btnSalirSeg.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnSalirSeg.ImageOptions.LargeImage")));
            this.btnSalirSeg.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalirSeg.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnSalirSeg.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalirSeg.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnSalirSeg.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalirSeg.ItemAppearance.Normal.Options.UseFont = true;
            this.btnSalirSeg.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnSalirSeg.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnSalirSeg.Name = "btnSalirSeg";
            this.btnSalirSeg.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSalirSeg_ItemClick);
            // 
            // btnMaetros
            // 
            this.btnMaetros.Caption = "Maestros";
            this.btnMaetros.Id = 43;
            this.btnMaetros.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMaetros.ImageOptions.Image")));
            this.btnMaetros.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMaetros.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnMaetros.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMaetros.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnMaetros.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMaetros.ItemAppearance.Normal.Options.UseFont = true;
            this.btnMaetros.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnMaetros.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnMaetros.Name = "btnMaetros";
            this.btnMaetros.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.btnMaetros.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMaetros_ItemClick);
            // 
            // btnDesConectar
            // 
            this.btnDesConectar.Caption = "Desconectar";
            this.btnDesConectar.Id = 44;
            this.btnDesConectar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnDesConectar.ImageOptions.Image")));
            this.btnDesConectar.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnDesConectar.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnDesConectar.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnDesConectar.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnDesConectar.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnDesConectar.ItemAppearance.Normal.Options.UseFont = true;
            this.btnDesConectar.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnDesConectar.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnDesConectar.Name = "btnDesConectar";
            this.btnDesConectar.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnDesConectar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConectar_ItemClick);
            // 
            // btnAdministrativos
            // 
            this.btnAdministrativos.Caption = "Administrativos";
            this.btnAdministrativos.Id = 46;
            this.btnAdministrativos.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdministrativos.ImageOptions.Image")));
            this.btnAdministrativos.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnAdministrativos.ImageOptions.LargeImage")));
            this.btnAdministrativos.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAdministrativos.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnAdministrativos.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAdministrativos.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnAdministrativos.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAdministrativos.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAdministrativos.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAdministrativos.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnAdministrativos.Name = "btnAdministrativos";
            this.btnAdministrativos.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.btnAdministrativos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdministrativos_ItemClick);
            // 
            // btnEst
            // 
            this.btnEst.Caption = "Estudiantes";
            this.btnEst.Id = 47;
            this.btnEst.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEst.ImageOptions.Image")));
            this.btnEst.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnEst.ImageOptions.LargeImage")));
            this.btnEst.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEst.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnEst.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEst.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnEst.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEst.ItemAppearance.Normal.Options.UseFont = true;
            this.btnEst.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnEst.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnEst.Name = "btnEst";
            this.btnEst.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnEst.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEst_ItemClick);
            // 
            // btnGenNotas
            // 
            this.btnGenNotas.Caption = "Generar Boletines";
            this.btnGenNotas.Id = 48;
            this.btnGenNotas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGenNotas.ImageOptions.Image")));
            this.btnGenNotas.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnGenNotas.ImageOptions.LargeImage")));
            this.btnGenNotas.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnGenNotas.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnGenNotas.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnGenNotas.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnGenNotas.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnGenNotas.ItemAppearance.Normal.Options.UseFont = true;
            this.btnGenNotas.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnGenNotas.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnGenNotas.Name = "btnGenNotas";
            this.btnGenNotas.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnGenNotas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGenNotas_ItemClick);
            // 
            // btnMaestroMat
            // 
            this.btnMaestroMat.Caption = "Asignar Materias a Maestro";
            this.btnMaestroMat.Id = 51;
            this.btnMaestroMat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnMaestroMat.ImageOptions.Image")));
            this.btnMaestroMat.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnMaestroMat.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnMaestroMat.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnMaestroMat.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnMaestroMat.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnMaestroMat.ItemAppearance.Normal.Options.UseFont = true;
            this.btnMaestroMat.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnMaestroMat.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnMaestroMat.Name = "btnMaestroMat";
            this.btnMaestroMat.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnMaestroMat.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMaestroMat_ItemClick);
            // 
            // btnGenBoletines
            // 
            this.btnGenBoletines.Caption = "Generar Boletines";
            this.btnGenBoletines.Id = 52;
            this.btnGenBoletines.Name = "btnGenBoletines";
            this.btnGenBoletines.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnGenBoletines.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGenBoletines_ItemClick);
            // 
            // btnCargarNotas
            // 
            this.btnCargarNotas.Caption = "Cargar Notas";
            this.btnCargarNotas.Id = 53;
            this.btnCargarNotas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCargarNotas.ImageOptions.Image")));
            this.btnCargarNotas.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCargarNotas.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnCargarNotas.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCargarNotas.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnCargarNotas.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCargarNotas.ItemAppearance.Normal.Options.UseFont = true;
            this.btnCargarNotas.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnCargarNotas.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnCargarNotas.Name = "btnCargarNotas";
            this.btnCargarNotas.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            this.btnCargarNotas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCargarNotas_ItemClick);
            // 
            // mnuCursos
            // 
            this.mnuCursos.Caption = "Cursos";
            this.mnuCursos.Id = 54;
            this.mnuCursos.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuCursos.ItemAppearance.Disabled.Options.UseFont = true;
            this.mnuCursos.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuCursos.ItemAppearance.Hovered.Options.UseFont = true;
            this.mnuCursos.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuCursos.ItemAppearance.Normal.Options.UseFont = true;
            this.mnuCursos.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.mnuCursos.ItemAppearance.Pressed.Options.UseFont = true;
            this.mnuCursos.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCursos),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAddMatCurso)});
            this.mnuCursos.Name = "mnuCursos";
            // 
            // btnAddMatCurso
            // 
            this.btnAddMatCurso.Caption = "Asignaturas por curso";
            this.btnAddMatCurso.Id = 55;
            this.btnAddMatCurso.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAddMatCurso.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnAddMatCurso.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAddMatCurso.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnAddMatCurso.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAddMatCurso.ItemAppearance.Normal.Options.UseFont = true;
            this.btnAddMatCurso.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAddMatCurso.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnAddMatCurso.Name = "btnAddMatCurso";
            this.btnAddMatCurso.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAddMatCurso_ItemClick);
            // 
            // btnProyectos
            // 
            this.btnProyectos.Caption = "Proyectos";
            this.btnProyectos.Id = 56;
            this.btnProyectos.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnProyectos.ImageOptions.Image")));
            this.btnProyectos.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnProyectos.ItemAppearance.Disabled.Options.UseFont = true;
            this.btnProyectos.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnProyectos.ItemAppearance.Hovered.Options.UseFont = true;
            this.btnProyectos.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnProyectos.ItemAppearance.Normal.Options.UseFont = true;
            this.btnProyectos.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnProyectos.ItemAppearance.Pressed.Options.UseFont = true;
            this.btnProyectos.Name = "btnProyectos";
            this.btnProyectos.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Configuracion";
            this.barSubItem1.Id = 57;
            this.barSubItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barSubItem1.ImageOptions.Image")));
            this.barSubItem1.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem1.ItemAppearance.Disabled.Options.UseFont = true;
            this.barSubItem1.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem1.ItemAppearance.Hovered.Options.UseFont = true;
            this.barSubItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barSubItem1.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.barSubItem1.ItemAppearance.Pressed.Options.UseFont = true;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuCursos),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMaterias),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnSedes),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAñoLectivo),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnJornadas),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConfigGeneral)});
            this.barSubItem1.Name = "barSubItem1";
            this.barSubItem1.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // btnMaterias
            // 
            this.btnMaterias.Caption = "Asignaturas";
            this.btnMaterias.Id = 58;
            this.btnMaterias.Name = "btnMaterias";
            this.btnMaterias.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMaterias_ItemClick);
            // 
            // btnSedes
            // 
            this.btnSedes.Caption = "Sedes";
            this.btnSedes.Id = 59;
            this.btnSedes.Name = "btnSedes";
            this.btnSedes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSedes_ItemClick);
            // 
            // btnAñoLectivo
            // 
            this.btnAñoLectivo.Caption = "Años lectivos";
            this.btnAñoLectivo.Id = 60;
            this.btnAñoLectivo.Name = "btnAñoLectivo";
            this.btnAñoLectivo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAñoLectivo_ItemClick);
            // 
            // btnJornadas
            // 
            this.btnJornadas.Caption = "Jornadas";
            this.btnJornadas.Id = 61;
            this.btnJornadas.Name = "btnJornadas";
            this.btnJornadas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnJornadas_ItemClick);
            // 
            // btnConfigGeneral
            // 
            this.btnConfigGeneral.Caption = "Configuración General";
            this.btnConfigGeneral.Id = 62;
            this.btnConfigGeneral.Name = "btnConfigGeneral";
            this.btnConfigGeneral.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConfigGeneral_ItemClick);
            // 
            // mnuEstudiantes
            // 
            this.mnuEstudiantes.Caption = "Estudiantes";
            this.mnuEstudiantes.Id = 63;
            this.mnuEstudiantes.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuEstudiantes.ImageOptions.Image")));
            this.mnuEstudiantes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnEstudiantes),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnObservador),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnGenCarnet)});
            this.mnuEstudiantes.Name = "mnuEstudiantes";
            this.mnuEstudiantes.RibbonStyle = ((DevExpress.XtraBars.Ribbon.RibbonItemStyles)(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) 
            | DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText)));
            // 
            // btnObservador
            // 
            this.btnObservador.Caption = "Observador";
            this.btnObservador.Id = 64;
            this.btnObservador.Name = "btnObservador";
            this.btnObservador.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnObservador_ItemClick);
            // 
            // btnGenCarnet
            // 
            this.btnGenCarnet.Caption = "Generar Carnet";
            this.btnGenCarnet.Id = 65;
            this.btnGenCarnet.Name = "btnGenCarnet";
            this.btnGenCarnet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGenCarnet_ItemClick);
            // 
            // rpPrincipal
            // 
            this.rpPrincipal.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rpPrincipal.Appearance.Options.UseFont = true;
            this.rpPrincipal.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpConexion,
            this.rpTerceros,
            this.rpgInicio,
            this.rpConfiguracion,
            this.ribbonPageGroup3});
            this.rpPrincipal.Name = "rpPrincipal";
            this.rpPrincipal.Text = "Inicio";
            // 
            // rpConexion
            // 
            this.rpConexion.ItemLinks.Add(this.btnDesConectar);
            this.rpConexion.Name = "rpConexion";
            this.rpConexion.ShowCaptionButton = false;
            this.rpConexion.Text = "Conexión";
            // 
            // rpTerceros
            // 
            this.rpTerceros.ItemLinks.Add(this.btnAdministrativos);
            this.rpTerceros.ItemLinks.Add(this.btnMaetros);
            this.rpTerceros.ItemLinks.Add(this.mnuEstudiantes, true);
            this.rpTerceros.Name = "rpTerceros";
            this.rpTerceros.ShowCaptionButton = false;
            this.rpTerceros.Text = "Terceros";
            // 
            // rpgInicio
            // 
            this.rpgInicio.ItemLinks.Add(this.btnMatriculas, true);
            this.rpgInicio.ItemLinks.Add(this.btnHorarios, true);
            this.rpgInicio.ItemLinks.Add(this.mnuNotas, true);
            this.rpgInicio.ItemLinks.Add(this.mnuInformes, true);
            this.rpgInicio.Name = "rpgInicio";
            this.rpgInicio.ShowCaptionButton = false;
            this.rpgInicio.Text = "Gestión";
            // 
            // rpConfiguracion
            // 
            this.rpConfiguracion.ItemLinks.Add(this.barSubItem1);
            this.rpConfiguracion.Name = "rpConfiguracion";
            this.rpConfiguracion.ShowCaptionButton = false;
            this.rpConfiguracion.Text = "Configuración";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnSalir);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "Salir";
            // 
            // rpSeguridad
            // 
            this.rpSeguridad.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.rpSeguridad.Appearance.Options.UseFont = true;
            this.rpSeguridad.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.rpSeguridad.Name = "rpSeguridad";
            this.rpSeguridad.Text = "Seguridad";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barSubItem3, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnRoles, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnUsuariosRol, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnPermisosXrol, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnSalirSeg, true);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "Módulo de seguridad";
            // 
            // rpboletin
            // 
            this.rpboletin.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.rpboletin.Appearance.Options.UseFont = true;
            this.rpboletin.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2});
            this.rpboletin.Name = "rpboletin";
            this.rpboletin.Text = "Boletines";
            this.rpboletin.Visible = false;
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnEst, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnGenNotas, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnGenBoletines, true);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnCargarNotas, true);
            this.ribbonPageGroup2.ItemsLayout = DevExpress.XtraBars.Ribbon.RibbonPageGroupItemsLayout.OneRow;
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Generar Boletines";
            // 
            // rpPPP
            // 
            this.rpPPP.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.rpPPP.Appearance.Options.UseFont = true;
            this.rpPPP.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgPPP});
            this.rpPPP.Name = "rpPPP";
            this.rpPPP.Text = "PPP";
            this.rpPPP.Visible = false;
            // 
            // rpgPPP
            // 
            this.rpgPPP.AllowTextClipping = false;
            this.rpgPPP.ItemLinks.Add(this.btnProyectos);
            this.rpgPPP.Name = "rpgPPP";
            this.rpgPPP.ShowCaptionButton = false;
            this.rpgPPP.Text = "Módulo PPP";
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbonControl1;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.skinBarSubItem1,
            this.msgAyuda,
            this.lblFechaIngreso,
            this.lblUser,
            this.lblVersion});
            this.barManager1.MaxItemId = 5;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Barra de estado";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.msgAyuda, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.skinBarSubItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblFechaIngreso, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblUser, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblVersion, true)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Barra de estado";
            // 
            // msgAyuda
            // 
            this.msgAyuda.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.msgAyuda.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.msgAyuda.Id = 1;
            this.msgAyuda.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgAyuda.ItemAppearance.Disabled.Options.UseFont = true;
            this.msgAyuda.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.msgAyuda.ItemAppearance.Hovered.Options.UseFont = true;
            this.msgAyuda.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.msgAyuda.ItemAppearance.Normal.Options.UseFont = true;
            this.msgAyuda.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.msgAyuda.ItemAppearance.Pressed.Options.UseFont = true;
            this.msgAyuda.Name = "msgAyuda";
            // 
            // skinBarSubItem1
            // 
            this.skinBarSubItem1.AllowRightClickInMenu = false;
            this.skinBarSubItem1.Caption = "Seleccione el tema";
            this.skinBarSubItem1.Id = 0;
            this.skinBarSubItem1.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.skinBarSubItem1.ItemAppearance.Disabled.Options.UseFont = true;
            this.skinBarSubItem1.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.skinBarSubItem1.ItemAppearance.Hovered.Options.UseFont = true;
            this.skinBarSubItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.skinBarSubItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.skinBarSubItem1.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.skinBarSubItem1.ItemAppearance.Pressed.Options.UseFont = true;
            this.skinBarSubItem1.Name = "skinBarSubItem1";
            // 
            // lblFechaIngreso
            // 
            this.lblFechaIngreso.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblFechaIngreso.Caption = "01-01-2018";
            this.lblFechaIngreso.Id = 2;
            this.lblFechaIngreso.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.lblFechaIngreso.ItemAppearance.Normal.Options.UseFont = true;
            this.lblFechaIngreso.Name = "lblFechaIngreso";
            // 
            // lblUser
            // 
            this.lblUser.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblUser.Caption = "user";
            this.lblUser.Id = 3;
            this.lblUser.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.lblUser.ItemAppearance.Normal.Options.UseFont = true;
            this.lblUser.Name = "lblUser";
            // 
            // lblVersion
            // 
            this.lblVersion.Caption = "V1.0";
            this.lblVersion.Id = 4;
            this.lblVersion.Name = "lblVersion";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(992, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 539);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(992, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 539);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(992, 0);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 539);
            // 
            // Imagenes32X32
            // 
            this.Imagenes32X32.ImageSize = new System.Drawing.Size(32, 32);
            this.Imagenes32X32.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("Imagenes32X32.ImageStream")));
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Stretch;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(992, 564);
            this.Controls.Add(this.ribbonControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Office 2016 Colorful";
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "FrmPrincipal";
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mdiPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Imagenes32X32)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.ImageList Imagenes16X16;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager mdiPrincipal;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpPrincipal;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgInicio;
        private DevExpress.XtraBars.BarButtonItem btnCursos;
        private DevExpress.XtraBars.BarSubItem mnuInformes;
        private DevExpress.XtraBars.BarSubItem mnuNotas;
        private DevExpress.XtraBars.BarButtonItem btnReportarNotas;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.SkinBarSubItem skinBarSubItem1;
        private DevExpress.XtraBars.BarStaticItem lblFechaIngreso;
        private DevExpress.XtraBars.BarStaticItem lblUser;
        public DevExpress.XtraBars.BarStaticItem msgAyuda;
        private DevExpress.XtraBars.BarButtonItem btnSalir;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem btnNuevMaestro;
        private DevExpress.XtraBars.BarButtonItem btnEditMaestro;
        private DevExpress.XtraBars.BarButtonItem btnListarMaestros;
        private DevExpress.XtraBars.BarButtonItem btnRoles;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarButtonItem btnCambiarContra;
        private DevExpress.XtraBars.BarButtonItem btnUsuariosRol;
        private DevExpress.XtraBars.BarButtonItem btnPermisosXrol;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpSeguridad;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem btnSalirSeg;
        private DevExpress.XtraBars.BarButtonItem btnRptNotas;
        private DevExpress.XtraBars.BarButtonItem btnRptEstActivosInacti;
        private DevExpress.XtraBars.BarButtonItem barButtonItem13;
        private DevExpress.XtraBars.BarButtonItem barButtonItem14;
        private DevExpress.XtraBars.BarButtonItem barButtonItem15;
        private DevExpress.XtraBars.BarButtonItem barButtonItem16;
        private DevExpress.XtraBars.BarButtonItem btnNotasPorCurso;
        public DevExpress.XtraBars.BarButtonItem btnEstudiantes;
        public DevExpress.XtraBars.BarButtonItem btnMatriculas;
        public DevExpress.XtraBars.BarButtonItem btnHorarios;
        private DevExpress.XtraBars.BarButtonItem btnMaetros;
        private DevExpress.XtraBars.BarButtonItem btnDesConectar;
        public DevExpress.Utils.ImageCollection Imagenes32X32;
        private DevExpress.XtraBars.BarButtonItem btnListUsuarios;
        private DevExpress.XtraBars.BarButtonItem btnAdministrativos;
        private DevExpress.XtraBars.BarButtonItem btnEst;
        private DevExpress.XtraBars.BarButtonItem btnGenNotas;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpboletin;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem btnRptNotasPorEstudiente;
        private DevExpress.XtraBars.BarButtonItem btnMaestroMat;
        private DevExpress.XtraBars.BarButtonItem btnGenBoletines;
        private DevExpress.XtraBars.BarButtonItem btnCargarNotas;
        private DevExpress.XtraBars.BarSubItem mnuCursos;
        private DevExpress.XtraBars.BarButtonItem btnAddMatCurso;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpPPP;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgPPP;
        private DevExpress.XtraBars.BarButtonItem btnProyectos;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpConexion;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpTerceros;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpConfiguracion;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem btnMaterias;
        private DevExpress.XtraBars.BarButtonItem btnSedes;
        private DevExpress.XtraBars.BarButtonItem btnAñoLectivo;
        private DevExpress.XtraBars.BarButtonItem btnJornadas;
        private DevExpress.XtraBars.BarButtonItem btnConfigGeneral;
        private DevExpress.XtraBars.BarSubItem mnuEstudiantes;
        private DevExpress.XtraBars.BarButtonItem btnObservador;
        private DevExpress.XtraBars.BarStaticItem lblVersion;
        private DevExpress.XtraBars.BarButtonItem btnGenCarnet;
    }
}

