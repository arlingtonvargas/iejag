﻿namespace Colegio.Vistas
{
    partial class FrmAcudientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAcudientes));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnEliminar = new DevExpress.XtraEditors.SimpleButton();
            this.gcAcudientes = new DevExpress.XtraGrid.GridControl();
            this.gvAcudientes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtDocumento = new dll.Controles.ucLabelTextBox();
            this.ucTextBoxAV1 = new dll.Controles.ucTextBoxAV();
            this.txtEstudiante = new dll.Controles.ucBusqueda();
            this.txtDireccion = new dll.Controles.ucLabelTextBox();
            this.txtParentesco = new dll.Controles.ucBusqueda();
            this.txtTelefono = new dll.Controles.ucLabelTextBox();
            this.txtNombres = new dll.Controles.ucLabelTextBox();
            this.txtApellidos = new dll.Controles.ucLabelTextBox();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCorreo = new dll.Controles.ucLabelTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcAcudientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAcudientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ucTextBoxAV1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(6, 122);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(81, 29);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(5, 23);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(81, 29);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(5, 56);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(81, 29);
            this.btnLimpiar.TabIndex = 2;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.btnEliminar);
            this.groupControl2.Controls.Add(this.btnGuardar);
            this.groupControl2.Controls.Add(this.btnSalir);
            this.groupControl2.Controls.Add(this.btnLimpiar);
            this.groupControl2.Location = new System.Drawing.Point(680, 12);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(92, 155);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Opciones";
            // 
            // btnEliminar
            // 
            this.btnEliminar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Appearance.Options.UseFont = true;
            this.btnEliminar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.ImageOptions.Image")));
            this.btnEliminar.Location = new System.Drawing.Point(6, 89);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(81, 29);
            this.btnEliminar.TabIndex = 3;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // gcAcudientes
            // 
            this.gcAcudientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcAcudientes.Location = new System.Drawing.Point(12, 173);
            this.gcAcudientes.MainView = this.gvAcudientes;
            this.gcAcudientes.Name = "gcAcudientes";
            this.gcAcudientes.Size = new System.Drawing.Size(760, 276);
            this.gcAcudientes.TabIndex = 3;
            this.gcAcudientes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvAcudientes});
            // 
            // gvAcudientes
            // 
            this.gvAcudientes.GridControl = this.gcAcudientes;
            this.gvAcudientes.Name = "gvAcudientes";
            // 
            // txtDocumento
            // 
            this.txtDocumento.AnchoTitulo = 87;
            this.txtDocumento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDocumento.Location = new System.Drawing.Point(3, 55);
            this.txtDocumento.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDocumento.MensajeDeAyuda = null;
            this.txtDocumento.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.PermiteSoloNumeros = false;
            this.txtDocumento.Size = new System.Drawing.Size(206, 24);
            this.txtDocumento.TabIndex = 3;
            this.txtDocumento.TextoTitulo = "Documento :";
            this.txtDocumento.ValorTextBox = "";
            // 
            // ucTextBoxAV1
            // 
            this.ucTextBoxAV1.Location = new System.Drawing.Point(0, 0);
            this.ucTextBoxAV1.MensajeDeAyuda = null;
            this.ucTextBoxAV1.Name = "ucTextBoxAV1";
            this.ucTextBoxAV1.PermiteSoloNumeros = false;
            this.ucTextBoxAV1.Size = new System.Drawing.Size(100, 20);
            this.ucTextBoxAV1.TabIndex = 0;
            // 
            // txtEstudiante
            // 
            this.txtEstudiante.AnchoTextBox = 120;
            this.txtEstudiante.AnchoTitulo = 90;
            this.tableLayoutPanel1.SetColumnSpan(this.txtEstudiante, 3);
            this.txtEstudiante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEstudiante.Location = new System.Drawing.Point(0, 0);
            this.txtEstudiante.Margin = new System.Windows.Forms.Padding(0);
            this.txtEstudiante.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtEstudiante.MensajeDeAyuda = null;
            this.txtEstudiante.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtEstudiante.Name = "txtEstudiante";
            this.txtEstudiante.PermiteSoloNumeros = false;
            this.txtEstudiante.Script = "";
            this.txtEstudiante.Size = new System.Drawing.Size(658, 28);
            this.txtEstudiante.TabIndex = 1;
            this.txtEstudiante.TextoTitulo = "Estudiante :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtEstudiante.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtEstudiante.ValorTextBox = "";
            // 
            // txtDireccion
            // 
            this.txtDireccion.AnchoTitulo = 87;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDireccion, 3);
            this.txtDireccion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDireccion.Location = new System.Drawing.Point(3, 107);
            this.txtDireccion.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDireccion.MensajeDeAyuda = null;
            this.txtDireccion.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.PermiteSoloNumeros = false;
            this.txtDireccion.Size = new System.Drawing.Size(652, 24);
            this.txtDireccion.TabIndex = 8;
            this.txtDireccion.TextoTitulo = "Dirección :";
            this.txtDireccion.ValorTextBox = "";
            // 
            // txtParentesco
            // 
            this.txtParentesco.AnchoTextBox = 120;
            this.txtParentesco.AnchoTitulo = 90;
            this.tableLayoutPanel1.SetColumnSpan(this.txtParentesco, 3);
            this.txtParentesco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtParentesco.Location = new System.Drawing.Point(0, 26);
            this.txtParentesco.Margin = new System.Windows.Forms.Padding(0);
            this.txtParentesco.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtParentesco.MensajeDeAyuda = null;
            this.txtParentesco.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtParentesco.Name = "txtParentesco";
            this.txtParentesco.PermiteSoloNumeros = false;
            this.txtParentesco.Script = "";
            this.txtParentesco.Size = new System.Drawing.Size(658, 28);
            this.txtParentesco.TabIndex = 2;
            this.txtParentesco.TextoTitulo = "Parenteso :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtParentesco.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtParentesco.ValorTextBox = "";
            // 
            // txtTelefono
            // 
            this.txtTelefono.AnchoTitulo = 80;
            this.txtTelefono.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTelefono.Location = new System.Drawing.Point(438, 81);
            this.txtTelefono.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtTelefono.MensajeDeAyuda = null;
            this.txtTelefono.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.PermiteSoloNumeros = true;
            this.txtTelefono.Size = new System.Drawing.Size(217, 24);
            this.txtTelefono.TabIndex = 7;
            this.txtTelefono.TextoTitulo = "Teléfono :";
            this.txtTelefono.ValorTextBox = "";
            // 
            // txtNombres
            // 
            this.txtNombres.AnchoTitulo = 80;
            this.txtNombres.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNombres.Location = new System.Drawing.Point(215, 55);
            this.txtNombres.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNombres.MensajeDeAyuda = null;
            this.txtNombres.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.PermiteSoloNumeros = false;
            this.txtNombres.Size = new System.Drawing.Size(217, 24);
            this.txtNombres.TabIndex = 4;
            this.txtNombres.TextoTitulo = "Nombres :";
            this.txtNombres.ValorTextBox = "";
            // 
            // txtApellidos
            // 
            this.txtApellidos.AnchoTitulo = 80;
            this.txtApellidos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtApellidos.Location = new System.Drawing.Point(438, 55);
            this.txtApellidos.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtApellidos.MensajeDeAyuda = null;
            this.txtApellidos.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.PermiteSoloNumeros = false;
            this.txtApellidos.Size = new System.Drawing.Size(217, 24);
            this.txtApellidos.TabIndex = 5;
            this.txtApellidos.TextoTitulo = "Apellidos :";
            this.txtApellidos.ValorTextBox = "";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(662, 155);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Datos Acudiente";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 212F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtCorreo, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtEstudiante, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtDireccion, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtTelefono, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtApellidos, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtParentesco, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtNombres, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtDocumento, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(658, 133);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // txtCorreo
            // 
            this.txtCorreo.AnchoTitulo = 87;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCorreo, 2);
            this.txtCorreo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCorreo.Location = new System.Drawing.Point(3, 81);
            this.txtCorreo.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCorreo.MensajeDeAyuda = null;
            this.txtCorreo.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.PermiteSoloNumeros = false;
            this.txtCorreo.Size = new System.Drawing.Size(429, 24);
            this.txtCorreo.TabIndex = 6;
            this.txtCorreo.TextoTitulo = "Correo :";
            this.txtCorreo.ValorTextBox = "";
            // 
            // FrmAcudientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.gcAcudientes);
            this.Controls.Add(this.groupControl2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(848, 499);
            this.Name = "FrmAcudientes";
            this.Text = "Acudientes";
            this.Load += new System.EventHandler(this.FrmAcudientes1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcAcudientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvAcudientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ucTextBoxAV1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gcAcudientes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvAcudientes;
        private dll.Controles.ucLabelTextBox txtDocumento;
        private dll.Controles.ucTextBoxAV ucTextBoxAV1;
        private dll.Controles.ucBusqueda txtEstudiante;
        private dll.Controles.ucLabelTextBox txtDireccion;
        private dll.Controles.ucBusqueda txtParentesco;
        private dll.Controles.ucLabelTextBox txtTelefono;
        private dll.Controles.ucLabelTextBox txtNombres;
        private dll.Controles.ucLabelTextBox txtApellidos;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnEliminar;
        private dll.Controles.ucLabelTextBox txtCorreo;
    }
}