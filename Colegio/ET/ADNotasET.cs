﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADNotasET 
    {

        public int Sec { get; set; }
        public int SecMatCurso { get; set; }
        public long IdEstudiante { get; set; }
        public int IdMatricula { get; set; }
        public string Descripcion { get; set; }
        public decimal P1 { get; set; }
        public decimal P2 { get; set; }
        public decimal P3 { get; set; }
        public decimal P4 { get; set; }
        public DateTime FechaReg { get; set; }
        public int UsuReg { get; set; }
    }

    public class ADNotasVistaET
    {

        public string NomEstudiante { get; set; }
        public string NomMateria { get; set; }
        public string Docente  { get; set; }
        public int IH { get; set; }
        public int AUS { get; set; } 
        public decimal P1 { get; set; }
        public decimal P2 { get; set; }
        public decimal P3 { get; set; }
        public decimal P4 { get; set; }
        public decimal Prom { get; set; }
        public string Desempeños { get; set; }
        public string Titular { get; set; }
    }

    public class ListADNotasET : List<ADNotasET> { }
    public class ListADNotasVistaET : List<ADNotasVistaET> { }
}
