﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Colegio.ET;

namespace Colegio.Clases
{
    public class ClPeriodos
    {
        public List<ADPeriodosET> GetPeriodos()
        {
            try
            {
                string sql = "SELECT PE.Sec AS IdPeriodo, PE.Nombre FROM ADPeriodos PE ";
                List<ADPeriodosET> res = ClFunciones.DataReaderMapToList<ADPeriodosET>(sql);
                return res;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
