﻿namespace Colegio.Vistas.Reports
{
    partial class FrmRptNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule1 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings1 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule2 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings2 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule3 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings3 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule4 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings4 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule5 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings5 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule6 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue6 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings6 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule7 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue7 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings7 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule8 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue8 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings8 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule9 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue9 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings9 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            DevExpress.XtraPivotGrid.PivotGridFormatRule pivotGridFormatRule10 = new DevExpress.XtraPivotGrid.PivotGridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue10 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings formatRuleTotalTypeSettings10 = new DevExpress.XtraPivotGrid.FormatRuleTotalTypeSettings();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptNotas));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            this.fieldP11 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldP21 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldP31 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldP41 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldProm2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pgEstudiantes = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.listADNotasVistaETBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fieldNomEstudiante1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldNomMateria1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDocente1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldIH1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldAUS1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDesempeos2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTitular2 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldProm1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldDesempeos1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.fieldTitular1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lkePeriodo = new dll.Controles.ucLabelCombo();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.txtCurso = new dll.Controles.ucBusqueda();
            this.btnConsultar = new DevExpress.XtraEditors.SimpleButton();
            this.tabPane1 = new DevExpress.XtraBars.Navigation.TabPane();
            this.tabNavigationPage1 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.tabNavigationPage2 = new DevExpress.XtraBars.Navigation.TabNavigationPage();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.pgEstudiantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listADNotasVistaETBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).BeginInit();
            this.tabPane1.SuspendLayout();
            this.tabNavigationPage1.SuspendLayout();
            this.tabNavigationPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            this.SuspendLayout();
            // 
            // fieldP11
            // 
            this.fieldP11.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldP11.AreaIndex = 0;
            this.fieldP11.FieldName = "P1";
            this.fieldP11.Name = "fieldP11";
            this.fieldP11.TotalCellFormat.FormatString = "N1";
            this.fieldP11.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldP11.UseNativeFormat = DevExpress.Utils.DefaultBoolean.False;
            this.fieldP11.Width = 51;
            // 
            // fieldP21
            // 
            this.fieldP21.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldP21.AreaIndex = 1;
            this.fieldP21.FieldName = "P2";
            this.fieldP21.Name = "fieldP21";
            this.fieldP21.TotalCellFormat.FormatString = "N1";
            this.fieldP21.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldP21.UseNativeFormat = DevExpress.Utils.DefaultBoolean.False;
            this.fieldP21.Width = 50;
            // 
            // fieldP31
            // 
            this.fieldP31.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldP31.AreaIndex = 2;
            this.fieldP31.FieldName = "P3";
            this.fieldP31.Name = "fieldP31";
            this.fieldP31.TotalCellFormat.FormatString = "N1";
            this.fieldP31.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldP31.UseNativeFormat = DevExpress.Utils.DefaultBoolean.False;
            this.fieldP31.Width = 44;
            // 
            // fieldP41
            // 
            this.fieldP41.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldP41.AreaIndex = 3;
            this.fieldP41.FieldName = "P4";
            this.fieldP41.Name = "fieldP41";
            this.fieldP41.TotalCellFormat.FormatString = "N1";
            this.fieldP41.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldP41.UseNativeFormat = DevExpress.Utils.DefaultBoolean.False;
            this.fieldP41.Width = 51;
            // 
            // fieldProm2
            // 
            this.fieldProm2.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.fieldProm2.AreaIndex = 4;
            this.fieldProm2.FieldName = "Prom";
            this.fieldProm2.Name = "fieldProm2";
            this.fieldProm2.TotalCellFormat.FormatString = "N1";
            this.fieldProm2.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.fieldProm2.UseNativeFormat = DevExpress.Utils.DefaultBoolean.False;
            this.fieldProm2.Width = 74;
            // 
            // pgEstudiantes
            // 
            this.pgEstudiantes.ActiveFilterString = "";
            this.pgEstudiantes.DataSource = this.listADNotasVistaETBindingSource;
            this.pgEstudiantes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgEstudiantes.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldNomEstudiante1,
            this.fieldNomMateria1,
            this.fieldDocente1,
            this.fieldIH1,
            this.fieldAUS1,
            this.fieldP11,
            this.fieldP21,
            this.fieldP31,
            this.fieldP41,
            this.fieldProm2,
            this.fieldDesempeos2,
            this.fieldTitular2});
            pivotGridFormatRule1.Measure = this.fieldP11;
            pivotGridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Between;
            formatConditionRuleValue1.Value1 = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            formatConditionRuleValue1.Value2 = new decimal(new int[] {
            3,
            0,
            0,
            0});
            pivotGridFormatRule1.Rule = formatConditionRuleValue1;
            pivotGridFormatRule1.Settings = formatRuleTotalTypeSettings1;
            pivotGridFormatRule2.Measure = this.fieldP21;
            pivotGridFormatRule2.Name = "Format1";
            formatConditionRuleValue2.Appearance.BackColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Between;
            formatConditionRuleValue2.Value1 = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            formatConditionRuleValue2.Value2 = new decimal(new int[] {
            3,
            0,
            0,
            0});
            pivotGridFormatRule2.Rule = formatConditionRuleValue2;
            pivotGridFormatRule2.Settings = formatRuleTotalTypeSettings2;
            pivotGridFormatRule3.Measure = this.fieldP31;
            pivotGridFormatRule3.Name = "Format2";
            formatConditionRuleValue3.Appearance.BackColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Between;
            formatConditionRuleValue3.Value1 = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            formatConditionRuleValue3.Value2 = new decimal(new int[] {
            3,
            0,
            0,
            0});
            pivotGridFormatRule3.Rule = formatConditionRuleValue3;
            pivotGridFormatRule3.Settings = formatRuleTotalTypeSettings3;
            pivotGridFormatRule4.Measure = this.fieldP41;
            pivotGridFormatRule4.Name = "Format3";
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Between;
            formatConditionRuleValue4.Value1 = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            formatConditionRuleValue4.Value2 = new decimal(new int[] {
            3,
            0,
            0,
            0});
            pivotGridFormatRule4.Rule = formatConditionRuleValue4;
            pivotGridFormatRule4.Settings = formatRuleTotalTypeSettings4;
            pivotGridFormatRule5.Measure = this.fieldProm2;
            pivotGridFormatRule5.Name = "Format4";
            formatConditionRuleValue5.Appearance.BackColor = System.Drawing.Color.Red;
            formatConditionRuleValue5.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Between;
            formatConditionRuleValue5.Value1 = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            formatConditionRuleValue5.Value2 = new decimal(new int[] {
            3,
            0,
            0,
            0});
            pivotGridFormatRule5.Rule = formatConditionRuleValue5;
            pivotGridFormatRule5.Settings = formatRuleTotalTypeSettings5;
            pivotGridFormatRule6.Measure = this.fieldP11;
            pivotGridFormatRule6.Name = "Format5";
            formatConditionRuleValue6.Appearance.BackColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue6.Appearance.ForeColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue6.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue6.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue6.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue6.Value1 = new decimal(new int[] {
            5,
            0,
            0,
            0});
            pivotGridFormatRule6.Rule = formatConditionRuleValue6;
            pivotGridFormatRule6.Settings = formatRuleTotalTypeSettings6;
            pivotGridFormatRule7.Measure = this.fieldP21;
            pivotGridFormatRule7.Name = "Format6";
            formatConditionRuleValue7.Appearance.BackColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue7.Appearance.ForeColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue7.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue7.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue7.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue7.Value1 = new decimal(new int[] {
            5,
            0,
            0,
            0});
            pivotGridFormatRule7.Rule = formatConditionRuleValue7;
            pivotGridFormatRule7.Settings = formatRuleTotalTypeSettings7;
            pivotGridFormatRule8.Measure = this.fieldP31;
            pivotGridFormatRule8.Name = "Format7";
            formatConditionRuleValue8.Appearance.BackColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue8.Appearance.ForeColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue8.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue8.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue8.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue8.Value1 = new decimal(new int[] {
            5,
            0,
            0,
            0});
            pivotGridFormatRule8.Rule = formatConditionRuleValue8;
            pivotGridFormatRule8.Settings = formatRuleTotalTypeSettings8;
            pivotGridFormatRule9.Measure = this.fieldP41;
            pivotGridFormatRule9.Name = "Format8";
            formatConditionRuleValue9.Appearance.BackColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue9.Appearance.ForeColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue9.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue9.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue9.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue9.Value1 = new decimal(new int[] {
            5,
            0,
            0,
            0});
            pivotGridFormatRule9.Rule = formatConditionRuleValue9;
            pivotGridFormatRule9.Settings = formatRuleTotalTypeSettings9;
            pivotGridFormatRule10.Measure = this.fieldProm2;
            pivotGridFormatRule10.Name = "Format9";
            formatConditionRuleValue10.Appearance.BackColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue10.Appearance.ForeColor = System.Drawing.Color.Transparent;
            formatConditionRuleValue10.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue10.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue10.Condition = DevExpress.XtraEditors.FormatCondition.Greater;
            formatConditionRuleValue10.Value1 = new decimal(new int[] {
            5,
            0,
            0,
            0});
            pivotGridFormatRule10.Rule = formatConditionRuleValue10;
            pivotGridFormatRule10.Settings = formatRuleTotalTypeSettings10;
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule1);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule2);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule3);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule4);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule5);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule6);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule7);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule8);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule9);
            this.pgEstudiantes.FormatRules.Add(pivotGridFormatRule10);
            this.pgEstudiantes.Location = new System.Drawing.Point(0, 0);
            this.pgEstudiantes.Name = "pgEstudiantes";
            this.pgEstudiantes.OptionsBehavior.ApplyBestFitOnFieldDragging = true;
            this.pgEstudiantes.OptionsCustomization.AllowCustomizationForm = false;
            this.pgEstudiantes.OptionsCustomization.AllowDrag = false;
            this.pgEstudiantes.OptionsCustomization.AllowDragInCustomizationForm = false;
            this.pgEstudiantes.OptionsCustomization.AllowEdit = false;
            this.pgEstudiantes.OptionsCustomization.AllowResizing = false;
            this.pgEstudiantes.OptionsCustomization.AllowSortBySummary = false;
            this.pgEstudiantes.OptionsCustomization.AllowSortInCustomizationForm = true;
            this.pgEstudiantes.OptionsView.ShowRowGrandTotalHeader = false;
            this.pgEstudiantes.OptionsView.ShowRowGrandTotals = false;
            this.pgEstudiantes.OptionsView.ShowRowTotals = false;
            this.pgEstudiantes.Size = new System.Drawing.Size(1169, 328);
            this.pgEstudiantes.TabIndex = 0;
            this.pgEstudiantes.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pgEstudiantes_CustomSummary);
            // 
            // listADNotasVistaETBindingSource
            // 
            this.listADNotasVistaETBindingSource.DataSource = typeof(Colegio.ET.ListADNotasVistaET);
            // 
            // fieldNomEstudiante1
            // 
            this.fieldNomEstudiante1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldNomEstudiante1.AreaIndex = 1;
            this.fieldNomEstudiante1.Caption = "Estudiante";
            this.fieldNomEstudiante1.FieldName = "NomEstudiante";
            this.fieldNomEstudiante1.Name = "fieldNomEstudiante1";
            this.fieldNomEstudiante1.Width = 250;
            // 
            // fieldNomMateria1
            // 
            this.fieldNomMateria1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldNomMateria1.AreaIndex = 0;
            this.fieldNomMateria1.Caption = "Materia";
            this.fieldNomMateria1.FieldName = "NomMateria";
            this.fieldNomMateria1.Name = "fieldNomMateria1";
            this.fieldNomMateria1.Width = 250;
            // 
            // fieldDocente1
            // 
            this.fieldDocente1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.fieldDocente1.AreaIndex = 2;
            this.fieldDocente1.Caption = "Docente";
            this.fieldDocente1.FieldName = "Docente";
            this.fieldDocente1.Name = "fieldDocente1";
            this.fieldDocente1.Width = 200;
            // 
            // fieldIH1
            // 
            this.fieldIH1.AreaIndex = 3;
            this.fieldIH1.FieldName = "IH";
            this.fieldIH1.Name = "fieldIH1";
            this.fieldIH1.Visible = false;
            // 
            // fieldAUS1
            // 
            this.fieldAUS1.AreaIndex = 4;
            this.fieldAUS1.FieldName = "AUS";
            this.fieldAUS1.Name = "fieldAUS1";
            this.fieldAUS1.Visible = false;
            // 
            // fieldDesempeos2
            // 
            this.fieldDesempeos2.AreaIndex = 10;
            this.fieldDesempeos2.FieldName = "Desempeños";
            this.fieldDesempeos2.Name = "fieldDesempeos2";
            this.fieldDesempeos2.Visible = false;
            // 
            // fieldTitular2
            // 
            this.fieldTitular2.AreaIndex = 0;
            this.fieldTitular2.FieldName = "Titular";
            this.fieldTitular2.Name = "fieldTitular2";
            this.fieldTitular2.Visible = false;
            // 
            // fieldProm1
            // 
            this.fieldProm1.FieldName = "Prom";
            this.fieldProm1.Name = "fieldProm1";
            // 
            // fieldDesempeos1
            // 
            this.fieldDesempeos1.FieldName = "Desempeños";
            this.fieldDesempeos1.Name = "fieldDesempeos1";
            // 
            // fieldTitular1
            // 
            this.fieldTitular1.FieldName = "Titular";
            this.fieldTitular1.Name = "fieldTitular1";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnPrint);
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Controls.Add(this.btnConsultar);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1187, 58);
            this.groupControl1.TabIndex = 9;
            this.groupControl1.Text = "Parametros";
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.ImageOptions.Image")));
            this.btnPrint.Location = new System.Drawing.Point(1107, 28);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 21);
            this.btnPrint.TabIndex = 12;
            this.btnPrint.Text = "Imprimir";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 243F));
            this.tableLayoutPanel1.Controls.Add(this.lkePeriodo, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtAño, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCurso, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1015, 30);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // lkePeriodo
            // 
            this.lkePeriodo.AnchoTitulo = 70;
            this.lkePeriodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lkePeriodo.Enabled = false;
            this.lkePeriodo.Location = new System.Drawing.Point(775, 0);
            this.lkePeriodo.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.lkePeriodo.MaximumSize = new System.Drawing.Size(3000, 28);
            this.lkePeriodo.MensajeDeAyuda = null;
            this.lkePeriodo.MinimumSize = new System.Drawing.Size(50, 28);
            this.lkePeriodo.Name = "lkePeriodo";
            this.lkePeriodo.Size = new System.Drawing.Size(237, 28);
            this.lkePeriodo.TabIndex = 3;
            this.lkePeriodo.TextoTitulo = "Periodo :";
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 50;
            this.txtAño.AnchoTitulo = 90;
            this.txtAño.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAño.Enabled = false;
            this.txtAño.Location = new System.Drawing.Point(0, 0);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(386, 28);
            this.txtAño.TabIndex = 1;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtAño.ValorTextBox = "";
            // 
            // txtCurso
            // 
            this.txtCurso.AnchoTextBox = 50;
            this.txtCurso.AnchoTitulo = 80;
            this.txtCurso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCurso.Location = new System.Drawing.Point(386, 0);
            this.txtCurso.Margin = new System.Windows.Forms.Padding(0);
            this.txtCurso.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCurso.MensajeDeAyuda = null;
            this.txtCurso.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.PermiteSoloNumeros = false;
            this.txtCurso.Script = "";
            this.txtCurso.Size = new System.Drawing.Size(386, 28);
            this.txtCurso.TabIndex = 2;
            this.txtCurso.TextoTitulo = "Curso :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtCurso.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtCurso.ValorTextBox = "";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.ImageOptions.Image")));
            this.btnConsultar.Location = new System.Drawing.Point(1026, 28);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 21);
            this.btnConsultar.TabIndex = 4;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // tabPane1
            // 
            this.tabPane1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabPane1.Controls.Add(this.tabNavigationPage1);
            this.tabPane1.Controls.Add(this.tabNavigationPage2);
            this.tabPane1.Location = new System.Drawing.Point(12, 76);
            this.tabPane1.Name = "tabPane1";
            this.tabPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.tabNavigationPage1,
            this.tabNavigationPage2});
            this.tabPane1.RegularSize = new System.Drawing.Size(1187, 373);
            this.tabPane1.SelectedPage = this.tabNavigationPage1;
            this.tabPane1.Size = new System.Drawing.Size(1187, 373);
            this.tabPane1.TabIndex = 10;
            this.tabPane1.Text = "tabPane1";
            // 
            // tabNavigationPage1
            // 
            this.tabNavigationPage1.Caption = "Reporte";
            this.tabNavigationPage1.Controls.Add(this.pgEstudiantes);
            this.tabNavigationPage1.Name = "tabNavigationPage1";
            this.tabNavigationPage1.Size = new System.Drawing.Size(1169, 328);
            // 
            // tabNavigationPage2
            // 
            this.tabNavigationPage2.Caption = "Gráfica";
            this.tabNavigationPage2.Controls.Add(this.chartControl1);
            this.tabNavigationPage2.Name = "tabNavigationPage2";
            this.tabNavigationPage2.Size = new System.Drawing.Size(1169, 328);
            // 
            // chartControl1
            // 
            this.chartControl1.DataSource = this.pgEstudiantes;
            xyDiagram1.AxisX.Title.Text = "Materia Estudiante Docente";
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Title.Text = "P1 P2 P3 P4 Prom";
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chartControl1.Diagram = xyDiagram1;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Legend.MaxHorizontalPercentage = 30D;
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.SeriesDataMember = "Series";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.SeriesTemplate.ArgumentDataMember = "Arguments";
            this.chartControl1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative;
            this.chartControl1.SeriesTemplate.ValueDataMembersSerializable = "Values";
            this.chartControl1.Size = new System.Drawing.Size(1169, 328);
            this.chartControl1.TabIndex = 0;
            // 
            // FrmRptNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1211, 461);
            this.Controls.Add(this.tabPane1);
            this.Controls.Add(this.groupControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRptNotas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Informe Detallado de Notas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmRptNotas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pgEstudiantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listADNotasVistaETBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabPane1)).EndInit();
            this.tabPane1.ResumeLayout(false);
            this.tabNavigationPage1.ResumeLayout(false);
            this.tabNavigationPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource listADNotasVistaETBindingSource;
        private DevExpress.XtraPivotGrid.PivotGridField fieldProm1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDesempeos1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTitular1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNomEstudiante1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldNomMateria1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDocente1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldIH1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldAUS1;
        private DevExpress.XtraPivotGrid.PivotGridField fieldP11;
        private DevExpress.XtraPivotGrid.PivotGridField fieldP21;
        private DevExpress.XtraPivotGrid.PivotGridField fieldP31;
        private DevExpress.XtraPivotGrid.PivotGridField fieldP41;
        private DevExpress.XtraPivotGrid.PivotGridField fieldProm2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldDesempeos2;
        private DevExpress.XtraPivotGrid.PivotGridField fieldTitular2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private dll.Controles.ucLabelCombo lkePeriodo;
        private dll.Controles.ucBusqueda txtAño;
        private dll.Controles.ucBusqueda txtCurso;
        private DevExpress.XtraEditors.SimpleButton btnConsultar;
        private DevExpress.XtraPivotGrid.PivotGridControl pgEstudiantes;
        private DevExpress.XtraEditors.SimpleButton btnPrint;
        private DevExpress.XtraBars.Navigation.TabPane tabPane1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage1;
        private DevExpress.XtraBars.Navigation.TabNavigationPage tabNavigationPage2;
        private DevExpress.XtraCharts.ChartControl chartControl1;
    }
}