﻿using System;
using System.Data.SqlClient;

namespace Colegio.Clases
{
    public class ClConexion
    {
        public SqlConnection Conexion;
        public static string _Servidor = "";
        bool vEstacConectado = false;
        private string cadCon = "";

        public static ClConexion clConexion = new ClConexion();

        public string Servidor
        {
            get
            {
                return _Servidor;
            }
            set
            {
                _Servidor = value;

                //cadCon = string.Format("Data Source = {0}; Initial Catalog = db_a775aa_bdiejag; User Id = db_a775aa_bdiejag_admin; Password = Mauros2121121512", Servidor);
                cadCon = string.Format("Data Source={0};Initial Catalog=BDIEJAG;user id = sa; password = 2121121512", Servidor);
                //cadCon = string.Format("Data Source={0};Initial Catalog=DB_A4124B_IEJAG;User Id=DB_A4124B_IEJAG_admin;Password=3114556084eA.;", value);
            }
        } 
        public string CadenaConexion
        {
            get
            {
                return cadCon;
            }
        } 


        public bool Ingreso(string server)
        {
            try
            {
                Servidor = server;
                Conexion = new SqlConnection(CadenaConexion);
                Conexion.Open();
                return true;
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
                return false;
            }

        }
        public void DesconectaBD()
        {
            Conexion.Close();
            EstaConectado = false;
        }

        public bool EstaConectado
        {
            get
            {
                return vEstacConectado;
            }
            set
            {
                vEstacConectado = value;
            }
        }
    }
}
