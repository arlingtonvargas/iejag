﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Colegio.Clases
{
     public class ClNotas
    {
        public bool InsertNotas(ListADNotasET lista, ADDesempeñosMateriaCursoET Desempeño, ADInasistenciaETList pIA)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        int Sec = ClFunciones.TraeSiguienteSecuencial("ADNotas", "Sec", con);
                        Sec -= 1;
                        string Inserto = "NO";
                        SqlCommand comando;
                        foreach (ADNotasET x in lista)
                        {
                            Sec += 1;
                            comando = new SqlCommand("SP_ADNotas_I", con);
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.Parameters.AddWithValue("@pSec", Sec);
                            comando.Parameters.AddWithValue("@pP1", x.P1);
                            comando.Parameters.AddWithValue("@pP2", x.P2);
                            comando.Parameters.AddWithValue("@pP3", x.P3);
                            comando.Parameters.AddWithValue("@pP4", x.P4);
                            comando.Parameters.AddWithValue("@pSecMatCurso", x.SecMatCurso);
                            comando.Parameters.AddWithValue("@pIdMatricula", x.IdMatricula);
                            comando.Parameters.AddWithValue("@pDescripcion", x.Descripcion);
                            comando.Parameters.AddWithValue("@pIdEstudiante", x.IdEstudiante);
                            comando.Parameters.AddWithValue("@pFechaReg", x.FechaReg);
                            comando.Parameters.AddWithValue("@pUsuReg", x.UsuReg);
                            comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                            comando.ExecuteNonQuery();
                            Inserto = comando.Parameters["@Inserto"].Value.ToString();
                            if (Inserto == "NO")
                            {
                                return false;
                            }
                        }

                        foreach (ADInasistenciaET x in pIA)
                        {
                            Sec += 1;
                            comando = new SqlCommand("[SP_ADInasistencia_I]", con);
                            comando.CommandType = CommandType.StoredProcedure;
                            comando.Parameters.AddWithValue("@IdEstudiante", x.IdEstudiante);
                            comando.Parameters.AddWithValue("@IdPeriodo", x.IdPeriodo);
                            comando.Parameters.AddWithValue("@CantInasistencias", x.CantInasistencias);
                            comando.Parameters.AddWithValue("@IdCurso", x.IdCurso);
                            comando.Parameters.AddWithValue("@IdMateria", x.IdMateria);
                            comando.Parameters.AddWithValue("@IdMaestro", x.IdMaestro);
                            comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                            comando.ExecuteNonQuery();
                            Inserto = comando.Parameters["@Inserto"].Value.ToString();
                            if (Inserto == "NO")
                            {
                                return false;
                            }
                        }


                        Inserto = "NO";
                        comando = new SqlCommand("SP_ADDesempeñosMateriaCurso_I", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        //comando.Parameters.AddWithValue("@pSecMatCurso", Desempeño.SecMateriaCurso);
                        //comando.Parameters.AddWithValue("@pSecPeriodo", Desempeño.SecPeriodo);
                        comando.Parameters.AddWithValue("@pSecDesMatCurso", Desempeño.Sec);
                        comando.Parameters.AddWithValue("@pDesempeño", Desempeño.Desempeño);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return false;
                        }
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public List<ADNotasVistaET> GetNotasByEstudiante(long idEstudiante, int idPeriodo, int pIdAño)
        {
            try
            {
                string sql = "";
                string Sprom = "(ISNULL(NT.p1,0)) AS Prom";
                switch (idPeriodo)
                {
                    case 2:
                        Sprom = "(ISNULL(ISNULL(NT.P1,0) + ISNULL(NT.P2,0),0))/2 AS Prom";
                        break;
                    case 3:
                        Sprom = "(ISNULL(NT.P1,0) + ISNULL(NT.P2,0) + ISNULL(NT.P3,0))/3 AS Prom";
                        break;
                    case 4:
                        Sprom = "(ISNULL(NT.P1,0) + ISNULL(NT.P2,0) + ISNULL(NT.P3,0)+ ISNULL(NT.P4,0))/4 AS Prom";
                        break;
                    default:
                        break;
                }
                sql = string.Format("SELECT '' AS NomEstudiante, MT.NomMateria, CONCAT(DC.Pnombre,' ',DC.Snombre,' ',DC.Papellido,' ',DC.Sapellido) AS Docente  " +
                    " ,MS.IH, IA.CantInasistencias AS AUS, NT.P1, NT.P2, NT.P3, NT.P4, {0}, MS.Desempeños" +
                    " , CONCAT(TI.Pnombre, ' ', TI.Snombre, ' ', TI.Papellido, ' ', TI.Sapellido) AS Titular " +
                    " FROM ADNotas NT " +
                    " INNER JOIN ADMateriasCursos MS ON NT.SecMatCurso = MS.Sec " +
                    " INNER JOIN ADCursos CS ON MS.IdCurso = CS.Sec " +
                    " INNER JOIN ADMaterias MT ON MS.IdMateria = MT.Sec " +
                    " INNER JOIN ADTerceros DC ON MS.IdMaestro = DC.IdTercero " +
                    " INNER JOIN ADTerceros TI ON CS.IdTitular = TI.IdTercero " +
                    " LEFT JOIN ADInasistencia IA ON NT.IdEstudiante = IA.IdEstudiante AND CS.Sec = IA.IdCurso AND MT.Sec = IA.IdMateria AND IdPeriodo = {3} " +
                    " LEFT JOIN ADDesempeñosMateriaCurso DS ON MS.Sec = DS.SecMateriaCurso AND DS.SecPeriodo = {3} " +
                    " WHERE NT.IdEstudiante = {1} AND CS.IdAñoLectivo = {2}", Sprom, idEstudiante, pIdAño, idPeriodo);
                List<ADNotasVistaET> listRes = ClFunciones.DataReaderMapToList<ADNotasVistaET>(sql);
                return listRes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public List<ADNotasVistaET> GetNotasByCursoPeriodo(int idPeriodo, int pIdCurso)
        {
            try
            {
                string sql = "";
                string Sprom = "(ISNULL(NT.p1,0)) AS Prom";
                switch (idPeriodo)
                {
                    case 2:
                        Sprom = "(ISNULL(ISNULL(NT.P1,0) + ISNULL(NT.P2,0),0))/2 AS Prom";
                        break;
                    case 3:
                        Sprom = "(ISNULL(NT.P1,0) + ISNULL(NT.P2,0) + ISNULL(NT.P3,0))/3 AS Prom";
                        break;
                    case 4:
                        Sprom = "(ISNULL(NT.P1,0) + ISNULL(NT.P2,0) + ISNULL(NT.P3,0)+ ISNULL(NT.P4,0))/4 AS Prom";
                        break;
                    default:
                        break;
                }
                sql = string.Format("SELECT CONCAT(ES.Papellido,' ',ES.Sapellido,' ', ES.Pnombre,' ',ES.Snombre) AS NomEstudiante,"+
                    " MT.NomMateria, CONCAT(DC.Pnombre,' ',DC.Snombre,' ',DC.Papellido,' ',DC.Sapellido) AS Docente  " +
                    " ,MS.IH, IA.CantInasistencias AS AUS, NT.P1, NT.P2, NT.P3, NT.P4, {0}, MS.Desempeños" +
                    " , CONCAT(TI.Pnombre, ' ', TI.Snombre, ' ', TI.Papellido, ' ', TI.Sapellido) AS Titular " +
                    " FROM ADNotas NT " +
                    " INNER JOIN ADMateriasCursos MS ON NT.SecMatCurso = MS.Sec " +
                    " INNER JOIN ADCursos CS ON MS.IdCurso = CS.Sec " +
                    " INNER JOIN ADMaterias MT ON MS.IdMateria = MT.Sec " +
                    " INNER JOIN ADTerceros DC ON MS.IdMaestro = DC.IdTercero " +
                    " INNER JOIN ADTerceros TI ON CS.IdTitular = TI.IdTercero " +
                    " INNER JOIN ADTerceros ES ON NT.IdEstudiante = ES.IdTercero" +
                    " LEFT JOIN ADInasistencia IA ON NT.IdEstudiante = IA.IdEstudiante AND CS.Sec = IA.IdCurso AND MT.Sec = IA.IdMateria AND IdPeriodo = {2} " +
                    " WHERE CS.Sec = {1}", Sprom, pIdCurso, idPeriodo);
                List<ADNotasVistaET> listRes = ClFunciones.DataReaderMapToList<ADNotasVistaET>(sql);
                return listRes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
