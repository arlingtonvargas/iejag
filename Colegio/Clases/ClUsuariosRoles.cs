﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClUsuariosRoles
    {
        public DataTable GetRolUsuario(int idUsuario)
        {
            try
            {
                string sql = "SELECT RL.Sec, RL.NomRol FROM SGRoles RL " +
                " INNER JOIN SGUsuarios UR ON RL.Sec = UR.IdRol" +
                " WHERE ISNULL(RL.Anulado, 0) = 0 AND UR.IdUsuario = " + idUsuario.ToString();
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool GuardarRolUsuario(int idRol, int idUsuario)
        {
            try
            {
                string sql = string.Format("UPDATE SGUsuarios SET IdRol = {0} WHERE IdUsuario = {1}",idRol, idUsuario);
                string res =  ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                if (res=="si")
                {
                    return true;
                }else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
