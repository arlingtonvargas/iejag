﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClGrados
    {
        public DataTable GetGrados()
        {
            try
            {
                string sql = "SELECT GD.Sec, GD.Grado FROM ADGrados GD";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
