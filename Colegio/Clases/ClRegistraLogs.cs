﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Colegio.ET;

namespace Colegio.Clases
{
    public class ClRegistraLogs
    {
        public static void RegistrarError(SGLogs sGLogs)
        {
            try
            {
                string sql = string.Format("INSERT INTO [dbo].[SGLogs] " +
                " ([NomError]" +
                " ,[NomFunction]" +
                " ,[NomArchivo]" +
                " ,[NumLinea]" +
                " ,[FechaReg]" +
                " ,[NomModulo]" +
                " ,[MensajeError])" +
                " VALUES" +
                " ('{0}','{1}', '{2}', {3}, GETDATE(),'{4}', '{5}')", 
                sGLogs.NomError,
                sGLogs.NomFunction,
                sGLogs.NomArchivo,
                sGLogs.NumLinea,
                sGLogs.NomModulo, 
                sGLogs.MensajeError.Replace("'",""));
                ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
            }
            catch (Exception ex)
            {
                
            }
        }

    }
}
