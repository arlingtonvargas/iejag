﻿using Colegio.Clases;
using Colegio.ET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio.Vistas.Reports
{
    public partial class FrmRptNotasByEstudiante : dll.Common.FrmBase
    {
        ClNotas clNotas = new ClNotas();
        ClAñoLectivo clAño = new ClAñoLectivo();
        ClTercero clTerceros = new ClTercero();
        public FrmRptNotasByEstudiante()
        {
            InitializeComponent();
        }

        private void FrmRptNotasByEstudiante_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenarCamposBusq();
            LlenarCbxPeriodos();
            txtAño.Focus();           
        }
   
        private void btnGenerar_Click(object sender, EventArgs e)
        {

        }

        private void repositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ok");
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            if (txtEstudiante.ValorTextBox == "" || txtAño.ValorTextBox == "" || lkePeriodo.lkeDatos.EditValue == null) return;
            int wIdAño = Convert.ToInt32(txtAño.ValorTextBox);
            List<ADNotasVistaET> res = clNotas.GetNotasByEstudiante(Convert.ToInt64(txtEstudiante.ValorTextBox), Convert.ToInt32(lkePeriodo.lkeDatos.EditValue), wIdAño);
            if (res.Count>0)
            {
                gcNotas.DataSource = res;//clNotas.GetNotasByEstudiante(Convert.ToInt64(txtEstudiante.ValorTextBox), Convert.ToInt32(lkePeriodo.lkeDatos.EditValue));
            }else
            {
                ClFunciones.msgExitoso("No se encontraron registros.");
                gcNotas.DataSource = new DataTable();
            }
        }
        private void LlenarCamposBusq()
        {
            try
            {
                DataTable dt = clAño.GetAñosLectivos();
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.DataTable = dt;
                    txtAño.ValorTextBox = ClFunciones.wADDatosInstitucion[0].IdAñoActual.ToString();
                }
                dt = clTerceros.GetEstudiantesMatriculadosByAño(Convert.ToInt32(txtAño.ValorTextBox));
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtEstudiante.DataTable = dt;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarCbxPeriodos()
        {
            try
            {
                Clases.ClPeriodos cl = new Clases.ClPeriodos();
                lkePeriodo.lkeDatos.Properties.DataSource = cl.GetPeriodos();
                lkePeriodo.lkeDatos.Properties.ValueMember = "IdPeriodo";
                lkePeriodo.lkeDatos.Properties.DisplayMember = "Nombre";
                lkePeriodo.lkeDatos.Properties.PopulateColumns();
                lkePeriodo.lkeDatos.Properties.Columns[0].Visible = false;
                lkePeriodo.lkeDatos.EditValue = ClFunciones.wADDatosInstitucion[0].PeriodoActual;
                lkePeriodo.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void CrearGrilla()
        {
            try
            {
                gvNotas = GrillaDevExpress.CrearGrilla(false, true);
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("NomMateria", "Materia", ancho: 100));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Docente", "Docente", ancho: 100));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("IH", "IH", ancho: 40));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("AUS", "AUS", ancho: 40));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P1", "P1", ancho: 50, tipo: DevExpress.Utils.FormatType.Numeric, formato: "N1"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P2", "P2", ancho: 50, tipo: DevExpress.Utils.FormatType.Numeric, formato: "N1"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P3", "P3", ancho: 50, tipo: DevExpress.Utils.FormatType.Numeric, formato: "N1"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P4", "P4", ancho: 50, tipo: DevExpress.Utils.FormatType.Numeric, formato: "N1"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Prom", "Prom Periodo", ancho: 50, tipo: DevExpress.Utils.FormatType.Numeric, formato: "N1"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Desempeños", "Desempeños", ancho: 200));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Titular", "Titular", ancho: 100));   
                gvNotas.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvNotas.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvNotas.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvNotas.OptionsCustomization.AllowColumnResizing = true;
                gcNotas.MainView = gvNotas;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtEstudiante_SaleControl(object sender, EventArgs e)
        {
            try
            {
                if (txtEstudiante.ValorTextBox == "" && txtAño.ValorTextBox == "") return;
                
                string sql = string.Format("SELECT CS.NomCurso FROM ADCursos CS " +
                " INNER JOIN ADMatriculas MT ON CS.Sec = MT.IdCurso" +
                " AND MT.IdAñoLectivo = {0} AND MT.IdEstudiante = {1}", 
                txtAño.ValorTextBox, txtEstudiante.ValorTextBox);
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count>0)
                {
                    txtCurso.ValorTextBox = dt.Rows[0][0].ToString();
                }
                else
                {
                    txtCurso.ValorTextBox = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }
    }
}
