﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Colegio.Clases;

namespace Colegio.Vistas.Configuracion
{
    public partial class FrmSedes : dll.Common.FrmBase
    {
        int SecSede = -1;
        bool EstaActualizando = false;
        public FrmSedes()
        {
            InitializeComponent();
        }

        private void FrmSedes_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenaGrilla();
            txtNomMateria.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtNomMateria.ValorTextBox == "")
            {
                ClFunciones.msgError("Debe digitar el nombre de la sede.");
                txtNomMateria.Focus();
                return;
            }
            if (!EstaActualizando) SecSede = ClFunciones.TraeSiguienteSecuencial("ADSedes", "Sec", ClConexion.clConexion.Conexion);
            if (SecSede > 0)
            {
                if (GuardarMat(SecSede, txtNomMateria.ValorTextBox))
                {
                    if (!EstaActualizando)
                    {
                        ClFunciones.msgExitoso("Sede creada de manera correcta.");

                    }
                    else
                    {
                        ClFunciones.msgExitoso("Sede actualizada de manera correcta.");
                    }
                    LlenaGrilla();
                    LimpiarCampos();
                }
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CrearGrilla()
        {
            try
            {
                gvSedes = GrillaDevExpress.CrearGrilla(false, true);
                gvSedes.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho: 20));
                gvSedes.Columns.Add(GrillaDevExpress.CrearColumna("NomSede", "Sede"));
                gvSedes.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvSedes.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvSedes.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvSedes.OptionsCustomization.AllowColumnResizing = true;
                gcSedes.MainView = gvSedes;
                this.gvSedes.DoubleClick += new System.EventHandler(this.gvMaterias_DoubleClick);

            }
            catch (Exception)
            {

            }
        }

        private bool GuardarMat(int secSede, string nomSede)
        {
            try
            {
                string sql = "UPDATE [dbo].[ADSedes] SET [NomSede] = '" + nomSede + "' WHERE Sec = " + secSede.ToString();
                if (EstaActualizando)
                {
                    if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) != "si") return false;

                }
                else
                {
                    sql = "INSERT INTO [dbo].[ADSedes]  ([Sec],[NomSede], [IdInstitucion]) VALUES (" + secSede.ToString() + ",'" + nomSede + "', 1)";
                    if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) != "si") return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        private void LlenaGrilla()
        {
            try
            {
                string sql = "SELECT Sec, NomSede FROM ADSedes";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                gcSedes.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LimpiarCampos()
        {
            txtNomMateria.ValorTextBox = "";
            SecSede= -1;
            EstaActualizando = false;
            txtNomMateria.Focus();
        }

        private void CargarMateria(int numFila)
        {
            try
            {
                SecSede = Convert.ToInt32(gvSedes.GetRowCellValue(numFila, "Sec"));
                txtNomMateria.ValorTextBox = gvSedes.GetRowCellValue(numFila, "NomSede").ToString();
                EstaActualizando = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gvMaterias_DoubleClick(object sender, EventArgs e)
        {
            int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
            if (numFila > -1)
            {
                CargarMateria(numFila);
            }
        }

    }
}