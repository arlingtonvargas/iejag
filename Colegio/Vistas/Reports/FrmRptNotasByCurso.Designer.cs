﻿namespace Colegio.Vistas.Reports
{
    partial class FrmRptNotasByCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptNotasByCurso));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            this.btnGenerar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lkePeriodo = new dll.Controles.ucLabelCombo();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.txtCurso = new dll.Controles.ucBusqueda();
            this.btnConsultar = new DevExpress.XtraEditors.SimpleButton();
            this.gcEstudiantes = new DevExpress.XtraGrid.GridControl();
            this.gvEstudiantes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcEstudiantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEstudiantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGenerar
            // 
            this.btnGenerar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerar.ImageOptions.Image")));
            this.btnGenerar.Location = new System.Drawing.Point(901, 28);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(75, 21);
            this.btnGenerar.TabIndex = 5;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Controls.Add(this.btnConsultar);
            this.groupControl1.Controls.Add(this.btnGenerar);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(984, 58);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "Parametros";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.tableLayoutPanel1.Controls.Add(this.lkePeriodo, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtAño, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCurso, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(808, 30);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // lkePeriodo
            // 
            this.lkePeriodo.AnchoTitulo = 70;
            this.lkePeriodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lkePeriodo.Enabled = false;
            this.lkePeriodo.Location = new System.Drawing.Point(571, 0);
            this.lkePeriodo.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.lkePeriodo.MaximumSize = new System.Drawing.Size(3000, 28);
            this.lkePeriodo.MensajeDeAyuda = null;
            this.lkePeriodo.MinimumSize = new System.Drawing.Size(50, 28);
            this.lkePeriodo.Name = "lkePeriodo";
            this.lkePeriodo.Size = new System.Drawing.Size(234, 28);
            this.lkePeriodo.TabIndex = 3;
            this.lkePeriodo.TextoTitulo = "Periodo :";
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 50;
            this.txtAño.AnchoTitulo = 90;
            this.txtAño.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAño.Enabled = false;
            this.txtAño.Location = new System.Drawing.Point(0, 0);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(284, 28);
            this.txtAño.TabIndex = 1;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtAño.ValorTextBox = "";
            this.txtAño.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtAño_SaleControl);
            // 
            // txtCurso
            // 
            this.txtCurso.AnchoTextBox = 50;
            this.txtCurso.AnchoTitulo = 80;
            this.txtCurso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCurso.Location = new System.Drawing.Point(284, 0);
            this.txtCurso.Margin = new System.Windows.Forms.Padding(0);
            this.txtCurso.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCurso.MensajeDeAyuda = null;
            this.txtCurso.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.PermiteSoloNumeros = false;
            this.txtCurso.Script = "";
            this.txtCurso.Size = new System.Drawing.Size(284, 28);
            this.txtCurso.TabIndex = 2;
            this.txtCurso.TextoTitulo = "Curso :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtCurso.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtCurso.ValorTextBox = "";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.ImageOptions.Image")));
            this.btnConsultar.Location = new System.Drawing.Point(820, 28);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 21);
            this.btnConsultar.TabIndex = 4;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // gcEstudiantes
            // 
            this.gcEstudiantes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcEstudiantes.Location = new System.Drawing.Point(12, 76);
            this.gcEstudiantes.MainView = this.gvEstudiantes;
            this.gcEstudiantes.Name = "gcEstudiantes";
            this.gcEstudiantes.Size = new System.Drawing.Size(984, 362);
            this.gcEstudiantes.TabIndex = 9;
            this.gcEstudiantes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvEstudiantes});
            // 
            // gvEstudiantes
            // 
            this.gvEstudiantes.GridControl = this.gcEstudiantes;
            this.gvEstudiantes.Name = "gvEstudiantes";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("report_16x16.png", "office2013/reports/report_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/reports/report_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "report_16x16.png");
            // 
            // FrmRptNotasByCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 450);
            this.Controls.Add(this.gcEstudiantes);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmRptNotasByCurso";
            this.Text = "Notas por Curso";
            this.Load += new System.EventHandler(this.FrmRptNotasByCurso_Load);
            this.SizeChanged += new System.EventHandler(this.FrmRptNotasByCurso_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcEstudiantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEstudiantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnGenerar;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnConsultar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private dll.Controles.ucLabelCombo lkePeriodo;
        private dll.Controles.ucBusqueda txtAño;
        private dll.Controles.ucBusqueda txtCurso;
        private DevExpress.XtraGrid.GridControl gcEstudiantes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvEstudiantes;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}