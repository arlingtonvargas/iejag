﻿namespace Colegio.Vistas
{
    partial class FrmMaestroMatCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaestroMatCurso));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET4 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            this.gcDatos = new DevExpress.XtraEditors.GroupControl();
            this.txtMaestro = new dll.Controles.ucBusqueda();
            this.txtCurso = new dll.Controles.ucBusqueda();
            this.txtMateria = new dll.Controles.ucBusqueda();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.gcOpciones = new DevExpress.XtraEditors.GroupControl();
            this.ucBtnCancelarAV1 = new dll.Controles.ucBtnCancelarAV();
            this.ucBtnGuardarAV1 = new dll.Controles.ucBtnGuardarAV();
            this.gcCursos = new DevExpress.XtraGrid.GridControl();
            this.gvCursos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcMaterias = new DevExpress.XtraGrid.GridControl();
            this.gvMaterias = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnAddMateria = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnQuitarCurso = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuitarMat = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcDatos)).BeginInit();
            this.gcDatos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcOpciones)).BeginInit();
            this.gcOpciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcCursos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCursos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMaterias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaterias)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gcDatos
            // 
            this.gcDatos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDatos.Controls.Add(this.txtMaestro);
            this.gcDatos.Location = new System.Drawing.Point(12, 12);
            this.gcDatos.Name = "gcDatos";
            this.gcDatos.Size = new System.Drawing.Size(745, 70);
            this.gcDatos.TabIndex = 0;
            this.gcDatos.Text = "Información";
            // 
            // txtMaestro
            // 
            this.txtMaestro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaestro.AnchoTextBox = 120;
            this.txtMaestro.AnchoTitulo = 80;
            this.txtMaestro.Location = new System.Drawing.Point(2, 29);
            this.txtMaestro.Margin = new System.Windows.Forms.Padding(0);
            this.txtMaestro.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMaestro.MensajeDeAyuda = null;
            this.txtMaestro.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMaestro.Name = "txtMaestro";
            this.txtMaestro.PermiteSoloNumeros = false;
            this.txtMaestro.Size = new System.Drawing.Size(736, 28);
            this.txtMaestro.TabIndex = 0;
            this.txtMaestro.TextoTitulo = "Maestro :";
            tituloColsBusquedaET4.Codigo = "Código";
            tituloColsBusquedaET4.Descripcion = "Descripción";
            this.txtMaestro.TituloColsBusqueda = tituloColsBusquedaET4;
            this.txtMaestro.ValorTextBox = "";
            this.txtMaestro.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtMaestro_SaleControl);
            // 
            // txtCurso
            // 
            this.txtCurso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurso.AnchoTextBox = 120;
            this.txtCurso.AnchoTitulo = 76;
            this.txtCurso.Location = new System.Drawing.Point(3, 17);
            this.txtCurso.Margin = new System.Windows.Forms.Padding(0);
            this.txtCurso.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCurso.MensajeDeAyuda = null;
            this.txtCurso.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.PermiteSoloNumeros = false;
            this.txtCurso.Size = new System.Drawing.Size(463, 28);
            this.txtCurso.TabIndex = 1;
            this.txtCurso.TextoTitulo = "Curso :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtCurso.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtCurso.ValorTextBox = "";
            // 
            // txtMateria
            // 
            this.txtMateria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMateria.AnchoTextBox = 120;
            this.txtMateria.AnchoTitulo = 80;
            this.txtMateria.Location = new System.Drawing.Point(2, 17);
            this.txtMateria.Margin = new System.Windows.Forms.Padding(0);
            this.txtMateria.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMateria.MensajeDeAyuda = null;
            this.txtMateria.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMateria.Name = "txtMateria";
            this.txtMateria.PermiteSoloNumeros = false;
            this.txtMateria.Size = new System.Drawing.Size(461, 28);
            this.txtMateria.TabIndex = 2;
            this.txtMateria.TextoTitulo = "Materia :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtMateria.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtMateria.ValorTextBox = "";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.ImageOptions.Image")));
            this.btnAdd.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAdd.Location = new System.Drawing.Point(499, 20);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(24, 21);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // gcOpciones
            // 
            this.gcOpciones.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gcOpciones.Controls.Add(this.btnRefresh);
            this.gcOpciones.Controls.Add(this.ucBtnCancelarAV1);
            this.gcOpciones.Controls.Add(this.ucBtnGuardarAV1);
            this.gcOpciones.Location = new System.Drawing.Point(763, 12);
            this.gcOpciones.Name = "gcOpciones";
            this.gcOpciones.Size = new System.Drawing.Size(319, 70);
            this.gcOpciones.TabIndex = 1;
            this.gcOpciones.Text = "Opciones";
            // 
            // ucBtnCancelarAV1
            // 
            this.ucBtnCancelarAV1.Location = new System.Drawing.Point(212, 26);
            this.ucBtnCancelarAV1.Name = "ucBtnCancelarAV1";
            this.ucBtnCancelarAV1.Size = new System.Drawing.Size(98, 38);
            this.ucBtnCancelarAV1.TabIndex = 1;
            // 
            // ucBtnGuardarAV1
            // 
            this.ucBtnGuardarAV1.Location = new System.Drawing.Point(5, 26);
            this.ucBtnGuardarAV1.Name = "ucBtnGuardarAV1";
            this.ucBtnGuardarAV1.Size = new System.Drawing.Size(98, 38);
            this.ucBtnGuardarAV1.TabIndex = 0;
            // 
            // gcCursos
            // 
            this.gcCursos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcCursos.Location = new System.Drawing.Point(3, 58);
            this.gcCursos.MainView = this.gvCursos;
            this.gcCursos.Name = "gcCursos";
            this.gcCursos.Size = new System.Drawing.Size(529, 319);
            this.gcCursos.TabIndex = 2;
            this.gcCursos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCursos});
            // 
            // gvCursos
            // 
            this.gvCursos.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gvCursos.GridControl = this.gcCursos;
            this.gvCursos.Name = "gvCursos";
            this.gvCursos.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvCursos_FocusedRowChanged_1);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            // 
            // gcMaterias
            // 
            this.gcMaterias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcMaterias.Location = new System.Drawing.Point(538, 58);
            this.gcMaterias.MainView = this.gvMaterias;
            this.gcMaterias.Name = "gcMaterias";
            this.gcMaterias.Size = new System.Drawing.Size(529, 319);
            this.gcMaterias.TabIndex = 3;
            this.gcMaterias.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaterias});
            // 
            // gvMaterias
            // 
            this.gvMaterias.GridControl = this.gcMaterias;
            this.gvMaterias.Name = "gvMaterias";
            // 
            // btnAddMateria
            // 
            this.btnAddMateria.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddMateria.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddMateria.ImageOptions.Image")));
            this.btnAddMateria.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnAddMateria.Location = new System.Drawing.Point(499, 20);
            this.btnAddMateria.Name = "btnAddMateria";
            this.btnAddMateria.Size = new System.Drawing.Size(24, 21);
            this.btnAddMateria.TabIndex = 4;
            this.btnAddMateria.Click += new System.EventHandler(this.btnAddMateria_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupControl2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.gcCursos, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gcMaterias, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 88);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1070, 380);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.btnQuitarMat);
            this.groupControl2.Controls.Add(this.btnAddMateria);
            this.groupControl2.Controls.Add(this.txtMateria);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(538, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(529, 49);
            this.groupControl2.TabIndex = 7;
            this.groupControl2.Text = "Seleccione la materia";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnQuitarCurso);
            this.groupControl1.Controls.Add(this.txtCurso);
            this.groupControl1.Controls.Add(this.btnAdd);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(3, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(529, 49);
            this.groupControl1.TabIndex = 6;
            this.groupControl1.Text = "Seleccione el curso";
            // 
            // btnQuitarCurso
            // 
            this.btnQuitarCurso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuitarCurso.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image1")));
            this.btnQuitarCurso.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnQuitarCurso.Location = new System.Drawing.Point(469, 20);
            this.btnQuitarCurso.Name = "btnQuitarCurso";
            this.btnQuitarCurso.Size = new System.Drawing.Size(24, 21);
            this.btnQuitarCurso.TabIndex = 4;
            this.btnQuitarCurso.Click += new System.EventHandler(this.btnQuitarCurso_Click);
            // 
            // btnQuitarMat
            // 
            this.btnQuitarMat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuitarMat.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.ImageOptions.Image")));
            this.btnQuitarMat.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnQuitarMat.Location = new System.Drawing.Point(469, 20);
            this.btnQuitarMat.Name = "btnQuitarMat";
            this.btnQuitarMat.Size = new System.Drawing.Size(24, 21);
            this.btnQuitarMat.TabIndex = 5;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Appearance.Options.UseFont = true;
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(108, 26);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(98, 38);
            this.btnRefresh.TabIndex = 2;
            this.btnRefresh.Text = "Refrescar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // FrmMaestroMatCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 474);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.gcOpciones);
            this.Controls.Add(this.gcDatos);
            this.Name = "FrmMaestroMatCurso";
            this.Text = "FrmMaestroMatCurso";
            this.Load += new System.EventHandler(this.FrmMaestroMatCurso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcDatos)).EndInit();
            this.gcDatos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcOpciones)).EndInit();
            this.gcOpciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcCursos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCursos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMaterias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaterias)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl gcDatos;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private dll.Controles.ucBusqueda txtMateria;
        private dll.Controles.ucBusqueda txtCurso;
        private dll.Controles.ucBusqueda txtMaestro;
        private DevExpress.XtraEditors.GroupControl gcOpciones;
        private dll.Controles.ucBtnCancelarAV ucBtnCancelarAV1;
        private dll.Controles.ucBtnGuardarAV ucBtnGuardarAV1;
        private DevExpress.XtraGrid.GridControl gcCursos;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCursos;
        private DevExpress.XtraGrid.GridControl gcMaterias;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMaterias;
        private DevExpress.XtraEditors.SimpleButton btnAddMateria;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.SimpleButton btnQuitarMat;
        private DevExpress.XtraEditors.SimpleButton btnQuitarCurso;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
    }
}