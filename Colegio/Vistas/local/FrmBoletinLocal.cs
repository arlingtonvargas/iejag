﻿using DevExpress.XtraReports.UI;
using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio.Vistas
{
    public partial class FrmBoletinLocal : FrmBase
    {
        public FrmBoletinLocal()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmBoletinLocal_Load(object sender, EventArgs e)
        {
            CargarEstudiantes();
            CrearGrilla();
        }

        private void CrearGrilla()
        {
            try
            {
                gvNotas = GrillaDevExpress.CrearGrilla(false, true);

                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Asignatura", "Asignatura",ancho:200));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Docente", "Docente", ancho: 200));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("IH", "IH", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("AUS", "AUS", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P1", "P1", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P2", "P2", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P3", "P3", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("P4", "P4", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("PROM", "PROM", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Desempeños", "Desempeños"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Observaciones", "Observaciones"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Periodo", "Periodo", ancho: 80));
                gvNotas.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvNotas.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvNotas.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvNotas.OptionsCustomization.AllowColumnResizing = true;
                gcNotas.MainView = gvNotas;
                this.gvNotas.DoubleClick += new System.EventHandler(this.gvNotas_DoubleClick);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void gvNotas_DoubleClick(object sender, EventArgs e)
        {
         
        }

        private void LlenarGrilla()
        {

        }

        private void CargarEstudiantes()
        {
            try
            {
                string sql = "SELECT Documento, NomCompleto FROM TEMPEstudiantes";
                DataTable dt = Clases.ClFunciones.Consultar(sql, Clases.ClConexion.clConexion.Conexion);
                cbxEstudientes.Properties.DataSource = dt;
                cbxEstudientes.Properties.ValueMember = "Documento";
                cbxEstudientes.Properties.DisplayMember = "NomCompleto";
                cbxEstudientes.Properties.PopulateColumns();
                cbxEstudientes.Properties.ShowFooter = false;
                cbxEstudientes.Properties.ShowHeader = false;
                cbxEstudientes.Properties.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                
            }
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsPatent = new DataSet();
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "xlsx Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string filename = openFileDialog1.FileName;
                    DataTable dt = ExcelDataBaseHelper.OpenFile(filename, "Notas");
                    if (dt == null)
                    {
                        return;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        gcNotas.DataSource = dt;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {

                if (cbxEstudientes.ItemIndex < 0)
                {
                    MessageBox.Show("Debe seleccionar el estudiente");
                    return;
                }
                else if (txtProm.Text == "")
                {
                    MessageBox.Show("Debe digitar el promedio.");
                    return;
                }
                else if (txtNivel.Text == "")
                {
                    MessageBox.Show("Debe digitar el nivel de desempeño del estudiente.");
                    return;
                }

                string sql = "SELECT Documento, Sede, Jornada, Grupo, PosicionGrupo, NomCompleto, Codigo FROM TEMPEstudiantes";
                DataTable dtEst = Clases.ClFunciones.Consultar(sql, Clases.ClConexion.clConexion.Conexion);
                if (dtEst.Rows.Count<=0 )
                {
                    MessageBox.Show("No se consulta la información del estudiante correctamente.");
                    return;
                }


                Reportes.RPTBoletin RPT = new Reportes.RPTBoletin();
                
                DataTable dt = (DataTable)gcNotas.DataSource;
                RPT.DataSource = dt;
                RPT.lblAsignatura.DataBindings.Add("Text", null, "Asignatura");
                RPT.lblNomDocente.DataBindings.Add("Text", null, "Docente");
                RPT.lblIH.DataBindings.Add("Text", null, "IH");
                RPT.lblAUS.DataBindings.Add("Text", null, "AUS");
                RPT.lblP1.DataBindings.Add("Text", null, "P1", "{0:N1}");
                RPT.lblP2.DataBindings.Add("Text", null, "P2", "{0:N1}");
                RPT.lblP3.DataBindings.Add("Text", null, "P3", "{0:N1}");
                RPT.lblP4.DataBindings.Add("Text", null, "P4", "{0:N1}");
                RPT.lblDesempeños.DataBindings.Add("Text", null, "Desempeños");

                RPT.lblSede.Text = dtEst.Rows[0]["Sede"].ToString();
                RPT.lblJornada.Text = dtEst.Rows[0]["Jornada"].ToString();
                RPT.lblGrupo.Text = dtEst.Rows[0]["Grupo"].ToString();
                RPT.lblPeriodo.Text = dt.Rows[0]["Periodo"].ToString();
                RPT.lblPosGrupo.Text = dtEst.Rows[0]["PosicionGrupo"].ToString();
                RPT.lblProm.Text = Convert.ToDouble(dt.Rows[0]["PROM"]).ToString("N1");
                RPT.lblDocumento.Text = dtEst.Rows[0]["Documento"].ToString();
                RPT.lblNomEst.Text = dtEst.Rows[0]["NomCompleto"].ToString();
                RPT.lblCodigo.Text = dtEst.Rows[0]["Codigo"].ToString();
                RPT.lblNivelDesemp.Text = txtNivel.Text;
                //RPT.lblObserv.Text = dt.Rows[0]["Observaciones"].ToString();
                RPT.lblPromGen.Text = txtProm.Text;
                RPT.CreateDocument();
                for (int i = 0; i < 10; i++)
                {
                    Reportes.RPTBoletin RPT2 = new Reportes.RPTBoletin();
                    RPT2.DataSource = dt;
                    RPT2.lblAsignatura.DataBindings.Add("Text", null, "Asignatura");
                    RPT2.lblNomDocente.DataBindings.Add("Text", null, "Docente");
                    RPT2.lblIH.DataBindings.Add("Text", null, "IH");
                    RPT2.lblAUS.DataBindings.Add("Text", null, "AUS");
                    RPT2.lblP1.DataBindings.Add("Text", null, "P1", "{0:N1}");
                    RPT2.lblP2.DataBindings.Add("Text", null, "P2", "{0:N1}");
                    RPT2.lblP3.DataBindings.Add("Text", null, "P3", "{0:N1}");
                    RPT2.lblP4.DataBindings.Add("Text", null, "P4", "{0:N1}");
                    RPT2.lblDesempeños.DataBindings.Add("Text", null, "Desempeños");

                    RPT2.lblSede.Text = dtEst.Rows[0]["Sede"].ToString();
                    RPT2.lblJornada.Text = dtEst.Rows[0]["Jornada"].ToString();
                    RPT2.lblGrupo.Text = dtEst.Rows[0]["Grupo"].ToString();
                    RPT2.lblPeriodo.Text = dt.Rows[0]["Periodo"].ToString();
                    RPT2.lblPosGrupo.Text = dtEst.Rows[0]["PosicionGrupo"].ToString();
                    RPT2.lblProm.Text = Convert.ToDouble(dt.Rows[0]["PROM"]).ToString("N1");
                    RPT2.lblDocumento.Text = dtEst.Rows[0]["Documento"].ToString();
                    RPT2.lblNomEst.Text = dtEst.Rows[0]["NomCompleto"].ToString();
                    RPT2.lblCodigo.Text = dtEst.Rows[0]["Codigo"].ToString();
                    RPT2.lblNivelDesemp.Text = txtNivel.Text;
                    //RPT2.lblObserv.Text = dt.Rows[0]["Observaciones"].ToString();
                    RPT2.lblPromGen.Text = txtProm.Text;
                    RPT2.CreateDocument();

                    RPT.Pages.AddRange(RPT2.Pages);

                    // Reset all page numbers in the resulting document. 
                    RPT.PrintingSystem.ContinuousPageNumbering = true;
                }
                FrmVistaPrevia report = new FrmVistaPrevia();
                report.document.DocumentSource = RPT;
                report.ShowDialog();
            }
            catch (Exception ex)
            {

            }

        }
    }


    //public class PrintHelper
    //{
    //    List<XtraReport> reports;
    //    Link link;
    //    public PrintHelper(List<XtraReport> reports)
    //    {
    //        this.reports = reports;
    //        link = new Link(new PrintingSystem());
    //        int height = 0, width = 0;
    //        foreach (XtraReport x in reports)
    //        {
    //            height += x.PageHeight;
    //            width = Math.Max(width, x.PageWidth);
    //        }
    //        link.PaperKind = PaperKind.Custom;
    //        link.CustomPaperSize = new Size(width, height);
    //        link.Margins = new Margins(reports.First<XtraReport>().Margins.Left, reports.First<XtraReport>().Margins.Right, reports.First<XtraReport>().Margins.Top, reports.Last<XtraReport>().Margins.Bottom);
    //        link.CreateDetailArea += link_CreateDetailArea;
    //    }
    //    public void CreateDocument()
    //    {
    //        link.CreateDocument();
    //    }
    //    public void ShowPreview()
    //    {
    //        link.ShowPreview();
    //    }
    //    void link_CreateDetailArea(object sender, CreateAreaEventArgs e)
    //    {
    //        int maxPages = 0;
    //        foreach (XtraReport x in reports)
    //        {
    //            x.CreateDocument();
    //            maxPages = Math.Max(maxPages, x.Pages.Count);
    //        }
    //        SizeF pageSize = e.Graph.ClientPageSize;
    //        float sectionHeight = reports != null && reports.Count() > 1 ? pageSize.Height / reports.Count() : 0;
    //        PanelBrick section;
    //        //for (int pageIndex = 0; pageIndex < maxPages; pageIndex++)
    //        //{
    //        //    for (int sectionIndex = 0; sectionIndex < reports.Count; sectionIndex++)
    //        //    {
    //        //        section = new PanelBrick { Size = new SizeF(e.Graph.ClientPageSize.Width, sectionHeight), BorderWidth = 0 };
    //        //        if (reports[sectionIndex].Pages.Count < pageIndex + 1)
    //        //        {
    //        //            section.Bricks.Add(new LineBrick()
    //        //            {
    //        //                Rect = section.Rect,
    //        //                LineDirection = DevExpress.XtraReports.UI.LineDirection.Slant,
    //        //                ForeColor = Color.Red,
    //        //                LineStyle = System.Drawing.Drawing2D.DashStyle.Dash,
    //        //                BorderWidth = 0
    //        //            });
    //        //            section.Bricks.Add(new LineBrick()
    //        //            {
    //        //                Rect = section.Rect,
    //        //                LineDirection = DevExpress.XtraReports.UI.LineDirection.BackSlant,
    //        //                ForeColor = Color.Red,
    //        //                BackColor = Color.Transparent,
    //        //                LineStyle = System.Drawing.Drawing2D.DashStyle.Dash,
    //        //                BorderWidth = 1,
    //        //                BorderColor = Color.Red,
    //        //                BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash,
    //        //                BorderStyle = BrickBorderStyle.Inset
    //        //            });
    //        //        }
    //        //        else
    //        //        {
    //        //            foreach (Brick brick in ((PSPage)reports[sectionIndex].Pages[pageIndex]).Bricks)
    //        //            {
    //        //                section.Bricks.Add(brick);
    //        //            }
    //        //        }
    //        //        e.Graph.Modifier = BrickModifier.None;
    //        //        e.Graph.Modifier = BrickModifier.Detail;
    //        //        e.Graph.DrawBrick(section);
    //        //    }
    //        //}
    //    }
    //}
}
