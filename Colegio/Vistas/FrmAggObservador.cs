﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DevExpress.XtraLayout.Helpers;
using DevExpress.XtraLayout;
using Colegio.Clases;
using Colegio.ET;

namespace Colegio.Vistas
{
    public partial class FrmAggObservador : DevExpress.XtraEditors.XtraForm
    {

        ClTercero mClTercero = new ClTercero();
        ClObservadorEstudiante mClObservadorEstudiante = new ClObservadorEstudiante();
        public FrmAggObservador()
        {
            InitializeComponent();   
        }
        private void FrmAggObservador_Load(object sender, EventArgs e)
        {
            LlenarControlesBusqueda();
            txtEstudiante.Focus();
            txtEstudiante.Select();
        }


        public static Image Base64ToImage(string base64Image)
        {
            using (MemoryStream ms = new MemoryStream(Convert.FromBase64String(base64Image)))
            {
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }

        private void txtEstudiante_SaleControl(object sender, EventArgs e)
        {
            try
            {
                aDObservacionEstudianteListBindingSource.Clear();
                if (txtEstudiante.ValorTextBox == "") return;
                long wIdEst = Convert.ToInt64(txtEstudiante.ValorTextBox);
                CargarDatos(wIdEst);
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void windowsUIButtonPanelMain_ButtonClick(object sender, DevExpress.XtraBars.Docking2010.ButtonEventArgs e)
        {
            switch (e.Button.Properties.Caption)
            {
                case "Salir":
                    this.Close();
                    break;
                case "Guardar":
                    if (GuardarObs())
                    {
                        //txtAño.ValorTextBox = "";
                        chkbtnOk.Checked = false;
                        chkbtnNeg.Checked = false;
                        txtMaestro.ValorTextBox = "";
                        txtObserv.Text = "";
                        dtpFecha.DateTime = DateTime.Now;                        
                        txtEstudiante.Focus();
                        txtMaestro.Focus();
                    }
                    break;
                case "Guardar y salir":
                    if(GuardarObs()) this.Close();
                    break;
                case "Limpiar":
                    LimpiarCampos();
                    break;
                default:
                    break;
            }
        }

        private void CargarDatos(long pIdEst)
        {
            try
            {
                DataTable dt = mClTercero.GetTerceroById(pIdEst);
                if (dt.Rows.Count > 0)
                {
                    pcbFoto.Image = null;
                    if(dt.Rows[0]["Foto"] != DBNull.Value)
                    {
                        byte[] wFoto = (byte[])dt.Rows[0]["Foto"];
                        pcbFoto.Image = ClFunciones.byteArrayToImage(wFoto);
                    }
                     
                    ADObservacionEstudianteList wAaDObservacionEstudiante = mClObservadorEstudiante.GetObservacionEstudiante(pIdEst);
                    aDObservacionEstudianteListBindingSource.DataSource = wAaDObservacionEstudiante;
                }
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void LlenarControlesBusqueda()
        {
            try
            {
                ClAñoLectivo clAñoLectivo = new ClAñoLectivo();


                DataTable dt = mClTercero.GetTercerosTipoTer(1);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                txtMaestro.TituloColsBusqueda = new dll.Common.ET.TituloColsBusquedaET() { Codigo = "Documento", Descripcion = "Nombre Maestro" };
                txtMaestro.DataTable = dt;


                dt = clAñoLectivo.GetAñosLectivos();
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.TituloColsBusqueda = new dll.Common.ET.TituloColsBusquedaET() { Codigo = "Documento", Descripcion = "Nombre Maestro" };
                    txtAño.DataTable = dt;
                    txtAño.ValorTextBox = ClFunciones.wADDatosInstitucion[0].IdAñoActual.ToString();
                }
                dt = mClTercero.GetEstudiantesMatriculadosByAño(ClFunciones.wADDatosInstitucion[0].IdAñoActual);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                txtEstudiante.TituloColsBusqueda = new dll.Common.ET.TituloColsBusquedaET() { Codigo = "Documento", Descripcion = "Nombre Estudiante" };
                txtEstudiante.DataTable = dt;



            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void LimpiarCampos()
        {
            txtEstudiante.ValorTextBox = "";
            //txtAño.ValorTextBox = "";
            txtMaestro.ValorTextBox = "";
            txtObserv.Text = "";
            dtpFecha.DateTime = DateTime.Now;
            txtEstudiante.Focus();
            pcbFoto.Image = null;
            chkbtnOk.Checked = false;
            chkbtnNeg.Checked = false;
            aDObservacionEstudianteListBindingSource.Clear();
        }

        private bool ValidarCampos()
        {
            try
            {
                if (txtEstudiante.ValorTextBox =="")
                {
                    ClFunciones.msgError("Debe seleccionar el estudiante.");
                    txtEstudiante.Focus();
                    return false;
                }
                else if (txtAño.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el año.");
                    txtAño.Focus();
                    return false;
                }
                else if (txtMaestro.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el maestro.");
                    txtMaestro.Focus();
                    return false;
                }
                else if (txtObserv.Text== "")
                {
                    ClFunciones.msgError("Debe digitar la observación.");
                    txtObserv.Focus();
                    return false;
                }
                else if (dtpFecha.EditValue == null)
                {
                    ClFunciones.msgError("Debe seleccionar la fecha de la observación.");
                    dtpFecha.Focus();
                    return false;
                }
                else if (!chkbtnOk.Checked && !chkbtnNeg.Checked)
                {
                    ClFunciones.msgError("Debe seleccionar si la observación es positiva o negativa.");
                    chkbtnOk.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
                return false;
            }
        }

        private bool GuardarObs()
        {
            try
            {
                if (!ValidarCampos()) return false;
                if (ClFunciones.msgResult("Seguro que desea registrar la observación?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ADObservacionEstudianteET aDObservacionEstudianteET = new ADObservacionEstudianteET();

                    bool wOsIsPositive = !chkbtnNeg.Checked;
                    if (chkbtnOk.Checked) wOsIsPositive = chkbtnOk.Checked;

                    aDObservacionEstudianteET.OSIdEstudiente = Convert.ToInt64(txtEstudiante.ValorTextBox);
                    aDObservacionEstudianteET.OSIdAño = Convert.ToInt32(txtAño.ValorTextBox);
                    aDObservacionEstudianteET.OSFechaObservacion = dtpFecha.DateTime;
                    aDObservacionEstudianteET.OSObservacion = txtObserv.Text;
                    aDObservacionEstudianteET.OSMaestroReistra = ClFunciones.UsuarioIngreso.IdTercero;
                    aDObservacionEstudianteET.OSMaestroReporta = Convert.ToInt64(txtMaestro.ValorTextBox);
                    aDObservacionEstudianteET.OSIsPositive = wOsIsPositive;
                    if (mClObservadorEstudiante.InsertObservacion(aDObservacionEstudianteET) == null)
                    {
                        ClFunciones.msgError("Ha ocurrido un error.");
                        return false;
                    }
                    else
                    {
                        ClFunciones.msgExitoso("Obervación registrada correctamente.");
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
                return false;
            }
        }

        private void chkbtnOk_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbtnOk.Checked)
            {
                chkbtnNeg.Checked = false;
            }
        }

        private void chkbtnNeg_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbtnNeg.Checked)
            {
                chkbtnOk.Checked = false;
            }
        }

     
    }
}