﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Colegio.ET;
using Colegio.Clases;

namespace Colegio.Vistas.Configuracion
{
    public partial class FrmConfGeneral : dll.Common.FrmBase
    {

        int SecDato = -1;
        ClADDatosInstitucion clADDatosInstitucion = new ClADDatosInstitucion();
        public FrmConfGeneral()
        {
            InitializeComponent();
        }

    

        private void FrmConfGeneral_Load(object sender, EventArgs e)
        {
            LlenaCombo();
            CargarDatos();
        }

        private void btnEditNit_Click(object sender, EventArgs e)
        {
            txtNit.Enabled = true;
        }

        private void btnSaveNit_Click(object sender, EventArgs e)
        {
            if (txtNit.ValorTextBox!="" && txtNit.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET Nit = '"+txtNit.ValorTextBox+"' WHERE Sec = "+ SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Nit actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    txtNit.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }

        private void btnEditNom_Click(object sender, EventArgs e)
        {
            txtNomComp.Enabled = true;
        }

        private void btnSaveNom_Click(object sender, EventArgs e)
        {
            if (txtNomComp.ValorTextBox != "" && txtNomComp.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET NombreCompleto = '" + txtNomComp.ValorTextBox + "' WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Nombre completo actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    txtNomComp.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }

        private void btnSaveNomCorto_Click(object sender, EventArgs e)
        {
            if (txtNomCorto.ValorTextBox != "" && txtNomCorto.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET NombreCorto = '" + txtNomCorto.ValorTextBox + "' WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Nombre corto actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    txtNomCorto.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }
        private void btnEditNomCorto_Click(object sender, EventArgs e)
        {
            txtNomCorto.Enabled = true;
        }

        private void btnEditPeriodo_Click(object sender, EventArgs e)
        {
            cbxPeriodo.Enabled = true;
        }

        private void btnSavePeriodo_Click(object sender, EventArgs e)
        {
            if (cbxPeriodo.lkeDatos.EditValue != null && cbxPeriodo.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET PeriodoActual = " + cbxPeriodo.lkeDatos.EditValue.ToString() + " WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Periodo actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    cbxPeriodo.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }      

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CargarDatos()
        {
            try
            {
                List<ADDatosInstitucionET> wADDatosInstitucion = clADDatosInstitucion.GetDatosInstitucion();
                if (wADDatosInstitucion.Count <= 0) return;
                SecDato = wADDatosInstitucion[0].Sec;
                txtNit.ValorTextBox = wADDatosInstitucion[0].Nit;
                txtNomComp.ValorTextBox = wADDatosInstitucion[0].NombreCompleto;
                txtNomCorto.ValorTextBox = wADDatosInstitucion[0].NombreCorto;
                txtNomRector.ValorTextBox = wADDatosInstitucion[0].NombreRector;
                txtDocRector.ValorTextBox = wADDatosInstitucion[0].DocRector.ToString();
                cbxPeriodo.lkeDatos.EditValue = wADDatosInstitucion[0].PeriodoActual;
                cbxAñoActual.lkeDatos.EditValue = wADDatosInstitucion[0].IdAñoActual;
                cbxReporteNotas.lkeDatos.EditValue = wADDatosInstitucion[0].ReportarNotas;
                txtCodDANA.ValorTextBox = wADDatosInstitucion[0].CodDANA;
                txtNit.Enabled = false;
                txtNomComp.Enabled = false;
                txtNomCorto.Enabled = false;
                txtNomRector.Enabled = false;
                txtDocRector.Enabled = false;
                txtCodDANA.Enabled = false;
                cbxPeriodo.Enabled = false;
                cbxAñoActual.Enabled = false;
                cbxReporteNotas.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void HabilitaBotones()
        {
            int rol = ClFunciones.UsuarioIngreso.IdRol;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1026)) btnSavePeriodo.Enabled = false;
        }

        private void LlenaCombo()
        {
            try
            {
                ClPeriodos clPeriodos = new ClPeriodos();
                ClAñoLectivo clAñoLectivo = new ClAñoLectivo();
                cbxPeriodo.lkeDatos.Properties.DataSource = clPeriodos.GetPeriodos();
                cbxPeriodo.lkeDatos.Properties.ValueMember = "IdPeriodo";
                cbxPeriodo.lkeDatos.Properties.DisplayMember = "Nombre";

                cbxAñoActual.lkeDatos.Properties.DataSource = clAñoLectivo.GetAñosLectivos();
                cbxAñoActual.lkeDatos.Properties.ValueMember = "Sec";
                cbxAñoActual.lkeDatos.Properties.DisplayMember = "Año";

                cbxReporteNotas.lkeDatos.Properties.DataSource = GetEnableDisabled();
                cbxReporteNotas.lkeDatos.Properties.ValueMember = "Value";
                cbxReporteNotas.lkeDatos.Properties.DisplayMember = "Opciones";
                cbxReporteNotas.lkeDatos.Properties.PopulateColumns();
                cbxReporteNotas.lkeDatos.Properties.Columns["Value"].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        protected DataTable GetEnableDisabled()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Value", typeof(bool));
            table.Columns.Add("Opciones", typeof(string));

            table.Rows.Add(1, "Habilitado");
            table.Rows.Add(0, "InHabilitado");

            return table;
        }

        private bool ValidaCampos()
        {
            try
            {
                if (txtNit.ValorTextBox == "" && !txtNit.Enabled)
                {
                    ClFunciones.msgError("El Nit esta vacío o no se ha habilitado su edición.");
                    txtNit.Focus();
                    return false;
                }
                else if (txtNomComp.ValorTextBox == "" && !txtNomComp.Enabled)
                {
                    ClFunciones.msgError("El nombre completo esta vacío o no se ha habilitado su edición.");
                    txtNomComp.Focus();
                    return false;
                }
                else if (txtNomCorto.ValorTextBox == "" && !txtNomCorto.Enabled)
                {
                    ClFunciones.msgError("El nombre corto esta vacío o no se ha habilitado su edición.");
                    txtNomCorto.Focus();
                    return false;
                }
                else if (txtCodDANA.ValorTextBox == "" && !txtCodDANA.Enabled)
                {
                    ClFunciones.msgError("El código DANA esta vacío o no se ha habilitado su edición.");
                    txtCodDANA.Focus();
                    return false;
                }
                else if (txtDocRector.ValorTextBox == "" && !txtDocRector.Enabled)
                {
                    ClFunciones.msgError("El documento del rector esta vacío o no se ha habilitado su edición.");
                    txtDocRector.Focus();
                    return false;
                }
                else if (txtNomRector.ValorTextBox == "" && !txtNomRector.Enabled)
                {
                    ClFunciones.msgError("El nombre del rector esta vacío o no se ha habilitado su edición.");
                    txtNomRector.Focus();
                    return false;
                }
                else if (cbxAñoActual.lkeDatos.EditValue == null && !cbxAñoActual.Enabled)
                {
                    ClFunciones.msgError("El año lectivo esta vacío o no se ha habilitado su edición.");
                    cbxAñoActual.Focus();
                    return false;
                }
                else if (cbxPeriodo.lkeDatos.EditValue == null && !cbxPeriodo.Enabled)
                {
                    ClFunciones.msgError("El periodo esta vacío o no se ha habilitado su edición.");
                    cbxPeriodo.Focus();
                    return false;
                }
                else if (cbxReporteNotas.lkeDatos.EditValue == null && !cbxReporteNotas.Enabled)
                {
                    ClFunciones.msgError("La opcion reporte de notas esta vacío o no se ha habilitado su edición.");
                    cbxPeriodo.Focus();
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void btnGuardarTodo_Click(object sender, EventArgs e)
        {
            if (!ValidaCampos()) return;
            string sql = "";
            if (SecDato>-1)
            {
                sql = string.Format("UPDATE [dbo].[ADDatosInstitucion] " +
                " SET [Nit] = '{0}', [NombreCompleto] = '{1}'" +
                " ,[NombreCorto] = '{2}'" +
                " ,[NombreRector] = '{3}'" +
                " ,[DocRector] = {4}" +
                " ,[PeriodoActual] = {5}" +
                " ,[IdAñoActual] = {6}" +
                " ,[ReportarNotas] = '{7}'" +
                " WHERE Sec = {8}", 
                txtNit.ValorTextBox,
                txtNomComp.ValorTextBox, 
                txtNomCorto.ValorTextBox,
                txtNomRector.ValorTextBox,
                txtDocRector.ValorTextBox,
                cbxPeriodo.lkeDatos.EditValue,
                cbxAñoActual.lkeDatos.EditValue, 
                cbxReporteNotas.lkeDatos.EditValue,
                SecDato);
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion)=="si")
                {
                    ClFunciones.msgExitoso("Información insertada correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                }
            }else
            {
                int Sec = ClFunciones.TraeSiguienteSecuencial("ADDatosInstitucion", "", ClConexion.clConexion.Conexion);
                if (Sec > 0)
                {
                    sql = string.Format("INSERT INTO [dbo].[ADDatosInstitucion] " +
                    " ([Sec] " + 
                    " ,[Nit] " +
                    " ,[NombreCompleto] " +
                    " ,[NombreCorto] " +
                    " ,[NombreRector] " +
                    " ,[DocRector] " +
                    " ,[PeriodoActual] " +
                    " ,[IdAñoActual]" +
                    ",[ReportarNotas]) " +
                    " VALUES " +
                    " ({0}> " +
                    " ,'{1}'> " +
                    " ,'{2}'" +
                    " ,'{3}' " +
                    " ,{4} " +
                    " ,{5} " +
                    " ,{6}",
                    " ,{7}" +
                    " ,'{8}')", 
                    Sec, 
                    txtNit.ValorTextBox,
                    txtNomComp.ValorTextBox, 
                    txtNomCorto.ValorTextBox,
                    txtNomRector.ValorTextBox,
                    txtDocRector.ValorTextBox,
                    cbxPeriodo.lkeDatos.EditValue,
                    cbxAñoActual.lkeDatos.EditValue,
                    cbxReporteNotas.lkeDatos.EditValue);
                    if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                    {
                        ClFunciones.msgExitoso("Información actualizada correctamente.");
                    }
                }
            }
        }

        private void FrmConfGeneral_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClFunciones.wADDatosInstitucion = clADDatosInstitucion.GetDatosInstitucion();
        }

        private void btnEditCodDANA_Click(object sender, EventArgs e)
        {
            txtCodDANA.Enabled = true;
        }

        private void btnSaveCodDANA_Click(object sender, EventArgs e)
        {
            if (txtCodDANA.ValorTextBox != "" && txtCodDANA.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET CodDANA = '" + txtCodDANA.ValorTextBox + "' WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Código DANA actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    txtCodDANA.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }

        private void btnEditNomRec_Click(object sender, EventArgs e)
        {
            txtNomRector.Enabled = true;
        }

        private void btnEditDocRec_Click(object sender, EventArgs e)
        {
            txtDocRector.Enabled = true;
        }

        private void btnSaveNomRec_Click(object sender, EventArgs e)
        {
            if (txtNomRector.ValorTextBox != "" && txtNomRector.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET NombreRector = '" + txtNomRector.ValorTextBox + "' WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Nombre de rector actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    txtNomRector.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }

        private void btnSaveDocRec_Click(object sender, EventArgs e)
        {
            if (txtDocRector.ValorTextBox != "" && txtDocRector.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET DocRector = '" + txtDocRector.ValorTextBox + "' WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Nombre de rector actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    txtDocRector.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }

        private void btnEditAño_Click(object sender, EventArgs e)
        {
            cbxAñoActual.Enabled = true;
        }

        private void btnSaveAño_Click(object sender, EventArgs e)
        {
            if (cbxAñoActual.lkeDatos.EditValue != null && cbxAñoActual.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET IdAñoActual = " + cbxAñoActual.lkeDatos.EditValue.ToString() + " WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Año actualizado correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    cbxAñoActual.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }

        private void btnEditRepNotas_Click(object sender, EventArgs e)
        {
            cbxReporteNotas.Enabled = true;
        }

        private void btnSaveRepNotas_Click(object sender, EventArgs e)
        {
            if (cbxReporteNotas.lkeDatos.EditValue != null && cbxReporteNotas.Enabled)
            {
                string sql = "UPDATE ADDatosInstitucion SET ReportarNotas = '" + cbxReporteNotas.lkeDatos.EditValue.ToString() + "' WHERE Sec = " + SecDato.ToString();
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "si")
                {
                    ClFunciones.msgExitoso("Opcion actualizada correctamente.");
                    ClFunciones.formPrinci.MdiChildren.ToList().ForEach(x => x.Close());
                    cbxReporteNotas.Enabled = false;
                }
                else
                    ClFunciones.msgError("Ha ocurrido un error.");
            }
        }
    }
}