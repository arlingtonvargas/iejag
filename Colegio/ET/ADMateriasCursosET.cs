﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADMateriasCursosET
    {
        public int Sec { get; set; }
        public int IdCurso { get; set; }
        public int IdMateria { get; set; }
        public long IdMaestro { get; set; }
        public string Desempeños { get; set; }
        public Single IH { get; set; }
    }
}

