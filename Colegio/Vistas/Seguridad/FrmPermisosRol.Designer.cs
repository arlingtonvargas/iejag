﻿namespace Colegio.Vistas.Seguridad
{
    partial class FrmPermisosRol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPermisosRol));
            this.grcOpciones = new DevExpress.XtraEditors.GroupControl();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.lbcPermisosRol = new DevExpress.XtraEditors.ListBoxControl();
            this.lbcPermisos = new DevExpress.XtraEditors.ListBoxControl();
            this.btnQuitar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.lblRol = new System.Windows.Forms.Label();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.cbxRoles = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.grcOpciones)).BeginInit();
            this.grcOpciones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lbcPermisosRol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbcPermisos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxRoles.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // grcOpciones
            // 
            this.grcOpciones.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.grcOpciones.AppearanceCaption.Options.UseFont = true;
            this.grcOpciones.Controls.Add(this.btnSalir);
            this.grcOpciones.Controls.Add(this.btnGuardar);
            this.grcOpciones.Location = new System.Drawing.Point(720, 42);
            this.grcOpciones.Name = "grcOpciones";
            this.grcOpciones.Size = new System.Drawing.Size(92, 546);
            this.grcOpciones.TabIndex = 9;
            this.grcOpciones.Text = "Opciones";
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(5, 69);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(80, 32);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(5, 31);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(80, 32);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lbcPermisosRol
            // 
            this.lbcPermisosRol.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.lbcPermisosRol.Appearance.Options.UseFont = true;
            this.lbcPermisosRol.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbcPermisosRol.Location = new System.Drawing.Point(384, 69);
            this.lbcPermisosRol.Name = "lbcPermisosRol";
            this.lbcPermisosRol.Size = new System.Drawing.Size(312, 462);
            this.lbcPermisosRol.TabIndex = 17;
            // 
            // lbcPermisos
            // 
            this.lbcPermisos.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.lbcPermisos.Appearance.Options.UseFont = true;
            this.lbcPermisos.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbcPermisos.Location = new System.Drawing.Point(7, 69);
            this.lbcPermisos.Name = "lbcPermisos";
            this.lbcPermisos.Size = new System.Drawing.Size(317, 462);
            this.lbcPermisos.TabIndex = 16;
            // 
            // btnQuitar
            // 
            this.btnQuitar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitar.Appearance.Options.UseFont = true;
            this.btnQuitar.Location = new System.Drawing.Point(330, 294);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(48, 32);
            this.btnQuitar.TabIndex = 14;
            this.btnQuitar.Text = "<<";
            this.btnQuitar.Click += new System.EventHandler(this.btnQuitar_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Appearance.Options.UseFont = true;
            this.btnAdd.Location = new System.Drawing.Point(330, 256);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(48, 32);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = ">>";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblRol
            // 
            this.lblRol.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.lblRol.Location = new System.Drawing.Point(5, 34);
            this.lblRol.Name = "lblRol";
            this.lblRol.Size = new System.Drawing.Size(122, 26);
            this.lblRol.TabIndex = 13;
            this.lblRol.Text = "Seleccione el rol :";
            this.lblRol.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.cbxRoles);
            this.groupControl1.Controls.Add(this.lbcPermisosRol);
            this.groupControl1.Controls.Add(this.lblRol);
            this.groupControl1.Controls.Add(this.lbcPermisos);
            this.groupControl1.Controls.Add(this.btnQuitar);
            this.groupControl1.Controls.Add(this.btnAdd);
            this.groupControl1.Location = new System.Drawing.Point(12, 42);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(702, 546);
            this.groupControl1.TabIndex = 10;
            this.groupControl1.Text = "Asignación de permisos por rol";
            // 
            // cbxRoles
            // 
            this.cbxRoles.Location = new System.Drawing.Point(133, 36);
            this.cbxRoles.Name = "cbxRoles";
            this.cbxRoles.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.cbxRoles.Properties.Appearance.Options.UseFont = true;
            this.cbxRoles.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxRoles.Properties.NullText = "Seleccione";
            this.cbxRoles.Size = new System.Drawing.Size(563, 24);
            this.cbxRoles.TabIndex = 18;
            this.cbxRoles.EditValueChanged += new System.EventHandler(this.cbxRoles_EditValueChanged);
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(0, 1);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitulo.Size = new System.Drawing.Size(835, 32);
            this.lblTitulo.TabIndex = 18;
            this.lblTitulo.Text = "Permisos por rol";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(833, 599);
            this.panel1.TabIndex = 19;
            // 
            // FrmPermisosRol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 600);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.grcOpciones);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(835, 600);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(835, 600);
            this.Name = "FrmPermisosRol";
            this.Text = "Permisos por rol";
            this.Load += new System.EventHandler(this.FrmPermisosRol_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grcOpciones)).EndInit();
            this.grcOpciones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lbcPermisosRol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbcPermisos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbxRoles.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblRol;
        private DevExpress.XtraEditors.GroupControl grcOpciones;
        private DevExpress.XtraEditors.SimpleButton btnQuitar;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.ListBoxControl lbcPermisos;
        private DevExpress.XtraEditors.ListBoxControl lbcPermisosRol;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LookUpEdit cbxRoles;
        public System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panel1;
    }
}