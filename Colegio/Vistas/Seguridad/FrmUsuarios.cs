﻿using Colegio.Clases;
using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio.Vistas.Seguridad
{
    public partial class FrmUsuarios : FrmBase
    {

        ClUsuarios clUsuarios = new ClUsuarios();
        public FrmUsuarios()
        {
            InitializeComponent();
        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            CrearGrilla();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarGrilla();
        }

        private void LlenarGrilla()
        {
            try
            {
                DataTable dt = clUsuarios.GetUsuarios();
                if (dt.Rows.Count>0)
                {
                    gcUsuarios.DataSource = dt;
                }
                else
                {
                    FrmMensaje frm = new FrmMensaje();
                    frm.lblMensaje.Text = "No se encontraron registros.";
                    frm.ShowDialog();
                }

            }
            catch (Exception ex)
            {
            }
        }

        private void CrearGrilla()
        {
            try
            {
                gvUsuarios = GrillaDevExpress.CrearGrilla(false, true);
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("IdUsuario", "Id"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("NomUsu", "Nombre de usuario"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("Documento", "Documento", DevExpress.Utils.FormatType.Numeric, "N0"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("NombreComp", "Nombre completo"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("IdUsuCra", "", visible: false));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("FechaUltIngreso", "Ult Ingreso", DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy hh:mm tt"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("NomUsuCrea", "Creador"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("FechaCrea", "Creado", DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy hh:mm tt"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("IdUsuMod", "", visible: false));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("NomUsuMod", "Modificado por"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("FechaMod", "Modificado", DevExpress.Utils.FormatType.DateTime, "dd/MM/yyyy hh:mm tt"));
                gvUsuarios.Columns.Add(GrillaDevExpress.CrearColumna("Anulado", "Anulado"));                
                gvUsuarios.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvUsuarios.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvUsuarios.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvUsuarios.OptionsCustomization.AllowColumnResizing = true;
                gcUsuarios.MainView = gvUsuarios;
                this.gvUsuarios.DoubleClick += new System.EventHandler(this.gvUsuarios_DoubleClick);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void gvUsuarios_DoubleClick(object sender, EventArgs e)
        {
        }
    }
}
