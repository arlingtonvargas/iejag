USE [BDIEJAG]
GO
/****** Object:  StoredProcedure [dbo].[SP_EstudianteMatriculado_G]    Script Date: 08/12/2018 9:27:35 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 11-11-2018
-- Description:	Consulta si el estudiante esta en una matricula activa
-- =============================================
-- Author:		Arlington
-- Create date: 08-12-2018
-- Description:	Agrega validación de Cancelada
-- =============================================
ALTER PROCEDURE [dbo].[SP_EstudianteMatriculado_G]
	@IdEstudiante BIGINT,
	@IdAñoLectivo INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SET DATEFORMAT DMY;

	SELECT MT.* FROM ADMatriculas MT WHERE  MT.IdEstudiante = @IdEstudiante AND MT.IdAñoLectivo = @IdAñoLectivo AND ISNULL(MT.Cancelada,0) = 0
	UNION
	SELECT MT.* FROM ADMatriculas MT 
	INNER JOIN ADAñoLectivo AL ON MT.IdAñoLectivo = AL.Sec
	WHERE  MT.IdEstudiante = @IdEstudiante AND FechaHasta > GETDATE() AND ISNULL(MT.Cancelada,0) = 0
END
