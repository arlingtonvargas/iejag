﻿using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio.Vistas
{
    public partial class FrmEstudiantesLocal : FrmBase
    {
        public FrmEstudiantesLocal()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsPatent = new DataSet();
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "xlsx Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string filename = openFileDialog1.FileName;
                    DataTable dt = ExcelDataBaseHelper.OpenFile(filename, "Estudiantes");
                    if (dt == null)
                    {
                        return;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            if (item["Documento"].ToString() == "")
                            {
                                LlenaGrilla();
                                MessageBox.Show("Estudiantes cargados de manera correcta.");
                                return;
                            }
                            string sql = string.Format("INSERT INTO [local].[Estudiantes] ([Documento] ,[Sede] ,[Jornada] ,[Grupo] ,[PosicionGrupo] " +
                            " ,[NomCompleto] ,[Codigo]) VALUES ({0},'{1}','{2}','{3}','{4}','{5}','{6}')", item["Documento"], item["Sede"],
                            item["Jornada"], item["Grupo"], item["PosicionGrupo"], item["Nombre"], item["Codigo"]);
                            string res = Clases.ClFunciones.EjecutaComando(sql, Clases.ClConexion.clConexion.Conexion);
                            if (res != "si")
                            {
                                MessageBox.Show("Ha ocurrido un error.");
                            }
                        }
                        MessageBox.Show("Estudiantes cargados de manera correcta.");
                        LlenaGrilla();
                    }
                }
            }
            catch (Exception ex)
            {

            }            
        }


        private void CrearGrilla()
        {
            try
            {
                gvEstudiantes = GrillaDevExpress.CrearGrilla(false, true);
               
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Documento", "Documento"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Codigo", "Codigo"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("NomCompleto", "Nombre completo"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Sede", "Sede"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Jornada", "Jornada"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Grupo", "Grupo"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("PosicionGrupo", "Posicion"));
                gvEstudiantes.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvEstudiantes.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvEstudiantes.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvEstudiantes.OptionsCustomization.AllowColumnResizing = true;
                gcEstudiantes.MainView = gvEstudiantes;
                this.gvEstudiantes.DoubleClick += new System.EventHandler(this.gvEstudiantes_DoubleClick);                

    }
            catch (Exception)
            {

                throw;
            }
        }


        private void LlenaGrilla()
        {
            try
            {
                string sql = "SELECT Documento, Sede, Jornada, Grupo, PosicionGrupo, NomCompleto, Codigo FROM [local].[Estudiantes]";
                DataTable dt = Clases.ClFunciones.Consultar(sql, Clases.ClConexion.clConexion.Conexion);
                gcEstudiantes.DataSource = dt;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void gvEstudiantes_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void FrmEstudiantesLocal_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenaGrilla();
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {

        }
    }

    public class ExcelDataBaseHelper
    {
        public static DataTable OpenFile(string fileName, string nomHoja)
        {
            try
            {
                string fullFileName = fileName;// string.Format("{0}\\{1}", Directory.GetCurrentDirectory(), fileName);
                if (!File.Exists(fullFileName))
                {
                    System.Windows.Forms.MessageBox.Show("No se encontro el archivo");
                    return null;
                }
                //var connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", fullFileName);
                var connectionString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + fullFileName + "; Extended Properties = Excel 12.0; ";
                var adapter = new OleDbDataAdapter( string.Format("SELECT * FROM [{0}$]",nomHoja), connectionString);
                var ds = new DataSet();
                string tableName = "excelData";
                adapter.Fill(ds, tableName);
                DataTable data = ds.Tables[tableName];
                return data;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
           
        }

    }
}
