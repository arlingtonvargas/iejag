﻿namespace Colegio.Vistas.Seguridad
{
    partial class FrmRoles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grcDatosRol = new DevExpress.XtraEditors.GroupControl();
            this.txtNomRol = new dll.Controles.ucLabelTextBox();
            this.ucBtnCancelar = new dll.Controles.ucBtnCancelarAV();
            this.ucBtnEliminar = new dll.Controles.ucBtnEliminarAV();
            this.ucBtnLimpiar = new dll.Controles.ucBtnLimpiarAV();
            this.ucBtnGuardar = new dll.Controles.ucBtnGuardarAV();
            this.gcRoles = new DevExpress.XtraGrid.GridControl();
            this.gvRoles = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMensaje = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.grcDatosRol)).BeginInit();
            this.grcDatosRol.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoles)).BeginInit();
            this.SuspendLayout();
            // 
            // grcDatosRol
            // 
            this.grcDatosRol.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.grcDatosRol.Appearance.Options.UseBackColor = true;
            this.grcDatosRol.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grcDatosRol.AppearanceCaption.Options.UseFont = true;
            this.grcDatosRol.Controls.Add(this.lblMensaje);
            this.grcDatosRol.Controls.Add(this.txtNomRol);
            this.grcDatosRol.Controls.Add(this.ucBtnCancelar);
            this.grcDatosRol.Controls.Add(this.ucBtnEliminar);
            this.grcDatosRol.Controls.Add(this.ucBtnLimpiar);
            this.grcDatosRol.Controls.Add(this.ucBtnGuardar);
            this.grcDatosRol.Location = new System.Drawing.Point(12, 42);
            this.grcDatosRol.Name = "grcDatosRol";
            this.grcDatosRol.Size = new System.Drawing.Size(798, 122);
            this.grcDatosRol.TabIndex = 0;
            this.grcDatosRol.Text = "Datos del rol";
            // 
            // txtNomRol
            // 
            this.txtNomRol.AnchoTitulo = 80;
            this.txtNomRol.Location = new System.Drawing.Point(5, 35);
            this.txtNomRol.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNomRol.MensajeDeAyuda = null;
            this.txtNomRol.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNomRol.Name = "txtNomRol";
            this.txtNomRol.PermiteSoloNumeros = false;
            this.txtNomRol.Size = new System.Drawing.Size(788, 24);
            this.txtNomRol.TabIndex = 6;
            this.txtNomRol.TextoTitulo = "Nombre :";
            this.txtNomRol.ValorTextBox = "";
            // 
            // ucBtnCancelar
            // 
            this.ucBtnCancelar.Location = new System.Drawing.Point(693, 65);
            this.ucBtnCancelar.Name = "ucBtnCancelar";
            this.ucBtnCancelar.Size = new System.Drawing.Size(100, 45);
            this.ucBtnCancelar.TabIndex = 5;
            this.ucBtnCancelar.OnUserControlButtonClicked += new dll.Controles.ucBtnCancelarAV.ButtonClickedEventHandler(this.ucBtnCancelar_OnUserControlButtonClicked);
            // 
            // ucBtnEliminar
            // 
            this.ucBtnEliminar.Location = new System.Drawing.Point(481, 65);
            this.ucBtnEliminar.Name = "ucBtnEliminar";
            this.ucBtnEliminar.Size = new System.Drawing.Size(100, 45);
            this.ucBtnEliminar.TabIndex = 3;
            this.ucBtnEliminar.OnUserControlButtonClicked += new dll.Controles.ucBtnEliminarAV.ButtonClickedEventHandler(this.ucBtnEliminar_OnUserControlButtonClicked);
            // 
            // ucBtnLimpiar
            // 
            this.ucBtnLimpiar.Location = new System.Drawing.Point(587, 65);
            this.ucBtnLimpiar.Name = "ucBtnLimpiar";
            this.ucBtnLimpiar.Size = new System.Drawing.Size(100, 45);
            this.ucBtnLimpiar.TabIndex = 4;
            this.ucBtnLimpiar.OnUserControlButtonClicked += new dll.Controles.ucBtnLimpiarAV.ButtonClickedEventHandler(this.ucBtnLimpiar_OnUserControlButtonClicked);
            // 
            // ucBtnGuardar
            // 
            this.ucBtnGuardar.Location = new System.Drawing.Point(375, 65);
            this.ucBtnGuardar.Name = "ucBtnGuardar";
            this.ucBtnGuardar.Size = new System.Drawing.Size(100, 45);
            this.ucBtnGuardar.TabIndex = 2;
            this.ucBtnGuardar.OnUserControlButtonClicked += new dll.Controles.ucBtnGuardarAV.ButtonClickedEventHandler(this.ucBtnGuardar_OnUserControlButtonClicked);
            // 
            // gcRoles
            // 
            this.gcRoles.Location = new System.Drawing.Point(12, 170);
            this.gcRoles.MainView = this.gvRoles;
            this.gcRoles.Name = "gcRoles";
            this.gcRoles.Size = new System.Drawing.Size(798, 328);
            this.gcRoles.TabIndex = 1;
            this.gcRoles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRoles});
            // 
            // gvRoles
            // 
            this.gvRoles.GridControl = this.gcRoles;
            this.gvRoles.Name = "gvRoles";
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitulo.Size = new System.Drawing.Size(826, 32);
            this.lblTitulo.TabIndex = 19;
            this.lblTitulo.Text = "Roles";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(824, 508);
            this.panel1.TabIndex = 20;
            // 
            // lblMensaje
            // 
            this.lblMensaje.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblMensaje.Appearance.Options.UseFont = true;
            this.lblMensaje.Appearance.Options.UseForeColor = true;
            this.lblMensaje.Appearance.Options.UseTextOptions = true;
            this.lblMensaje.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lblMensaje.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMensaje.Location = new System.Drawing.Point(33, 65);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(336, 45);
            this.lblMensaje.TabIndex = 7;
            // 
            // FrmRoles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 510);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcRoles);
            this.Controls.Add(this.grcDatosRol);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmRoles";
            this.Text = "Roles";
            this.Load += new System.EventHandler(this.FrmRoles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grcDatosRol)).EndInit();
            this.grcDatosRol.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoles)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl grcDatosRol;
        private DevExpress.XtraGrid.GridControl gcRoles;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRoles;
        private dll.Controles.ucBtnGuardarAV ucBtnGuardar;
        private dll.Controles.ucBtnEliminarAV ucBtnEliminar;
        private dll.Controles.ucBtnLimpiarAV ucBtnLimpiar;
        private dll.Controles.ucBtnCancelarAV ucBtnCancelar;
        public System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel panel1;
        private dll.Controles.ucLabelTextBox txtNomRol;
        private DevExpress.XtraEditors.LabelControl lblMensaje;
    }
}