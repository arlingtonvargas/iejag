﻿using Colegio.Clases;
using Colegio.ET;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Colegio.Vistas.Reports
{
    public partial class FrmRptNotasByCurso : Form
    {
        DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnEditGenerar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();

        ClCursos clCursos = new ClCursos();
        ClAñoLectivo clAño = new ClAñoLectivo();
        public FrmRptNotasByCurso()
        {
            InitializeComponent();
        }

        private void FrmRptNotasByCurso_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenarCamposBusq();
            LlenarCbxPeriodos();
            txtAño.Focus();
            txtAño.Select();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtAño.ValorTextBox != "" && txtCurso.ValorTextBox != "" && lkePeriodo.lkeDatos.EditValue != null)
                {
                    int idAño = Convert.ToInt32(txtAño.ValorTextBox);
                    int idCurso = Convert.ToInt32(txtCurso.ValorTextBox);
                    int idPeriodo = Convert.ToInt32(lkePeriodo.lkeDatos.EditValue);
                    Consultar(idAño, idCurso, idPeriodo);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            GenerarBoletinesTodos();
        }
        private string Desempeño(double prom)
        {
            try
            {
                if (prom >= 0 && prom <= 2.9)
                {
                    return "Bajo";
                }
                else if (prom >= 3 && prom <= 3.9)
                {
                    return "Básico";

                }
                else if (prom >= 4 && prom <= 4.5)
                {

                    return "Alto";
                }
                else if (prom >= 4.6 && prom <= 5)
                {
                    return "Superior";

                }
                else
                {
                    return "";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "";

            }
        }


        private void CrearGrilla()
        {
            try
            {
                gvEstudiantes = GrillaDevExpress.CrearGrilla(false, true);
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Posicion", "Posición", ancho: 60));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Documento", "Documento", ancho: 100));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("NomCompleto", "Estudiante", ancho: 500));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Curso", "Curso", ancho: 100));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Sede", "Sede", ancho: 70));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Jornada", "Jornada", ancho: 80));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Promedio", "Prom", ancho: 50, tipo: DevExpress.Utils.FormatType.Numeric, formato: "N1"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("btnEditGenerar", "Generar", ancho: 120, soloLectura: true, permiteEditar: true, permiteFoco: true));
                gvEstudiantes.Columns["btnEditGenerar"].ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
                btnEditGenerar.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
                btnEditGenerar.Buttons[0].ImageOptions.Image = imageCollection1.Images[0];
                btnEditGenerar.Buttons[0].ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
                btnEditGenerar.Buttons[0].IsLeft = true;
                btnEditGenerar.Buttons[0].Width = 120;
                btnEditGenerar.Buttons[0].Caption = "Boletin";
                btnEditGenerar.Click += new System.EventHandler(this.btnEditGenerar_Click);
                gvEstudiantes.Columns["btnEditGenerar"].ColumnEdit = btnEditGenerar;
                gvEstudiantes.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvEstudiantes.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvEstudiantes.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvEstudiantes.OptionsCustomization.AllowColumnResizing = true;
                gcEstudiantes.MainView = gvEstudiantes;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarCombo()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarCamposBusq()
        {
            try
            {
                //string sql = "SELECT Sec, Año FROM ADAñoLectivo";
                DataTable dt = clAño.GetAñosLectivos(); //Clases.ClFunciones.Consultar(sql, Clases.ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.DataTable = dt;
                    txtAño.ValorTextBox = ClFunciones.wADDatosInstitucion[0].IdAñoActual.ToString();

                    txtCurso.DataTable = new DataTable();
                    if (txtAño.ValorTextBox != "")
                    {
                        dt = clCursos.GetCursosByAño(Convert.ToInt32(txtAño.ValorTextBox));
                        if (dt.Rows.Count > 0)
                        {
                            dt.Columns[0].ColumnName = "Codigo";
                            dt.Columns[1].ColumnName = "Descripcion";
                        }
                        txtCurso.DataTable = dt;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarCbxPeriodos()
        {
            try
            {
                Clases.ClPeriodos cl = new Clases.ClPeriodos();
                lkePeriodo.lkeDatos.Properties.DataSource = cl.GetPeriodos();
                lkePeriodo.lkeDatos.Properties.ValueMember = "IdPeriodo";
                lkePeriodo.lkeDatos.Properties.DisplayMember = "Nombre";
                lkePeriodo.lkeDatos.Properties.PopulateColumns();
                lkePeriodo.lkeDatos.Properties.Columns[0].Visible = false;
                lkePeriodo.lkeDatos.EditValue = ClFunciones.wADDatosInstitucion[0].PeriodoActual;
                lkePeriodo.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Consultar(int idAño, int idCurso, int idPeriodo)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList();
                listaParam.Add(new ParametersSpET() { NombreParametro = "@pIdAñoLectivo", Valor = idAño });
                listaParam.Add(new ParametersSpET() { NombreParametro = "@pIdCurso", Valor = idCurso });
                listaParam.Add(new ParametersSpET() { NombreParametro = "@pIdPeriodo", Valor = idPeriodo });
                //List<RPTEstudiantesCursoET> list = Clases.ClFunciones.DataReaderMapToListConSP<RPTEstudiantesCursoET>("SP_EstudiantesCurso_G", listaParam);
                DataTable dt = Clases.ClFunciones.ConsultarConSP("SP_EstudiantesCurso_G", listaParam, Clases.ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    //rPTEstudiantesCursoBindingSource.DataSource = list;
                    gcEstudiantes.DataSource = dt;
                }
                else
                {
                    Clases.ClFunciones.msgError("No se encontraron registros.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    
        private void GenerarBoletinesTodos()
        {
            try
            {                
                if (gcEstudiantes.MainView.RowCount > 0)
                {
                    DataTable dt = (DataTable)gcEstudiantes.DataSource; //ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    string sql = "";
                    string SPromPeri = "Sum(P1)/";
                    string Sprom = "(ISNULL(NT.p1,0)) AS Prom";
                    int idPeriodo = (int)lkePeriodo.lkeDatos.EditValue;
                    int wIdAño = Convert.ToInt32(txtAño.ValorTextBox);
                    long pIdEstudiante = Convert.ToInt64(dt.Rows[0]["Documento"]);

                    sql = CreateScriptNotas(Sprom, idPeriodo, wIdAño, pIdEstudiante, ref SPromPeri);                    

                    DataTable dtMat = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    object prom = dtMat.Compute(SPromPeri + dtMat.Rows.Count, "");
                    dtMat = dtMat.AsEnumerable()
                                   .OrderBy(r => r.Field<string>("NomMateria"))
                                   .CopyToDataTable();
                    Reportes.RPTBoletin RPT = new Reportes.RPTBoletin();
                    RPT.DetailReport.DataSource = dtMat;
                    RPT.lblAsignatura.DataBindings.Add("Text", null, "NomMateria", "     {0}");
                    RPT.lblNomDocente.DataBindings.Add("Text", null, "Docente", "Docente: {0}");
                    RPT.lblIH.DataBindings.Add("Text", null, "IH");
                    RPT.lblAUS.DataBindings.Add("Text", null, "AUS");
                    RPT.lblP1.DataBindings.Add("Text", null, "P1", "{0:N1}");
                    RPT.lblP2.DataBindings.Add("Text", null, "P2", "{0:N1}");
                    RPT.lblP3.DataBindings.Add("Text", null, "P3", "{0:N1}");
                    RPT.lblP4.DataBindings.Add("Text", null, "P4", "{0:N1}");
                    RPT.lblProm.DataBindings.Add("Text", null, "Prom", "{0:N1}");
                    RPT.lblDesempeños.DataBindings.Add("Text", null, "Desempeños");

                    RPT.lblTitulo.Text = "INFORME ACADEMICO " + txtAño.lblDescripcion.Text;
                    RPT.lblSede.Text = dt.Rows[0]["Sede"].ToString();
                    RPT.lblJornada.Text = dt.Rows[0]["Jornada"].ToString();
                    RPT.lblPosGrupo.Text = dt.Rows[0]["Posicion"].ToString(); ;
                    RPT.lblGrupo.Text = dt.Rows[0]["Curso"].ToString();
                    RPT.lblPeriodo.Text = idPeriodo.ToString();
                    RPT.lblDocumento.Text = dt.Rows[0]["Documento"].ToString();
                    RPT.lblNomEst.Text = dt.Rows[0]["NomCompleto"].ToString();
                    RPT.lblCodigo.Text = dt.Rows[0]["Documento"].ToString();
                    RPT.lblNivelDesemp.Text = Desempeño(Convert.ToDouble(prom));
                    RPT.lblPromGen.Text = Convert.ToDouble(prom).ToString("N1");
                    RPT.lblTitularGrupo.Text = dtMat.Rows[0]["Titular"].ToString();
                    RPT.CreateDocument();
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        DataRow item = dt.Rows[i];
                        pIdEstudiante = Convert.ToInt64(item["Documento"]);

                        sql = CreateScriptNotas(Sprom, idPeriodo, wIdAño, pIdEstudiante, ref SPromPeri);
                        
                        dtMat = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);

                        dtMat = dtMat.AsEnumerable()
                                    .OrderBy(r => r.Field<string>("NomMateria"))
                                    .CopyToDataTable();

                        if (dtMat.Rows.Count > 0)
                        {
                            Reportes.RPTBoletin RPT2 = new Reportes.RPTBoletin();
                            prom = dtMat.Compute(SPromPeri + dtMat.Rows.Count, "");
                            RPT2.DetailReport.DataSource = dtMat;
                            RPT2.lblAsignatura.DataBindings.Add("Text", null, "NomMateria", "     {0}");
                            RPT2.lblNomDocente.DataBindings.Add("Text", null, "Docente", "Docente: {0}");
                            RPT2.lblIH.DataBindings.Add("Text", null, "IH");
                            RPT2.lblAUS.DataBindings.Add("Text", null, "AUS");
                            RPT2.lblP1.DataBindings.Add("Text", null, "P1", "{0:N1}");
                            RPT2.lblP2.DataBindings.Add("Text", null, "P2", "{0:N1}");
                            RPT2.lblP3.DataBindings.Add("Text", null, "P3", "{0:N1}");
                            RPT2.lblP4.DataBindings.Add("Text", null, "P4", "{0:N1}");
                            RPT2.lblProm.DataBindings.Add("Text", null, "Prom", "{0:N1}");
                            RPT2.lblDesempeños.DataBindings.Add("Text", null, "Desempeños");

                            RPT2.lblTitulo.Text = "INFORME ACADEMICO " + txtAño.lblDescripcion.Text;
                            RPT2.lblSede.Text = item["Sede"].ToString();
                            RPT2.lblJornada.Text = item["Jornada"].ToString();
                            RPT2.lblPosGrupo.Text = item["Posicion"].ToString();
                            RPT2.lblGrupo.Text = item["Curso"].ToString();
                            RPT2.lblPeriodo.Text = idPeriodo.ToString();
                            RPT2.lblDocumento.Text = item["Documento"].ToString();
                            RPT2.lblNomEst.Text = item["NomCompleto"].ToString();
                            RPT2.lblCodigo.Text = item["Documento"].ToString();
                            RPT2.lblNivelDesemp.Text = Desempeño(Convert.ToDouble(prom));
                            RPT2.lblPromGen.Text = Convert.ToDouble(prom).ToString("N1");
                            RPT2.lblTitularGrupo.Text = dtMat.Rows[0]["Titular"].ToString();
                            RPT2.CreateDocument();

                            RPT.Pages.AddRange(RPT2.Pages);

                            RPT.PrintingSystem.ContinuousPageNumbering = true;
                        }
                    }

                    FrmVistaPrevia report = new FrmVistaPrevia();
                    report.document.DocumentSource = RPT;
                    report.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void GenerarBoletineEstudiente(int numFila)
        {

            try
            {
                DataTable dt = (DataTable)gcEstudiantes.DataSource; //ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                string sql = "";
                if (dt.Rows.Count > 0)
                {
                    string SPromPeri = "Sum(P1)/";
                    string Sprom = "(ISNULL(NT.p1,0)) AS Prom";
                    int idPeriodo = (int)lkePeriodo.lkeDatos.EditValue;
                    int wIdAño = Convert.ToInt32(txtAño.ValorTextBox);
                    long pIdEstudiante = Convert.ToInt64(gvEstudiantes.GetRowCellValue(numFila, "Documento"));           

                    sql = CreateScriptNotas(Sprom, idPeriodo, wIdAño, pIdEstudiante, ref SPromPeri);

                    DataTable dtMat = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    object prom = dtMat.Compute(SPromPeri + dtMat.Rows.Count, "");
                    Reportes.RPTBoletin RPT = new Reportes.RPTBoletin();
                    RPT.DetailReport.DataSource = dtMat;
                    RPT.lblAsignatura.DataBindings.Add("Text", null, "NomMateria", "     {0}");
                    RPT.lblNomDocente.DataBindings.Add("Text", null, "Docente", "Docente: {0}");
                    RPT.lblIH.DataBindings.Add("Text", null, "IH");
                    RPT.lblAUS.DataBindings.Add("Text", null, "AUS");
                    RPT.lblP1.DataBindings.Add("Text", null, "P1", "{0:N1}");
                    RPT.lblP2.DataBindings.Add("Text", null, "P2", "{0:N1}");
                    RPT.lblP3.DataBindings.Add("Text", null, "P3", "{0:N1}");
                    RPT.lblP4.DataBindings.Add("Text", null, "P4", "{0:N1}");
                    RPT.lblProm.DataBindings.Add("Text", null, "Prom", "{0:N1}");
                    RPT.lblDesempeños.DataBindings.Add("Text", null, "Desempeños");

                    RPT.lblTitulo.Text = "INFORME ACADEMICO " + txtAño.lblDescripcion.Text;
                    RPT.lblSede.Text = gvEstudiantes.GetRowCellValue(numFila, "Sede").ToString();
                    RPT.lblJornada.Text = gvEstudiantes.GetRowCellValue(numFila, "Jornada").ToString();
                    RPT.lblPosGrupo.Text = gvEstudiantes.GetRowCellValue(numFila, "Posicion").ToString(); ;
                    RPT.lblGrupo.Text = gvEstudiantes.GetRowCellValue(numFila, "Curso").ToString();
                    RPT.lblPeriodo.Text = idPeriodo.ToString();
                    RPT.lblDocumento.Text = gvEstudiantes.GetRowCellValue(numFila, "Documento").ToString();
                    RPT.lblNomEst.Text = gvEstudiantes.GetRowCellValue(numFila, "NomCompleto").ToString();
                    RPT.lblCodigo.Text = gvEstudiantes.GetRowCellValue(numFila, "Documento").ToString();
                    RPT.lblNivelDesemp.Text = Desempeño(Convert.ToDouble(prom));
                    RPT.lblPromGen.Text = Convert.ToDouble(prom).ToString("N1");
                    RPT.lblTitularGrupo.Text = dtMat.Rows[0]["Titular"].ToString();
                    FrmVistaPrevia report = new FrmVistaPrevia();
                    report.document.DocumentSource = RPT;
                    report.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private string CreateScriptNotas(string pSprom, int pIdPeriodo, int pIdAño, long pIdEstudiante, ref string SPromPeri)
        {
            try
            {

                switch (pIdPeriodo)
                {
                    case 2:
                        pSprom = "(ISNULL(ISNULL(NT.P1,0) + ISNULL(NT.P2,0),0))/2 AS Prom";
                        SPromPeri = "Sum(P2)/";
                        break;
                    case 3:
                        pSprom = "(ISNULL(NT.P1,0) + ISNULL(NT.P2,0) + ISNULL(NT.P3,0))/3 AS Prom";
                        SPromPeri = "Sum(P3)/";
                        break;
                    case 4:
                        pSprom = "(ISNULL(NT.P1,0) + ISNULL(NT.P2,0) + ISNULL(NT.P3,0)+ ISNULL(NT.P4,0))/4 AS Prom";
                        SPromPeri = "Sum(P4)/";
                        break;
                    default:
                        break;
                }


                string sql = string.Format("SELECT  MT.NomMateria, CONCAT(DC.Pnombre,' ',DC.Snombre,' ',DC.Papellido,' ',DC.Sapellido) AS Docente  " +
                    " ,MS.IH, IA.CantInasistencias AS AUS, NT.P1, NT.P2, NT.P3, NT.P4, {0}, MS.Desempeños" +
                    " , CONCAT(TI.Pnombre, ' ', TI.Snombre, ' ', TI.Papellido, ' ', TI.Sapellido) AS Titular " +
                    " FROM ADNotas NT " +
                    " INNER JOIN ADMateriasCursos MS ON NT.SecMatCurso = MS.Sec " +
                    " INNER JOIN ADCursos CS ON MS.IdCurso = CS.Sec " +
                    " INNER JOIN ADMaterias MT ON MS.IdMateria = MT.Sec " +
                    " INNER JOIN ADTerceros DC ON MS.IdMaestro = DC.IdTercero " +
                    " INNER JOIN ADTerceros TI ON CS.IdTitular = TI.IdTercero " +
                    " INNER JOIN ADMatriculas MC ON NT.IdMatricula = MC.Sec" +
                    " LEFT JOIN ADInasistencia IA ON NT.IdEstudiante = IA.IdEstudiante AND CS.Sec = IA.IdCurso AND MT.Sec = IA.IdMateria  AND IdPeriodo = {2} " +
                    " LEFT JOIN ADDesempeñosMateriaCurso DS ON MS.Sec = DS.SecMateriaCurso AND DS.SecPeriodo = {2} " +
                    " WHERE NT.IdEstudiante = {1} AND CS.IdAñoLectivo = {3} AND MC.Cancelada = 0", pSprom, pIdEstudiante, pIdPeriodo, pIdAño);

                return sql;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        private void txtAño_SaleControl(object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {

            }
        }

        private void btnEditGenerar_Click(object sender, EventArgs e)
        {
            GenerarBoletineEstudiente(gvEstudiantes.FocusedRowHandle);
        }

        private void FrmRptNotasByCurso_SizeChanged(object sender, EventArgs e)
        {
            if (gvEstudiantes.Columns.Count>0)
            {
                gvEstudiantes.Columns["Posicion"].Width = (5 * (groupControl1.Width - 20) / 100);
                gvEstudiantes.Columns["Documento"].Width = (10 * (groupControl1.Width - 20) / 100);
                gvEstudiantes.Columns["NomCompleto"].Width = (45 * (groupControl1.Width - 20) / 100);
                gvEstudiantes.Columns["Curso"].Width = (8 * (groupControl1.Width - 20) / 100);
                gvEstudiantes.Columns["Sede"].Width = (8 * (groupControl1.Width - 20) / 100);
                gvEstudiantes.Columns["Jornada"].Width = (8 * (groupControl1.Width - 20) / 100);
                gvEstudiantes.Columns["Promedio"].Width = (5 * (groupControl1.Width - 20) / 100);
                gvEstudiantes.Columns["btnEditGenerar"].Width = (7 * (groupControl1.Width - 18) / 100);
                // gvEstudiantes.Columns["btnEditGenerar"].ColumnEdit.BestFitWidth = (6 * (groupControl1.Width) / 100);
                btnEditGenerar.Buttons[0].Width = (7 * (groupControl1.Width - 20) / 100);
            }
        }
    }
}
