
GO
-- =============================================
-- Author:		Arlington
-- Create date: 16-06-2019
-- Description:	Lista los estudiantes matriculados por año lectivo
-- =============================================
CREATE PROCEDURE SP_EstudiantesMatriculadosByAño_G
	
		@IdAñoLectivo INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT M.IdTercero  ,CONCAT(RTRIM(LTRIM(M.Papellido)), ' ', RTRIM(LTRIM(M.Sapellido)), ' '
	, RTRIM(LTRIM(M.Pnombre)), ' ', RTRIM(LTRIM(M.Snombre))) AS NombreComp ,M.TipoDoc ,M.Pnombre 
	,M.Snombre ,M.Papellido ,M.Sapellido ,M.Correo ,M.Direccion ,M.Sexo ,M.FechaNacimiento 
	,M.LugarNaciento AS IdLugarNacimiento ,MN.NombreMunicipio AS NomLugarNacimiento ,M.Telefono 
	,M.FechaCrea ,M.IdUsuario ,U.NomUsu ,U.IdRol ,RL.NomRol ,DP.CodDpto ,M.TipoTercero ,M.Foto 
	,DP.Pais ,M.RH ,M.EPS 
	FROM ADTerceros M 
	INNER JOIN SGUsuarios U ON M.IdUsuario = U.IdUsuario 
	INNER JOIN SGRoles RL ON U.IdRol = RL.Sec 
	INNER JOIN ADMatriculas MT ON MT.IdEstudiante = M.IdTercero AND MT.IdAñoLectivo = @IdAñoLectivo
	LEFT JOIN GEMunicipio MN ON M.LugarNaciento = MN.IdMunicipio 
	LEFT JOIN GEDepartamento DP ON MN.Departamento = DP.CodDpto 
	WHERE M.TipoTercero = 2 ORDER BY NombreComp

END
GO
