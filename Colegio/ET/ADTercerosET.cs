﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADTercerosET
    {
        public long IdTercero { get; set; }
        public string NombreComp { get; set; }
        public string Pnombre { get; set; }
        public string Snombre { get; set; }
        public string Papellido { get; set; }
        public string Sapellido { get; set; }
        public string Direccion { get; set; }
        public string Correo { get; set; }
        public int Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string IdLugarNacimiento { get; set; }
        public string NomLugarNacimiento { get; set; }
        public double Telefono { get; set; }
        public DateTime FechaCrea { get; set; }
        public int IdUsuario { get; set; }
        public string NomUsu { get; set; }
        public int IdRol { get; set; }
        public string NomRol { get; set; }
        public string CodDpto { get; set; }
        public string Pais { get; set; }
        public int TipoDoc { get; set; }
        public int TipoTercero { get; set; }
        public Image Foto { get; set; }
        public string RH { get; set; }
        public string EPS { get; set; }
        
    }

    public class ADTercerosCarnetET : ADTercerosET
    {
        public string Sede { get; set; }
        public string Grado { get; set; }

    }


    public class ADTercerosCarnetETList : List<ADTercerosCarnetET>   { }

}
