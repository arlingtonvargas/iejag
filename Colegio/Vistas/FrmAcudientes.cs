﻿using Colegio.Clases;
using Colegio.ET;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio.Vistas
{
    public partial class FrmAcudientes : dll.Common.FrmBase
    {
        ADTercerosET wEstudianteActual = new ADTercerosET();
        ADAcudientesEstudianteET wADAcudienteActual = new ADAcudientesEstudianteET();
        ClADAcudientesEstudiante clADAcudientesEstudiante = new ClADAcudientesEstudiante();
        ClADParentesco clADParentesco = new ClADParentesco();
        ClTercero clTerceros = new ClTercero();
        bool EstaActualizando = false;
        public FrmAcudientes(ADTercerosET Estudiate)
        {
            InitializeComponent();
            this.wEstudianteActual = Estudiate;
        }

        private void FrmAcudientes1_Load(object sender, EventArgs e)
        {
            LlenaTxtBusqueda();
            CrearGrilla();
            LlenaGrilla();
            txtParentesco.Focus();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!ValidaCampos()) return;
            wADAcudienteActual = new ADAcudientesEstudianteET();
            wADAcudienteActual.Apellidos = txtApellidos.ValorTextBox;
            wADAcudienteActual.Direccion = txtDireccion.ValorTextBox;
            wADAcudienteActual.Email = txtCorreo.ValorTextBox;
            wADAcudienteActual.IdAcudiente = Convert.ToInt64(txtDocumento.ValorTextBox);
            wADAcudienteActual.IdEstudiante = wEstudianteActual.IdTercero;
            wADAcudienteActual.Nombres = txtNombres.ValorTextBox;
            wADAcudienteActual.Parentesco = Convert.ToInt32(txtParentesco.ValorTextBox);
            wADAcudienteActual.Telefono = Convert.ToInt64(txtTelefono.ValorTextBox);
            GuardarAcudiente(wADAcudienteActual);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (EstaActualizando)
            {
                if (ClFunciones.msgResult("Esta seguro que desea eliminar el acudiente seleccionado.",MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (clADAcudientesEstudiante.DeleteAcudientesEstudiante(wADAcudienteActual))
                    {
                        ClFunciones.msgExitoso("Acudiente eliminado correctamente.");
                        LimpiarCampos();
                        LlenaGrilla();
                    }
                    else
                    {
                        ClFunciones.msgError("Ha ocurrido un error.");
                    }
                }
            }
        }

        private void LlenaTxtBusqueda()
        {
            try
            {
                DataTable dt = clTerceros.GetTercerosTipoTer(2);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                txtEstudiante.DataTable = dt;
                txtEstudiante.ValorTextBox = wEstudianteActual.IdTercero.ToString();
                txtEstudiante.Enabled = false;

                dt = clADParentesco.GetParentescos();
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                txtParentesco.DataTable = dt;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CrearGrilla()
        {
            try
            {
                gvAcudientes = GrillaDevExpress.CrearGrilla(false, true);
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("IdEstudiante", "Id", visible: false));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("NomParentesco", "Parentesco"));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("IdAcudiente", "Documento"));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("Parentesco", "", visible: false));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("Nombres", "Nombres"));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("Apellidos", "Apellidos"));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("Telefono", "Teléfono"));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("Direccion", "Dirección"));
                gvAcudientes.Columns.Add(GrillaDevExpress.CrearColumna("Email", "Email"));
                gvAcudientes.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvAcudientes.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvAcudientes.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvAcudientes.OptionsCustomization.AllowColumnResizing = true;
                gcAcudientes.MainView = gvAcudientes;
                this.gvAcudientes.DoubleClick += new System.EventHandler(this.gvMaterias_DoubleClick);

            }
            catch (Exception)
            {

            }
        }

        private void GuardarAcudiente(ADAcudientesEstudianteET Acudiente)
        {
            try
            {
                if (EstaActualizando)
                {
                    if (clADAcudientesEstudiante.UpdateAcudientesEstudiante(Acudiente) != null)
                    {
                        ClFunciones.msgExitoso("Acudiente Actualizado correctamente.");
                        LimpiarCampos();
                        LlenaGrilla();
                        return;
                    }
                }
                else
                {
                    if (clADAcudientesEstudiante.InsertAcudientesEstudiante(Acudiente) != null)
                    {
                        ClFunciones.msgExitoso("Acudiente creado correctamente.");
                        LimpiarCampos();
                        LlenaGrilla();
                        return;
                    }
                }
                ClFunciones.msgError("Lo sentimos, ha ocurrido un error.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool ValidaCampos()
        {
            try
            {
                if (txtParentesco.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el parentesco.");
                    txtParentesco.Focus();
                    return false;
                }
                else if (txtDocumento.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el documento.");
                    txtDocumento.Focus();
                    return false;
                }
                else if (txtNombres.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el nombre.");
                    txtNombres.Focus();
                    return false;
                }
                else if (txtTelefono.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el teléfono.");
                    txtTelefono.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void LlenaGrilla()
        {
            try
            {
                try
                {
                    List<ADAcudientesEstudianteET> listaAcudientes = clADAcudientesEstudiante.GetAcudientesEstudiante(wEstudianteActual.IdTercero);
                    if (listaAcudientes!=null && listaAcudientes.Count > 0)
                    {
                        gcAcudientes.DataSource = listaAcudientes;
                    }
                    else
                    {
                        //Clases.ClFunciones.msgError("No se encontraron registros.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LimpiarCampos()
        {
            txtParentesco.ValorTextBox = "";
            txtDocumento.ValorTextBox = "";
            txtNombres.ValorTextBox = "";
            txtApellidos.ValorTextBox = "";
            txtCorreo.ValorTextBox = "";
            txtTelefono.ValorTextBox = "";
            txtDireccion.ValorTextBox = "";
            wADAcudienteActual = new ADAcudientesEstudianteET();
            txtParentesco.Enabled = true;
            EstaActualizando = false;
            txtParentesco.Focus();
        }

        private void CargarAcudiente(int numFila)
        {
            try
            {
                wADAcudienteActual.Apellidos = (gvAcudientes.GetRowCellValue(numFila, "Apellidos")!=null) ? gvAcudientes.GetRowCellValue(numFila, "Apellidos").ToString():"";
                wADAcudienteActual.Direccion = (gvAcudientes.GetRowCellValue(numFila, "Direccion")!=null)?gvAcudientes.GetRowCellValue(numFila, "Direccion").ToString():"";
                wADAcudienteActual.Email = (gvAcudientes.GetRowCellValue(numFila, "Email") !=null)?gvAcudientes.GetRowCellValue(numFila, "Email").ToString():"";
                wADAcudienteActual.IdAcudiente = Convert.ToInt64(gvAcudientes.GetRowCellValue(numFila, "IdAcudiente"));
                wADAcudienteActual.IdEstudiante = Convert.ToInt64(gvAcudientes.GetRowCellValue(numFila, "IdEstudiante"));
                wADAcudienteActual.Nombres = gvAcudientes.GetRowCellValue(numFila, "Nombres").ToString();
                wADAcudienteActual.Parentesco = Convert.ToInt32(gvAcudientes.GetRowCellValue(numFila, "Parentesco"));
                wADAcudienteActual.Telefono = Convert.ToInt64(gvAcudientes.GetRowCellValue(numFila, "Telefono"));

                txtParentesco.ValorTextBox = wADAcudienteActual.Parentesco.ToString();
                txtDocumento.ValorTextBox = wADAcudienteActual.IdAcudiente.ToString();
                txtNombres.ValorTextBox = wADAcudienteActual.Nombres;
                txtApellidos.ValorTextBox = wADAcudienteActual.Apellidos;
                txtCorreo.ValorTextBox = wADAcudienteActual.Email;
                txtTelefono.ValorTextBox = wADAcudienteActual.Telefono.ToString();
                txtDireccion.ValorTextBox = wADAcudienteActual.Direccion;
                txtParentesco.Enabled = false;                
                EstaActualizando = true;
                txtDocumento.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gvMaterias_DoubleClick(object sender, EventArgs e)
        {
            int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
            if (numFila > -1)
            {
                CargarAcudiente(numFila);
            }
        }
    }
}
