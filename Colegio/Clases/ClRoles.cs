﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClRoles
    {

        public DataTable GetRoles()
        {
            try
            {
                string sql = "SELECT RL.Sec, RL.NomRol, RL.FechaCrea, RL.FechaMod, RL.Anulado, RL.UsuCrea " +
                            " FROM SGRoles RL" +
                            " WHERE ISNULL(RL.Anulado, 0) = 0";
                DataTable dt = ClFunciones.f.AbrirTabla(ClConexion.clConexion.Conexion, sql);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool InsertRol(string NomRol, int UsuCrea)
        {
            try
            {
                string sql = string.Format("INSERT INTO SGRoles (NomRol,FechaCrea, UsuCrea) VALUES ('{0}', '{1:dd/MM/yyyy HH:mm:ss}' ,{2})",
                                            NomRol, DateTime.Now, UsuCrea);
                ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateRol(int idRol, string NomRol)
        {
            try
            {
                string sql = string.Format("UPDATE SGRoles SET NomRol = '{0}' WHERE Sec = {1}", NomRol, idRol);
                ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteRol(int idRol)
        {
            try
            {
                string sql = string.Format("UPDATE SGRoles SET Anulado = 1 WHERE Sec = {0}",idRol);
                ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool EstaEnUsuario(int idRol)
        {
            try
            {
                string sql = "SELECT * FROM SGUsuariosRoles WHERE IdRol = " + idRol;
                DataTable dt = ClFunciones.f.AbrirTabla(ClConexion.clConexion.Conexion, sql);
                if (dt.Rows.Count>0)
                {
                    return true;
                }else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
