﻿namespace Colegio.Vistas
{
    partial class FrmAggMateriasCurso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAggMateriasCurso));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET3 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET4 = new dll.Common.ET.TituloColsBusquedaET();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.seIH = new DevExpress.XtraEditors.SpinEdit();
            this.txtCurso = new dll.Controles.ucBusqueda();
            this.txtDesempeños = new DevExpress.XtraEditors.MemoEdit();
            this.lblDesempeños = new DevExpress.XtraEditors.LabelControl();
            this.txtMaestro = new dll.Controles.ucBusqueda();
            this.txtMaterias = new dll.Controles.ucBusqueda();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.gcMaterias = new DevExpress.XtraGrid.GridControl();
            this.gvMaterias = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seIH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesempeños.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMaterias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaterias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(796, 152);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "Configuración de materias por curso";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCurso, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtDesempeños, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblDesempeños, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtMaestro, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtMaterias, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtAño, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 126);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel2.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.seIH, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(724, 30);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(68, 30);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(14, 24);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "IH";
            // 
            // seIH
            // 
            this.seIH.Dock = System.Windows.Forms.DockStyle.Fill;
            this.seIH.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.seIH.Location = new System.Drawing.Point(23, 3);
            this.seIH.Name = "seIH";
            this.seIH.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seIH.Properties.Appearance.Options.UseFont = true;
            this.seIH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.seIH.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.seIH.Size = new System.Drawing.Size(42, 22);
            this.seIH.TabIndex = 5;
            this.seIH.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.seIH_KeyPress);
            // 
            // txtCurso
            // 
            this.txtCurso.AnchoTextBox = 120;
            this.txtCurso.AnchoTitulo = 70;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCurso, 2);
            this.txtCurso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCurso.Location = new System.Drawing.Point(407, 0);
            this.txtCurso.Margin = new System.Windows.Forms.Padding(0);
            this.txtCurso.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCurso.MensajeDeAyuda = null;
            this.txtCurso.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.PermiteSoloNumeros = false;
            this.txtCurso.Script = "";
            this.txtCurso.Size = new System.Drawing.Size(385, 28);
            this.txtCurso.TabIndex = 2;
            this.txtCurso.TextoTitulo = "Curso :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtCurso.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtCurso.ValorTextBox = "";
            this.txtCurso.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtCurso_SaleControl);
            // 
            // txtDesempeños
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtDesempeños, 3);
            this.txtDesempeños.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDesempeños.Location = new System.Drawing.Point(93, 63);
            this.txtDesempeños.Margin = new System.Windows.Forms.Padding(3, 3, 4, 3);
            this.txtDesempeños.Name = "txtDesempeños";
            this.txtDesempeños.Size = new System.Drawing.Size(695, 60);
            this.txtDesempeños.TabIndex = 6;
            // 
            // lblDesempeños
            // 
            this.lblDesempeños.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesempeños.Appearance.Options.UseFont = true;
            this.lblDesempeños.Appearance.Options.UseTextOptions = true;
            this.lblDesempeños.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDesempeños.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDesempeños.Location = new System.Drawing.Point(3, 63);
            this.lblDesempeños.Name = "lblDesempeños";
            this.lblDesempeños.Size = new System.Drawing.Size(84, 20);
            this.lblDesempeños.TabIndex = 4;
            this.lblDesempeños.Text = "Desempeños :";
            // 
            // txtMaestro
            // 
            this.txtMaestro.AnchoTextBox = 120;
            this.txtMaestro.AnchoTitulo = 70;
            this.txtMaestro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMaestro.Location = new System.Drawing.Point(407, 30);
            this.txtMaestro.Margin = new System.Windows.Forms.Padding(0);
            this.txtMaestro.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMaestro.MensajeDeAyuda = null;
            this.txtMaestro.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMaestro.Name = "txtMaestro";
            this.txtMaestro.PermiteSoloNumeros = false;
            this.txtMaestro.Script = "";
            this.txtMaestro.Size = new System.Drawing.Size(317, 28);
            this.txtMaestro.TabIndex = 4;
            this.txtMaestro.TextoTitulo = "Maestro :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtMaestro.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtMaestro.ValorTextBox = "";
            // 
            // txtMaterias
            // 
            this.txtMaterias.AnchoTextBox = 50;
            this.txtMaterias.AnchoTitulo = 90;
            this.tableLayoutPanel1.SetColumnSpan(this.txtMaterias, 2);
            this.txtMaterias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMaterias.Location = new System.Drawing.Point(0, 30);
            this.txtMaterias.Margin = new System.Windows.Forms.Padding(0);
            this.txtMaterias.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMaterias.MensajeDeAyuda = null;
            this.txtMaterias.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMaterias.Name = "txtMaterias";
            this.txtMaterias.PermiteSoloNumeros = false;
            this.txtMaterias.Script = "";
            this.txtMaterias.Size = new System.Drawing.Size(407, 28);
            this.txtMaterias.TabIndex = 3;
            this.txtMaterias.TextoTitulo = "Materia :";
            tituloColsBusquedaET3.Codigo = "Código";
            tituloColsBusquedaET3.Descripcion = "Descripción";
            this.txtMaterias.TituloColsBusqueda = tituloColsBusquedaET3;
            this.txtMaterias.ValorTextBox = "";
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 50;
            this.txtAño.AnchoTitulo = 90;
            this.tableLayoutPanel1.SetColumnSpan(this.txtAño, 2);
            this.txtAño.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAño.Location = new System.Drawing.Point(0, 0);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(407, 28);
            this.txtAño.TabIndex = 1;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET4.Codigo = "Código";
            tituloColsBusquedaET4.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET4;
            this.txtAño.ValorTextBox = "";
            this.txtAño.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtAño_SaleControl);
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(6, 155);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(83, 37);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(6, 26);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(83, 37);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // gcMaterias
            // 
            this.gcMaterias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcMaterias.Location = new System.Drawing.Point(12, 170);
            this.gcMaterias.MainView = this.gvMaterias;
            this.gcMaterias.Margin = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.gcMaterias.Name = "gcMaterias";
            this.gcMaterias.Padding = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.gcMaterias.Size = new System.Drawing.Size(796, 309);
            this.gcMaterias.TabIndex = 13;
            this.gcMaterias.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaterias,
            this.gridView2});
            // 
            // gvMaterias
            // 
            this.gvMaterias.GridControl = this.gcMaterias;
            this.gvMaterias.Name = "gvMaterias";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcMaterias;
            this.gridView2.Name = "gridView2";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.groupControl2.Controls.Add(this.btnRefresh);
            this.groupControl2.Controls.Add(this.btnLimpiar);
            this.groupControl2.Controls.Add(this.btnSalir);
            this.groupControl2.Controls.Add(this.btnGuardar);
            this.groupControl2.Location = new System.Drawing.Point(814, 12);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(95, 467);
            this.groupControl2.TabIndex = 14;
            this.groupControl2.Text = "Opciones";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Appearance.Options.UseFont = true;
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(6, 112);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(83, 37);
            this.btnRefresh.TabIndex = 66;
            this.btnRefresh.Text = "Refrescar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(6, 68);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(83, 37);
            this.btnLimpiar.TabIndex = 17;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // FrmAggMateriasCurso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 488);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.gcMaterias);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmAggMateriasCurso";
            this.Text = "Agregar Materias por Curso";
            this.Load += new System.EventHandler(this.FrmAggMateriasCurso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seIH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesempeños.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMaterias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaterias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private dll.Controles.ucBusqueda txtMaestro;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.MemoEdit txtDesempeños;
        private DevExpress.XtraEditors.LabelControl lblDesempeños;
        private DevExpress.XtraGrid.GridControl gcMaterias;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMaterias;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private dll.Controles.ucBusqueda txtCurso;
        private dll.Controles.ucBusqueda txtMaterias;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SpinEdit seIH;
        private dll.Controles.ucBusqueda txtAño;
    }
}