
GO
-- =============================================
-- Author:		Arlington
-- Create date: 24-10-2017
-- Description:	Consulta los estudiantes de un curso con la posicion de acuerdo a sus notas
-- =============================================
CREATE PROCEDURE SP_EstudiantesCurso_G
	@pIdCurso INT,
	@pIdAñoLectivo INT
AS
BEGIN
	DECLARE 
	@IdCurso INT = @pIdCurso,
	@IdAñoLectivo INT = @pIdAñoLectivo
	SET NOCOUNT ON;

    SELECT ROW_NUMBER() OVER (ORDER BY Z.Promedio DESC) AS Posicion  
	, Z.IdTercero, Z.NombreComp, Z.NomCurso, Z.NomSede, Z.NomJornada, Z.Promedio
	FROM(SELECT ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NombreComp ,
	CS.NomCurso, SD.NomSede, JD.NomJornada
	,(SELECT AVG(ISNULL(P1, 0)) AS Prom 
		FROM ADNotas NT 
		INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec 
		INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec
		WHERE MT.IdEstudiante = ET.IdTercero) AS Promedio 
	FROM ADTerceros ET 
	INNER JOIN ADMatriculas MT ON ET.IdTercero = MT.IdEstudiante 
	INNER JOIN ADSedes SD ON MT.IdSede = SD.Sec
	INNER JOIN ADJornadas JD ON MT.IdJornada = JD.Sec
	INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
	WHERE MT.IdCurso = @IdCurso AND MT.IdAñoLectivo = @IdAñoLectivo) AS Z
	END
GO
