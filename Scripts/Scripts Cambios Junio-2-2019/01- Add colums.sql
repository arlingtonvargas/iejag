
GO
ALTER TABLE ADTerceros ADD RH VARCHAR(5) NULL

GO
ALTER TABLE ADTerceros ADD EPS VARCHAR(500) NULL

GO
ALTER TABLE ADObservacionEstudiante ADD OSIsPositive BIT NOT NULL


GO
DROP TABLE  [dbo].[ADInasistencia]
GO
CREATE TABLE [dbo].[ADInasistencia](
	[Sec] [int] IDENTITY(1,1) NOT NULL,
	[IdEstudiante] [bigint] NOT NULL,
	[IdPeriodo] [int] NOT NULL,
	[CantInasistencias] [int] NOT NULL,
	[IdCurso] [int] NULL,
	[IdMateria] [int] NULL,
	[IdMaestro] [bigint] NULL,
 CONSTRAINT [PK_ADInasistencia_1] PRIMARY KEY CLUSTERED 
(
	[Sec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [IX_ADInasistencia]    Script Date: 16/06/2019 5:33:57 p.m. ******/
CREATE NONCLUSTERED INDEX [IX_ADInasistencia] ON [dbo].[ADInasistencia]
(
	[IdCurso] ASC,
	[IdEstudiante] ASC,
	[IdMaestro] ASC,
	[IdMateria] ASC,
	[IdPeriodo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
