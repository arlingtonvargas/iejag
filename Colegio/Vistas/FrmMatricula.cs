﻿using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;
using Colegio.ET;

namespace Colegio.Vistas
{
    public partial class FrmMatricula : FrmBase
    {
        Clases.ClTercero clTerceros = new Clases.ClTercero();
        Clases.ClAñoLectivo clAñoLectivo = new Clases.ClAñoLectivo();
        Clases.ClCursos clCursos = new Clases.ClCursos();
        ClMatricula clMatricula = new ClMatricula();
        DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnEditGenerar = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();



        public FrmMatricula()
        {
            InitializeComponent();
        }

        private void FrmMatricula_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenaTxtBusqueda();
            LlenaCombos();
            LlenaGrilla();
            HabilitaBotones();
        }

        private void HabilitaBotones()
        {
            int rol = ClFunciones.UsuarioIngreso.IdRol;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1015)) btnGuardar.Enabled = false;

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBuscarEstu_Click(object sender, EventArgs e)
        {
            try
            {
                Clases.ClTercero cl = new Clases.ClTercero();
                DataTable dt = cl.GetTercerosTipoTer(2);
                if (dt.Rows.Count>0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                FrmBusqueda frm = new FrmBusqueda(dt);
                frm.ShowDialog();
                dll.Common.ET.SeleccionBusquedaET SeleccionBusqueda = new dll.Common.ET.SeleccionBusquedaET();
                SeleccionBusqueda = frm.SeleccionBusqueda;
                if (SeleccionBusqueda.Codigo!="")
                {
                    //txtEstudiante.Text = SeleccionBusqueda.Codigo;
                    //lblDesEst.Text = SeleccionBusqueda.Descripcion;
                }
            }
            catch (Exception ex)
            {


            }
        }

        private void LlenaTxtBusqueda()
        {
            try
            {
                DataTable dt = clTerceros.GetTercerosTipoTer(2);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                txtEstudianteB.DataTable = dt;               

                dt = clAñoLectivo.GetAñosLectivos();
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.DataTable = dt;
                    txtAño.ValorTextBox = ClFunciones.wADDatosInstitucion[0].IdAñoActual.ToString();
                    
                    if (txtAño.ValorTextBox != "")
                    {
                        dt = clCursos.GetCursosByAño(Convert.ToInt32(txtAño.ValorTextBox));
                        if (dt.Rows.Count > 0)
                        {
                            dt.Columns[0].ColumnName = "Codigo";
                            dt.Columns[1].ColumnName = "Descripcion";
                        }
                        txtCursos.DataTable = dt;
                    }
                }
               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenaCombos()
        {
            try
            {
                string sql = "SELECT Sec, NomSede FROM ADSedes";
                DataTable dt = ClFunciones.Consultar(sql,ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    lkeSede.lkeDatos.Properties.DataSource = dt;
                    lkeSede.lkeDatos.Properties.ValueMember = "Sec";
                    lkeSede.lkeDatos.Properties.DisplayMember = "NomSede";
                }
                sql = "SELECT Sec, NomJornada FROM ADJornadas";
                dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    lkeJornada.lkeDatos.Properties.DataSource = dt;
                    lkeJornada.lkeDatos.Properties.ValueMember = "Sec";
                    lkeJornada.lkeDatos.Properties.DisplayMember = "NomJornada";
                }
                Dictionary<int, string> r = new Dictionary<int, string>();
                r.Add(1, "SI");
                r.Add(2, "NO");
                ucLabelCombo1.lkeDatos.Properties.DataSource = r;

               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CrearGrilla()
        {
            try
            {
                gvMatriculas = GrillaDevExpress.CrearGrilla(false, true);
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("IdMatricula", "Id Matrícula", ancho:40));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("FechaCrea", "Creado", ancho: 80));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("IdTercero", "Id Estudiante", ancho: 110));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("NombreCompleto", "Estudiante", ancho: 200));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("Internado", "Internado", ancho: 60));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("UsuCrea", "Usuario creo", ancho: 40));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("IdGrado", "Grado", ancho: 40));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("NomCurso", "Curso", ancho: 90));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("SecAñoLectivo", "",  visible: false));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("AñoLectivo", "Año lectivo", ancho: 80));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("Cancelada", "Cancelada", ancho: 50));
                gvMatriculas.Columns.Add(GrillaDevExpress.CrearColumna("btnEditCancelar", "Cancelar Mat.", ancho: 130, soloLectura: true, permiteEditar: true, permiteFoco: true));
                gvMatriculas.Columns["btnEditCancelar"].ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
                btnEditGenerar.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
                btnEditGenerar.Buttons[0].ImageOptions.Image = imageCollection1.Images[0];
                btnEditGenerar.Buttons[0].ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
                btnEditGenerar.Buttons[0].IsLeft = true;
                btnEditGenerar.Buttons[0].Width = 120;
                btnEditGenerar.Buttons[0].Caption = "Cancelar Mat.";
                btnEditGenerar.Click += new System.EventHandler(this.btnEditGenerar_Click);
                gvMatriculas.Columns["btnEditCancelar"].ColumnEdit = btnEditGenerar;
                gvMatriculas.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMatriculas.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMatriculas.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvMatriculas.OptionsCustomization.AllowColumnResizing = true;
                gvMatriculas.OptionsView.ColumnAutoWidth = false;
                gcMatriculas.MainView = gvMatriculas;
                this.gvMatriculas.DoubleClick += new System.EventHandler(this.gvMatriculas_DoubleClick);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void btnEditGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                int secMatricula = Convert.ToInt32(gvMatriculas.GetFocusedRowCellValue("IdMatricula"));
                int idAñoActual = Convert.ToInt32(gvMatriculas.GetFocusedRowCellValue("SecAñoLectivo"));
                bool Cancelada = Convert.ToBoolean(gvMatriculas.GetFocusedRowCellValue("Cancelada"));

                if (Cancelada)
                {
                    ClFunciones.msgError("La matricula ya se encuentra cancelada!");
                    return;
                }
                ADAñoLectivoET AñoActual = clAñoLectivo.GetAñoLectivoVigente(idAñoActual);
                if (AñoActual.Sec > 0)
                {
                    ClFunciones.msgError("El año lectivo de la matricula NO se encuentra vigente!");
                    return;
                }
                else
                {
                    if (ClFunciones.msgResult("Esta apunto de cancelar por completo una matrícula. ¿Confirma que desea continuar?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        if (clMatricula.CancelaMatricula(secMatricula, ClConexion.clConexion.Conexion))
                        {
                            ClFunciones.msgExitoso("Matrícula cancelada de manera correcta.");
                            LlenaGrilla();
                        }
                        else
                        {
                            ClFunciones.msgError("Lo sentimos, ha ocurrido un error.");
                            return;
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void gvMatriculas_DoubleClick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidarCampos())
                {                    
                    ADMatriculasET pParam = new ADMatriculasET()
                    {
                        FechaCrea = DateTime.Now,
                        IdAñoLectivo = Convert.ToInt32(txtAño.ValorTextBox),
                        IdCurso = Convert.ToInt32(txtCursos.ValorTextBox),
                        IdEstudiante = Convert.ToInt64(txtEstudianteB.ValorTextBox),
                        UsuCrea = ClFunciones.UsuarioIngreso.IdUsuario,
                        IdSede = Convert.ToInt32(lkeSede.lkeDatos.EditValue),
                        IdJornada = Convert.ToInt32(lkeJornada.lkeDatos.EditValue),
                        Internado= (Convert.ToInt32(ucLabelCombo1.lkeDatos.EditValue) == 1) ? true : false
                    };
                    bool res = clMatricula.EstudianteEstaMatricualdo(pParam, ClConexion.clConexion.Conexion);
                    if(res)
                    {
                        ClFunciones.msgError("El estudiante ya cuenta con una matricula vigente para el año y curso seleccionado.");
                        return;
                    }
                    
                    res = clMatricula.InsertMatricula(pParam, ClConexion.clConexion.Conexion);
                    if (res)
                    {
                        ClFunciones.msgExitoso("Se registro la matricula de manera correcta.");
                        LlenaGrilla();
                        Limpiar();
                    }
                    else
                    {
                        ClFunciones.msgError("Lo sentimos, ha ocurrido un error, por favor intentelo nuevamente.");
                    }
                }
            }
            catch (Exception ex)
            {
                ClRegistraLogs.RegistrarError(new ET.SGLogs()
                {
                    FechaReg = DateTime.Now,
                    MensajeError = ex.Message,
                    NomArchivo = "FrmMatricula",
                    NomError = "Exception",
                    NomFunction = "btnGuardar_Click",
                    NomModulo = "Matricula",
                    NumLinea = 116,
                    Sec = 0
                });
            }
        }

        private void LlenaGrilla()
        {
            List <ADMatriculasETVista>  res = clMatricula.GetMatriculas();
            gcMatriculas.DataSource = res;
        }

        private void Limpiar()
        {
            txtEstudianteB.ValorTextBox = "";
            txtCursos.ValorTextBox = "";
            txtCursos.lblDescripcion.Text = "";
            txtAño.ValorTextBox = "";
            txtAño.lblDescripcion.Text = "";
            txtEstudianteB.Focus();
        }

        private bool ValidarCampos()
        {
            try
            {
                if (txtEstudianteB.ValorTextBox =="")
                {
                    ClFunciones.msgError("Debe seleccionar un estudiante.");
                    txtEstudianteB.Focus();
                    return false;
                }
                else if (txtCursos.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar un curso.");
                    txtCursos.Focus();
                    return false;
                }
                else if (txtAño.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar un año lectivo.");
                    txtAño.Focus();
                    return false;
                }
                else if (lkeSede.lkeDatos.EditValue == null)
                {
                    ClFunciones.msgError("Debe seleccionar la sede.");
                    lkeSede.Focus();
                    return false;
                }
                else if (lkeJornada.lkeDatos.EditValue == null)
                {
                    ClFunciones.msgError("Debe seleccionar la jornada.");
                    lkeJornada.Focus();
                    return false;
                }
                else if (ucLabelCombo1.lkeDatos.EditValue == null)
                {
                    ClFunciones.msgError("Debe seleccionar si el estudiante es interno o no.");
                    ucLabelCombo1.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                LlenaGrilla();
                LlenaTxtBusqueda();
            }
            catch (Exception ex)
            {
                ClRegistraLogs.RegistrarError(new ET.SGLogs()
                    {
                        FechaReg = DateTime.Now,
                        MensajeError =  ex.Message.Replace('\'',new char()),
                        NomArchivo = "FrmMatricula",
                        NomError = "Exception",
                        NomFunction = "btnRefresh_Click",
                        NomModulo = "Matricula",
                        NumLinea = 126,
                        Sec = 0                
                    });
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void txtAño_SaleControl(object sender, EventArgs e)
        {
            

        }

        private void FrmMatricula_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                if (gvMatriculas.Columns.Count > 0)
                {
                    if (gvMatriculas.Columns.Count > 0)
                    {
                        gvMatriculas.Columns["IdMatricula"].Width = (4 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["FechaCrea"].Width = (7 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["IdTercero"].Width = (10 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["NombreCompleto"].Width = (44 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["Internado"].Width = (4 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["UsuCrea"].Width = (5 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["IdGrado"].Width = (4 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["NomCurso"].Width = (8 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["AñoLectivo"].Width = (5 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["Cancelada"].Width = (4 * (grcInfo.Width + 180) / 100);
                        gvMatriculas.Columns["btnEditGenerar"].Width = 130;
                        gvMatriculas.Columns.ToList().ForEach(x => x.BestFit());
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
