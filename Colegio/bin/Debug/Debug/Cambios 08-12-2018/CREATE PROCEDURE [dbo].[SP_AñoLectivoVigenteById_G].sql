USE [BDIEJAG]
GO
/****** Object:  StoredProcedure [dbo].[SP_EstudiantesCurso_G]    Script Date: 08/12/2018 9:53:38 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 08-12-2017
-- Description:	Consulta año lectivo por id
-- =============================================
CREATE PROCEDURE [dbo].[SP_AñoLectivoVigenteById_G]
	@pSec INT
AS
BEGIN
	DECLARE 
	@Sec INT = @pSec

	SELECT Sec, Año, FechaDesde, FechaHasta FROM ADAñoLectivo WHERE Sec = @Sec AND FechaHasta < GETDATE()

	END
