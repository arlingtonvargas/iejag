﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class SGLogs
    {
        public long Sec { get; set; }
        public string NomError { get; set; }
        public string NomFunction { get; set; }
        public string NomArchivo { get; set; }
        public int NumLinea { get; set; }
        public DateTime FechaReg { get; set; }
        public string NomModulo { get; set; }
        public string MensajeError { get; set; }
    }
}
