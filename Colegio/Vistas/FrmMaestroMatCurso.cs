﻿using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;
using DevExpress.XtraEditors.Repository;

namespace Colegio.Vistas
{
    public partial class FrmMaestroMatCurso : FrmBase
    {
        DataTable dtCursos = new DataTable();
        DataTable dtMaterias = new DataTable();
        List<DataTable> listDtMaterias = new List<DataTable>();

        ClMaterias clMaterias = new ClMaterias();
        public FrmMaestroMatCurso()
        {
            InitializeComponent();
        }

        private void FrmMaestroMatCurso_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenarControlesBusqueda();
            CrearTablas();
        }

        private void CrearTablas()
        {
            dtCursos.Columns.Add("IdCurso");
            dtCursos.Columns.Add("NomCurso");
            dtMaterias.Columns.Add("IdMateria");
            dtMaterias.Columns.Add("NomMateria");
            dtMaterias.Columns.Add("IdCurso");
        }

        private void LlenarGrilla(int idCurso, long idMaestro)
        {
            try
            {
                string sql = string.Format("SELECT  MC.Sec, MC.IdMateria, MR.NomMateria, TR.IdTercero, " +
                " CONCAT(TR.Pnombre, ' ', TR.Snombre, ' ', TR.Papellido, ' ', TR.Sapellido) AS NomMaestro, " +
                " MT.IdCurso, CS.NomCurso " +
                " FROM ADMateriasCursoMaestro MC " +
                " INNER JOIN ADMaestroMatCurso MT ON MC.IdMaestroMatCurso = MT.Sec " +
                " INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec " +
                " INNER JOIN ADMaterias MR ON MC.IdMateria = MR.Sec " +
                " INNER JOIN ADTerceros TR ON MT.IdMaestro = TR.IdTercero " +
                " WHERE CS.Sec = {0} AND MT.IdMaestro = {1}", idCurso, idMaestro);                

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void CrearGrilla()
        {
            try
            {
                gvCursos = GrillaDevExpress.CrearGrilla(false, true);
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("IdCurso", "Id Curso", ancho: 30));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("NomCurso", "Curso"));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("btn", "..."));
                RepositoryItemButtonEdit repositoryItemButtonEdit1 = new RepositoryItemButtonEdit()
                {
                    AutoHeight = false,
                };
                repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
                repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
                repositoryItemButtonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEdit1_ButtonClick);
                // 
                gvCursos.Columns[2].ColumnEdit = repositoryItemButtonEdit1;
                gvCursos.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvCursos.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvCursos.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvCursos.OptionsCustomization.AllowColumnResizing = true;
                gcCursos.MainView = gvCursos;
                this.gvCursos.DoubleClick += new System.EventHandler(this.gvCursos_DoubleClick);
                this.gvCursos.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvCursos_FocusedRowChanged);



                gvMaterias = GrillaDevExpress.CrearGrilla(false, true);
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("IdMateria", "Id Materia", ancho:30));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("NomMateria", "Materia"));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("IdCurso", "", visible:false));
                gvMaterias.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvMaterias.OptionsCustomization.AllowColumnResizing = true;
                gcMaterias.MainView = gvMaterias;
                this.gvMaterias.DoubleClick += new System.EventHandler(this.gvMaterias_DoubleClick);


            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void AgregarMateriaCurso(int idCurso)
        {
            try
            {
                DataRow[] filas;
                if (listDtMaterias.Count > 0)
                {
                    listDtMaterias.ForEach(x =>
                    {
                        filas = x.Select("IdCurso = " + idCurso.ToString());
                        if (filas.Length > 0)
                        {
                            if (x.Select(string.Format("IdCurso = {0} AND IdMateria = '{1}'", idCurso, txtMateria.ValorTextBox)).Length > 0)
                            {
                                MessageBox.Show("La materia ya se encuentra en el curso seleccionado.");
                                return;
                            }
                            DataRow fila = x.NewRow();
                            fila["IdMateria"] = txtMateria.ValorTextBox;
                            fila["NomMateria"] = txtMateria.lblDescripcion.Text;
                            fila["IdCurso"] = idCurso;
                            x.Rows.Add(fila);
                            x.AcceptChanges();
                            return;
                        }
                    });
                }

                DataRow fla = dtMaterias.NewRow();
                fla["IdMateria"] = txtMateria.ValorTextBox;
                fla["NomMateria"] = txtMateria.lblDescripcion.Text;
                fla["IdCurso"] = idCurso;
                dtMaterias.Rows.Add(fla);
                dtMaterias.AcceptChanges();
                listDtMaterias.Add(dtMaterias);
                gcMaterias.DataSource = dtMaterias;
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BuscarMateriasByCurso(int idCurso)
        {
            try
            {
                if (listDtMaterias.Count>0)
                {
                    listDtMaterias.ForEach(x => 
                    {
                        if (x.Select("IdCurso = " + idCurso.ToString()).Length>0)
                        {
                            gcMaterias.DataSource = x;
                            return;
                        }
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void gvCursos_FocusedRowChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void gvCursos_DoubleClick(object sender, EventArgs e)
        {

        }

        private void LlenarControlesBusqueda()
        {
            ClTercero clTercero = new ClTercero();
            try
            {
                string sql = "";
                DataTable dt = clTercero.GetTercerosTipoTer(1);// ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count>0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                txtMaestro.TituloColsBusqueda = new dll.Common.ET.TituloColsBusquedaET() { Codigo = "Documento", Descripcion = "Nombre Maestro"};
                txtMaestro.DataTable = dt;
                sql = "SELECT Sec AS Codigo, NomCurso AS Descripcion FROM ADCursos";
                dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                txtCurso.TituloColsBusqueda = new dll.Common.ET.TituloColsBusquedaET() { Codigo ="Id", Descripcion = "Nombre curso" };
                txtCurso.DataTable = dt;               
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        private void LLenarControlMat(int idCurso)
        {
            try
            {
                //DataTable dt = clMaterias.GetMateriasVistaByCursoDT(idCurso);
                //dt.Columns[0].ColumnName = "Codigo";
                //dt.Columns[1].ColumnName = "Descripcion";
                //txtMateria.DataTable = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gvMaterias_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void txtMaestro_SaleControl(object sender, EventArgs e)
        {

        }

        private void gvCursos_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle>-1)
            {
                int idCurso = Convert.ToInt32(gvCursos.GetRowCellValue(e.FocusedRowHandle, "IdCurso"));
                LLenarControlMat(idCurso);
            }
            else
            {
                txtMateria.DataTable = new DataTable();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCurso.ValorTextBox == "") return;
                DataRow[] filas = dtCursos.Select("IdCurso = "+ txtCurso.ValorTextBox);
                if (filas.Length>0)
                {
                    ClFunciones.msgError("Ya se agrego la materia seleccionada.");
                    return;
                }
                DataRow fila = dtCursos.NewRow();
                fila["IdCurso"] = txtCurso.ValorTextBox;
                fila["NomCurso"] = txtCurso.lblDescripcion.Text;
                dtCursos.Rows.Add(fila);
                dtCursos.AcceptChanges();
                gcCursos.DataSource = dtCursos;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddMateria_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvCursos.FocusedRowHandle>=0)
                {
                    int idCurso = Convert.ToInt32(gvCursos.GetFocusedRowCellValue("IdCurso"));
                    AgregarMateriaCurso(idCurso);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            MessageBox.Show("ok");
        }

        private void btnQuitarCurso_Click(object sender, EventArgs e)
        {
            if (gvCursos.FocusedRowHandle >= 0)
            {
                gvCursos.DeleteRow(gvCursos.FocusedRowHandle);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarControlesBusqueda();
        }

        private void gvCursos_FocusedRowChanged_1(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

        }
    }
}
