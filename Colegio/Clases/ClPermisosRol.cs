﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClPermisosRol
    {
        public DataTable GetPermisosByRol(int idRol)
        {
            try
            {
                string sql = "SELECT PE.SecTransaccion, PE.NomPermiso, PE.DesPermiso " +
                " FROM SGPermisosRoles PR" +
                " INNER JOIN SGPermisos PE ON PR.IdPermiso = PE.SecTransaccion" +
                " INNER JOIN SGRoles RL ON PR.IdRol = RL.Sec" +
                " WHERE ISNULL(RL.Anulado, 0) = 0 AND PR.IdRol = " + idRol.ToString();
                DataTable dt = ClFunciones.f.AbrirTabla(ClConexion.clConexion.Conexion, sql);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable GetPermisosNotInRol(int idRol)
        {
            try
            {
                string sql = string.Format("SELECT PE.SecTransaccion, PE.NomPermiso, PE.DesPermiso FROM SGPermisos PE WHERE SecTransaccion NOT IN " +
                " (SELECT IdPermiso FROM SGPermisosRoles PR INNER JOIN SGRoles RL ON  " +
                " PR.IdRol = RL.Sec WHERE IdRol = {0} AND ISNULL(RL.Anulado, 0) = 0)", idRol.ToString());
                DataTable dt = ClFunciones.f.AbrirTabla(ClConexion.clConexion.Conexion, sql);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public  bool GuardarPermisosRol(int idRol, DataTable dtPermisos)
        {
            try
            {
                string sql = "";
                EliminarPermisosRol(idRol, dtPermisos);
                foreach (DataRow item in dtPermisos.Rows)
                {
                    sql = string.Format("INSERT INTO SGPermisosRoles(IdRol, IdPermiso) VALUES({0},{1})", idRol, item["SecTransaccion"]);
                    ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EliminarPermisosRol(int idRol, DataTable dtPermisos)
        {
            try
            {
                string sql = "DELETE FROM SGPermisosRoles WHERE IdRol = " + idRol.ToString();
                ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
