﻿using Colegio.Clases;
using Colegio.Vistas;
using Colegio.Vistas.Seguridad;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Vistas.Configuracion;

namespace Colegio
{
    public partial class FrmPrincipal : XtraForm
    {
        public static FrmPrincipal frmPrinc = new FrmPrincipal();
        private bool Cerrando = true;

        public FrmPrincipal()
        {
            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AcomodaForm();
            HabilitaBotones();
            SetSkin();
            lblFechaIngreso.Caption = "Fecha | " + DateTime.Now.ToShortDateString();
            lblUser.Caption = "Usuario | " + ClFunciones.UsuarioIngreso.NomUsu;
            lblVersion.Caption = "V" + Application.ProductVersion;
        }
        public void SetSkin()
        {
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = "Office 2016 Colorful";
        }
        private void AcomodaForm()
        {
            ribbonControl1.Items.ToList().ForEach(x=> 
            {
                Type t = x.GetType();
                if (t.Equals(typeof(DevExpress.XtraBars.BarButtonItem)) || t.Equals(typeof(DevExpress.XtraBars.BarStaticItem)) || t.Equals(typeof(DevExpress.XtraBars.BarSubItem)) || t.Equals(typeof(DevExpress.XtraBars.SkinBarSubItem)))
                {
                    x.ItemAppearance.Disabled.Font = new System.Drawing.Font("Segoe UI", 9.25F);
                    x.ItemAppearance.Disabled.Options.UseFont = true;
                    x.ItemAppearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9.25F);
                    x.ItemAppearance.Hovered.Options.UseFont = true;
                    x.ItemAppearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9.25F);
                    x.ItemAppearance.Normal.Options.UseFont = true;
                    x.ItemAppearance.Pressed.Font = new System.Drawing.Font("Segoe UI", 9.25F);
                    x.ItemAppearance.Pressed.Options.UseFont = true;
                }
            });
            foreach (DevExpress.XtraBars.BarItem ctrl in barManager1.Items)
            {
                Type t = ctrl.GetType();
                if (t.Equals(typeof(DevExpress.XtraBars.BarButtonItem)) || t.Equals(typeof(DevExpress.XtraBars.BarSubItem)) || t.Equals(typeof(DevExpress.XtraBars.SkinBarSubItem)))
                {
                    ctrl.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 9.25F);
                    ctrl.ItemAppearance.Disabled.Options.UseFont = true;
                    ctrl.ItemAppearance.Hovered.Font = new System.Drawing.Font("Tahoma", 9.25F);
                    ctrl.ItemAppearance.Hovered.Options.UseFont = true;
                    ctrl.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 9.25F);
                    ctrl.ItemAppearance.Normal.Options.UseFont = true;
                    ctrl.ItemAppearance.Pressed.Font = new System.Drawing.Font("Tahoma", 9.25F);
                    ctrl.ItemAppearance.Pressed.Options.UseFont = true;
                }
            }
        }

        private void HabilitaBotones()
        {
            int rol = ClFunciones.UsuarioIngreso.IdRol;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1001)) btnRoles.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1002)) btnUsuariosRol.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1003)) btnPermisosXrol.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1004)) btnListUsuarios.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1005)) btnMaetros.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1008)) btnAdministrativos.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1011)) btnEstudiantes.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1014)) btnMatriculas.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1016)) btnCursos.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1017)) btnAddMatCurso.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1018) || !ClFunciones.wADDatosInstitucion[0].ReportarNotas) btnReportarNotas.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1019)) btnRptNotas.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1020)) btnRptNotasPorEstudiente.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1021)) btnMaterias.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1022)) btnSedes.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1023)) btnAñoLectivo.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1024)) btnJornadas.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1025)) btnConfigGeneral.Enabled = false;            
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1028)) btnGenCarnet.Enabled = false;
        }

        private void inHabilitaBotones()
        {
            btnMaetros.Enabled = false;
            btnAdministrativos.Enabled = false;
            btnEstudiantes.Enabled = false;
            btnMatriculas.Enabled = false;
            btnHorarios.Enabled = false;
            btnCursos.Enabled = false;
            mnuNotas.Enabled = false;
            mnuInformes.Enabled = false;

            btnRoles.Enabled = false;
            btnUsuariosRol.Enabled = false;
            btnPermisosXrol.Enabled = false;

        }
        private void CerrarVentanas()
        {
            try
            {
                //if (this.MdiChildren.Length == 0)
                //{
                //    Registro.Guardar_Clave_Del_Registro("General", "Skin", nowtema);
                //    this.Close();
                //    this.TabManagerPrincipal.MdiParent = null;
                //}
                //else
                //{
                //    Form forma = Application.OpenForms.OfType<Form>().Where(pre => pre.Text == TabManagerPrincipal.SelectedPage.Text).SingleOrDefault<Form>();
                //    forma.Close();
                //    this.TabManagerPrincipal.MdiParent = this;
                //}
                //this.ValidaMdiParent();

                if (this.MdiChildren.Length > 0)
                {
                    if (ActiveMdiChild != null)
                        ActiveMdiChild.Close();
                }
                else
                {
                    this.Close();
                }

            }
            catch (Exception ex)
            {
                Clases.ClFunciones.msgError(ex.Message);
            }
        }      

        private void btnConectar_Click(object sender, EventArgs e)
        {
            if (Clases.ClConexion.clConexion.EstaConectado)
            {
                if (Clases.ClFunciones.msgResult("Seguro que desea desconectarse del sistema?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, "") == DialogResult.OK)
                {
                    Clases.ClConexion.clConexion.DesconectaBD();
                    inHabilitaBotones();
                    //btnConectar.Image = ClFunciones.Imagen_boton16X16(ClFunciones.EnumImagenes16X16.Conectar);
                    //btnConectar.Text = "Conectar";
                    //sbtnConectar.Text = "Conectar";
                    foreach (Form c in this.MdiChildren)
                    {
                        c.Close();
                    }
                    this.IsMdiContainer = false;
                }
            }
            else
            {
                Form frm = new Vistas.FrmLogin();
                frm.ShowDialog();
                if (Clases.ClConexion.clConexion.EstaConectado)
                {
                    HabilitaBotones();
                    //btnConectar.Image = ClFunciones.Imagen_boton16X16(ClFunciones.EnumImagenes16X16.Desconectar);
                    //btnConectar.Text = "Desconectar";
                    //sbtnConectar.Text = "Desconectar";
                    ClFunciones.formPrinci = this;
                }
            }
        }

  

        private void FrmPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Cerrando)
                return;
            FrmConfirmacion frm = new FrmConfirmacion();
            frm.lblMensaje.Text = "Esta seguro que desea salir de la aplicacion?";
            frm.ShowDialog();
            if (frm.Confirmo)
            {
                ClConexion.clConexion.DesconectaBD();
                //Application.Exit();
                FrmLogin obj = (FrmLogin)Application.OpenForms["FrmLogin"];
                obj.Close();
            }
            //if (ClFunciones.msgResult("¿Esta seguro que desea salir de la aplicacion?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, "Advertencia") == System.Windows.Forms.DialogResult.Yes)
            //{
            //    ClFunciones.f.DesconectaBD();
            //    FrmLogin obj = (FrmLogin)Application.OpenForms["FrmLogin"];
            //    obj.Close();
            //}
            else
            {
                e.Cancel = true;
            }
        }

        private void mdiPrincipal_PageAdded(object sender, DevExpress.XtraTabbedMdi.MdiTabPageEventArgs e)
        {
            try
            {
                this.IsMdiContainer = true;
                mdiPrincipal.MdiParent = this;
                if (e.Page.Text =="Maestros")
                {
                    //e.Page.Image = ClFunciones.Imagen_boton16X16(ClFunciones.EnumImagenes16X16.Maestros);
                }
            }
            catch (Exception )
            {

                throw;
            }
            
            
        }

        private void mdiPrincipal_PageRemoved(object sender, DevExpress.XtraTabbedMdi.MdiTabPageEventArgs e)
        {
            if(this.MdiChildren.Length <= 0)
            {
                this.IsMdiContainer = false;
                mdiPrincipal.MdiParent = null;
            }
        }

        private void btnMaestros_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmMaestros);
                if (existe != null)
                {
                    if (existe.Text == "Maestros")
                    {
                        existe.WindowState = FormWindowState.Normal;
                        existe.BringToFront();
                    }else
                    {
                        if (!this.IsMdiContainer)
                        {
                            this.IsMdiContainer = true;
                            mdiPrincipal.MdiParent = this;
                        }
                        FrmMaestros frm = new FrmMaestros();
                        frm.MdiParent = this;
                        frm.Show();
                    }
                   
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmMaestros frm = new FrmMaestros();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }         
        }

        private void btnListarMaestros_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmMaestros);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmMaestros frm = new FrmMaestros();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
           
        }

        private void btnNuevMaestro_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmNuevoMaestro frm = new FrmNuevoMaestro(null);
            frm.ShowDialog();
        }

        private void btnRoles_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmRoles frm = new FrmRoles();
            frm.ShowDialog();
        }

        private void btnPermisosXrol_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmPermisosRol frm = new FrmPermisosRol();
            frm.ShowDialog();
        }

        private void btnUsuariosRol_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmUsuRol frm = new FrmUsuRol();
            frm.ShowDialog();
        }

        private void btnMaetros_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmMaestros);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmMaestros frm = new FrmMaestros();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
           
        }

        private void btnConectar_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmConfirmacion frm = new FrmConfirmacion();
            frm.lblMensaje.Text = "Seguro que desea desconectarse del sistema?";
            frm.ShowDialog();
            if (frm.Confirmo)
            {
                ClFunciones.UsuarioIngreso = new ET.SGUsuariosET();
                ClFunciones.formLogin.Show();
                Cerrando = false;
                this.Hide();
                this.Close();
            }
        }

        private void btnSalir_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CerrarVentanas();
        }

        private void btnSalirSeg_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CerrarVentanas();
        }

        private void btnCambiarContra_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (ClFunciones.UsuarioIngreso == null)
            {
                FrmMensaje f = new FrmMensaje();
                f.lblMensaje.Text = "Lo sentimos, al parecer no se cargo la información del usuario actual, por favor cierre sesión y vuelva a intentarlo.";
                f.ShowDialog();
                return;
            }
            FrmCambiarClave frm = new FrmCambiarClave(ClFunciones.UsuarioIngreso);
            frm.ShowDialog();
        }

        private void btnListUsuarios_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmUsuarios);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmUsuarios frm = new FrmUsuarios();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
      
        }

        private void btnEstudiantes_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmEstudiante);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmEstudiante frm = new FrmEstudiante();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
           
        }

        private void btnAdministrativos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmMaestros);
                if (existe != null )
                {
                    if (existe.Text == "Administrativos")
                    {
                        existe.WindowState = FormWindowState.Normal;
                        existe.BringToFront();
                    }else
                    {
                        if (!this.IsMdiContainer)
                        {
                            this.IsMdiContainer = true;
                            mdiPrincipal.MdiParent = this;
                        }
                        FrmMaestros frm = new FrmMaestros();
                        frm.Text = "Administrativos";
                        frm.EsMaestro = false;
                        frm.lblTitulo.Text = "Listado de Administrativos existentes";
                        frm.MdiParent = this;
                        frm.Show();
                    }
                   
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmMaestros frm = new FrmMaestros();
                    frm.Text = "Administrativos";
                    frm.EsMaestro = false;
                    frm.lblTitulo.Text = "Listado de Administrativos existentes";
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
             
        }

        private void btnEst_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmEstudiantesLocal frm = new FrmEstudiantesLocal();
            frm.ShowDialog();
        }

        private void btnGenNotas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //FrmBoletinLocal frm = new FrmBoletinLocal();
            //frm.ShowDialog();
            try
            {
                if (!this.IsMdiContainer)
                {
                    this.IsMdiContainer = true;
                    mdiPrincipal.MdiParent = this;
                }
                Vistas.local.FrmGenerarBoletines frm = new Vistas.local.FrmGenerarBoletines();
                frm.MdiParent = this;
                frm.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnMatriculas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmMatricula);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmMatricula frm = new FrmMatricula();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }          
        }

        private void btnCursos_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmCursos);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmCursos frm = new FrmCursos();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
           
        }

        private void btnMaestroMat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmMaestroMatCurso);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmMaestroMatCurso frm = new FrmMaestroMatCurso();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
           
        }

        private void btnReportarNotas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmReportarNotas);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmReportarNotas frm = new FrmReportarNotas();
                    frm.MdiParent = this;
                    frm.Show();
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnGenBoletines_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is Vistas.local.FrmGenerarBoletines);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    Vistas.local.FrmGenerarBoletines frm = new Vistas.local.FrmGenerarBoletines();
                    frm.MdiParent = this;
                    frm.Show();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnCargarNotas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is Vistas.local.FrmCargarNotas);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    Vistas.local.FrmCargarNotas frm = new Vistas.local.FrmCargarNotas();
                    frm.MdiParent = this;
                    frm.Show();
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnRptNotas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is Vistas.Reports.FrmRptNotasByCurso);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    Vistas.Reports.FrmRptNotasByCurso frm = new Vistas.Reports.FrmRptNotasByCurso();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRptNotasPorEstudiente_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is Vistas.Reports.FrmRptNotasByEstudiante);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    Vistas.Reports.FrmRptNotasByEstudiante frm = new Vistas.Reports.FrmRptNotasByEstudiante();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAddMatCurso_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmAggMateriasCurso);
                if (existe != null)
                {
                    existe.WindowState = FormWindowState.Normal;
                    existe.BringToFront();
                }
                else
                {
                    if (!this.IsMdiContainer)
                    {
                        this.IsMdiContainer = true;
                        mdiPrincipal.MdiParent = this;
                    }
                    FrmAggMateriasCurso frm = new FrmAggMateriasCurso();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnMaterias_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmMaterias frm = new FrmMaterias();
            frm.ShowDialog();
        }

        private void btnSedes_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmSedes frm = new FrmSedes();
            frm.ShowDialog();
        }

        private void btnAñoLectivo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmAñosLectivos frm = new FrmAñosLectivos();
            frm.ShowDialog();
        }

        private void btnJornadas_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmJornadas frm = new FrmJornadas();
            frm.ShowDialog();
        }

        private void btnConfigGeneral_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmConfGeneral frm = new FrmConfGeneral();
            frm.ShowDialog();
        }

        private void btnObservador_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                //Form existe = this.MdiChildren.FirstOrDefault(x => x is FrmObservador);
                //if (existe != null)
                //{
                //    existe.WindowState = FormWindowState.Normal;
                //    existe.BringToFront();
                //}
                //else
                //{
                //    if (!this.IsMdiContainer)
                //    {
                //        this.IsMdiContainer = true;
                //        mdiPrincipal.MdiParent = this;
                //    }
                //    FrmObservador frm = new FrmObservador();
                //    frm.MdiParent = this;
                //    frm.Show();
                //}
                FrmAggObservador frm = new FrmAggObservador();
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnGenCarnet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                FrmGeneraCarnet frm = new FrmGeneraCarnet();
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnNotasPorCurso_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Vistas.Reports.FrmRptNotas frm = new Vistas.Reports.FrmRptNotas();
            frm.ShowDialog();
        }
    }
}
