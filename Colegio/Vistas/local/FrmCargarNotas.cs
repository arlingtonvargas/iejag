﻿using Colegio.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

namespace Colegio.Vistas.local
{
    public partial class FrmCargarNotas : Form
    {
        public FrmCargarNotas()
        {
            InitializeComponent();
        }

        private void FrmCargarNotas_Load(object sender, EventArgs e)
        {
            CrearGrilla();
        }

        private void CrearGrilla()
        {
            try
            {
                gvNotas = GrillaDevExpress.CrearGrilla(false, true);
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Sec", ancho: 20));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("idestudiante", "idestudiante", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("idgrado", "idgrado", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("idmateria", "idmateria", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("idMaestro", "idMaestro", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("competencias", "competencias", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("p1", "P1", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("p2", "P2", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("p3", "P3", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("p4", "P4", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("IH", "IH", ancho: 60));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("AUS", "AUS"));
                gvNotas.Columns.Add(GrillaDevExpress.CrearColumna("idtitular", "idtitular"));
                gvNotas.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvNotas.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvNotas.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvNotas.OptionsCustomization.AllowColumnResizing = true;
                gcNotas.MainView = gvNotas;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void btnCargar_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet dsPatent = new DataSet();
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "xlsx Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string filename = openFileDialog1.FileName;
                    DataTable dt = ExcelDataBaseHelper.OpenFile(filename, "Notas");
                    if (dt == null)
                    {
                        return;
                    }
                    if (dt.Rows.Count > 0)
                    {
                        gcNotas.DataSource = dt;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvNotas.RowCount==0)
                {
                    return;
                }
                DataTable dt = (DataTable)gcNotas.DataSource;
                string sql = "";
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        foreach (DataRow item in dt.Rows)
                        {
                            sql = string.Format("INSERT INTO [local].[Notas] " +
                            " ([Sec]  ,[idestudiante] ,[idgrado] ,[idmateria] ,[idMaestro],[competencias]  ,[p1] " +
                            " ,[p2]  ,[p3]  ,[p4]  ,[IH]  ,[AUS] ,[idtitular]) " +
                            " VALUES  ({0}, {1}, {2}, {3} ,{4}, '{5}', {6}, {7}, {8}, {9}, {10},{11},{12})",
                            item["Sec"], item["idestudiante"], item["idgrado"], item["idmateria"], item["idMaestro"], 
                            item["competencias"], item["p1"], item["p2"], item["p3"]
                            ,item["p4"], item["IH"], item["AUS"], item["idtitular"]);
                            string res = ClFunciones.EjecutaComando(sql, con);
                            if (res != "si")
                            {
                                MessageBox.Show("Ha ocurrido un error.");
                                return;
                            }

                        }
                        scope.Complete();
                        MessageBox.Show("Inserción realizada con éxito.");
                        gcNotas.DataSource = new DataTable();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
