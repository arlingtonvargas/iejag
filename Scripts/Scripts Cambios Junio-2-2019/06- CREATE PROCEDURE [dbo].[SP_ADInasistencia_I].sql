
GO
-- =============================================
-- Author:		Arlington
-- Create date: 16-06-2019
-- Description:	Inserta inasistencia
-- =============================================
CREATE PROCEDURE [dbo].[SP_ADInasistencia_I]
	
		@IdEstudiante bigint
        ,@IdPeriodo int
        ,@CantInasistencias int
        ,@IdCurso int
        ,@IdMateria int
        ,@IdMaestro bigint
		,@Inserto VARCHAR(2) OUTPUT

AS
BEGIN
	DECLARE @Count int = 0
	
	SET @Inserto = 'NO'

	SET @Count = (SELECT COUNT(*) FROM [dbo].[ADInasistencia] WHERE IdEstudiante = @IdEstudiante AND IdPeriodo = @IdPeriodo AND IdCurso = @IdCurso AND IdMateria = @IdMateria)
	
	IF(@Count>0)
		BEGIN
			UPDATE [dbo].[ADInasistencia] SET CantInasistencias = @CantInasistencias 
			WHERE IdEstudiante = @IdEstudiante AND IdPeriodo = @IdPeriodo AND IdCurso = @IdCurso AND IdMateria = @IdMateria
			SET @Inserto = 'SI'
		END
	ELSE
		BEGIN
			INSERT INTO [dbo].[ADInasistencia]
				([IdEstudiante]
				,[IdPeriodo]
				,[CantInasistencias]
				,[IdCurso]
				,[IdMateria]
				,[IdMaestro])
			VALUES
				(@IdEstudiante
				,@IdPeriodo
				,@CantInasistencias
				,@IdCurso
				,@IdMateria
				,@IdMaestro)
			SET @Inserto = 'SI'
		END

	SET NOCOUNT ON;
END
