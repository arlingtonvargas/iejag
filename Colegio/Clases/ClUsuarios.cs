﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClUsuarios
    {
        public DataTable GetUsuarios()
        {
            try
            {
                string sql = "SELECT US.IdUsuario, US.NomUsu, US.UsuCrea AS IdUsuCra, " +
                " (SELECT NomUsu FROM SGUsuarios WHERE IdUsuario = US.UsuCrea) AS NomUsuCrea" +
                " , US.FechaCrea, US.UsuMod AS IdUsuMod, (SELECT NomUsu FROM SGUsuarios WHERE IdUsuario = US.UsuMod) AS NomUsuMod " +
                " , US.FechaMod, US.FechaUltIngreso, MS.IdTercero AS Documento" +
                " ,CONCAT(RTRIM(LTRIM(MS.Pnombre)), ' ', RTRIM(LTRIM(MS.Snombre)), ' ', RTRIM(LTRIM(MS.Papellido)), ' ', RTRIM(LTRIM(MS.Sapellido))) AS NombreComp" +
                " , US.Anulado" +
                " FROM SGUsuarios US" +
                " INNER JOIN ADTerceros MS ON US.IdUsuario = MS.IdUsuario";
                DataTable dt = ClFunciones.f.AbrirTabla(ClConexion.clConexion.Conexion, sql);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int CrearUsuario(string NomUsu,
                                int UsuCrea,
                                DateTime FechaCrea,
                                string password,
                                int IdRol,
                                SqlConnection con)
        {
            try
            {
                string pass = ClFunciones.Encrypt(password);
                string sql = string.Format("INSERT INTO [dbo].[SGUsuarios] " +
                " ([NomUsu]" +
                " ,[UsuCrea]" +
                " ,[FechaCrea]" +
                " ,[password]" +
                " ,[Anulado]" +
                " ,[IdRol])" +
                " VALUES" +
                " ('{0}', {1}, '{2:dd/MM/yyyy HH:mm:ss}', '{3}', 0, {4}); SELECT IdUsuario FROM SGUsuarios WHERE IdUsuario = SCOPE_IDENTITY();", 
                NomUsu,UsuCrea, FechaCrea, pass, IdRol);
                int idNuewUsu = ClFunciones.EjecutaComandoReturId(sql, con);
                return idNuewUsu;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public bool UpdateUsuario(int IdUsuario
                                ,int UsuMod
                                ,bool Anulado
                                ,int IdRol
                                ,SqlConnection con)
        {
            try
            {
                string sql = string.Format("UPDATE [dbo].[SGUsuarios] " +
                " SET [FechaMod] = '{0:dd/MM/yyyy HH:mm:ss}'" +
                " ,[UsuMod] = {1}" +
                " ,[Anulado] = '{2}'" +
                " ,[IdRol] = {3}" +
                " WHERE IdUsuario = {4}", 
                DateTime.Now, UsuMod, Anulado, IdRol, IdUsuario);
                string res = ClFunciones.EjecutaComando(sql, con);
                if (res=="si")
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public bool UpdatePswUsuario(ET.SGUsuariosET Usuario, SqlConnection con)
        {
            try
            {
                string sql = string.Format("UPDATE [dbo].[SGUsuarios] " +
                " SET [FechaMod] = '{0:dd/MM/yyyy HH:mm:ss}'" +
                " ,[Password] = '{1}'" +
                " WHERE IdUsuario = {2}",
                DateTime.Now, Usuario.password,Usuario.IdUsuario);
                string res = ClFunciones.EjecutaComando(sql, con);
                if (res == "si")
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool ExisteUsuarioByNomUsu(string nomUsu, SqlConnection con)
        {
            try
            {
                string sql = string.Format("SELECT US.NomUsu FROM SGUsuarios US WHERE LOWER(US.NomUsu) = LOWER('{0}')", nomUsu);
                DataTable dt = ClFunciones.Consultar(sql, con);
                if (dt.Rows.Count>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public string CreaNomUsuario(string pNombre, string pApe, SqlConnection con)
        {            
            string newNomUsu = (pNombre.Substring(0, 1) + pApe).ToLower();
            string newNomUsu1 = (pApe.Substring(0, 1) + pNombre).ToLower();
            string newNomUsu2 = (pNombre + pApe.Substring(0, 2)).ToLower();
            string newNomUsu3 = (pApe + pNombre.Substring(0, 2)).ToLower();
            string newNomUsu4 = (pNombre).ToLower();
            string newNomUsu5 = (pApe).ToLower();
            string newNomUsu6 = (pNombre +"."+ pApe).ToLower();
            string newNomUsu7 = (pApe + "." + pNombre).ToLower();
            string newNomUsu8 = (pNombre.Substring(0, 2) + pApe).ToLower();
            string newNomUsu9 = (pApe.Substring(0, 2) + pNombre).ToLower();
            try
            {
                if (!ExisteUsuarioByNomUsu(newNomUsu, con))
                {
                    return newNomUsu;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu1, con))
                {
                    return newNomUsu1;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu2, con))
                {
                    return newNomUsu2;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu3, con))
                {
                    return newNomUsu3;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu4, con))
                {
                    return newNomUsu4;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu5, con))
                {
                    return newNomUsu5;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu6, con))
                {
                    return newNomUsu6;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu7, con))
                {
                    return newNomUsu7;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu8, con))
                {
                    return newNomUsu8;
                }
                else if (!ExisteUsuarioByNomUsu(newNomUsu9, con))
                {
                    return newNomUsu9;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public SGUsuariosET Ingreso(string nomUsu, string psw)
        {
            try
            {

                string pass = ClFunciones.Encrypt(psw);
                SGUsuariosET UsuarioIngreso = new SGUsuariosET();
                string sql = string.Format("SELECT US.IdUsuario, US.NomUsu, US.UsuCrea, US.FechaCrea,  " +
                " US.FechaMod, US.UsuMod, US.password, US.FechaUltIngreso, US.Anulado, US.IdRol, TE.IdTercero " +
                " FROM SGUsuarios US " +
                " INNER JOIN ADTerceros TE ON US.IdUsuario = TE.IdUsuario " +
                " WHERE NomUsu = '{0}' and password = '{1}'", nomUsu, pass);
                DataTable dt = new DataTable();
                dt = Clases.ClFunciones.Consultar(sql, Clases.ClConexion.clConexion.Conexion);
                if (dt.Rows.Count <= 0)
                    return null;
                DataRow fila = dt.Rows[0];
                UsuarioIngreso.IdUsuario = Convert.ToInt32(fila["IdUsuario"]);
                UsuarioIngreso.NomUsu = Convert.ToString(fila["NomUsu"]);
                UsuarioIngreso.UsuCrea = Convert.ToInt32(fila["UsuCrea"]);
                UsuarioIngreso.FechaCrea = Convert.ToDateTime(fila["FechaCrea"]);
                UsuarioIngreso.FechaMod = (fila["FechaMod"] != DBNull.Value) ? Convert.ToDateTime(fila["FechaMod"]) : new DateTime();
                UsuarioIngreso.UsuMod = (fila["UsuMod"] != DBNull.Value) ? Convert.ToInt32(fila["UsuMod"]) : -1;
                UsuarioIngreso.password = Convert.ToString(fila["password"]);
                UsuarioIngreso.FechaUltIngreso = (fila["FechaUltIngreso"] != DBNull.Value) ? Convert.ToDateTime(fila["FechaUltIngreso"]) : new DateTime();
                UsuarioIngreso.Anulado = Convert.ToBoolean(fila["Anulado"]);
                UsuarioIngreso.IdRol = Convert.ToInt32(fila["IdRol"]);
                UsuarioIngreso.IdTercero = Convert.ToInt64(fila["IdTercero"]);
                RegistrarIngreso(UsuarioIngreso.IdUsuario, ClConexion.clConexion.Conexion);
                return UsuarioIngreso;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void RegistrarIngreso(int IdUsuario, SqlConnection con)
        {
            try
            {
                string sql = string.Format("UPDATE [dbo].[SGUsuarios] " +
                " SET [FechaUltIngreso] = '{0:dd/MM/yyyy HH:mm:ss}'" +
                " WHERE IdUsuario = {1}",
                DateTime.Now, IdUsuario);
                string res = ClFunciones.EjecutaComando(sql, con);
               
            }
            catch (Exception ex)
            {
            }
        }
    }
}
