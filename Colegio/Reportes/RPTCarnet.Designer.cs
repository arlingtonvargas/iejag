﻿namespace Colegio.Reportes
{
    partial class RPTCarnet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RPTCarnet));
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle1 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle2 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle3 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle4 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.pcbFoto = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNomRector = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrShape3 = new DevExpress.XtraReports.UI.XRShape();
            this.lblAño = new DevExpress.XtraReports.UI.XRLabel();
            this.xrShape4 = new DevExpress.XtraReports.UI.XRShape();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNombres = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblApellidos = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGrado = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblRh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSede = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblEps = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrShape1 = new DevExpress.XtraReports.UI.XRShape();
            this.xrShape2 = new DevExpress.XtraReports.UI.XRShape();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.pcbFoto,
            this.xrPictureBox1,
            this.xrLabel2,
            this.lblNomRector,
            this.xrLabel1,
            this.xrShape3,
            this.lblAño,
            this.xrShape4,
            this.xrTable1,
            this.xrLabel3,
            this.xrPictureBox2,
            this.xrShape1,
            this.xrShape2});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 645.5834F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(1184.833F, 403.5266F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(272.5208F, 67.83536F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Rector(a)";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // pcbFoto
            // 
            this.pcbFoto.Dpi = 254F;
            this.pcbFoto.LocationFloat = new DevExpress.Utils.PointFloat(582.9583F, 260.731F);
            this.pcbFoto.Name = "pcbFoto";
            this.pcbFoto.SizeF = new System.Drawing.SizeF(254F, 293.6051F);
            this.pcbFoto.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Dpi = 254F;
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(25.00001F, 64.39578F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(161.0176F, 162.6671F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Segoe UI", 10.25F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(909.6667F, 86.20663F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(804.3331F, 149.8562F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Este Carnet es personal e intransferible, y acredita al portador como estudiante " +
    "y miembro de la institución educativa:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNomRector
            // 
            this.lblNomRector.CanGrow = false;
            this.lblNomRector.Dpi = 254F;
            this.lblNomRector.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblNomRector.LocationFloat = new DevExpress.Utils.PointFloat(909.6667F, 343.8404F);
            this.lblNomRector.Name = "lblNomRector";
            this.lblNomRector.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblNomRector.SizeF = new System.Drawing.SizeF(804.3332F, 59.68622F);
            this.lblNomRector.StylePriority.UseFont = false;
            this.lblNomRector.StylePriority.UseTextAlignment = false;
            this.lblNomRector.Text = "[Rector]";
            this.lblNomRector.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Segoe UI", 12.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.ForeColor = System.Drawing.Color.White;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(163.4811F, 76.62331F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(682.4772F, 131.1237F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseForeColor = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Institución Educativa                    José Antonio Galán";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrShape3
            // 
            this.xrShape3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.xrShape3.BorderWidth = 1F;
            this.xrShape3.Dpi = 254F;
            this.xrShape3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.xrShape3.LineWidth = 3;
            this.xrShape3.LocationFloat = new DevExpress.Utils.PointFloat(576.0833F, 252.9379F);
            this.xrShape3.Name = "xrShape3";
            shapeRectangle1.Fillet = 5;
            this.xrShape3.Shape = shapeRectangle1;
            this.xrShape3.SizeF = new System.Drawing.SizeF(269.8751F, 311.0656F);
            this.xrShape3.StylePriority.UseBorderColor = false;
            this.xrShape3.StylePriority.UseBorderWidth = false;
            this.xrShape3.StylePriority.UseForeColor = false;
            // 
            // lblAño
            // 
            this.lblAño.Dpi = 254F;
            this.lblAño.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAño.LocationFloat = new DevExpress.Utils.PointFloat(909.6667F, 486.5007F);
            this.lblAño.Name = "lblAño";
            this.lblAño.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblAño.SizeF = new System.Drawing.SizeF(804.3332F, 67.83536F);
            this.lblAño.StylePriority.UseFont = false;
            this.lblAño.StylePriority.UseTextAlignment = false;
            this.lblAño.Text = "[Valido hasta]";
            this.lblAño.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrShape4
            // 
            this.xrShape4.BackColor = System.Drawing.Color.Empty;
            this.xrShape4.BorderColor = System.Drawing.Color.Transparent;
            this.xrShape4.BorderWidth = 0F;
            this.xrShape4.Dpi = 254F;
            this.xrShape4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.xrShape4.LineWidth = 0;
            this.xrShape4.LocationFloat = new DevExpress.Utils.PointFloat(19.00005F, 59.39584F);
            this.xrShape4.Name = "xrShape4";
            shapeRectangle2.Fillet = 13;
            this.xrShape4.Shape = shapeRectangle2;
            this.xrShape4.SizeF = new System.Drawing.SizeF(840.0001F, 176.667F);
            this.xrShape4.StylePriority.UseBackColor = false;
            this.xrShape4.StylePriority.UseBorderColor = false;
            this.xrShape4.StylePriority.UseBorderWidth = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(44.27088F, 252.9379F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow2,
            this.xrTableRow6,
            this.xrTableRow5});
            this.xrTable1.SizeF = new System.Drawing.SizeF(522.5834F, 301.3982F);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.lblDoc});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.Text = "Doc :";
            this.xrTableCell7.Weight = 0.765277756418143D;
            // 
            // lblDoc
            // 
            this.lblDoc.CanGrow = false;
            this.lblDoc.Dpi = 254F;
            this.lblDoc.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.lblDoc.Name = "lblDoc";
            this.lblDoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblDoc.StylePriority.UseFont = false;
            this.lblDoc.StylePriority.UsePadding = false;
            this.lblDoc.Weight = 1.234722243581857D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.lblNombres});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "Nombres :";
            this.xrTableCell1.Weight = 0.765277756418143D;
            // 
            // lblNombres
            // 
            this.lblNombres.CanGrow = false;
            this.lblNombres.Dpi = 254F;
            this.lblNombres.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.lblNombres.Name = "lblNombres";
            this.lblNombres.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblNombres.StylePriority.UseFont = false;
            this.lblNombres.Weight = 1.234722243581857D;
            this.lblNombres.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblNombres_BeforePrint);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.lblApellidos});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell5.StylePriority.UseFont = false;
            this.xrTableCell5.StylePriority.UsePadding = false;
            this.xrTableCell5.Text = "Apellidos :";
            this.xrTableCell5.Weight = 0.765277756418143D;
            // 
            // lblApellidos
            // 
            this.lblApellidos.CanGrow = false;
            this.lblApellidos.Dpi = 254F;
            this.lblApellidos.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblApellidos.StylePriority.UseFont = false;
            this.lblApellidos.StylePriority.UsePadding = false;
            this.lblApellidos.Weight = 1.234722243581857D;
            this.lblApellidos.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblNombres_BeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.lblGrado,
            this.xrTableCell6,
            this.lblRh});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.Text = "Grado :";
            this.xrTableCell3.Weight = 0.765277756418143D;
            // 
            // lblGrado
            // 
            this.lblGrado.Dpi = 254F;
            this.lblGrado.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblGrado.Name = "lblGrado";
            this.lblGrado.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblGrado.StylePriority.UseFont = false;
            this.lblGrado.StylePriority.UsePadding = false;
            this.lblGrado.StylePriority.UseTextAlignment = false;
            this.lblGrado.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblGrado.Weight = 0.71476363130370268D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell6.StylePriority.UseFont = false;
            this.xrTableCell6.StylePriority.UsePadding = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "RH:";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 0.26927559330304063D;
            // 
            // lblRh
            // 
            this.lblRh.CanGrow = false;
            this.lblRh.Dpi = 254F;
            this.lblRh.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lblRh.Name = "lblRh";
            this.lblRh.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblRh.StylePriority.UseFont = false;
            this.lblRh.StylePriority.UsePadding = false;
            this.lblRh.StylePriority.UseTextAlignment = false;
            this.lblRh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblRh.Weight = 0.2506830189751138D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.lblSede});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.Text = "Sede :";
            this.xrTableCell11.Weight = 0.765277756418143D;
            // 
            // lblSede
            // 
            this.lblSede.CanGrow = false;
            this.lblSede.Dpi = 254F;
            this.lblSede.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.lblSede.Name = "lblSede";
            this.lblSede.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblSede.StylePriority.UseFont = false;
            this.lblSede.StylePriority.UsePadding = false;
            this.lblSede.Weight = 1.234722243581857D;
            this.lblSede.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblNombres_BeforePrint);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.lblEps});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.Text = "EPS :";
            this.xrTableCell2.Weight = 0.765277756418143D;
            // 
            // lblEps
            // 
            this.lblEps.Dpi = 254F;
            this.lblEps.Font = new System.Drawing.Font("Segoe UI", 9.5F);
            this.lblEps.Name = "lblEps";
            this.lblEps.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblEps.StylePriority.UseFont = false;
            this.lblEps.StylePriority.UsePadding = false;
            this.lblEps.Weight = 1.234722243581857D;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1013.002F, 251.9379F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(640.1439F, 59.68617F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "JOSÉ ANTONIO GALÁN";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Dpi = 254F;
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.LocationFloat = new DevExpress.Utils.PointFloat(909.5189F, 75.62329F);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.SizeF = new System.Drawing.SizeF(804.481F, 492.2308F);
            this.xrPictureBox2.Sizing = DevExpress.XtraPrinting.ImageSizeMode.Squeeze;
            // 
            // xrShape1
            // 
            this.xrShape1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xrShape1.Dpi = 254F;
            this.xrShape1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.xrShape1.LineWidth = 3;
            this.xrShape1.LocationFloat = new DevExpress.Utils.PointFloat(19.00005F, 59.39584F);
            this.xrShape1.Name = "xrShape1";
            shapeRectangle3.Fillet = 5;
            this.xrShape1.Shape = shapeRectangle3;
            this.xrShape1.SizeF = new System.Drawing.SizeF(840F, 520F);
            this.xrShape1.StylePriority.UseBackColor = false;
            this.xrShape1.StylePriority.UseForeColor = false;
            // 
            // xrShape2
            // 
            this.xrShape2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(248)))), ((int)(((byte)(248)))));
            this.xrShape2.Dpi = 254F;
            this.xrShape2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.xrShape2.LineWidth = 3;
            this.xrShape2.LocationFloat = new DevExpress.Utils.PointFloat(885.4799F, 58.39582F);
            this.xrShape2.Name = "xrShape2";
            shapeRectangle4.Fillet = 5;
            this.xrShape2.Shape = shapeRectangle4;
            this.xrShape2.SizeF = new System.Drawing.SizeF(840F, 520F);
            this.xrShape2.StylePriority.UseBackColor = false;
            this.xrShape2.StylePriority.UseForeColor = false;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 254F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 254F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Dpi = 254F;
            this.PageHeader.HeightF = 76.80782F;
            this.PageHeader.Name = "PageHeader";
            // 
            // RPTCarnet
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(214, 206, 254, 254);
            this.PageHeight = 2794;
            this.PageWidth = 2159;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 25F;
            this.Version = "17.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblNomRector;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRShape xrShape3;
        private DevExpress.XtraReports.UI.XRLabel lblAño;
        private DevExpress.XtraReports.UI.XRShape xrShape4;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRShape xrShape1;
        private DevExpress.XtraReports.UI.XRShape xrShape2;
        public DevExpress.XtraReports.UI.XRPictureBox pcbFoto;
        public DevExpress.XtraReports.UI.XRTableCell lblDoc;
        public DevExpress.XtraReports.UI.XRTableCell lblNombres;
        public DevExpress.XtraReports.UI.XRTableCell lblApellidos;
        public DevExpress.XtraReports.UI.XRTableCell lblRh;
        public DevExpress.XtraReports.UI.XRTableCell lblSede;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        public DevExpress.XtraReports.UI.XRTableCell lblGrado;
        public DevExpress.XtraReports.UI.XRTableCell lblEps;
    }
}
