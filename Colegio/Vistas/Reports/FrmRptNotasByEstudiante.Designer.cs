﻿namespace Colegio.Vistas.Reports
{
    partial class FrmRptNotasByEstudiante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRptNotasByEstudiante));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnConsultar = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lkePeriodo = new dll.Controles.ucLabelCombo();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.txtEstudiante = new dll.Controles.ucBusqueda();
            this.txtCurso = new dll.Controles.ucLabelTextBox();
            this.gcNotas = new DevExpress.XtraGrid.GridControl();
            this.gvNotas = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcNotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNotas)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnConsultar);
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(977, 83);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Parametros";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.ImageOptions.Image")));
            this.btnConsultar.Location = new System.Drawing.Point(893, 28);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(75, 49);
            this.btnConsultar.TabIndex = 9;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 242F));
            this.tableLayoutPanel1.Controls.Add(this.lkePeriodo, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtAño, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtEstudiante, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCurso, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(882, 57);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // lkePeriodo
            // 
            this.lkePeriodo.AnchoTitulo = 70;
            this.lkePeriodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lkePeriodo.Location = new System.Drawing.Point(643, 0);
            this.lkePeriodo.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.lkePeriodo.MaximumSize = new System.Drawing.Size(3000, 28);
            this.lkePeriodo.MensajeDeAyuda = null;
            this.lkePeriodo.MinimumSize = new System.Drawing.Size(50, 28);
            this.lkePeriodo.Name = "lkePeriodo";
            this.lkePeriodo.Size = new System.Drawing.Size(236, 28);
            this.lkePeriodo.TabIndex = 3;
            this.lkePeriodo.TextoTitulo = "Periodo :";
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 50;
            this.txtAño.AnchoTitulo = 90;
            this.txtAño.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAño.Enabled = false;
            this.txtAño.Location = new System.Drawing.Point(0, 0);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(288, 28);
            this.txtAño.TabIndex = 1;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtAño.ValorTextBox = "";
            // 
            // txtEstudiante
            // 
            this.txtEstudiante.AnchoTextBox = 140;
            this.txtEstudiante.AnchoTitulo = 80;
            this.txtEstudiante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEstudiante.Location = new System.Drawing.Point(288, 0);
            this.txtEstudiante.Margin = new System.Windows.Forms.Padding(0);
            this.txtEstudiante.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtEstudiante.MensajeDeAyuda = null;
            this.txtEstudiante.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtEstudiante.Name = "txtEstudiante";
            this.txtEstudiante.PermiteSoloNumeros = false;
            this.txtEstudiante.Script = "";
            this.txtEstudiante.Size = new System.Drawing.Size(352, 28);
            this.txtEstudiante.TabIndex = 2;
            this.txtEstudiante.TextoTitulo = "Estudiante :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtEstudiante.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtEstudiante.ValorTextBox = "";
            this.txtEstudiante.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtEstudiante_SaleControl);
            // 
            // txtCurso
            // 
            this.txtCurso.AnchoTitulo = 87;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCurso, 2);
            this.txtCurso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCurso.Enabled = false;
            this.txtCurso.Location = new System.Drawing.Point(3, 31);
            this.txtCurso.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCurso.MensajeDeAyuda = null;
            this.txtCurso.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.PermiteSoloNumeros = false;
            this.txtCurso.Size = new System.Drawing.Size(634, 24);
            this.txtCurso.TabIndex = 4;
            this.txtCurso.TextoTitulo = "Curso :";
            this.txtCurso.ValorTextBox = "";
            // 
            // gcNotas
            // 
            this.gcNotas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcNotas.Location = new System.Drawing.Point(12, 101);
            this.gcNotas.MainView = this.gvNotas;
            this.gcNotas.Name = "gcNotas";
            this.gcNotas.Size = new System.Drawing.Size(977, 338);
            this.gcNotas.TabIndex = 2;
            this.gcNotas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvNotas});
            // 
            // gvNotas
            // 
            this.gvNotas.GridControl = this.gcNotas;
            this.gvNotas.Name = "gvNotas";
            // 
            // FrmRptNotasByEstudiante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 451);
            this.Controls.Add(this.gcNotas);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmRptNotasByEstudiante";
            this.Text = "Notas por Estudiante";
            this.Load += new System.EventHandler(this.FrmRptNotasByEstudiante_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcNotas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNotas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private dll.Controles.ucBusqueda txtAño;
        private dll.Controles.ucBusqueda txtEstudiante;
        private DevExpress.XtraGrid.GridControl gcNotas;
        private DevExpress.XtraGrid.Views.Grid.GridView gvNotas;
        private DevExpress.XtraEditors.SimpleButton btnConsultar;
        private dll.Controles.ucLabelTextBox txtCurso;
        private dll.Controles.ucLabelCombo lkePeriodo;
    }
}