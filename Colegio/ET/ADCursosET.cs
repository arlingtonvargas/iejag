﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADCursosET
    {
        public int Sec { get; set; }
        public string NomCurso { get; set; }
        public long IdTitular { get; set; }
        public int IdAñoLectivo { get; set; }
        public int IdGrado { get; set; }
    }
    public class ADCursosETVista
    {
        public int IdCurso { get; set; }
        public string NomCurso { get; set; }
        public long DocTitular { get; set; }
        public string NomTitular { get; set; }
        public Single Grado { get; set; }
        public string NomGrado { get; set; }
        public int IdAñoLectivo { get; set; }
        public string Año { get; set; }
    }
}
