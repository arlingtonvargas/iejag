﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    class ADMateriasET
    {
    }

    //public class ADMateriasETVista
    // {
    //    public int Sec { get; set; }
    //    public int IdCurso { get; set; }
    //    public int IdMateria { get; set; }
    //    public long IdMaestro { get; set; }
    //    public string Desempeños { get; set; }
    //    public int IH { get; set; }
    //    public string NomMateria { get; set; }

    //}

    public class ADMateriasETVista
    {
        public int Sec { get; set; }
        public int IdCurso { get; set; }
        public string NomCurso { get; set; }
        public int IdMateria { get; set; }
        public string NomMateria { get; set; }
        public long IdMaestro { get; set; }
        public string NomMaestro { get; set; }
        public string Desempeños { get; set; }
        public int IH { get; set; }
        public int IdAñoLectivo { get; set; }
        public string Año { get; set; }

    }
}
