﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace dll.Controles
{
    public partial class ucTextBoxAV : DevExpress.XtraEditors.TextEdit
    {
        private bool _PermiteSoloNumeros;
        private string _MensajeDeAyuda;
        public ucTextBoxAV()
        {
            InitializeComponent();
        }

        private void ucTextBoxAV_Enter(object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt != null)
                {
                    txt.BackColor = Color.LightCyan;
                    txt.SelectionStart = 0;
                    txt.SelectionLength = txt.Text.Length;
                    txt.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                    dll.Common.Class.ClFuncionesdll.BarStaticItemPrincipal.Caption = this.MensajeDeAyuda;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ucTextBoxAV_Leave(object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt != null)
                {
                    txt.BackColor = Color.White;
                    txt.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular);
                    //Txt.BorderStyle = BorderStyle.FixedSingle;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ucTextBoxAV_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Enter)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
                else if (e.KeyChar == (char)Keys.Escape)
                {
                    e.Handled = true;
                    SendKeys.Send("+{TAB}");
                }

                if (PermiteSoloNumeros)
                {
                    if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void ucTextBoxAV_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Up)
                {
                    e.Handled = true;
                    SendKeys.Send("+{TAB}");
                }
                else if (e.KeyCode == Keys.Down)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [Category("Propiedades propias")]
        [Description("Determina si el campo solo recibe numeros.")]
        public bool PermiteSoloNumeros
        {
            get
            {
                return _PermiteSoloNumeros;
            }
            set
            {
                _PermiteSoloNumeros = value;
            }
        }

        [Category("Propiedades propias")]
        [Description("Determina si el campo solo recibe numeros.")]
        public string MensajeDeAyuda
        {
            get
            {
                return _MensajeDeAyuda;
            }
            set
            {
                _MensajeDeAyuda = value;
            }
        }
    }
}
