﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADObservacionEstudianteET
    {
        public int OSId { get; set; }
        public long OSIdEstudiente { get; set; }
        public string OSNomEstudiente { get; set; }
        public int OSIdAño { get; set; }
        public string OSDesAño { get; set; }
        public DateTime OSFechaObservacion { get; set; }
        public string OSObservacion { get; set; }
        public DateTime OSFechaCrea { get; set; }
        public long OSMaestroReistra { get; set; }
        public string OSNomMaestroReistra { get; set; }
        public long OSMaestroReporta { get; set; }
        public string OSNomMaestroReporta { get; set; }
        public bool OSIsPositive { get; set; }
    }

    public class ADObservacionEstudianteList : List<ADObservacionEstudianteET>   { }
}
