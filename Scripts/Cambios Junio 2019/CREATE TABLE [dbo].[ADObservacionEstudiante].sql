
GO

CREATE TABLE [dbo].[ADObservacionEstudiante](
	[OSId] [int] IDENTITY(1,1) NOT NULL,
	[OSIdEstudiente] [bigint] NOT NULL,
	[OSIdAño] [int] NOT NULL,
	[OSFechaObservacion] [datetime] NOT NULL,
	[OSObservacion] [varchar](max) NOT NULL,
	[OSFechaCrea] [datetime] NOT NULL,
	[OSMaestroReistra] [bigint] NOT NULL,
	[OSMaestroReporta] [bigint] NOT NULL,
 CONSTRAINT [PK_ADObservacionEstudiante] PRIMARY KEY CLUSTERED 
(
	[OSId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ADObservacionEstudiante]  WITH CHECK ADD  CONSTRAINT [FK_ADObservacionEstudiante_ADAñoLectivo] FOREIGN KEY([OSIdAño])
REFERENCES [dbo].[ADAñoLectivo] ([Sec])
GO

ALTER TABLE [dbo].[ADObservacionEstudiante] CHECK CONSTRAINT [FK_ADObservacionEstudiante_ADAñoLectivo]
GO

ALTER TABLE [dbo].[ADObservacionEstudiante]  WITH CHECK ADD  CONSTRAINT [FK_ADObservacionEstudiante_ADTerceros] FOREIGN KEY([OSIdEstudiente])
REFERENCES [dbo].[ADTerceros] ([IdTercero])
GO

ALTER TABLE [dbo].[ADObservacionEstudiante] CHECK CONSTRAINT [FK_ADObservacionEstudiante_ADTerceros]
GO

ALTER TABLE [dbo].[ADObservacionEstudiante]  WITH CHECK ADD  CONSTRAINT [FK_ADObservacionEstudiante_ADTerceros1] FOREIGN KEY([OSMaestroReistra])
REFERENCES [dbo].[ADTerceros] ([IdTercero])
GO

ALTER TABLE [dbo].[ADObservacionEstudiante] CHECK CONSTRAINT [FK_ADObservacionEstudiante_ADTerceros1]
GO

ALTER TABLE [dbo].[ADObservacionEstudiante]  WITH CHECK ADD  CONSTRAINT [FK_ADObservacionEstudiante_ADTerceros2] FOREIGN KEY([OSMaestroReporta])
REFERENCES [dbo].[ADTerceros] ([IdTercero])
GO

ALTER TABLE [dbo].[ADObservacionEstudiante] CHECK CONSTRAINT [FK_ADObservacionEstudiante_ADTerceros2]
GO


