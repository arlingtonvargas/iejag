﻿using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;
using Colegio.ET;
using dll.Common.ET;

namespace Colegio.Vistas
{
    public partial class FrmCursos : FrmBase
    {
        ClTercero clTercero = new ClTercero();
        ClGrados clGrados = new ClGrados();
        ClCursos clCursos = new ClCursos();
        ClAñoLectivo clAñoLectivo = new ClAñoLectivo();

        int SecCurso = -1;
        bool EstaActualizando = false;


        public FrmCursos()
        {
            InitializeComponent();
        }
        

        private void FrmCursos_Load(object sender, EventArgs e)
        {
            LlenarControlesBusqueda();
            CreaGrillaCursos();
            LlenaGrillaCursos();
            txtTitular.Focus();
        }

        private void LlenarControlesBusqueda()
        {
            try
            {
                DataTable dt = clTercero.GetTercerosTipoTer(1);
                if (dt.Rows.Count>0)
                {
                    TituloColsBusquedaET TituloColsBusqueda = new TituloColsBusquedaET() {Codigo ="Documento", Descripcion="Nombre completo" };
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtTitular.TituloColsBusqueda = TituloColsBusqueda;
                    txtTitular.DataTable = dt;
                }
                dt = clGrados.GetGrados();
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtGrado.DataTable = dt;
                }
                dt = clAñoLectivo.GetAñosLectivos();
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.DataTable = dt;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void LlenaGrillaCursos()
        {
            try
            {
                List<ADCursosETVista> res = clCursos.GetCursosVista();
                gcCursos.DataSource = res;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        private void CreaGrillaCursos()
        {
            try
            {
                gvCursos = GrillaDevExpress.CrearGrilla(false, true);
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("IdCurso", "Id Curso", ancho: 60));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("NomCurso", "Nombre", ancho: 100));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("DocTitular", "Doc. Titular", ancho: 100));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("NomTitular", "Titular", ancho: 250));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("Grado", "Grado", ancho: 70));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("NomGrado", "Nom. Grado", ancho: 120));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("IdAñoLectivo", "", visible:false));
                gvCursos.Columns.Add(GrillaDevExpress.CrearColumna("Año", "Año lectivo", ancho: 50));
                gvCursos.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvCursos.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvCursos.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvCursos.OptionsCustomization.AllowColumnResizing = true;
                gcCursos.MainView = gvCursos;
                this.gvCursos.Click += new System.EventHandler(this.gvCursos_Click);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void LimpiarCampos()
        {
            txtTitular.ValorTextBox = "";
            txtNombre.ValorTextBox = "";
            txtGrado.ValorTextBox = "";
            txtAño.ValorTextBox = "";
            txtGrado.Enabled = true;
            txtAño.Enabled = true;
            SecCurso = -1;
            EstaActualizando = false;
            txtTitular.Focus();

        }

        private bool ValidaCampos()
        {
            try
            {
                if (txtTitular.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el titular del curso.");
                    txtTitular.Focus();
                    return false;
                }
                else if (txtGrado.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el grado del curso.");
                    txtGrado.Focus();
                    return false;
                }
                else if (txtAño.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el año lectivo del curso.");
                    txtAño.Focus();
                    return false;
                }
                else if (txtNombre.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el nombre del curso.");
                    txtNombre.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        private void gvCursos_Click(object sender, EventArgs e)
        {
            int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
            if (numFila>-1)
            {
                txtGrado.Enabled = true;
                txtAño.Enabled = true;
                txtTitular.ValorTextBox = gvCursos.GetRowCellValue(numFila, "DocTitular").ToString();
                txtNombre.ValorTextBox = gvCursos.GetRowCellValue(numFila, "NomCurso").ToString();
                txtAño.ValorTextBox = gvCursos.GetRowCellValue(numFila, "IdAñoLectivo").ToString();
                txtGrado.ValorTextBox = gvCursos.GetRowCellValue(numFila, "Grado").ToString();
                SecCurso = Convert.ToInt32(gvCursos.GetRowCellValue(numFila, "IdCurso"));
                if (clCursos.CursoTieneMatriculas(SecCurso)) { txtGrado.Enabled = false; txtAño.Enabled = false; }
                txtTitular.Focus(); 
                EstaActualizando = true;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
            LlenarControlesBusqueda();
            LlenaGrillaCursos();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidaCampos()) return;
                ADCursosET mCurso = new ADCursosET()
                {
                    NomCurso = txtNombre.ValorTextBox,
                    IdTitular = Convert.ToInt64(txtTitular.ValorTextBox),
                    IdAñoLectivo = Convert.ToInt32(txtAño.ValorTextBox),
                    IdGrado = Convert.ToInt32(txtGrado.ValorTextBox),
                    Sec = SecCurso
                };
                if (EstaActualizando)
                {
                    if(SecCurso == -1)
                    {
                        ClFunciones.msgError("Cargue nuevamente el curso.");
                        LimpiarCampos();
                        return;
                    }
                    if (clCursos.UpdateCurso(mCurso))
                    {
                        ClFunciones.msgExitoso("Curso actualizado correctamente.");
                        LimpiarCampos();
                        LlenaGrillaCursos();
                        LlenarControlesBusqueda();
                    }
                    else
                    {
                        ClFunciones.msgError("Lo sentimos, ha ocurrido un error.");
                        return;
                    }
                }
                else
                {
                    if (clCursos.InsertCurso(mCurso))
                    {
                        ClFunciones.msgExitoso("Curso insertado correctamente.");
                        LimpiarCampos();
                        LlenaGrillaCursos();
                        LlenarControlesBusqueda();
                    }
                    else
                        ClFunciones.msgError("Lo sentimos, ha ocurrido un error.");

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
