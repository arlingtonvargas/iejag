﻿namespace Colegio.Vistas
{
    partial class FrmConfirmacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.ucBtnAceptarAV1 = new dll.Controles.ucBtnAceptarAV();
            this.ucBtnCancelarAV1 = new dll.Controles.ucBtnCancelarAV();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(550, 44);
            this.lblTitulo.TabIndex = 18;
            this.lblTitulo.Text = "Mensaje IEJAG | Sistema de Gestión";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMensaje
            // 
            this.lblMensaje.Font = new System.Drawing.Font("Segoe UI", 11F);
            this.lblMensaje.Location = new System.Drawing.Point(31, 72);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(492, 74);
            this.lblMensaje.TabIndex = 17;
            this.lblMensaje.Text = "msj";
            // 
            // ucBtnAceptarAV1
            // 
            this.ucBtnAceptarAV1.Location = new System.Drawing.Point(155, 173);
            this.ucBtnAceptarAV1.Name = "ucBtnAceptarAV1";
            this.ucBtnAceptarAV1.Size = new System.Drawing.Size(105, 40);
            this.ucBtnAceptarAV1.TabIndex = 19;
            this.ucBtnAceptarAV1.OnUserControlButtonClicked += new dll.Controles.ucBtnAceptarAV.ButtonClickedEventHandler(this.ucBtnAceptarAV1_OnUserControlButtonClicked);
            // 
            // ucBtnCancelarAV1
            // 
            this.ucBtnCancelarAV1.Location = new System.Drawing.Point(299, 173);
            this.ucBtnCancelarAV1.Name = "ucBtnCancelarAV1";
            this.ucBtnCancelarAV1.Size = new System.Drawing.Size(105, 40);
            this.ucBtnCancelarAV1.TabIndex = 20;
            this.ucBtnCancelarAV1.OnUserControlButtonClicked += new dll.Controles.ucBtnCancelarAV.ButtonClickedEventHandler(this.ucBtnCancelarAV1_OnUserControlButtonClicked);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(0, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(550, 203);
            this.panel1.TabIndex = 21;
            // 
            // FrmConfirmacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 247);
            this.Controls.Add(this.ucBtnCancelarAV1);
            this.Controls.Add(this.ucBtnAceptarAV1);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmConfirmacion";
            this.Text = "Confirmación";
            this.Load += new System.EventHandler(this.FrmConfirmacion_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblTitulo;
        public System.Windows.Forms.Label lblMensaje;
        private dll.Controles.ucBtnAceptarAV ucBtnAceptarAV1;
        private dll.Controles.ucBtnCancelarAV ucBtnCancelarAV1;
        private System.Windows.Forms.Panel panel1;
    }
}