﻿namespace Colegio.Vistas
{
    partial class FrmMatricula
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMatricula));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET3 = new dll.Common.ET.TituloColsBusquedaET();
            this.grcInfo = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtEstudianteB = new dll.Controles.ucBusqueda();
            this.lkeJornada = new dll.Controles.ucLabelCombo();
            this.txtCursos = new dll.Controles.ucBusqueda();
            this.lkeSede = new dll.Controles.ucLabelCombo();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.ucLabelCombo1 = new dll.Controles.ucLabelCombo();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcMatriculas = new DevExpress.XtraGrid.GridControl();
            this.gvMatriculas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).BeginInit();
            this.grcInfo.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcMatriculas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMatriculas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // grcInfo
            // 
            this.grcInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grcInfo.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grcInfo.AppearanceCaption.Options.UseFont = true;
            this.grcInfo.Controls.Add(this.tableLayoutPanel1);
            this.grcInfo.Location = new System.Drawing.Point(12, 12);
            this.grcInfo.Name = "grcInfo";
            this.grcInfo.Size = new System.Drawing.Size(858, 107);
            this.grcInfo.TabIndex = 26;
            this.grcInfo.Text = "Información básica de la Matrícula";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.14173F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.85827F));
            this.tableLayoutPanel1.Controls.Add(this.txtEstudianteB, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lkeJornada, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtCursos, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lkeSede, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtAño, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ucLabelCombo1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 22);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(854, 83);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // txtEstudianteB
            // 
            this.txtEstudianteB.AnchoTextBox = 150;
            this.txtEstudianteB.AnchoTitulo = 90;
            this.txtEstudianteB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEstudianteB.Location = new System.Drawing.Point(0, 0);
            this.txtEstudianteB.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.txtEstudianteB.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtEstudianteB.MensajeDeAyuda = null;
            this.txtEstudianteB.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtEstudianteB.Name = "txtEstudianteB";
            this.txtEstudianteB.PermiteSoloNumeros = true;
            this.txtEstudianteB.Script = "";
            this.txtEstudianteB.Size = new System.Drawing.Size(562, 28);
            this.txtEstudianteB.TabIndex = 1;
            this.txtEstudianteB.TextoTitulo = "Estudiante :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtEstudianteB.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtEstudianteB.ValorTextBox = "";
            // 
            // lkeJornada
            // 
            this.lkeJornada.AnchoTitulo = 65;
            this.lkeJornada.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lkeJornada.Location = new System.Drawing.Point(567, 55);
            this.lkeJornada.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.lkeJornada.MaximumSize = new System.Drawing.Size(3000, 28);
            this.lkeJornada.MensajeDeAyuda = null;
            this.lkeJornada.MinimumSize = new System.Drawing.Size(50, 28);
            this.lkeJornada.Name = "lkeJornada";
            this.lkeJornada.Size = new System.Drawing.Size(284, 28);
            this.lkeJornada.TabIndex = 5;
            this.lkeJornada.TextoTitulo = "Jornada :";
            // 
            // txtCursos
            // 
            this.txtCursos.AnchoTextBox = 150;
            this.txtCursos.AnchoTitulo = 90;
            this.txtCursos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCursos.Location = new System.Drawing.Point(0, 54);
            this.txtCursos.Margin = new System.Windows.Forms.Padding(0);
            this.txtCursos.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCursos.MensajeDeAyuda = null;
            this.txtCursos.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCursos.Name = "txtCursos";
            this.txtCursos.PermiteSoloNumeros = true;
            this.txtCursos.Script = "";
            this.txtCursos.Size = new System.Drawing.Size(564, 28);
            this.txtCursos.TabIndex = 3;
            this.txtCursos.TextoTitulo = "Curso :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtCursos.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtCursos.ValorTextBox = "";
            // 
            // lkeSede
            // 
            this.lkeSede.AnchoTitulo = 65;
            this.lkeSede.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lkeSede.Location = new System.Drawing.Point(567, 27);
            this.lkeSede.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.lkeSede.MaximumSize = new System.Drawing.Size(3000, 28);
            this.lkeSede.MensajeDeAyuda = null;
            this.lkeSede.MinimumSize = new System.Drawing.Size(50, 28);
            this.lkeSede.Name = "lkeSede";
            this.lkeSede.Size = new System.Drawing.Size(284, 28);
            this.lkeSede.TabIndex = 4;
            this.lkeSede.TextoTitulo = "Sede :";
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 150;
            this.txtAño.AnchoTitulo = 90;
            this.txtAño.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAño.Enabled = false;
            this.txtAño.Location = new System.Drawing.Point(0, 27);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = true;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(564, 28);
            this.txtAño.TabIndex = 2;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET3.Codigo = "Código";
            tituloColsBusquedaET3.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET3;
            this.txtAño.ValorTextBox = "";
            this.txtAño.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtAño_SaleControl);
            // 
            // ucLabelCombo1
            // 
            this.ucLabelCombo1.AnchoTitulo = 65;
            this.ucLabelCombo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucLabelCombo1.Location = new System.Drawing.Point(567, 0);
            this.ucLabelCombo1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ucLabelCombo1.MaximumSize = new System.Drawing.Size(3000, 28);
            this.ucLabelCombo1.MensajeDeAyuda = null;
            this.ucLabelCombo1.MinimumSize = new System.Drawing.Size(50, 28);
            this.ucLabelCombo1.Name = "ucLabelCombo1";
            this.ucLabelCombo1.Size = new System.Drawing.Size(284, 28);
            this.ucLabelCombo1.TabIndex = 6;
            this.ucLabelCombo1.TextoTitulo = "Internado :";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Appearance.Options.UseFont = true;
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(99, 25);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(85, 37);
            this.btnRefresh.TabIndex = 66;
            this.btnRefresh.Text = "Refrescar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(99, 67);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(85, 37);
            this.btnSalir.TabIndex = 18;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(7, 25);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(85, 37);
            this.btnGuardar.TabIndex = 16;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(7, 67);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(85, 37);
            this.btnLimpiar.TabIndex = 17;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.btnGuardar);
            this.groupControl1.Controls.Add(this.btnRefresh);
            this.groupControl1.Controls.Add(this.btnLimpiar);
            this.groupControl1.Controls.Add(this.btnSalir);
            this.groupControl1.Location = new System.Drawing.Point(876, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(189, 107);
            this.groupControl1.TabIndex = 67;
            this.groupControl1.Text = "Opciones";
            // 
            // gcMatriculas
            // 
            this.gcMatriculas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcMatriculas.Location = new System.Drawing.Point(12, 125);
            this.gcMatriculas.MainView = this.gvMatriculas;
            this.gcMatriculas.Name = "gcMatriculas";
            this.gcMatriculas.Size = new System.Drawing.Size(1053, 467);
            this.gcMatriculas.TabIndex = 68;
            this.gcMatriculas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMatriculas});
            // 
            // gvMatriculas
            // 
            this.gvMatriculas.GridControl = this.gcMatriculas;
            this.gvMatriculas.Name = "gvMatriculas";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertGalleryImage("deletelist2_16x16.png", "office2013/actions/deletelist2_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("office2013/actions/deletelist2_16x16.png"), 0);
            this.imageCollection1.Images.SetKeyName(0, "deletelist2_16x16.png");
            // 
            // FrmMatricula
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1077, 604);
            this.Controls.Add(this.gcMatriculas);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.grcInfo);
            this.Name = "FrmMatricula";
            this.Text = "Matricula";
            this.Load += new System.EventHandler(this.FrmMatricula_Load);
            this.SizeChanged += new System.EventHandler(this.FrmMatricula_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).EndInit();
            this.grcInfo.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcMatriculas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMatriculas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.GroupControl grcInfo;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private dll.Controles.ucBusqueda txtEstudianteB;
        private dll.Controles.ucBusqueda txtAño;
        private dll.Controles.ucBusqueda txtCursos;
        private DevExpress.XtraGrid.GridControl gcMatriculas;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMatriculas;
        private dll.Controles.ucLabelCombo lkeSede;
        private dll.Controles.ucLabelCombo lkeJornada;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private dll.Controles.ucLabelCombo ucLabelCombo1;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}