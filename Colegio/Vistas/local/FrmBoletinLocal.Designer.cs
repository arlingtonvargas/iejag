﻿namespace Colegio.Vistas
{
    partial class FrmBoletinLocal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBoletinLocal));
            this.gcNotas = new DevExpress.XtraGrid.GridControl();
            this.gvNotas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnCargar = new DevExpress.XtraEditors.SimpleButton();
            this.btnGenerar = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.cbxEstudientes = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTipoDoc = new dll.Controles.ucLabelAV();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ucLabelAV1 = new dll.Controles.ucLabelAV();
            this.ucLabelAV2 = new dll.Controles.ucLabelAV();
            this.txtNivel = new DevExpress.XtraEditors.TextEdit();
            this.txtProm = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxEstudientes.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNivel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProm.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcNotas
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gcNotas, 5);
            this.gcNotas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcNotas.Location = new System.Drawing.Point(23, 88);
            this.gcNotas.MainView = this.gvNotas;
            this.gcNotas.Name = "gcNotas";
            this.gcNotas.Size = new System.Drawing.Size(922, 349);
            this.gcNotas.TabIndex = 0;
            this.gcNotas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvNotas});
            // 
            // gvNotas
            // 
            this.gvNotas.GridControl = this.gcNotas;
            this.gvNotas.Name = "gvNotas";
            // 
            // btnCargar
            // 
            this.btnCargar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCargar.Appearance.Options.UseFont = true;
            this.btnCargar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCargar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCargar.ImageOptions.Image")));
            this.btnCargar.Location = new System.Drawing.Point(591, 23);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(114, 59);
            this.btnCargar.TabIndex = 1;
            this.btnCargar.Text = "Cargar Notas";
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // btnGenerar
            // 
            this.btnGenerar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerar.Appearance.Options.UseFont = true;
            this.btnGenerar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGenerar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerar.ImageOptions.Image")));
            this.btnGenerar.Location = new System.Drawing.Point(711, 23);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(114, 59);
            this.btnGenerar.TabIndex = 2;
            this.btnGenerar.Text = "Generar\r\nBoletin";
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(831, 23);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(114, 59);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // cbxEstudientes
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.cbxEstudientes, 3);
            this.cbxEstudientes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxEstudientes.Location = new System.Drawing.Point(103, 3);
            this.cbxEstudientes.Name = "cbxEstudientes";
            this.cbxEstudientes.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxEstudientes.Properties.Appearance.Options.UseFont = true;
            this.cbxEstudientes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxEstudientes.Properties.NullText = "Seleccione";
            this.cbxEstudientes.Properties.NullValuePrompt = "Seleccione";
            this.cbxEstudientes.Size = new System.Drawing.Size(456, 22);
            this.cbxEstudientes.TabIndex = 62;
            // 
            // lblTipoDoc
            // 
            this.lblTipoDoc.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoDoc.Location = new System.Drawing.Point(3, 3);
            this.lblTipoDoc.Name = "lblTipoDoc";
            this.lblTipoDoc.Size = new System.Drawing.Size(94, 22);
            this.lblTipoDoc.TabIndex = 63;
            this.lblTipoDoc.Text = "Estudiante :";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.gcNotas, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnSalir, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnGenerar, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCargar, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(20);
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(968, 460);
            this.tableLayoutPanel1.TabIndex = 64;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.ucLabelAV1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblTipoDoc, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.cbxEstudientes, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.ucLabelAV2, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtNivel, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtProm, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(23, 23);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(562, 59);
            this.tableLayoutPanel2.TabIndex = 69;
            // 
            // ucLabelAV1
            // 
            this.ucLabelAV1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.ucLabelAV1.Location = new System.Drawing.Point(3, 32);
            this.ucLabelAV1.Name = "ucLabelAV1";
            this.ucLabelAV1.Size = new System.Drawing.Size(94, 22);
            this.ucLabelAV1.TabIndex = 65;
            this.ucLabelAV1.Text = "Prom. General :";
            // 
            // ucLabelAV2
            // 
            this.ucLabelAV2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.ucLabelAV2.Location = new System.Drawing.Point(153, 32);
            this.ucLabelAV2.Name = "ucLabelAV2";
            this.ucLabelAV2.Size = new System.Drawing.Size(114, 22);
            this.ucLabelAV2.TabIndex = 67;
            this.ucLabelAV2.Text = "Nivel desempeño :";
            // 
            // txtNivel
            // 
            this.txtNivel.Location = new System.Drawing.Point(273, 32);
            this.txtNivel.Name = "txtNivel";
            this.txtNivel.Size = new System.Drawing.Size(100, 20);
            this.txtNivel.TabIndex = 68;
            // 
            // txtProm
            // 
            this.txtProm.Location = new System.Drawing.Point(103, 32);
            this.txtProm.Name = "txtProm";
            this.txtProm.Size = new System.Drawing.Size(44, 20);
            this.txtProm.TabIndex = 68;
            // 
            // FrmBoletinLocal
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 460);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FrmBoletinLocal";
            this.Text = "Boletin local";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmBoletinLocal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcNotas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNotas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxEstudientes.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNivel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtProm.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcNotas;
        private DevExpress.XtraGrid.Views.Grid.GridView gvNotas;
        private DevExpress.XtraEditors.SimpleButton btnCargar;
        private DevExpress.XtraEditors.SimpleButton btnGenerar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        public DevExpress.XtraEditors.LookUpEdit cbxEstudientes;
        private dll.Controles.ucLabelAV lblTipoDoc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private dll.Controles.ucLabelAV ucLabelAV1;
        private dll.Controles.ucLabelAV ucLabelAV2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.TextEdit txtNivel;
        private DevExpress.XtraEditors.TextEdit txtProm;
    }
}