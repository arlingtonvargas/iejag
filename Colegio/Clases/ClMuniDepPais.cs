﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClMuniDepPais
    {
        public  DataTable GetPaises()
        {
            try
            {
                string sql = "SELECT PS.CodPais, PS.NomPais, PS.CodIso FROM GEPais PS";
                DataTable dt = ClFunciones.Consultar(sql, ClFunciones.f.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetDepartamentosByPais(string idPais)
        {
            try
            {
                string sql = string.Format("SELECT DP.Pais, DP.CodDpto, DP.NomDpto FROM GEDepartamento DP WHERE DP.Pais = '{0}'", idPais);
                DataTable dt = ClFunciones.Consultar(sql, ClFunciones.f.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataTable GetMuniByDepto(string idDepto)
        {
            try
            {
                string sql = string.Format("SELECT MN.IdMunicipio, MN.CodMunicipio, MN.NombreMunicipio FROM GEMunicipio MN WHERE MN.Departamento = '{0}'", idDepto);
                DataTable dt = ClFunciones.Consultar(sql, ClFunciones.f.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
