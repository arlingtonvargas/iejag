﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Colegio.Clases;
using Colegio.ET;

namespace Colegio.Vistas
{
    public partial class FrmGeneraCarnet : DevExpress.XtraEditors.XtraForm
    {

        ClCursos clCursos = new ClCursos();
        ClAñoLectivo clAño = new ClAñoLectivo();
        ClTercero mClTercero = new ClTercero();

        public FrmGeneraCarnet()
        {
            InitializeComponent();
        }

        private void FrmGeneraCarnet_Load(object sender, EventArgs e)
        {
            LlenarCamposBusq();
        }

        private void rgOpciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtEstudiante.Enabled = false;
            txtCurso.Enabled = false;
            txtEstudiante.ValorTextBox = "";
            txtCurso.ValorTextBox = "";
            switch (rgOpciones.SelectedIndex)
            {
                case 0:
                    txtEstudiante.Enabled = true;
                    break;

                case 1:
                    txtCurso.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int wIdAño = Convert.ToInt32(txtAño.ValorTextBox);
                int? wIdCurso = null;
                long? wIdEst = null;


                switch (rgOpciones.SelectedIndex)
                {
                    case 0:
                        if (txtEstudiante.ValorTextBox == "")
                        {
                            ClFunciones.msgError("Debe seleccionar un estudiante.");
                            txtEstudiante.Focus();
                            return;
                        }else
                        {
                            wIdEst = Convert.ToInt64(txtEstudiante.ValorTextBox);
                        }
                        break;

                    case 1:
                        if (txtCurso.ValorTextBox == "")
                        {
                            ClFunciones.msgError("Debe seleccionar un curso.");
                            txtCurso.Focus();
                            return;
                        }
                        else
                        {
                            wIdCurso = Convert.ToInt32(txtCurso.ValorTextBox);
                        }
                        break;

                    default:
                        break;
                }

                aDTercerosCarnetETListBindingSource.DataSource = mClTercero.GetEstudiantesPrintCarnet(wIdAño, wIdCurso, wIdEst);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                if (aDTercerosCarnetETListBindingSource.Count <= 0)
                    return;

                Reportes.RPTCarnet RPT = new Reportes.RPTCarnet();
                RPT.DataSource = aDTercerosCarnetETListBindingSource.DataSource;

                RPT.lblDoc.DataBindings.Add("Text", null, "IdTercero");
                RPT.lblNombres.DataBindings.Add("Text", null, "Pnombre");
                RPT.lblApellidos.DataBindings.Add("Text", null, "Papellido");
                RPT.lblGrado.DataBindings.Add("Text", null, "Grado");
                RPT.lblSede.DataBindings.Add("Text", null, "Sede");
                RPT.lblRh.DataBindings.Add("Text", null, "RH");
                RPT.lblEps.DataBindings.Add("Text", null, "EPS");
                RPT.pcbFoto.DataBindings.Add("Image", null, "Foto");
                FrmVistaPrevia report = new FrmVistaPrevia();
                report.document.DocumentSource = RPT;
                report.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarCamposBusq()
        {
            try
            {

                DataTable dt = clAño.GetAñosLectivos(); 
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.DataTable = dt;
                    txtAño.ValorTextBox = ClFunciones.wADDatosInstitucion[0].IdAñoActual.ToString();
                }


                if (txtAño.ValorTextBox != "")
                {
                    dt = clCursos.GetCursosByAño(Convert.ToInt32(txtAño.ValorTextBox));
                    if (dt.Rows.Count > 0)
                    {
                        dt.Columns[0].ColumnName = "Codigo";
                        dt.Columns[1].ColumnName = "Descripcion";
                    }
                    txtCurso.DataTable = dt;                    
                }
                dt = mClTercero.GetEstudiantesMatriculadosByAño(Convert.ToInt32(txtAño.ValorTextBox));
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtEstudiante.DataTable = dt;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}