﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class RPTEstudiantesCursoET
    {
        public long Posicion { get; set; }
        public long Documento { get; set; }
        public string NomCompleto { get; set; }
        public string Curso { get; set; }
        public string Sede { get; set; }
        public string Jornada { get; set; }
        public decimal Promedio { get; set; }
    }

    public class ListRPTEstudiantesCursoET : List<RPTEstudiantesCursoET>
    {
    }

}
