﻿namespace Colegio.Vistas
{
    partial class FrmEstudiante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEstudiante));
            this.grcInfo = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCorreo = new dll.Controles.ucLabelTextBox();
            this.txtSape = new dll.Controles.ucLabelTextBox();
            this.txtPnom = new dll.Controles.ucLabelTextBox();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.txtDocumento = new dll.Controles.ucLabelTextBox();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAddFoto = new DevExpress.XtraEditors.SimpleButton();
            this.btnQuitaFoto = new DevExpress.XtraEditors.SimpleButton();
            this.btnCam = new DevExpress.XtraEditors.SimpleButton();
            this.txtPape = new dll.Controles.ucLabelTextBox();
            this.txtUsu = new dll.Controles.ucLabelTextBox();
            this.pcbFoto = new System.Windows.Forms.PictureBox();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.txtDir = new dll.Controles.ucLabelTextBox();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.txtTel = new dll.Controles.ucLabelTextBox();
            this.dtpFecNac = new DevExpress.XtraEditors.DateEdit();
            this.txtSnom = new dll.Controles.ucLabelTextBox();
            this.cbxSexo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lblAcudiente = new dll.Controles.ucLabelAV();
            this.cbxMuni = new DevExpress.XtraEditors.LookUpEdit();
            this.lblMuni = new dll.Controles.ucLabelAV();
            this.lblDpto = new dll.Controles.ucLabelAV();
            this.cbxPais = new DevExpress.XtraEditors.LookUpEdit();
            this.cbxDpto = new DevExpress.XtraEditors.LookUpEdit();
            this.lblSexo = new dll.Controles.ucLabelAV();
            this.lblPais = new dll.Controles.ucLabelAV();
            this.lblFecNac = new dll.Controles.ucLabelAV();
            this.cbxTipoDoc = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTipoDoc = new dll.Controles.ucLabelAV();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAcudiente = new DevExpress.XtraEditors.SimpleButton();
            this.txtRh = new dll.Controles.ucLabelTextBox();
            this.txtEps = new dll.Controles.ucLabelTextBox();
            this.gcEstudiantes = new DevExpress.XtraGrid.GridControl();
            this.gvEstudiantes = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).BeginInit();
            this.grcInfo.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFoto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNac.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNac.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSexo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxMuni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPais.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDpto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxTipoDoc.Properties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcEstudiantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEstudiantes)).BeginInit();
            this.SuspendLayout();
            // 
            // grcInfo
            // 
            this.grcInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grcInfo.Controls.Add(this.tableLayoutPanel1);
            this.grcInfo.Location = new System.Drawing.Point(12, 12);
            this.grcInfo.Name = "grcInfo";
            this.grcInfo.Size = new System.Drawing.Size(1139, 191);
            this.grcInfo.TabIndex = 2;
            this.grcInfo.Text = "Información básica del estudiante";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 123F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel1.Controls.Add(this.txtCorreo, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtSape, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtPnom, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnRefresh, 7, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtDocumento, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSalir, 7, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtPape, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtUsu, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.pcbFoto, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnGuardar, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtDir, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.btnLimpiar, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtTel, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.dtpFecNac, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtSnom, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.cbxSexo, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblAcudiente, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbxMuni, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblMuni, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblDpto, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbxPais, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbxDpto, 6, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblSexo, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblPais, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblFecNac, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbxTipoDoc, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblTipoDoc, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 4, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1135, 169);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // txtCorreo
            // 
            this.txtCorreo.AnchoTitulo = 114;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCorreo, 2);
            this.txtCorreo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCorreo.Location = new System.Drawing.Point(126, 87);
            this.txtCorreo.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCorreo.MensajeDeAyuda = null;
            this.txtCorreo.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.PermiteSoloNumeros = false;
            this.txtCorreo.Size = new System.Drawing.Size(299, 24);
            this.txtCorreo.TabIndex = 10;
            this.txtCorreo.TextoTitulo = "Correo :";
            this.txtCorreo.ValorTextBox = "";
            // 
            // txtSape
            // 
            this.txtSape.AnchoTitulo = 117;
            this.tableLayoutPanel1.SetColumnSpan(this.txtSape, 2);
            this.txtSape.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSape.Location = new System.Drawing.Point(739, 31);
            this.txtSape.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtSape.MensajeDeAyuda = null;
            this.txtSape.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtSape.Name = "txtSape";
            this.txtSape.PermiteSoloNumeros = false;
            this.txtSape.Size = new System.Drawing.Size(302, 24);
            this.txtSape.TabIndex = 6;
            this.txtSape.TextoTitulo = "Segundo Apellido :";
            this.txtSape.ValorTextBox = "";
            // 
            // txtPnom
            // 
            this.txtPnom.AnchoTitulo = 117;
            this.tableLayoutPanel1.SetColumnSpan(this.txtPnom, 2);
            this.txtPnom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPnom.Location = new System.Drawing.Point(739, 3);
            this.txtPnom.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPnom.MensajeDeAyuda = null;
            this.txtPnom.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPnom.Name = "txtPnom";
            this.txtPnom.PermiteSoloNumeros = false;
            this.txtPnom.Size = new System.Drawing.Size(302, 24);
            this.txtPnom.TabIndex = 3;
            this.txtPnom.TextoTitulo = "Primer Nombre :";
            this.txtPnom.ValorTextBox = "";
            this.txtPnom.Leave += new System.EventHandler(this.txtPnom_Leave);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Appearance.Options.UseFont = true;
            this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(1047, 143);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(85, 22);
            this.btnRefresh.TabIndex = 66;
            this.btnRefresh.Text = "Refrescar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtDocumento
            // 
            this.txtDocumento.AnchoTitulo = 114;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDocumento, 2);
            this.txtDocumento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDocumento.Location = new System.Drawing.Point(431, 3);
            this.txtDocumento.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDocumento.MensajeDeAyuda = null;
            this.txtDocumento.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.PermiteSoloNumeros = true;
            this.txtDocumento.Size = new System.Drawing.Size(302, 24);
            this.txtDocumento.TabIndex = 2;
            this.txtDocumento.TextoTitulo = "Documento :";
            this.txtDocumento.ValorTextBox = "";
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(1047, 112);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(85, 28);
            this.btnSalir.TabIndex = 18;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.Controls.Add(this.btnAddFoto, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnQuitaFoto, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnCam, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 140);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(123, 28);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // btnAddFoto
            // 
            this.btnAddFoto.AllowFocus = false;
            this.btnAddFoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddFoto.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAddFoto.ImageOptions.Image")));
            this.btnAddFoto.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.btnAddFoto.Location = new System.Drawing.Point(20, 2);
            this.btnAddFoto.Margin = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.btnAddFoto.Name = "btnAddFoto";
            this.btnAddFoto.Size = new System.Drawing.Size(40, 25);
            this.btnAddFoto.TabIndex = 62;
            this.btnAddFoto.Click += new System.EventHandler(this.btnAddFoto_Click);
            // 
            // btnQuitaFoto
            // 
            this.btnQuitaFoto.AllowFocus = false;
            this.btnQuitaFoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnQuitaFoto.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnQuitaFoto.ImageOptions.Image")));
            this.btnQuitaFoto.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.btnQuitaFoto.Location = new System.Drawing.Point(60, 2);
            this.btnQuitaFoto.Margin = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.btnQuitaFoto.Name = "btnQuitaFoto";
            this.btnQuitaFoto.Size = new System.Drawing.Size(40, 25);
            this.btnQuitaFoto.TabIndex = 63;
            this.btnQuitaFoto.Click += new System.EventHandler(this.btnQuitaFoto_Click);
            // 
            // btnCam
            // 
            this.btnCam.AllowFocus = false;
            this.btnCam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCam.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCam.ImageOptions.Image")));
            this.btnCam.ImageOptions.ImageToTextAlignment = DevExpress.XtraEditors.ImageAlignToText.BottomCenter;
            this.btnCam.Location = new System.Drawing.Point(10, 2);
            this.btnCam.Margin = new System.Windows.Forms.Padding(0, 2, 0, 1);
            this.btnCam.Name = "btnCam";
            this.btnCam.Size = new System.Drawing.Size(10, 25);
            this.btnCam.TabIndex = 63;
            this.btnCam.Click += new System.EventHandler(this.btnCam_Click);
            // 
            // txtPape
            // 
            this.txtPape.AnchoTitulo = 114;
            this.tableLayoutPanel1.SetColumnSpan(this.txtPape, 2);
            this.txtPape.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPape.Location = new System.Drawing.Point(431, 31);
            this.txtPape.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPape.MensajeDeAyuda = null;
            this.txtPape.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPape.Name = "txtPape";
            this.txtPape.PermiteSoloNumeros = false;
            this.txtPape.Size = new System.Drawing.Size(302, 24);
            this.txtPape.TabIndex = 5;
            this.txtPape.TextoTitulo = "Primer Apellido :";
            this.txtPape.ValorTextBox = "";
            this.txtPape.Leave += new System.EventHandler(this.txtPape_Leave);
            // 
            // txtUsu
            // 
            this.txtUsu.AnchoTitulo = 114;
            this.tableLayoutPanel1.SetColumnSpan(this.txtUsu, 2);
            this.txtUsu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUsu.Enabled = false;
            this.txtUsu.Location = new System.Drawing.Point(431, 59);
            this.txtUsu.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtUsu.MensajeDeAyuda = null;
            this.txtUsu.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtUsu.Name = "txtUsu";
            this.txtUsu.PermiteSoloNumeros = false;
            this.txtUsu.Size = new System.Drawing.Size(302, 24);
            this.txtUsu.TabIndex = 8;
            this.txtUsu.TextoTitulo = "Usuario :";
            this.txtUsu.ValorTextBox = "";
            // 
            // pcbFoto
            // 
            this.pcbFoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbFoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcbFoto.Location = new System.Drawing.Point(4, 0);
            this.pcbFoto.Margin = new System.Windows.Forms.Padding(4, 0, 0, 4);
            this.pcbFoto.Name = "pcbFoto";
            this.tableLayoutPanel1.SetRowSpan(this.pcbFoto, 5);
            this.pcbFoto.Size = new System.Drawing.Size(119, 136);
            this.pcbFoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pcbFoto.TabIndex = 0;
            this.pcbFoto.TabStop = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(1048, 4);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.Name = "btnGuardar";
            this.tableLayoutPanel1.SetRowSpan(this.btnGuardar, 2);
            this.btnGuardar.Size = new System.Drawing.Size(83, 48);
            this.btnGuardar.TabIndex = 16;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txtDir
            // 
            this.txtDir.AnchoTitulo = 115;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDir, 4);
            this.txtDir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDir.Location = new System.Drawing.Point(431, 87);
            this.txtDir.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDir.MensajeDeAyuda = null;
            this.txtDir.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDir.Name = "txtDir";
            this.txtDir.PermiteSoloNumeros = false;
            this.txtDir.Size = new System.Drawing.Size(610, 24);
            this.txtDir.TabIndex = 11;
            this.txtDir.TextoTitulo = "Dirección :";
            this.txtDir.ValorTextBox = "";
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(1048, 60);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.tableLayoutPanel1.SetRowSpan(this.btnLimpiar, 2);
            this.btnLimpiar.Size = new System.Drawing.Size(83, 48);
            this.btnLimpiar.TabIndex = 17;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // txtTel
            // 
            this.txtTel.AnchoTitulo = 114;
            this.tableLayoutPanel1.SetColumnSpan(this.txtTel, 2);
            this.txtTel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTel.Location = new System.Drawing.Point(126, 59);
            this.txtTel.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtTel.MensajeDeAyuda = null;
            this.txtTel.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtTel.Name = "txtTel";
            this.txtTel.PermiteSoloNumeros = true;
            this.txtTel.Size = new System.Drawing.Size(299, 24);
            this.txtTel.TabIndex = 7;
            this.txtTel.TextoTitulo = "Teléfono :";
            this.txtTel.ValorTextBox = "";
            // 
            // dtpFecNac
            // 
            this.dtpFecNac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFecNac.EditValue = null;
            this.dtpFecNac.Location = new System.Drawing.Point(243, 115);
            this.dtpFecNac.Name = "dtpFecNac";
            this.dtpFecNac.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecNac.Properties.Appearance.Options.UseFont = true;
            this.dtpFecNac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecNac.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecNac.Size = new System.Drawing.Size(182, 22);
            this.dtpFecNac.TabIndex = 12;
            this.dtpFecNac.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxTipoDoc_KeyPress);
            // 
            // txtSnom
            // 
            this.txtSnom.AnchoTitulo = 114;
            this.tableLayoutPanel1.SetColumnSpan(this.txtSnom, 2);
            this.txtSnom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSnom.Location = new System.Drawing.Point(126, 31);
            this.txtSnom.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtSnom.MensajeDeAyuda = null;
            this.txtSnom.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtSnom.Name = "txtSnom";
            this.txtSnom.PermiteSoloNumeros = false;
            this.txtSnom.Size = new System.Drawing.Size(299, 24);
            this.txtSnom.TabIndex = 4;
            this.txtSnom.TextoTitulo = "Segundo Nombre :";
            this.txtSnom.ValorTextBox = "";
            // 
            // cbxSexo
            // 
            this.cbxSexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxSexo.Location = new System.Drawing.Point(859, 59);
            this.cbxSexo.Name = "cbxSexo";
            this.cbxSexo.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxSexo.Properties.Appearance.Options.UseFont = true;
            this.cbxSexo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxSexo.Properties.DropDownRows = 3;
            this.cbxSexo.Properties.Items.AddRange(new object[] {
            "Seleccione",
            "Masculino\t",
            "Femenino"});
            this.cbxSexo.Properties.NullText = "Seleccione";
            this.cbxSexo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxSexo.Size = new System.Drawing.Size(182, 22);
            this.cbxSexo.TabIndex = 9;
            this.cbxSexo.Enter += new System.EventHandler(this.cbxSexo_Enter);
            this.cbxSexo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxTipoDoc_KeyPress);
            // 
            // lblAcudiente
            // 
            this.lblAcudiente.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblAcudiente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAcudiente.Location = new System.Drawing.Point(431, 143);
            this.lblAcudiente.Name = "lblAcudiente";
            this.lblAcudiente.Size = new System.Drawing.Size(114, 22);
            this.lblAcudiente.TabIndex = 60;
            this.lblAcudiente.Text = "Acudientes :";
            // 
            // cbxMuni
            // 
            this.cbxMuni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxMuni.Location = new System.Drawing.Point(243, 143);
            this.cbxMuni.Name = "cbxMuni";
            this.cbxMuni.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbxMuni.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxMuni.Properties.Appearance.Options.UseFont = true;
            this.cbxMuni.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxMuni.Properties.NullText = "Seleccione";
            this.cbxMuni.Properties.NullValuePrompt = "Seleccione";
            this.cbxMuni.Size = new System.Drawing.Size(182, 22);
            this.cbxMuni.TabIndex = 15;
            this.cbxMuni.EditValueChanged += new System.EventHandler(this.cbxMuni_EditValueChanged);
            this.cbxMuni.Enter += new System.EventHandler(this.cbxDpto_Enter);
            this.cbxMuni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxTipoDoc_KeyPress);
            // 
            // lblMuni
            // 
            this.lblMuni.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMuni.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMuni.Location = new System.Drawing.Point(126, 143);
            this.lblMuni.Name = "lblMuni";
            this.lblMuni.Size = new System.Drawing.Size(111, 22);
            this.lblMuni.TabIndex = 61;
            this.lblMuni.Text = "Municipio Nac :";
            // 
            // lblDpto
            // 
            this.lblDpto.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDpto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDpto.Location = new System.Drawing.Point(739, 115);
            this.lblDpto.Name = "lblDpto";
            this.lblDpto.Size = new System.Drawing.Size(114, 22);
            this.lblDpto.TabIndex = 65;
            this.lblDpto.Text = "Departamento Nac :";
            // 
            // cbxPais
            // 
            this.cbxPais.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxPais.Location = new System.Drawing.Point(551, 115);
            this.cbxPais.Name = "cbxPais";
            this.cbxPais.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbxPais.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxPais.Properties.Appearance.Options.UseFont = true;
            this.cbxPais.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxPais.Properties.NullText = "Seleccione";
            this.cbxPais.Properties.NullValuePrompt = "Seleccione";
            this.cbxPais.Size = new System.Drawing.Size(182, 22);
            this.cbxPais.TabIndex = 13;
            this.cbxPais.EditValueChanged += new System.EventHandler(this.cbxPais_EditValueChanged);
            this.cbxPais.Enter += new System.EventHandler(this.cbxDpto_Enter);
            this.cbxPais.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxTipoDoc_KeyPress);
            // 
            // cbxDpto
            // 
            this.cbxDpto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxDpto.Location = new System.Drawing.Point(859, 115);
            this.cbxDpto.Name = "cbxDpto";
            this.cbxDpto.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbxDpto.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxDpto.Properties.Appearance.Options.UseFont = true;
            this.cbxDpto.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxDpto.Properties.NullText = "Seleccione";
            this.cbxDpto.Properties.NullValuePrompt = "Seleccione";
            this.cbxDpto.Size = new System.Drawing.Size(182, 22);
            this.cbxDpto.TabIndex = 14;
            this.cbxDpto.EditValueChanged += new System.EventHandler(this.cbxDpto_EditValueChanged);
            this.cbxDpto.Enter += new System.EventHandler(this.cbxDpto_Enter);
            this.cbxDpto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxTipoDoc_KeyPress);
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSexo.Location = new System.Drawing.Point(739, 59);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(114, 22);
            this.lblSexo.TabIndex = 59;
            this.lblSexo.Text = "Sexo :";
            // 
            // lblPais
            // 
            this.lblPais.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPais.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPais.Location = new System.Drawing.Point(431, 115);
            this.lblPais.Name = "lblPais";
            this.lblPais.Size = new System.Drawing.Size(114, 22);
            this.lblPais.TabIndex = 63;
            this.lblPais.Text = "Pais Nac :";
            // 
            // lblFecNac
            // 
            this.lblFecNac.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFecNac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFecNac.Location = new System.Drawing.Point(126, 115);
            this.lblFecNac.Name = "lblFecNac";
            this.lblFecNac.Size = new System.Drawing.Size(111, 22);
            this.lblFecNac.TabIndex = 57;
            this.lblFecNac.Text = "Fecha Nac :";
            // 
            // cbxTipoDoc
            // 
            this.cbxTipoDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxTipoDoc.Location = new System.Drawing.Point(243, 3);
            this.cbxTipoDoc.Name = "cbxTipoDoc";
            this.cbxTipoDoc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.cbxTipoDoc.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxTipoDoc.Properties.Appearance.Options.UseFont = true;
            this.cbxTipoDoc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxTipoDoc.Properties.DropDownRows = 5;
            this.cbxTipoDoc.Properties.NullText = "Seleccione";
            this.cbxTipoDoc.Properties.NullValuePrompt = "Seleccione";
            this.cbxTipoDoc.Size = new System.Drawing.Size(182, 22);
            this.cbxTipoDoc.TabIndex = 1;
            this.cbxTipoDoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxTipoDoc_KeyPress);
            // 
            // lblTipoDoc
            // 
            this.lblTipoDoc.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTipoDoc.Location = new System.Drawing.Point(126, 3);
            this.lblTipoDoc.Name = "lblTipoDoc";
            this.lblTipoDoc.Size = new System.Drawing.Size(111, 22);
            this.lblTipoDoc.TabIndex = 61;
            this.lblTipoDoc.Text = "Tipo doc :";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 3);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btnAcudiente, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtRh, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtEps, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(548, 140);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(496, 28);
            this.tableLayoutPanel2.TabIndex = 67;
            // 
            // btnAcudiente
            // 
            this.btnAcudiente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcudiente.Enabled = false;
            this.btnAcudiente.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAcudiente.ImageOptions.Image")));
            this.btnAcudiente.Location = new System.Drawing.Point(3, 0);
            this.btnAcudiente.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btnAcudiente.Name = "btnAcudiente";
            this.btnAcudiente.Size = new System.Drawing.Size(67, 28);
            this.btnAcudiente.TabIndex = 16;
            this.btnAcudiente.Text = "Agregar";
            this.btnAcudiente.Click += new System.EventHandler(this.btnAcudiente_Click);
            // 
            // txtRh
            // 
            this.txtRh.AnchoTitulo = 50;
            this.txtRh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRh.Location = new System.Drawing.Point(73, 3);
            this.txtRh.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtRh.MensajeDeAyuda = null;
            this.txtRh.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtRh.Name = "txtRh";
            this.txtRh.PermiteSoloNumeros = false;
            this.txtRh.Size = new System.Drawing.Size(112, 24);
            this.txtRh.TabIndex = 6;
            this.txtRh.TextoTitulo = "RH :";
            this.txtRh.ValorTextBox = "";
            // 
            // txtEps
            // 
            this.txtEps.AnchoTitulo = 50;
            this.txtEps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEps.Location = new System.Drawing.Point(191, 3);
            this.txtEps.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtEps.MensajeDeAyuda = null;
            this.txtEps.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtEps.Name = "txtEps";
            this.txtEps.PermiteSoloNumeros = false;
            this.txtEps.Size = new System.Drawing.Size(302, 24);
            this.txtEps.TabIndex = 6;
            this.txtEps.TextoTitulo = "Eps :";
            this.txtEps.ValorTextBox = "";
            // 
            // gcEstudiantes
            // 
            this.gcEstudiantes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcEstudiantes.Location = new System.Drawing.Point(12, 207);
            this.gcEstudiantes.MainView = this.gvEstudiantes;
            this.gcEstudiantes.Name = "gcEstudiantes";
            this.gcEstudiantes.Size = new System.Drawing.Size(1139, 530);
            this.gcEstudiantes.TabIndex = 25;
            this.gcEstudiantes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvEstudiantes});
            // 
            // gvEstudiantes
            // 
            this.gvEstudiantes.GridControl = this.gcEstudiantes;
            this.gvEstudiantes.Name = "gvEstudiantes";
            // 
            // FrmEstudiante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 749);
            this.Controls.Add(this.gcEstudiantes);
            this.Controls.Add(this.grcInfo);
            this.Name = "FrmEstudiante";
            this.Text = "Estudiantes";
            this.Load += new System.EventHandler(this.FrmEstudiante_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).EndInit();
            this.grcInfo.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbFoto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNac.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNac.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSexo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxMuni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPais.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDpto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxTipoDoc.Properties)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcEstudiantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEstudiantes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl grcInfo;
        public DevExpress.XtraEditors.LookUpEdit cbxMuni;
        public DevExpress.XtraEditors.LookUpEdit cbxPais;
        private dll.Controles.ucLabelAV lblMuni;
        private dll.Controles.ucLabelAV lblPais;
        private dll.Controles.ucLabelAV lblSexo;
        public DevExpress.XtraEditors.ComboBoxEdit cbxSexo;
        public DevExpress.XtraEditors.LookUpEdit cbxTipoDoc;
        private dll.Controles.ucLabelAV lblTipoDoc;
        private DevExpress.XtraGrid.GridControl gcEstudiantes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvEstudiantes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pcbFoto;
        private dll.Controles.ucLabelAV lblAcudiente;
        private DevExpress.XtraEditors.SimpleButton btnAcudiente;
        private DevExpress.XtraEditors.SimpleButton btnQuitaFoto;
        private DevExpress.XtraEditors.SimpleButton btnAddFoto;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public DevExpress.XtraEditors.DateEdit dtpFecNac;
        private dll.Controles.ucLabelAV lblFecNac;
        private dll.Controles.ucLabelAV lblDpto;
        public DevExpress.XtraEditors.LookUpEdit cbxDpto;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private dll.Controles.ucLabelTextBox txtSnom;
        private dll.Controles.ucLabelTextBox txtTel;
        private dll.Controles.ucLabelTextBox txtDir;
        private dll.Controles.ucLabelTextBox txtUsu;
        private dll.Controles.ucLabelTextBox txtPape;
        private dll.Controles.ucLabelTextBox txtSape;
        private dll.Controles.ucLabelTextBox txtPnom;
        private dll.Controles.ucLabelTextBox txtDocumento;
        private dll.Controles.ucLabelTextBox txtCorreo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private dll.Controles.ucLabelTextBox txtRh;
        private dll.Controles.ucLabelTextBox txtEps;
        private DevExpress.XtraEditors.SimpleButton btnCam;
    }
}