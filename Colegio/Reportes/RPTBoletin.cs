﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Colegio.Clases;

namespace Colegio.Reportes
{
    public partial class RPTBoletin : DevExpress.XtraReports.UI.XtraReport
    {
        private int numRegistros =0;
        public RPTBoletin()
        {
            InitializeComponent();
            lblNomRector.Text = ClFunciones.wADDatosInstitucion[0].NombreRector;
            
        }

        private void lblDesempeños_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                XRLabel label = (XRLabel)sender;
                string txt = label.Text;
                if (txt!="")
                {
                    string[] text = txt.Split(';');
                    if (text.Length>0)
                    {
                        txt = "";
                        foreach (string item in text)
                        {
                            txt += "-"+item+"\n";
                        }
                        txt.Replace(';', '\'');
                    }
                }
                label.Text = txt;

            }
            catch (Exception ex)
            {

            }
        }

        private void lblNDp1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {            
            try
            {
                if (lblP1.Text != "")
                {
                    double p1 = Convert.ToDouble(lblP1.Text);
                    lblNDp1.Text = Desempeño(p1);
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private string Desempeño(double prom)
        {
            try
            {
                if (prom >= 0 && prom <= 2.9)
                {
                    return "Bajo";
                }
                else if (prom >= 3 && prom <= 3.9)
                {
                    return "Básico";

                }
                else if (prom >= 4 && prom <= 4.5)
                {

                    return "Alto";
                }
                else if (prom >= 4.6 && prom <= 5)
                {
                    return "Superior";

                }
                else
                {
                    return "";

                }
            }
            catch (Exception ex)
            {
                return "";

            }
        }

        private void lblNDp2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (lblP2.Text != "")
                {
                    double p1 = Convert.ToDouble(lblP2.Text);
                    lblNDp2.Text = Desempeño(p1);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void lblNDp3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (lblP3.Text != "")
                {
                    double p1 = Convert.ToDouble(lblP3.Text);
                    lblNDp3.Text = Desempeño(p1);
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void lblNDp4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (lblP4.Text != "")
                {
                    double p1 = Convert.ToDouble(lblP4.Text);
                    lblNDp4.Text = Desempeño(p1);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void lblNDprom_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (lblProm.Text != "")
                {
                    double p1 = Convert.ToDouble(lblProm.Text);
                    lblNDprom.Text = Desempeño(p1);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void Detail1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            numRegistros += 1;
            if (numRegistros==9)
            {
                Detail1.PageBreak = PageBreak.AfterBand;
                numRegistros = 0;
            }
            else
            {
                Detail1.PageBreak = PageBreak.None;
            }            
        }
    }
}
