﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADInasistenciaET
    {
        public int Sec { get; set; }
        public long IdEstudiante { get; set; }
        public int IdPeriodo { get; set; }
        public int CantInasistencias { get; set; }
        public int IdCurso { get; set; }
        public int IdMateria { get; set; }
        public long IdMaestro { get; set; }
    }

    public class ADInasistenciaETList : List<ADInasistenciaET> { }
}
