USE [BDIEJAG]
GO
/****** Object:  StoredProcedure [dbo].[SP_ADDesempeñosMateriaCurso_I]    Script Date: 22/11/2018 9:12:58 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 22-11-2018
-- Description:	Inserta en la tabla ADDesempeñosMateriaCurso
-- =============================================

ALTER PROC [dbo].[SP_ADDesempeñosMateriaCurso_I] 	

	@pSecMatCurso		int,
	@pSecPeriodo		int,
	@pSecDesMatCurso    int,
	@pDesempeño			NVARCHAR(4000),
	@Inserto			VARCHAR(2) OUTPUT
AS
BEGIN
	DECLARE 
		@SecMatCurso int = @pSecMatCurso,
		@SecPeriodo int = @pSecPeriodo,
		@SecDesMatCurso int = @pSecDesMatCurso,
		@Desempeño NVARCHAR(4000) = @pDesempeño

	IF(@SecDesMatCurso>0)
		BEGIN
			UPDATE ADDesempeñosMateriaCurso SET Desempeño = @Desempeño WHERE SecMateriaCurso = @SecMatCurso AND SecPeriodo = @SecPeriodo
			SET @Inserto = 'SI'
		END
	ELSE
		BEGIN
			INSERT INTO ADDesempeñosMateriaCurso (SecMateriaCurso, SecPeriodo, Desempeño) VALUES
			(@SecMatCurso, @SecPeriodo, @Desempeño)
			SET @Inserto = 'SI'
		END
END

