﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class EstudianteET
    {
        public int Sede { get; set; }
        public int Jornada { get; set; }
        public int Documento { get; set; }
        public int Nombre { get; set; }
        public int Codigo { get; set; }
        public int Grupo { get; set; }
        public int PosicionGrupo { get; set; }
    }
}
