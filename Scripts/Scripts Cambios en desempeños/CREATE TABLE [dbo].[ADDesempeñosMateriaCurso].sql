USE [BDIEJAG]
GO

/****** Object:  Table [dbo].[ADDesempe˝osMateriaCurso]    Script Date: 22/11/2018 9:11:14 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ADDesempe˝osMateriaCurso](
	[Sec] [int] IDENTITY(1,1) NOT NULL,
	[SecMateriaCurso] [int] NOT NULL,
	[SecPeriodo] [int] NOT NULL,
	[Desempe˝o] [nchar](4000) NOT NULL,
 CONSTRAINT [PK_ADDesempe˝osMateriaCurso_1] PRIMARY KEY CLUSTERED 
(
	[SecMateriaCurso] ASC,
	[SecPeriodo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ADDesempe˝osMateriaCurso]  WITH CHECK ADD  CONSTRAINT [FK_ADDesempe˝osMateriaCurso_ADMateriasCursos] FOREIGN KEY([SecMateriaCurso])
REFERENCES [dbo].[ADMateriasCursos] ([Sec])
GO

ALTER TABLE [dbo].[ADDesempe˝osMateriaCurso] CHECK CONSTRAINT [FK_ADDesempe˝osMateriaCurso_ADMateriasCursos]
GO

ALTER TABLE [dbo].[ADDesempe˝osMateriaCurso]  WITH CHECK ADD  CONSTRAINT [FK_ADDesempe˝osMateriaCurso_ADPeriodos] FOREIGN KEY([SecPeriodo])
REFERENCES [dbo].[ADPeriodos] ([Sec])
GO

ALTER TABLE [dbo].[ADDesempe˝osMateriaCurso] CHECK CONSTRAINT [FK_ADDesempe˝osMateriaCurso_ADPeriodos]
GO


