﻿namespace Colegio.Vistas.Seguridad
{
    partial class FrmCambiarClave
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnCancelar = new dll.Controles.ucBtnCancelarAV();
            this.btnGuardar = new dll.Controles.ucBtnGuardarAV();
            this.grcInfo = new DevExpress.XtraEditors.GroupControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCerrar = new DevExpress.XtraEditors.LabelControl();
            this.txtIdUsu = new dll.Controles.ucLabelTextBox();
            this.txtUsu = new dll.Controles.ucLabelTextBox();
            this.txtPswNew = new dll.Controles.ucLabelTextBox();
            this.txtPswActual = new dll.Controles.ucLabelTextBox();
            this.txtPswNewConfirm = new dll.Controles.ucLabelTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).BeginInit();
            this.grcInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitulo.Size = new System.Drawing.Size(800, 32);
            this.lblTitulo.TabIndex = 19;
            this.lblTitulo.Text = "Cambiar contraseña";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(275, 246);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(97, 41);
            this.btnCancelar.TabIndex = 22;
            this.btnCancelar.OnUserControlButtonClicked += new dll.Controles.ucBtnCancelarAV.ButtonClickedEventHandler(this.btnCancelar_OnUserControlButtonClicked);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(172, 246);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(97, 41);
            this.btnGuardar.TabIndex = 21;
            this.btnGuardar.OnUserControlButtonClicked += new dll.Controles.ucBtnGuardarAV.ButtonClickedEventHandler(this.btnGuardar_OnUserControlButtonClicked);
            // 
            // grcInfo
            // 
            this.grcInfo.Controls.Add(this.txtPswNewConfirm);
            this.grcInfo.Controls.Add(this.txtIdUsu);
            this.grcInfo.Controls.Add(this.txtPswNew);
            this.grcInfo.Controls.Add(this.txtUsu);
            this.grcInfo.Controls.Add(this.txtPswActual);
            this.grcInfo.Location = new System.Drawing.Point(18, 50);
            this.grcInfo.Name = "grcInfo";
            this.grcInfo.Size = new System.Drawing.Size(354, 181);
            this.grcInfo.TabIndex = 20;
            this.grcInfo.Text = "Información del usuario";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(393, 276);
            this.panel1.TabIndex = 23;
            // 
            // lblCerrar
            // 
            this.lblCerrar.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblCerrar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrar.Appearance.ForeColor = System.Drawing.Color.White;
            this.lblCerrar.Appearance.Options.UseBackColor = true;
            this.lblCerrar.Appearance.Options.UseFont = true;
            this.lblCerrar.Appearance.Options.UseForeColor = true;
            this.lblCerrar.Appearance.Options.UseTextOptions = true;
            this.lblCerrar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblCerrar.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblCerrar.Location = new System.Drawing.Point(359, 2);
            this.lblCerrar.Name = "lblCerrar";
            this.lblCerrar.Size = new System.Drawing.Size(33, 27);
            this.lblCerrar.TabIndex = 27;
            this.lblCerrar.Text = "X";
            this.lblCerrar.Click += new System.EventHandler(this.lblCerrar_Click);
            // 
            // txtIdUsu
            // 
            this.txtIdUsu.AnchoTitulo = 180;
            this.txtIdUsu.Location = new System.Drawing.Point(7, 27);
            this.txtIdUsu.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtIdUsu.MensajeDeAyuda = null;
            this.txtIdUsu.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtIdUsu.Name = "txtIdUsu";
            this.txtIdUsu.PermiteSoloNumeros = false;
            this.txtIdUsu.Size = new System.Drawing.Size(339, 24);
            this.txtIdUsu.TabIndex = 28;
            this.txtIdUsu.TextoTitulo = "Id :";
            this.txtIdUsu.ValorTextBox = "";
            // 
            // txtUsu
            // 
            this.txtUsu.AnchoTitulo = 180;
            this.txtUsu.Location = new System.Drawing.Point(7, 57);
            this.txtUsu.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtUsu.MensajeDeAyuda = null;
            this.txtUsu.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtUsu.Name = "txtUsu";
            this.txtUsu.PermiteSoloNumeros = false;
            this.txtUsu.Size = new System.Drawing.Size(339, 24);
            this.txtUsu.TabIndex = 29;
            this.txtUsu.TextoTitulo = "Usuario :";
            this.txtUsu.ValorTextBox = "";
            // 
            // txtPswNew
            // 
            this.txtPswNew.AnchoTitulo = 180;
            this.txtPswNew.Location = new System.Drawing.Point(7, 117);
            this.txtPswNew.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPswNew.MensajeDeAyuda = null;
            this.txtPswNew.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPswNew.Name = "txtPswNew";
            this.txtPswNew.PermiteSoloNumeros = false;
            this.txtPswNew.Size = new System.Drawing.Size(339, 24);
            this.txtPswNew.TabIndex = 31;
            this.txtPswNew.TextoTitulo = "Nueva contraseña :";
            this.txtPswNew.ValorTextBox = "";
            // 
            // txtPswActual
            // 
            this.txtPswActual.AnchoTitulo = 180;
            this.txtPswActual.Location = new System.Drawing.Point(7, 87);
            this.txtPswActual.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPswActual.MensajeDeAyuda = null;
            this.txtPswActual.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPswActual.Name = "txtPswActual";
            this.txtPswActual.PermiteSoloNumeros = false;
            this.txtPswActual.Size = new System.Drawing.Size(339, 24);
            this.txtPswActual.TabIndex = 30;
            this.txtPswActual.TextoTitulo = "Contraseña actual :";
            this.txtPswActual.ValorTextBox = "";
            // 
            // txtPswNewConfirm
            // 
            this.txtPswNewConfirm.AnchoTitulo = 180;
            this.txtPswNewConfirm.Location = new System.Drawing.Point(7, 147);
            this.txtPswNewConfirm.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPswNewConfirm.MensajeDeAyuda = null;
            this.txtPswNewConfirm.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPswNewConfirm.Name = "txtPswNewConfirm";
            this.txtPswNewConfirm.PermiteSoloNumeros = false;
            this.txtPswNewConfirm.Size = new System.Drawing.Size(339, 24);
            this.txtPswNewConfirm.TabIndex = 32;
            this.txtPswNewConfirm.TextoTitulo = "Confirma nueva contraseña :";
            this.txtPswNewConfirm.ValorTextBox = "";
            // 
            // FrmCambiarClave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 307);
            this.Controls.Add(this.lblCerrar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.grcInfo);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmCambiarClave";
            this.Text = "Cambiar contraseña";
            this.Load += new System.EventHandler(this.FrmCambiarClave_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).EndInit();
            this.grcInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblTitulo;
        private dll.Controles.ucBtnCancelarAV btnCancelar;
        private dll.Controles.ucBtnGuardarAV btnGuardar;
        private DevExpress.XtraEditors.GroupControl grcInfo;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.LabelControl lblCerrar;
        private dll.Controles.ucLabelTextBox txtPswNewConfirm;
        private dll.Controles.ucLabelTextBox txtIdUsu;
        private dll.Controles.ucLabelTextBox txtPswNew;
        private dll.Controles.ucLabelTextBox txtUsu;
        private dll.Controles.ucLabelTextBox txtPswActual;
    }
}