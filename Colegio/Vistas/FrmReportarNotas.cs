﻿using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;
using DevExpress.XtraGrid.Views.Grid;
using Colegio.ET;
using DevExpress.XtraGrid.Columns;

namespace Colegio.Vistas
{
    public partial class FrmReportarNotas : FrmBase
    {
        ClTercero clTercero = new ClTercero();
        ClCursos clCursos = new ClCursos();
        ClAñoLectivo clAño = new ClAñoLectivo();

        ADDesempeñosMateriaCursoET Desempeños = new ADDesempeñosMateriaCursoET();

        public FrmReportarNotas()
        {
            InitializeComponent();
        }

        private void FrmReportarNotas_Load(object sender, EventArgs e)
        {
            LlenarControlesBusqueda();
            LlenarCbxPeriodos();
            CrearGrilla();
            txtMaestro.Focus();
        }

        private void CrearGrilla()
        {
            try
            {
                gvMaterias = new GridView();
                gcMaterias.MainView = gvMaterias;
                gvMaterias.OptionsView.ShowViewCaption = true;
                gvMaterias.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 11.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 10.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.ViewCaption.Font = new Font("Segoe UI", 11.0f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.Font = new Font("Segoe UI", 11.0f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvMaterias.Appearance.FocusedRow.Options.UseTextOptions = false;
                gvMaterias.OptionsCustomization.AllowColumnResizing = true;
                gvMaterias.OptionsView.ShowGroupPanel = false;
                gvMaterias.OptionsBehavior.Editable = true;
                gvMaterias.OptionsCustomization.AllowGroup = false;
                gvMaterias.OptionsCustomization.AllowColumnMoving = false;                
                gvMaterias.ViewCaption = "Lista de estudiantes";
                gvMaterias.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvMaterias_CellValueChanging);
                gvMaterias.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gvMaterias_ValidatingEditor);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarCbxPeriodos()
        {
            try
            {
                Clases.ClPeriodos cl = new Clases.ClPeriodos();
                lkePeriodo.lkeDatos.Properties.DataSource = cl.GetPeriodos();
                lkePeriodo.lkeDatos.Properties.ValueMember = "IdPeriodo";
                lkePeriodo.lkeDatos.Properties.DisplayMember = "Nombre";
                lkePeriodo.lkeDatos.Properties.PopulateColumns();
                lkePeriodo.lkeDatos.Properties.Columns[0].Visible = false;
                lkePeriodo.lkeDatos.EditValue = ClFunciones.wADDatosInstitucion[0].PeriodoActual;
                lkePeriodo.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarControlesBusqueda()
        {
            try
            {
                string sql = "";
                DataTable dtTerceros = clTercero.GetTercerosTipoTer(1);
                if (dtTerceros.Rows.Count>0)
                {
                    if (!ClSeguridad.ValidaPermisosLocal(ClFunciones.UsuarioIngreso.IdRol,1027))
                    {
                        DataRow[] filas = dtTerceros.Select("IdTercero = " + ClFunciones.UsuarioIngreso.IdTercero.ToString());
                        if (filas.Length>0)
                        {
                            DataTable dtFinal = new DataTable();
                            dtFinal = dtTerceros.Clone();
                            filas.ToList().ForEach(x=> { dtFinal.ImportRow(x); });
                            dtTerceros = dtFinal;
                        }
                    }

                    dtTerceros.Columns[0].ColumnName = "Codigo";
                    dtTerceros.Columns[1].ColumnName = "Descripcion";
                    txtMaestro.TituloColsBusqueda = new dll.Common.ET.TituloColsBusquedaET() { Codigo = "Documento", Descripcion = "Maestro" };
                    txtMaestro.DataTable = dtTerceros;
                }

                DataTable dtAños = clAño.GetAñosLectivos();
                if (dtAños.Rows.Count > 0)
                {
                    dtAños.Columns[0].ColumnName = "Codigo";
                    dtAños.Columns[1].ColumnName = "Descripcion";
                    txtAñoLectivo.TituloColsBusqueda = new dll.Common.ET.TituloColsBusquedaET() { Codigo = "Id", Descripcion = "Año" };
                    txtAñoLectivo.DataTable = dtAños;
                    txtAñoLectivo.ValorTextBox = ClFunciones.wADDatosInstitucion[0].IdAñoActual.ToString();
                }

                if (txtAñoLectivo.ValorTextBox != "")
                {
                    DataTable dtCursos = clCursos.GetCursosByAño(Convert.ToInt32(txtAñoLectivo.ValorTextBox));
                    
                    if (dtCursos.Rows.Count > 0)
                    {
                        dtCursos.Columns[0].ColumnName = "Codigo";
                        dtCursos.Columns[1].ColumnName = "Descripcion";
                    }
                    txtCurso.DataTable = dtCursos;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarControlAñoLectivo(int idCurso)
        {
            try
            {
                string sql = string.Format("SELECT AL.Sec, AL.Año FROM ADAñoLectivo AL " +
                " INNER JOIN ADMatriculas MT ON AL.Sec = MT.IdAñoLectivo " +
                " INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec " +
                " WHERE MT.IdCurso = {0} " +
                " GROUP BY AL.Sec, AL.Año", idCurso);
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                txtAñoLectivo.DataTable = dt;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        private void LlenarControlPeriodos(int idCurso)
        {
            try
            {
                //string sql = string.Format("SELECT PR.Sec, PR.Nombre FROM ADPeriodos PR " +
                //" INNER JOIN ADPeriodosCurso PC ON PR.Sec = PC.IdPeriodo" +
                //" WHERE PC.IdCurso = {0}", idCurso);
                //DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                DataTable dt = new DataTable();
                dt.Columns.Add("Id");
                dt.Columns.Add("Nombre");
                DataRow fila = dt.NewRow();
                fila["Id"] = 1;
                fila["Nombre"] = "Primer pariodo";
                dt.Rows.Add(fila);
                fila = dt.NewRow();
                fila["Id"] = 2;
                fila["Nombre"] = "Segundo pariodo";
                dt.Rows.Add(fila);
                fila = dt.NewRow();
                fila["Id"] = 3;
                fila["Nombre"] = "Tercer pariodo";
                dt.Rows.Add(fila);
                fila = dt.NewRow();
                fila["Id"] = 4;
                fila["Nombre"] = "Cuarto pariodo";
                dt.Rows.Add(fila);
                dt.AcceptChanges();                
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                //txtPeriodo.DataTable = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void LlenarControlMaterias(int idCurso)
        {
            try
            {
                ClMaterias cl = new ClMaterias();                
                DataTable dt = cl.GetMateriasVistaByCursoDT(idCurso);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                }
                else
                {
                    ClFunciones.msgError("No se encontraron materias para el curso con el id " + idCurso.ToString());
                    txtMateria.DataTable = new DataTable();
                    return;
                }
                txtMateria.DataTable = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void BuscarEstudiantes(int idAñoLectivo, int idCurso, long idTercero, int idMateria)
        {
            try
            {

                ParametersSpETList listParam = new ParametersSpETList()
                {
                    new ParametersSpET { NombreParametro ="@pIdAñoLectivo", Valor= idAñoLectivo.ToString()},
                    new ParametersSpET { NombreParametro ="@pIdCurso", Valor= idCurso.ToString() },
                    new ParametersSpET { NombreParametro ="@pIdMateria", Valor= idMateria.ToString() },
                    new ParametersSpET { NombreParametro ="@pIdMaestro", Valor= idTercero.ToString() },
                };
               
                DataTable dt = ClFunciones.ConsultarConSP("SP_NotasEstudiantesMateria_G", listParam, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count>0)
                {
                    gcMaterias.DataSource = dt;
                    gvMaterias.Columns[0].Visible = false;
                    gvMaterias.Columns[1].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[1].Width = 100;
                    gvMaterias.Columns[1].Caption = "Documento";
                    gvMaterias.Columns[2].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[2].Caption = "Estudiante";
                    gvMaterias.Columns[3].Visible = false;
                    gvMaterias.Columns[4].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[4].Width = 100;
                    gvMaterias.Columns[4].Caption = "Materia";
                    gvMaterias.Columns[5].Visible = false;
                    //Notas y ausencias
                    gvMaterias.Columns[6].Width = 25;
                    gvMaterias.Columns[7].Width = 25;
                    gvMaterias.Columns[8].Width = 25;
                    gvMaterias.Columns[9].Width = 25;
                    gvMaterias.Columns[10].Width = 25;
                    gvMaterias.Columns[11].Width = 25;
                    gvMaterias.Columns[12].Width = 25;
                    gvMaterias.Columns[13].Width = 25;
                    gvMaterias.Columns[6].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[7].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[8].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[9].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[10].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[11].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[12].OptionsColumn.AllowEdit = false;
                    gvMaterias.Columns[13].OptionsColumn.AllowEdit = false;

                    gvMaterias.Columns[14].Width = 25;
                    gvMaterias.Columns[14].OptionsColumn.AllowEdit = false;
                    GridColumn colModelPrice = gvMaterias.Columns[14];
                    colModelPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    colModelPrice.DisplayFormat.FormatString = "N1";
                    switch (Convert.ToInt32(lkePeriodo.lkeDatos.EditValue))
                    {
                        case 1:
                            gvMaterias.Columns[6].OptionsColumn.AllowEdit = true;
                            gvMaterias.Columns[7].OptionsColumn.AllowEdit = true;                            
                            break;
                        case 2:
                            gvMaterias.Columns[8].OptionsColumn.AllowEdit = true;
                            gvMaterias.Columns[9].OptionsColumn.AllowEdit = true;
                            break;
                        case 3:
                            gvMaterias.Columns[10].OptionsColumn.AllowEdit = true;
                            gvMaterias.Columns[11].OptionsColumn.AllowEdit = true;
                            break;
                        case 4:
                            gvMaterias.Columns[12].OptionsColumn.AllowEdit = true;
                            gvMaterias.Columns[13].OptionsColumn.AllowEdit = true;
                            break;
                        default:
                            break;
                    }
                    ConsultaDesempeño(Convert.ToInt32(lkePeriodo.lkeDatos.EditValue), Convert.ToInt32(dt.Rows[0]["SecMatCurso"]));
                }
                else
                {
                    ClFunciones.msgError("No se encontraron materias con los parametros solicitados.");
                    gcMaterias.DataSource = new DataTable();
                }
             
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ConsultaDesempeño(int secPeriodo, int secMatecurso)
        {
            try
            {
                ParametersSpETList listParam = new ParametersSpETList()
                {
                    new ParametersSpET { NombreParametro ="@pSecMateriaCurso", Valor= secMatecurso.ToString()},
                    new ParametersSpET { NombreParametro ="@pSecPeriodo", Valor= secPeriodo.ToString() }
                };

                DataTable dt = ClFunciones.ConsultarConSP("SP_ADDesempeñosMateriaCurso_G", listParam, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    Desempeños.Sec = Convert.ToInt32(dt.Rows[0]["Sec"]);
                    //Desempeños.SecMateriaCurso = Convert.ToInt32(dt.Rows[0]["SecMateriaCurso"]);
                    //Desempeños.SecPeriodo = Convert.ToInt32(dt.Rows[0]["SecPeriodo"]);
                    Desempeños.Desempeño = dt.Rows[0]["Desempeños"].ToString();
                    txtDesempeño.Text = Desempeños.Desempeño;
                }
                else
                {
                    ClFunciones.msgError("No se encontraron desempeños en el periodo actual.");
                    //gcMaterias.DataSource = new DataTable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool ValidaCampos()
        {
            try
            {
                if (txtMaestro.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el maestro.");
                    txtMaestro.Focus();
                    return false;
                }
                else if (txtCurso.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el curso.");
                    txtCurso.Focus();
                    return false;
                }
                else if (txtAñoLectivo.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el año.");
                    txtAñoLectivo.Focus();
                    return false;
                }
                //else if (txtPeriodo.ValorTextBox == "")
                //{
                //    MessageBox.Show("Debe seleccionar el periodo.");
                //    txtPeriodo.Focus();
                //    return false;
                //}
                else if (txtMateria.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar la materia");
                    txtMateria.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void gvMaterias_CustomDrawColumnHeader(object sender, ColumnHeaderCustomDrawEventArgs e)
        {
            try
            {
                if (e.Column != null)
                {
                    e.Info.Caption = "";
                    e.Painter.DrawObject(e.Info);
                    System.Drawing.Drawing2D.GraphicsState state = e.Graphics.Save();
                    Rectangle r = e.Info.CaptionRect;
                    StringFormat sf = new StringFormat();
                    sf.Trimming = StringTrimming.EllipsisCharacter;
                    sf.FormatFlags = sf.FormatFlags | StringFormatFlags.NoWrap;
                    if (e.Column.Caption == "Documento")
                    {
                        sf.LineAlignment = StringAlignment.Far;
                    }
                    else if (e.Column.Caption == "Estudiante")
                    {
                        sf.LineAlignment = StringAlignment.Far;
                    }
                    else
                    {
                        e.Graphics.RotateTransform(270);
                        SizeF stringSize = e.Graphics.MeasureString(e.Column.Caption, e.Appearance.Font);
                        int startX;
                        if (stringSize.Width > e.Bounds.Height)
                            startX = -e.Bounds.Bottom;
                        else
                            startX = -e.Bounds.Bottom + (e.Bounds.Height - (int)stringSize.Width) / 2;
                        r.X = startX;
                        r.Y = e.Info.CaptionRect.X + (e.Info.CaptionRect.Width - (int)stringSize.Height) / 2;
                        r.Width = e.Bounds.Height;
                        r.Height = e.Bounds.Width;
                    }
                    e.Handled = true;
                    e.Graphics.DrawString(e.Column.Caption, e.Appearance.Font, e.Appearance.GetForeBrush(e.Cache), r, sf);
                    e.Graphics.Restore(state);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void gvMaterias_DoubleClick(object sender, EventArgs e)
        {
           
        }

        private void txtCurso_SaleControl(object sender, EventArgs e)
        {
            try
            {
                if (txtCurso.ValorTextBox != "")
                {
                    //LlenarControlAñoLectivo(Convert.ToInt32(txtCurso.ValorTextBox));
                    //LlenarControlPeriodos(Convert.ToInt32(txtCurso.ValorTextBox));
                    LlenarControlMaterias(Convert.ToInt32(txtCurso.ValorTextBox));
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidaCampos()) return;
                int idAño = Convert.ToInt32(txtAñoLectivo.ValorTextBox);
                int idCurso = Convert.ToInt32(txtCurso.ValorTextBox);
                int idMateria = Convert.ToInt32(txtMateria.ValorTextBox);
                long idMaestro = Convert.ToInt64(txtMaestro.ValorTextBox);
                BuscarEstudiantes(idAño, idCurso, idMaestro, idMateria);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarControlesBusqueda();
            LimpiarCampos();
        }

        private void LimpiarCampos()
        {
            //txtAñoLectivo.ValorTextBox = "";
            txtCurso.ValorTextBox = "";
            txtMaestro.ValorTextBox = "";
            txtMateria.ValorTextBox = "";
            txtDesempeño.Text = "";
            gcMaterias.DataSource = new DataTable();
            txtMaestro.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (ClFunciones.msgResult("Cofirma la inserción de las notas cargadas?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Clases.ClNotas clNotas = new ClNotas();
                try
                {
                    DataTable dt = (DataTable)gcMaterias.DataSource;
                    ListADNotasET lista = new ListADNotasET();
                    ADInasistenciaETList listaIA = new ADInasistenciaETList();
                    foreach (DataRow item in dt.Rows)
                    {
                        ADNotasET Reg = new ADNotasET()
                        {
                            Sec = -1,
                            IdEstudiante = Convert.ToInt64(item["IdTercero"]),
                            FechaReg = DateTime.Now,
                            UsuReg = ClFunciones.UsuarioIngreso.IdUsuario,
                            Descripcion = "",
                            SecMatCurso = Convert.ToInt32(item["SecMatCurso"]),
                            IdMatricula = Convert.ToInt32(item["IdMatricula"]),
                            P1 = (item["P1"] != DBNull.Value) ? Convert.ToDecimal(item["P1"]) : 0,
                            P2 = (item["P2"] != DBNull.Value) ? Convert.ToDecimal(item["P2"]) : 0,
                            P3 = (item["P3"] != DBNull.Value) ? Convert.ToDecimal(item["P3"]) : 0,
                            P4 = (item["P4"] != DBNull.Value) ? Convert.ToDecimal(item["P4"]) : 0,
                        };
                        lista.Add(Reg);

                        int idAño = Convert.ToInt32(txtAñoLectivo.ValorTextBox);
                        int idCurso = Convert.ToInt32(txtCurso.ValorTextBox);
                        int idMateria = Convert.ToInt32(txtMateria.ValorTextBox);
                        long idMaestro = Convert.ToInt64(txtMaestro.ValorTextBox);
                        int idPeriodo = Convert.ToInt32(lkePeriodo.lkeDatos.EditValue);

                        switch (idPeriodo)
                        {
                            case 1:
                                ADInasistenciaET IA1 = new ADInasistenciaET()
                                {
                                    CantInasistencias = (item["IAP1"] != DBNull.Value) ? Convert.ToInt32(item["IAP1"]) : 0,
                                    IdCurso = idCurso,
                                    IdEstudiante = Convert.ToInt64(item["IdTercero"]),
                                    IdMateria = idMateria,
                                    IdPeriodo = 1,
                                    IdMaestro = idMaestro
                                };
                                listaIA.Add(IA1);
                                break;

                            case 2:
                                ADInasistenciaET IA2 = new ADInasistenciaET()
                                {
                                    CantInasistencias = (item["IAP2"] != DBNull.Value) ? Convert.ToInt32(item["IAP2"]) : 0,
                                    IdCurso = idCurso,
                                    IdEstudiante = Convert.ToInt64(item["IdTercero"]),
                                    IdMateria = idMateria,
                                    IdPeriodo = 2,
                                    IdMaestro = idMaestro
                                };
                                listaIA.Add(IA2);
                                break;
                            case 3:
                                ADInasistenciaET IA3 = new ADInasistenciaET()
                                {
                                    CantInasistencias = (item["IAP3"] != DBNull.Value) ? Convert.ToInt32(item["IAP3"]) : 0,
                                    IdCurso = idCurso,
                                    IdEstudiante = Convert.ToInt64(item["IdTercero"]),
                                    IdMateria = idMateria,
                                    IdPeriodo = 3,
                                    IdMaestro = idMaestro
                                };
                                listaIA.Add(IA3);
                                break;
                            case 4:
                                ADInasistenciaET IA4 = new ADInasistenciaET()
                                {
                                    CantInasistencias = (item["IAP4"] != DBNull.Value) ? Convert.ToInt32(item["IAP4"]) : 0,
                                    IdCurso = idCurso,
                                    IdEstudiante = Convert.ToInt64(item["IdTercero"]),
                                    IdMateria = idMateria,
                                    IdPeriodo = 4,
                                    IdMaestro = idMaestro
                                };
                                listaIA.Add(IA4);
                                break;

                            default:
                                break;
                        }

                    }
                    ADDesempeñosMateriaCursoET Desempeños = new ADDesempeñosMateriaCursoET()
                    {
                        Sec = this.Desempeños.Sec,
                        Desempeño = txtDesempeño.Text,
                        SecMateriaCurso = Convert.ToInt32(dt.Rows[0]["SecMatCurso"]),
                        SecPeriodo = Convert.ToInt32(lkePeriodo.lkeDatos.EditValue)
                    };
                    bool res = clNotas.InsertNotas(lista, Desempeños, listaIA);
                    if (res)
                    {
                        ClFunciones.msgExitoso("Insercion de notas realizada de manera correcta.");
                        gcMaterias.DataSource = new DataTable();
                        txtDesempeño.Text = "";
                    } else
                        ClFunciones.msgError("Lo sentimos, ha ocurrio un error!");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }           
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmReportarNotas_SizeChanged(object sender, EventArgs e)
        {
            try
            {
                if (gvMaterias.Columns.Count>0)
                {
                    //gvMaterias.Columns[0].Width = 140;
                    //gvMaterias.Columns[1].Width = groupControl1.Width-(((gvMaterias.Columns.Count-2)*20)+300);

                }
            }
            catch (Exception ex)
            {
            }
        }

        private void gvMaterias_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            //if (Convert.ToDecimal(e.Value)>5)
            //{
            //    ClFunciones.msgError("La nota maxima es de 5");
            //    e.Value = 5;
            //}
        }
        public bool IsNumeric(string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        private void gvMaterias_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            try
            {
                List<string> p = new List<string>() { "P1", "P2", "P3", "P4" };
                if (p.Contains(e.Column.FieldName))
                {
                    if (IsNumeric(e.Value.ToString()))
                    {
                        if (Convert.ToDecimal(e.Value) > 5)
                        {
                            ClFunciones.msgError("La nota maxima es de 5");
                            gvMaterias.SetRowCellValue(e.RowHandle, e.Column, 5);
                        }
                        else if (Convert.ToDecimal(e.Value) < 0)
                        {
                            ClFunciones.msgError("La nota minima es de 0");
                            gvMaterias.SetRowCellValue(e.RowHandle, e.Column, 0);
                        }
                    }
                    else
                    {
                        gvMaterias.SetRowCellValue(e.RowHandle, e.Column, "");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }          
        }

        private void txtAñoLectivo_SaleControl(object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {

            }
        }
    }
}
