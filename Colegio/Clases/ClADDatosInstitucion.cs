﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClADDatosInstitucion
    {
        public List<ADDatosInstitucionET> GetDatosInstitucion()
        {
            try
            {
                string sql = "SELECT Sec, Nit, NombreCompleto, NombreCorto, PeriodoActual, CodDANA, NombreRector, DocRector, IdAñoActual, ReportarNotas FROM ADDatosInstitucion";
                List<ADDatosInstitucionET> wADDatosInstitucion = ClFunciones.DataReaderMapToList<ADDatosInstitucionET>(sql);
                return wADDatosInstitucion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
