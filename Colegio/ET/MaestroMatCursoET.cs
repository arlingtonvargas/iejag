﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class MaestroMatCursoET
    {
        public int Sec { get; set; }
        public int IdMateria { get; set; }
        public string NomMateria { get; set; }
        public long IdTercero { get; set; }
        public string NomMaestro { get; set; }
        public int IdCurso { get; set; }
        public string NomCurso { get; set; }
    }
}
