﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClADParentesco
    {
        public DataTable GetParentescos()
        {
            try
            {
                string sql = "SELECT IdParentesco, Nombre FROM ADParentesco";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }

        }
    }
}
