﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADDesempeñosMateriaCursoET
    {
        public int Sec { get; set; }
        public int SecMateriaCurso { get; set; }
        public int SecPeriodo { get; set; }
        public string Desempeño { get; set; }
    }
}
