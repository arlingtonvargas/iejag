USE [BDIEJAG]
GO
/****** Object:  StoredProcedure [dbo].[SP_ADDesempeñosMateriaCurso_G]    Script Date: 22/11/2018 9:12:45 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 07-11-2018
-- Description:	Consulta los ADDesempeñosMateriaCurso.
-- =============================================
ALTER PROCEDURE [dbo].[SP_ADDesempeñosMateriaCurso_G]
	
	@pSecMateriaCurso	int,
	@pSecPeriodo	int

AS
BEGIN

DECLARE 
	@SecMateriaCurso int = @pSecMateriaCurso,
	@SecPeriodo int = @pSecPeriodo
	SET NOCOUNT ON;

	SELECT Sec, SecMateriaCurso, SecPeriodo, Desempeño
	FROM ADDesempeñosMateriaCurso AD WHERE SecMateriaCurso = @SecMateriaCurso AND SecPeriodo = @SecPeriodo
END
