﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClTercero
    {
        /// <summary>
        /// Retorna los terceros por tipo; {1}=Maestro; {2}=Estudiante;{3}=Administrativo.
        /// </summary>
        /// <param name="tipoTercero">{1}=Maestro; {2}=Estudiante;{3}=Administrativo</param>
        /// <returns></returns>
        public DataTable GetTercerosTipoTer(int tipoTercero)
        {
            try
            {
                string sql = "SELECT M.IdTercero " +
                " ,CONCAT(RTRIM(LTRIM(M.Papellido)), ' ', RTRIM(LTRIM(M.Sapellido)), ' ', RTRIM(LTRIM(M.Pnombre)), ' ', RTRIM(LTRIM(M.Snombre))) AS NombreComp" +
                " ,M.TipoDoc" +
                " ,M.Pnombre" +
                " ,M.Snombre" +
                " ,M.Papellido" +
                " ,M.Sapellido" +
                " ,M.Correo" +
                " ,M.Direccion" +
                " ,M.Sexo" +
                " ,M.FechaNacimiento" +
                " ,M.LugarNaciento AS IdLugarNacimiento" +
                " ,MN.NombreMunicipio AS NomLugarNacimiento" +
                " ,M.Telefono" +
                " ,M.FechaCrea" +
                " ,M.IdUsuario" +
                " ,U.NomUsu" +
                " ,U.IdRol" +
                " ,RL.NomRol" +
                " ,DP.CodDpto" +
                " ,M.TipoTercero" +
                " ,M.Foto" +
                " ,DP.Pais" +
                " ,M.RH" +
                " ,M.EPS" +
                " FROM ADTerceros M" +
                " INNER JOIN SGUsuarios U ON M.IdUsuario = U.IdUsuario" +
                " INNER JOIN SGRoles RL ON U.IdRol = RL.Sec" +
                " LEFT JOIN GEMunicipio MN ON M.LugarNaciento = MN.IdMunicipio" +
                " LEFT JOIN GEDepartamento DP ON MN.Departamento = DP.CodDpto" +
                " WHERE M.TipoTercero = " + tipoTercero.ToString()+ " ORDER BY NombreComp";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Retorna los terceros por tipo; {1}=Maestro; {2}=Estudiante;{3}=Administrativo.
        /// </summary>
        /// <param name="tipoTercero">{1}=Maestro; {2}=Estudiante;{3}=Administrativo</param>
        /// <returns></returns>
        public DataTable GetEstudiantesMatriculadosByAño(int pIdAño)
        {
            try
            {
                ParametersSpETList listParam = new ParametersSpETList()
                {
                    new ParametersSpET { NombreParametro ="@IdAñoLectivo", Valor= pIdAño.ToString()}                    
                };
                DataTable dt = ClFunciones.ConsultarConSP("[dbo].[SP_EstudiantesMatriculadosByAño_G]", listParam, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public DataTable GetTerceroById(long idTercero)
        {
            try
            {
                string sql = "SELECT M.IdTercero " +
                " ,CONCAT(RTRIM(LTRIM(M.Papellido)), ' ', RTRIM(LTRIM(M.Sapellido)), ' ', RTRIM(LTRIM(M.Pnombre)), ' ', RTRIM(LTRIM(M.Snombre))) AS NombreComp" +
                " ,M.TipoDoc" +
                " ,M.Pnombre" +
                " ,M.Snombre" +
                " ,M.Papellido" +
                " ,M.Sapellido" +
                " ,M.Direccion" +
                " ,M.Sexo" +
                " ,M.FechaNacimiento" +
                " ,M.LugarNaciento AS IdLugarNacimiento" +
                " ,MN.NombreMunicipio AS NomLugarNacimiento" +
                " ,M.Telefono" +
                " ,M.FechaCrea" +
                " ,M.IdUsuario" +
                " ,M.TipoTercero" +
                " ,U.NomUsu" +
                " ,U.IdRol" +
                " ,RL.NomRol" +
                " ,DP.CodDpto" +
                " ,M.TipoTercero" +
                " ,M.Foto" +
                " ,DP.Pais" +
                " ,M.RH" +
                " ,M.EPS" +
                " FROM ADTerceros M" +
                " INNER JOIN SGUsuarios U ON M.IdUsuario = U.IdUsuario" +
                " INNER JOIN SGRoles RL ON U.IdRol = RL.Sec" +
                " LEFT JOIN GEMunicipio MN ON M.LugarNaciento = MN.IdMunicipio" +
                " LEFT JOIN GEDepartamento DP ON MN.Departamento = DP.CodDpto" +
                " WHERE M.IdTercero = " + idTercero.ToString();
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertTercero(long IdMaestro,
                                int TipoDoc,
                                string Pnombre,
                                string Snombre,
                                string Papellido,
                                string Sapellido,
                                string Direccion,
                                string Correo,
                                double Telefono,
                                DateTime FechaCrea,
                                int UsuCrea,
                                int IdUsuario,
                                int Sexo,
                                DateTime FechaNacimiento,
                                string LugarNaciento,
                                int TipoTercero,
                                Image img,
                                string RH,
                                string EPS,
                                SqlConnection con)
        {
            try
            {
                string sql = string.Format("INSERT INTO [dbo].[ADTerceros] " +
                " ([IdTercero] " +
                " ,[TipoDoc] " +
                " ,[Pnombre] " +
                " ,[Snombre] " +
                " ,[Papellido] " +
                " ,[Sapellido] " +
                " ,[Direccion] " +
                " ,[Telefono] " +
                " ,[FechaCrea] " +
                " ,[UsuCrea] " +
                " ,[IdUsuario] " +
                " ,[Sexo] " +
                " ,[FechaNacimiento] " +
                " ,[LugarNaciento] " +
                " ,[TipoTercero]" +
                " ,[Correo]" +
                " ,[RH]" +
                " ,[EPS])" +
                " VALUES " +
                " ({0}, {14}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}, " +
                " '{7:dd/MM/yyyy HH:mm:ss}', {8}, {9}, {10}, " +
                " '{11:dd/MM/yyyy HH:mm:ss}', '{12}', {13}, '{15}', '{16}', '{17}')",
                IdMaestro, Pnombre, Snombre, Papellido, Sapellido,
                Direccion, Telefono, FechaCrea, UsuCrea, IdUsuario,
                Sexo, FechaNacimiento, LugarNaciento, TipoTercero, 
                TipoDoc, Correo, RH, EPS);
                string res = ClFunciones.EjecutaComando(sql, con);
                if (res == "si")
                {
                    if (img!=null)
                    {
                        byte[] arr;
                        using (MemoryStream ms = new MemoryStream())
                        {
                            img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            arr = ms.ToArray();
                        }
                        sql = string.Format("UPDATE ADTerceros SET Foto = '{0}' WHERE IdTercero = {1}", arr, IdMaestro);
                        if (ClFunciones.EjecutaComando(sql, con) == "no")
                        {
                            return false;
                        }
                    }                    
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateTercero(long IdMaestro,
                                int TipoDoc,
                                string Pnombre,
                                string Snombre,
                                string Papellido,
                                string Sapellido,
                                string Direccion,
                                string Correo,
                                double Telefono,
                                DateTime FechaMod,
                                int UsuMod,
                                int Sexo,
                                DateTime FechaNacimiento,
                                string LugarNaciento,
                                Image img,
                                string RH,
                                string EPS,
                                SqlConnection con)
        {
            try
            {
                string sql = string.Format("UPDATE [dbo].[ADTerceros] " +
                " SET[Pnombre] = '{0}'" +
                " ,[TipoDoc] = {13}" +
                " ,[Snombre] = '{1}'" +
                " ,[Papellido] = '{2}'" +
                " ,[Sapellido] = '{3}'" +
                " ,[Direccion] = '{4}'" +
                " ,[Telefono] = {5}" +
                " ,[FechaMod] = '{6:dd/MM/yyyy HH:mm:ss}'" +
                " ,[UsuMod] = {7}" +
                " ,[Sexo] = {8}" +
                " ,[FechaNacimiento] = '{9:dd/MM/yyyy}'" +
                " ,[LugarNaciento] = '{10}'" +
                " ,[Correo] = '{11}'" +
                " ,[RH] = '{14}'" +
                " ,[EPS] = '{15}'" + 
                " WHERE [IdTercero] = {12}",Pnombre,Snombre,
                Papellido,Sapellido,Direccion,Telefono,FechaMod,UsuMod,
                Sexo,FechaNacimiento,LugarNaciento,Correo, IdMaestro, TipoDoc, RH, EPS );
                string res = ClFunciones.EjecutaComando(sql, con);
                if (res == "si")
                {
                    if (img != null)
                    {
                        byte[] arr = ClFunciones.ImageToByteArray(img);


                        string qry = "UPDATE ADTerceros SET Foto =  @pFoto WHERE IdTercero = @pIdTercero";

                        //Initialize SqlCommand object for insert.
                        SqlCommand SqlCom = new SqlCommand(qry, con);

                        //We are passing Original Image Path and 
                        //Image byte data as SQL parameters.
                        SqlCom.Parameters.Add(new SqlParameter("@pIdTercero", (object)IdMaestro));
                        SqlCom.Parameters.Add(new SqlParameter("@pFoto", (object)arr));                     
                        SqlCom.ExecuteNonQuery();

                        //Image imgq = ClFunciones.byteArrayToImage(arr);
                        //sql = string.Format("UPDATE ADTerceros SET Foto = '{0}' WHERE IdTercero = {1}", arr, IdMaestro);
                        //if (ClFunciones.EjecutaComando(sql, con) == "no")
                        //{
                        //    return false;
                        //}
                    }
                    else
                    {
                        sql = string.Format("UPDATE ADTerceros SET Foto = '' WHERE IdTercero = {0}", IdMaestro);
                        if (ClFunciones.EjecutaComando(sql, con) == "no")
                        {
                            return false;
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public bool InsertTerceroExcel(long IdMaestro,
                              int TipoDoc,
                              string Pnombre,
                              string Snombre,
                              string Papellido,
                              string Sapellido,
                              string Direccion,
                              string Correo,
                              double Telefono,
                              DateTime FechaCrea,
                              int UsuCrea,
                              int IdUsuario,
                              int Sexo,
                              DateTime FechaNacimiento,
                              string LugarNaciento,
                              int TipoTercero,
                              Image img,
                              SqlConnection con)
        {
            try
            {
                string sql = string.Format("INSERT INTO [dbo].[ADTerceros] " +
                " ([IdTercero] " +
                " ,[TipoDoc] " +
                " ,[Pnombre] " +
                " ,[Snombre] " +
                " ,[Papellido] " +
                " ,[Sapellido] " +
                " ,[Direccion] " +
                " ,[FechaCrea] " +
                " ,[UsuCrea] " +
                " ,[IdUsuario] " +
                " ,[FechaNacimiento] " +
                " ,[TipoTercero]" +
                " ,[Correo]" +
                " ,[Telefono])" +
                " VALUES " +
                " ({0}, {1}, '{2}', '{3}', '{4}', '{5}', '{6}', " +
                " '{7:dd/MM/yyyy HH:mm:ss}', {8}, {9}, '{10:dd/MM/yyyy HH:mm:ss}', {11}, '{12}', {13})",
                IdMaestro, TipoDoc, Pnombre, Snombre, Papellido, Sapellido,
                Direccion, FechaCrea, UsuCrea, IdUsuario,
                FechaNacimiento, TipoTercero, Correo, Telefono);
                string res = ClFunciones.EjecutaComando(sql, con);
                if (res == "si")
                {
                    if (img != null)
                    {
                        byte[] arr;
                        using (MemoryStream ms = new MemoryStream())
                        {
                            img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            arr = ms.ToArray();
                        }
                        sql = string.Format("UPDATE ADTerceros SET Foto = '{0}' WHERE IdTercero = {1}", arr, IdMaestro);
                        if (ClFunciones.EjecutaComando(sql, con) == "no")
                        {
                            return false;
                        }
                    }
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteTercero()
        {
            try
            {
                string sql = "";
                ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public DataTable GetTiposDocumentos()
        {
            try
            {
                string sql = "SELECT TD.TipoDoc, TD.Nombre FROM ADTipoDocumento TD";
                DataTable dt = ClFunciones.f.AbrirTabla(ClConexion.clConexion.Conexion, sql);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public ADTercerosCarnetETList GetEstudiantesPrintCarnet(int pIdAño, int? pSecCurso, long? pIdTercero)
        {
            try
            {
                ADTercerosCarnetETList wLista = new ADTercerosCarnetETList();
                ADTercerosCarnetET wItem = null;
                using (SqlConnection cnn = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "[SP_RPTEstudiantesCarnet_G]";
                        cmd.Parameters.AddWithValue("@IdTercero", pIdTercero);
                        cmd.Parameters.AddWithValue("@IdAñoLectivo", pIdAño);
                        cmd.Parameters.AddWithValue("@SecCurso", pSecCurso);
                        cnn.Open();
                        SqlDataReader rd = cmd.ExecuteReader();
                        while (rd.Read())
                        {
                            wItem = new ADTercerosCarnetET();
                            wItem.IdTercero = Convert.ToInt64(rd["IdTercero"]);
                            wItem.NombreComp = Convert.ToString(rd["NombreComp"]);
                            wItem.Papellido = Convert.ToString(rd["Papellido"]);
                            wItem.Sapellido = Convert.ToString(rd["Sapellido"]);
                            wItem.Pnombre = Convert.ToString(rd["Pnombre"]);
                            wItem.Snombre = Convert.ToString(rd["Snombre"]);
                            wItem.RH = Convert.ToString(rd["RH"]);
                            wItem.EPS = Convert.ToString(rd["EPS"]);
                            if (rd["Foto"] != DBNull.Value)
                            {
                                byte[] wFoto = (byte[])rd["Foto"];
                                wItem.Foto = ClFunciones.byteArrayToImage(wFoto);
                            }
                            wItem.Sede = Convert.ToString(rd["NomSede"]);
                            wItem.Grado = Convert.ToString(rd["NomGrado"]);
                            wLista.Add(wItem);
                        }
                    }
                    return wLista;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }



    }
}
