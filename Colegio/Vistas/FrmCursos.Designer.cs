﻿namespace Colegio.Vistas
{
    partial class FrmCursos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET3 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCursos));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            this.txtTitular = new dll.Controles.ucBusqueda();
            this.txtGrado = new dll.Controles.ucBusqueda();
            this.txtNombre = new dll.Controles.ucLabelTextBox();
            this.gcCursos = new DevExpress.XtraGrid.GridControl();
            this.gvCursos = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.btnLimpiar = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcCursos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCursos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTitular
            // 
            this.txtTitular.AnchoTextBox = 150;
            this.txtTitular.AnchoTitulo = 70;
            this.tableLayoutPanel3.SetColumnSpan(this.txtTitular, 2);
            this.txtTitular.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTitular.Location = new System.Drawing.Point(0, 0);
            this.txtTitular.Margin = new System.Windows.Forms.Padding(0);
            this.txtTitular.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtTitular.MensajeDeAyuda = null;
            this.txtTitular.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtTitular.Name = "txtTitular";
            this.txtTitular.PermiteSoloNumeros = false;
            this.txtTitular.Script = "";
            this.txtTitular.Size = new System.Drawing.Size(792, 28);
            this.txtTitular.TabIndex = 1;
            this.txtTitular.TextoTitulo = "Titular :";
            tituloColsBusquedaET3.Codigo = "Código";
            tituloColsBusquedaET3.Descripcion = "Descripción";
            this.txtTitular.TituloColsBusqueda = tituloColsBusquedaET3;
            this.txtTitular.ValorTextBox = "";
            // 
            // txtGrado
            // 
            this.txtGrado.AnchoTextBox = 150;
            this.txtGrado.AnchoTitulo = 70;
            this.txtGrado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGrado.Location = new System.Drawing.Point(0, 26);
            this.txtGrado.Margin = new System.Windows.Forms.Padding(0);
            this.txtGrado.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtGrado.MensajeDeAyuda = null;
            this.txtGrado.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtGrado.Name = "txtGrado";
            this.txtGrado.PermiteSoloNumeros = false;
            this.txtGrado.Script = "";
            this.txtGrado.Size = new System.Drawing.Size(396, 28);
            this.txtGrado.TabIndex = 2;
            this.txtGrado.TextoTitulo = "Grado :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtGrado.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtGrado.ValorTextBox = "";
            // 
            // txtNombre
            // 
            this.txtNombre.AnchoTitulo = 67;
            this.tableLayoutPanel3.SetColumnSpan(this.txtNombre, 2);
            this.txtNombre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNombre.Location = new System.Drawing.Point(3, 55);
            this.txtNombre.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNombre.MensajeDeAyuda = null;
            this.txtNombre.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.PermiteSoloNumeros = false;
            this.txtNombre.Size = new System.Drawing.Size(786, 24);
            this.txtNombre.TabIndex = 4;
            this.txtNombre.TextoTitulo = "Nombre :";
            this.txtNombre.ValorTextBox = "";
            // 
            // gcCursos
            // 
            this.gcCursos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcCursos.Location = new System.Drawing.Point(12, 120);
            this.gcCursos.MainView = this.gvCursos;
            this.gcCursos.Name = "gcCursos";
            this.gcCursos.Size = new System.Drawing.Size(796, 359);
            this.gcCursos.TabIndex = 3;
            this.gcCursos.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCursos});
            // 
            // gvCursos
            // 
            this.gvCursos.GridControl = this.gcCursos;
            this.gvCursos.Name = "gvCursos";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.tableLayoutPanel3);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(796, 104);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Datos básicos del curso";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.txtNombre, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtAño, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtGrado, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTitular, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(2, 22);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(792, 80);
            this.tableLayoutPanel3.TabIndex = 6;
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 80;
            this.txtAño.AnchoTitulo = 80;
            this.txtAño.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAño.Location = new System.Drawing.Point(396, 26);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(396, 28);
            this.txtAño.TabIndex = 3;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtAño.ValorTextBox = "";
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.groupControl2.Controls.Add(this.btnGuardar);
            this.groupControl2.Controls.Add(this.btnRefresh);
            this.groupControl2.Controls.Add(this.btnLimpiar);
            this.groupControl2.Controls.Add(this.btnSalir);
            this.groupControl2.Location = new System.Drawing.Point(814, 12);
            this.groupControl2.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(95, 467);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Opciones";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Appearance.Options.UseFont = true;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(6, 26);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(83, 37);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefresh.Appearance.Options.UseFont = true;
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(6, 111);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(83, 37);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refrescar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Appearance.Options.UseFont = true;
            this.btnLimpiar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpiar.ImageOptions.Image")));
            this.btnLimpiar.Location = new System.Drawing.Point(6, 68);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(83, 37);
            this.btnLimpiar.TabIndex = 2;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Appearance.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Appearance.Options.UseFont = true;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(7, 154);
            this.btnSalir.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(83, 37);
            this.btnSalir.TabIndex = 4;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // FrmCursos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 488);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.gcCursos);
            this.Controls.Add(this.groupControl1);
            this.Name = "FrmCursos";
            this.Text = "Cursos";
            this.Load += new System.EventHandler(this.FrmCursos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcCursos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCursos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private dll.Controles.ucBusqueda txtTitular;
        private dll.Controles.ucBusqueda txtGrado;
        private dll.Controles.ucLabelTextBox txtNombre;
        private DevExpress.XtraGrid.GridControl gcCursos;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCursos;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private dll.Controles.ucBusqueda txtAño;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.SimpleButton btnLimpiar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
    }
}