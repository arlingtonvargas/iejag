﻿using Colegio.Clases;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

namespace Colegio.Vistas
{
    public partial class FrmNuevoMaestro : XtraForm
    {
        public bool EstaActualizando = false;
        public bool EsMaestro = false;
        ClRoles clRoles = new ClRoles();
        ClTercero clTercero = new ClTercero();
        ClUsuarios clUsuarios = new ClUsuarios();
        ClMuniDptoPais clMuniDptoPais = new ClMuniDptoPais();
        ET.ADTercerosET pMaestro = new ET.ADTercerosET();
        public FrmNuevoMaestro(ET.ADTercerosET _pMaestro)
        {
            InitializeComponent();
            pMaestro = _pMaestro;
        }

        private void FrmNuevoMaestro_Load(object sender, EventArgs e)
        {
            LlenarCbxRoles();
            LlenarCbxPaises();
            LlenarCbxTipoDoc();
            txtDocumento.txtCampo.Properties.MaxLength = 18;
            txtTel.txtCampo.Properties.MaxLength = 18;
            txtTel.PermiteSoloNumeros = true;
            if (pMaestro != null)
            {
                LlenarCampos();
            }
            txtDocumento.Focus();
        }

        private void ucBtnCancelarAV1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        #region[Eventos]

        private void cbxRol_Enter(object sender, EventArgs e)
        {
            LookUpEdit lkp = sender as LookUpEdit;
            lkp.ShowPopup();
        }

        private void cbxSexoMaestro_Enter(object sender, EventArgs e)
        {
            ComboBoxEdit cbx = sender as ComboBoxEdit;
            cbx.ShowPopup();
        }
        private void cbxPais_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                cbxDpto.Properties.DataSource = new DataTable();
                cbxMuni.Properties.DataSource = new DataTable();
                if (cbxPais.ItemIndex > 0)
                {
                    LlenarCbxDpto(cbxPais.EditValue.ToString());
                    //cbxDpto.Focus();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void cbxDpto_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                cbxMuni.Properties.DataSource = new DataTable();
                if (cbxDpto.ItemIndex > 0)
                {
                    LlenarCbxMunis(cbxDpto.EditValue.ToString());
                    //cbxDpto.Focus();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void cbxRol_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
            else if (e.KeyChar == (char)Keys.Escape)
            {
                e.Handled = true;
                SendKeys.Send("+{TAB}");
            }
        }
        #endregion

        #region[LlenarCombos]

        private void LlenarCbxPaises()
        {
            try
            {
                cbxPais.Properties.DataSource = clMuniDptoPais.GetPaises();
                cbxPais.Properties.ValueMember = "CodPais";
                cbxPais.Properties.DisplayMember = "NomPais";
                cbxPais.Properties.PopulateColumns();
                cbxPais.Properties.ShowFooter = false;
                cbxPais.Properties.ShowHeader = false;
                cbxPais.Properties.Columns[0].Visible = false;
                cbxPais.Properties.Columns[2].Visible = false;

            }
            catch (Exception ex)
            {
            }
        }

        private void LlenarCbxDpto(string idPais)
        {
            try
            {
                cbxDpto.Properties.DataSource = clMuniDptoPais.GetDptosByPais(idPais);
                cbxDpto.Properties.ValueMember = "CodDpto";
                cbxDpto.Properties.DisplayMember = "NomDpto";
                cbxDpto.Properties.PopulateColumns();
                cbxDpto.Properties.ShowFooter = false;
                cbxDpto.Properties.ShowHeader = false;
                cbxDpto.Properties.Columns[0].Visible = false;
                cbxDpto.Properties.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
            }
            
        }

        private void LlenarCbxMunis(string idDpto)
        {
            try
            {
                cbxMuni.Properties.DataSource = clMuniDptoPais.GetMuniByDpto(idDpto);
                cbxMuni.Properties.ValueMember = "IdMunicipio";
                cbxMuni.Properties.DisplayMember = "NombreMunicipio";
                cbxMuni.Properties.PopulateColumns();
                cbxMuni.Properties.ShowFooter = false;
                cbxMuni.Properties.ShowHeader = false;
                cbxMuni.Properties.Columns[0].Visible = false;
                cbxMuni.Properties.Columns[1].Visible = false;
            }
            catch (Exception ex)
            {
            }
            
        }

        private void LlenarCbxRoles()
        {
            try
            {
                DataTable dt = clRoles.GetRoles();
                cbxRoles.Properties.DataSource = dt;
                cbxRoles.Properties.ValueMember = "Sec";
                cbxRoles.Properties.DisplayMember = "NomRol";
                cbxRoles.Properties.PopulateColumns();
                cbxRoles.Properties.ShowFooter = false;
                cbxRoles.Properties.ShowHeader = false;
                cbxRoles.Properties.DropDownRows = dt.Rows.Count;
                cbxRoles.Properties.Columns[0].Visible = false;
                cbxRoles.Properties.Columns[2].Visible = false;
                cbxRoles.Properties.Columns[3].Visible = false;
                cbxRoles.Properties.Columns[4].Visible = false;
                cbxRoles.Properties.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
            }
            
        }
        private void LlenarCbxTipoDoc()
        {
            try
            {
                DataTable dt = clTercero.GetTiposDocumentos();
                DataTable dtNew = new DataTable();
                dtNew.Columns.Add("TipoDoc", typeof(String));
                dtNew.Columns.Add("Nombre", typeof(String));
                foreach (DataRow item in dt.Rows)
                {
                    DataRow newFila = dtNew.NewRow();
                    newFila["TipoDoc"] = item["TipoDoc"].ToString();
                    newFila["Nombre"] = item["Nombre"].ToString();
                    dtNew.Rows.Add(newFila);
                    dtNew.AcceptChanges();
                }
                cbxTipoDoc.Properties.DataSource = dtNew;
                cbxTipoDoc.Properties.ValueMember = "TipoDoc";
                cbxTipoDoc.Properties.DisplayMember = "Nombre";
                cbxTipoDoc.Properties.PopulateColumns();
                cbxTipoDoc.Properties.ShowFooter = false;
                cbxTipoDoc.Properties.ShowHeader = false;
                cbxTipoDoc.Properties.Columns[0].Visible = false;
                cbxTipoDoc.Properties.DropDownRows = dt.Rows.Count;
            }
            catch (Exception ex)
            {

            }          
        }

        #endregion

        #region[Validacion]

        private bool ValidaCampos()
        {
            try
            {
                DateTime fechaNull = new DateTime();
                if (cbxTipoDoc.ItemIndex <= -1)
                {
                    ClFunciones.msgError("Debe seleccionar el tipo de documento.");                    
                    cbxTipoDoc.Focus();
                    return false;
                }else if (txtDocumento.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el número de documento.");                   
                    txtDocumento.Focus();
                    return false;
                }
                else if (txtPnom.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el primer nombre.");                   
                    txtPnom.Focus();
                    return false;
                }
                else if (txtPape.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el primer apellido.");                   
                    txtPape.Focus();
                    return false;
                }
                //else if (txtTel.ValorTextBox == "")
                //{
                //    ClFunciones.msgError("Debe digitar el teléfono.");                   
                //    txtTel.Focus();
                //    return false;
                //}
                else if (cbxRoles.ItemIndex <= -1)
                {
                    ClFunciones.msgError("Debe seleccionar el rol inical.");                   
                    cbxRoles.Focus();
                    return false;
                }
                else if (cbxSexoMaestro.SelectedIndex <= 0)
                {
                    ClFunciones.msgError("Debe seleccionar el sexo.");                   
                    cbxSexoMaestro.Focus();
                    return false;
                }
                else if (dtpFecNacMaestro.DateTime.Date == fechaNull)
                {
                    ClFunciones.msgError("Debe seleccionar la fecha de nacimiento.");                   
                    dtpFecNacMaestro.Focus();
                    return false;
                }
                //else if (cbxMuni.ItemIndex <= -1)
                //{
                //    ClFunciones.msgError("Debe seleccionar el lugar de nacimiento.");                   
                //    cbxMuni.Focus();
                //    return false;
                //}
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion


        #region[LimpiarCampos]
        private void LimpiarCampos()
        {
            txtDocumento.ValorTextBox = "";
            txtPnom.ValorTextBox = "";
            txtSnom.ValorTextBox = "";
            txtPape.ValorTextBox = "";
            txtSape.ValorTextBox = "";
            txtTel.ValorTextBox = "";
            txtUsu.ValorTextBox = "";
            cbxRoles.ItemIndex = -1;
            txtDir.ValorTextBox = "";
            cbxSexoMaestro.SelectedIndex = 0;
            dtpFecNacMaestro.DateTime = DateTime.Now;
            cbxPais.ItemIndex = -1;
            cbxDpto.ItemIndex = -1;
            cbxMuni.ItemIndex = -1;
            EstaActualizando = false;
            txtDocumento.Focus();
        }
        #endregion

        private void cbxMuni_Leave(object sender, EventArgs e)
        {
            btnGuardar.btnGuardar.Focus();
        }

        private void btnGuardar_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            if (ValidaCampos())
            {
                bool inserto = true;
                int tipoTercer = 3;
                FrmMensaje frm = new FrmMensaje();
                try
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                        {
                            con.Open();
                            if (EstaActualizando)
                            {
                                bool res = clUsuarios.UpdateUsuario(pMaestro.IdUsuario, 
                                                                    ClFunciones.UsuarioIngreso.IdUsuario, 
                                                                    false, Convert.ToInt32(cbxRoles.EditValue), con);
                                if (res)
                                {
                                    res = clTercero.UpdateTercero(Convert.ToInt64(txtDocumento.ValorTextBox)
                                                                        , Convert.ToInt32(cbxTipoDoc.EditValue)
                                                                        , txtPnom.ValorTextBox
                                                                        , txtSnom.ValorTextBox
                                                                        , txtPape.ValorTextBox
                                                                        , txtSape.ValorTextBox
                                                                        , txtDir.ValorTextBox
                                                                        , txtCorreo.ValorTextBox
                                                                        , Convert.ToDouble(txtTel.ValorTextBox)
                                                                        , DateTime.Now
                                                                        , ClFunciones.UsuarioIngreso.IdUsuario
                                                                        , cbxSexoMaestro.SelectedIndex
                                                                        , dtpFecNacMaestro.DateTime
                                                                        , cbxMuni.EditValue.ToString()
                                                                        , null
                                                                        , null
                                                                        , null
                                                                        , con);

                                    if (res)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        inserto = false;
                                    }
                                }                             
                            }
                            else
                            {
                                string psw = txtDocumento.ValorTextBox.Substring(txtDocumento.ValorTextBox.Length - 4, 4);
                                int idNewUsu = clUsuarios.CrearUsuario(txtUsu.ValorTextBox
                                                                    , ClFunciones.UsuarioIngreso.IdUsuario
                                                                    , DateTime.Now, psw
                                                                    , Convert.ToInt32(cbxRoles.EditValue), con);
                                if (idNewUsu > 0)
                                {
                                    if (EsMaestro)
                                    {
                                        tipoTercer = 1;
                                    }
                                    bool res = clTercero.InsertTercero(Convert.ToInt64(txtDocumento.ValorTextBox)
                                                                    , Convert.ToInt32(cbxTipoDoc.EditValue)
                                                                    , txtPnom.ValorTextBox
                                                                    , txtSnom.ValorTextBox
                                                                    , txtPape.ValorTextBox
                                                                    , txtSape.ValorTextBox
                                                                    , txtDir.ValorTextBox
                                                                    , txtCorreo.ValorTextBox
                                                                    , Convert.ToDouble(txtTel.ValorTextBox)
                                                                    , DateTime.Now
                                                                    , ClFunciones.UsuarioIngreso.IdUsuario
                                                                    , idNewUsu
                                                                    , cbxSexoMaestro.SelectedIndex                                                                   
                                                                    , dtpFecNacMaestro.DateTime
                                                                    , cbxMuni.EditValue.ToString()
                                                                    , tipoTercer
                                                                    , null
                                                                    , null
                                                                    , null
                                                                    , con);
                                    if (res)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        inserto = false;
                                    }
                                }
                                else
                                {
                                    inserto = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    inserto = false;
                }

                if (inserto)
                {
                    frm.lblMensaje.Text = string.Format("Andministrativo creado de manera correcta, el usuario es {0} y la contraseña provisional son los 4 digitos finales del id.", txtUsu.ValorTextBox);
                    if (EsMaestro)
                    {
                        frm.lblMensaje.Text = string.Format("Maestro creado de manera correcta, el usuario es {0} y la contraseña provisional son los 4 digitos finales del id.", txtUsu.ValorTextBox);
                    }
                    if (EstaActualizando)
                    {
                        frm.lblMensaje.Text = "Información actualizada de manera correcta.";
                    }
                    frm.ShowDialog();
                    this.Close();
                    //LimpiarCampos();
                }
                else
                {
                    frm.lblMensaje.Text = "Lo sentimos, ocurrio un error, por favor intentelo nuevamente.";
                    frm.ShowDialog();
                }

            }
        }

        private void txtPnom_Leave(object sender, EventArgs e)
        {
            if (EstaActualizando)
                return;
            txtUsu.ValorTextBox = "";
            if (txtPnom.ValorTextBox.Length > 0 && txtPape.ValorTextBox.Length > 0)
            {
                CrearNomUsu();
            }
        }

        private void txtPape_Leave(object sender, EventArgs e)
        {
            if (EstaActualizando)
                return;
            txtUsu.ValorTextBox = "";
            if (txtPnom.ValorTextBox.Length > 0 && txtPape.ValorTextBox.Length > 0)
            {
                CrearNomUsu();
            }
        }

        private void CrearNomUsu()
        {
            string nomUsu = clUsuarios.CreaNomUsuario(txtPnom.ValorTextBox, txtPape.ValorTextBox, ClConexion.clConexion.Conexion);
            if (nomUsu.Length > 20)
            {
                nomUsu = nomUsu.Substring(0, 20);
            }
            txtUsu.ValorTextBox = nomUsu;
        }

        private void txtIdMaestro_Leave(object sender, EventArgs e)
        {            
            if (txtDocumento.ValorTextBox.Length > 0)
            {
                if (EstaActualizando)
                    return;
                DataTable dt = clTercero.GetTerceroById(Convert.ToInt64(txtDocumento.ValorTextBox));
                if (dt.Rows.Count > 0)
                {
                    int TipoTercero = Convert.ToInt32(dt.Rows[0]["TipoTercero"]);
                    if (TipoTercero == 1)
                    {                       
                        FrmMensaje f = new FrmMensaje();
                        f.lblMensaje.Text = string.Format("Ya existe un usuario con el numero de id {0}.", txtDocumento.ValorTextBox);
                        f.ShowDialog();
                        return;
                    }
                    FrmConfirmacion frm = new FrmConfirmacion();
                    frm.lblMensaje.Text = string.Format("Ya existe un usuario con el numero de id {0}, desea cargar la información?", txtDocumento.ValorTextBox);
                    frm.ShowDialog();
                    if (frm.Confirmo)
                    {
                        CargarDatosMaestro(dt);
                        EsMaestro = true;
                        txtPnom.BackColor = Color.LightCyan;
                    }
                    else
                    {
                        //txtDocumento.Focus();
                        cbxTipoDoc.Focus();
                        txtPnom.BackColor = Color.White;
                    }
                }
            }
        }

        private void CargarDatosMaestro(DataTable dt)
        {
            try
            {
                ET.ADTercerosET clMaestroET = new ET.ADTercerosET();
                DataRow fila = dt.Rows[0];
                clMaestroET.IdTercero = Convert.ToInt64(fila["IdMaestro"]);
                clMaestroET.NombreComp = fila["NombreComp"].ToString();
                clMaestroET.Pnombre= fila["Pnombre"].ToString();
                clMaestroET.Snombre= fila["Snombre"].ToString();
                clMaestroET.Papellido= fila["Papellido"].ToString();
                clMaestroET.Sapellido= fila["Sapellido"].ToString();
                clMaestroET.Direccion= fila["Direccion"].ToString();
                clMaestroET. Correo= fila["Correo"].ToString();
                clMaestroET.Sexo= Convert.ToInt32(fila["Sexo"]);
                clMaestroET.FechaNacimiento= Convert.ToDateTime(fila["FechaNacimiento"]);
                clMaestroET.IdLugarNacimiento= fila["IdLugarNacimiento"].ToString();
                clMaestroET.NomLugarNacimiento= fila["NomLugarNacimiento"].ToString();
                clMaestroET.Telefono= Convert.ToDouble(fila["Telefono"]);
                clMaestroET.FechaCrea= Convert.ToDateTime(fila["FechaCrea"]);
                clMaestroET.IdUsuario= Convert.ToInt32(fila["IdUsuario"]);
                clMaestroET.NomUsu= fila["NomUsu"].ToString();
                clMaestroET.IdRol= Convert.ToInt32(fila["IdRol"]);
                clMaestroET.NomRol= fila["NomRol"].ToString();
                clMaestroET.CodDpto= fila["CodDpto"].ToString();
                clMaestroET.Pais= fila["Pais"].ToString();
                pMaestro = clMaestroET;
                EstaActualizando = true;
                LlenarCampos();
                 
            }
            catch (Exception ex)
            {

            }
        }

        private void LlenarCampos()
        {
            try
            {
                txtDocumento.ValorTextBox = pMaestro.IdTercero.ToString();
                txtPnom.ValorTextBox = pMaestro.Pnombre;
                txtSnom.ValorTextBox = pMaestro.Snombre;
                txtPape.ValorTextBox = pMaestro.Papellido;
                txtSape.ValorTextBox = pMaestro.Sapellido;
                txtTel.ValorTextBox = pMaestro.Telefono.ToString();
                txtDir.ValorTextBox = pMaestro.Direccion;
                txtCorreo.ValorTextBox = pMaestro.Correo;
                txtUsu.ValorTextBox = pMaestro.NomUsu;
                cbxRoles.EditValue = pMaestro.IdRol;
                cbxTipoDoc.EditValue = pMaestro.TipoDoc.ToString();
                cbxSexoMaestro.SelectedIndex = pMaestro.Sexo;
                dtpFecNacMaestro.DateTime = pMaestro.FechaNacimiento;
                cbxPais.EditValue = pMaestro.Pais.ToString();
                cbxDpto.EditValue = pMaestro.CodDpto.ToString();
                cbxMuni.EditValue = pMaestro.IdLugarNacimiento.ToString();
                txtDocumento.Enabled = false;
                //txtDocumento.Focus();
                cbxTipoDoc.Focus();
            }
            catch (Exception ex)
            {

            }
        }

     
    }
}





