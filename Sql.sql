SELECT RL.Sec, RL.NomRol, RL.FechaCrea, RL.FechaMod, RL.Anulado, RL.UsuCrea 
FROM SGRoles RL
WHERE ISNULL(RL.Anulado,0) = 0

INSERT INTO SGRoles (NomRol, FechaCrea, UsuCrea) VALUES ('','',)

SELECT * FROM SGUsuariosRoles WHERE IdRol = 1

SELECT PE.Sec, PE.NomPermiso, PE.DesPermiso
FROM SGPermisosRoles PR 
INNER JOIN SGPermisos PE ON PR.IdPermiso = PE.Sec
INNER JOIN SGRoles RL ON PR.IdRol = RL.Sec
WHERE PR.IdRol = 3 AND ISNULL(RL.Anulado,0) = 0

SELECT *
FROM SGRoles RL
LEFT JOIN SGPermisosRoles PR ON PR.IdRol = RL.Sec
WHERE PR.IdRol = 3 AND ISNULL(RL.Anulado,0) = 0

SELECT PE.Sec, PE.NomPermiso, PE.DesPermiso FROM SGPermisos PE WHERE Sec NOT IN 
	(SELECT IdPermiso FROM SGPermisosRoles PR INNER JOIN SGRoles RL ON  PR.IdRol = RL.Sec WHERE IdRol = 3 AND ISNULL(RL.Anulado,0) =0)


DELETE FROM SGPermisosRoles WHERE IdRol = 3

INSERT INTO SGPermisosRoles (IdRol, IdPermiso) VALUES ({0},{1})

SELECT US.IdUsuario, US.NomUsu, US.UsuCrea, US.FechaCrea,US.FechaMod, US.UsuMod, US.FechaUltIngreso FROM SGUsuarios US WHERE ISNULL(Anulado,0) = 0

SELECT RL.Sec, RL.NomRol FROM SGRoles RL 
INNER JOIN SGUsuariosRoles UR ON RL.Sec = UR.IdRol
WHERE UR.IdUsuario =1 AND ISNULL(RL.Anulado,0) = 0

UPDATE SGUsuariosRoles SET IdRol = 1 WHERE IdUsuario =1

SELECT PS.CodPais, PS.NomPais, PS.CodIso FROM GEPais PS

SELECT DP.Pais, DP.CodDpto, DP.NomDpto FROM GEDepartamento DP WHERE DP.Pais = 169

SELECT MN.IdMunicipio, MN.CodMunicipio, MN.NombreMunicipio FROM GEMunicipio MN WHERE MN.Departamento = ''

INSERT INTO [dbo].[ADMaestros]
           ([IdMaestro]
           ,[Pnombre]
           ,[Snombre]
           ,[Papellido]
           ,[Sapellido]
           ,[Direccion]
           ,[Telefono]
           ,[FechaCrea]
           ,[UsuCrea]
           ,[IdUsuario]
           ,[Sexo]
           ,[FechaNacimiento]
           ,[LugarNaciento])
     VALUES
           (1,'','','','','',12,'',1,'','','')



INSERT INTO [dbo].[SGUsuarios]
           ([NomUsu]
           ,[UsuCrea]
           ,[FechaCrea]
           ,[FechaMod]
           ,[password]
           ,[FechaUltIngreso]
           ,[Anulado])
     VALUES
           ('',1,'','','','', 0)

SELECT US.NomUsu FROM SGUsuarios US WHERE LOWER(US.NomUsu) = LOWER('admin')

SELECT ME.IdMaestro, ME.Pnombre FROM ADMaestros ME
INNER JOIN SGUsuarios US ON ME.IdUsuario = US.IdUsuario
INNER JOIN GEMunicipio MN ON ME.LugarNaciento = MN.IdMunicipio

SELECT M.IdMaestro  
,CONCAT(RTRIM(LTRIM(M.Pnombre)), ' ', RTRIM(LTRIM(M.Snombre)), ' ', RTRIM(LTRIM(M.Papellido)), ' ', RTRIM(LTRIM(M.Sapellido))) AS NombreComp  
,M.Pnombre  
,M.Snombre  
,M.Papellido  
,M.Sapellido  
,M.Direccion  
,M.Sexo
,M.FechaNacimiento
,M.LugarNaciento AS IdLugarNacimiento
,MN.NombreMunicipio AS NomLugarNacimiento
,M.Telefono  
,M.FechaCrea  
,M.IdUsuario  
,U.NomUsu  
,U.IdRol
,RL.NomRol
,DP.CodDpto
,DP.Pais
FROM ADMaestros M  
INNER JOIN SGUsuarios U ON M.IdUsuario = U.IdUsuario 
INNER JOIN SGRoles RL ON U.IdRol = RL.Sec
INNER JOIN GEMunicipio MN ON M.LugarNaciento = MN.IdMunicipio
INNER JOIN GEDepartamento DP ON MN.Departamento = DP.CodDpto
WHERE M.IdMaestro = 1117521997

SELECT M.IdMaestro  
,CONCAT(RTRIM(LTRIM(M.Pnombre)), ' ', RTRIM(LTRIM(M.Snombre)), ' ', RTRIM(LTRIM(M.Papellido)), ' ', RTRIM(LTRIM(M.Sapellido))) AS NombreComp  
,M.Pnombre  
,M.Snombre  
,M.Papellido  
,M.Sapellido  
,M.Direccion  
,M.Sexo
,M.FechaNacimiento
,M.LugarNaciento AS IdLugarNacimiento
--,MN.NombreMunicipio AS NomLugarNacimiento
,M.Telefono  
,M.FechaCrea  
,M.IdUsuario  
,U.NomUsu  
--,UR.IdRol
--,RL.NomRol
--,DP.CodDpto
--,DP.Pais
FROM ADMaestros M  
INNER JOIN SGUsuarios U ON M.IdUsuario = U.IdUsuario 
--INNER JOIN SGUsuariosRoles UR ON U.IdUsuario = UR.IdUsuario
--INNER JOIN SGRoles RL ON UR.IdRol = RL.Sec
--INNER JOIN GEMunicipio MN ON M.LugarNaciento = MN.IdMunicipio
--INNER JOIN GEDepartamento DP ON MN.Departamento = DP.CodDpto


UPDATE [dbo].[ADMaestros]
   SET [Pnombre] =				Pnombre,	
      ,[Snombre] =				Snombre,	
      ,[Papellido] =		 Papellido,	
      ,[Sapellido] =		 Sapellido,	
      ,[Direccion] =		 Direccion,	
      ,[Telefono] =				Telefono,	
      ,[FechaMod] =				FechaMod,		
      ,[UsuMod] =				UsuMod,		
      ,[Sexo] =				Sexo,			
      ,[FechaNacimiento] = FechaNacimiento,
      ,[LugarNaciento] =	LugarNaciento, 
 WHERE [IdMaestro] = 1
GO


SELECT US.IdUsuario, US.NomUsu, US.UsuCrea, US.FechaCrea, 
US.FechaMod, US.UsuMod, US.password, US.FechaUltIngreso, 
US.Anulado, US.IdRol 
FROM SGUsuarios US 
WHERE NomUsu = 'admin' and password = '123'

SELECT US.IdUsuario, US.NomUsu, US.UsuCrea AS IdUsuCra, 
(SELECT NomUsu FROM SGUsuarios WHERE IdUsuario = US.UsuCrea) AS NomUsuCrea 
,US.FechaCrea, US.UsuMod AS IdUsuMod, (SELECT NomUsu FROM SGUsuarios WHERE IdUsuario = US.UsuMod) AS NomUsuMod
,US.FechaMod, US.FechaUltIngreso, MS.IdTercero AS Documento
,CONCAT(RTRIM(LTRIM(MS.Pnombre)), ' ', RTRIM(LTRIM(MS.Snombre)), ' ', RTRIM(LTRIM(MS.Papellido)), ' ', RTRIM(LTRIM(MS.Sapellido))) AS NombreComp  
,US.Anulado
FROM SGUsuarios US
INNER JOIN ADTerceros MS ON US.IdUsuario = MS.IdUsuario

SELECT * FROM SGUsuarios US
INNER JOIN ADMaestros MS ON US.IdUsuario = MS.IdUsuario


SELECT TD.TipoDoc, TD.Nombre FROM ADTipoDocumento TD


SELECT PE.Nombre, MT.NomMateria, NT.* FROM ADNotas NT
INNER JOIN ADMateriasCursos MC ON NT.SecCursoMateria = MC.Sec
INNER JOIN ADMaterias MT ON MC.IdMateria = MT.Sec
INNER JOIN ADCursos CS ON MC.IdCurso = CS.Sec
INNER JOIN ADMatriculas ML ON CS.Sec = ML.IdCurso
INNER JOIN ADAñoLectivo AL ON ML.IdAñoLectivo = AL.Sec
INNER JOIN ADTerceros ET ON ML.IdEstudiante = ET.IdTercero
INNER JOIN ADPeriodosCurso PC ON CS.Sec = PC.IdCurso
INNER JOIN ADPeriodos PE ON PC.IdPeriodo = PE.Sec
WHERE GETDATE() BETWEEN AL.FechaDesde AND AL.FechaHasta



SELECT CS.Sec, CS.NomCurso, CS.IdTitular, CS.IdGrado FROM ADCursos CS
SELECT AL.Sec, AL.Año, AL.FechaDesde, AL.FechaHasta FROM ADAñoLectivo AL



SELECT * FROM ADMatriculas

SELECT MT.Sec AS IdMatricula, MT.FechaCrea, ET.IdTercero, 
CONCAT(Pnombre,' ', Snombre, ' ', Papellido, ' ', Sapellido) AS NombreCompleto
,US.NomUsu AS UsuCrea, CS.IdGrado, CS.NomCurso, AL.Año AS AñoLectivo
  FROM ADMatriculas MT 
INNER JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
INNER JOIN ADAñoLectivo AL ON MT.IdAñoLectivo = AL.Sec
INNER JOIN SGUsuarios US ON MT.UsuCrea = US.IdUsuario

select * from sglogs

SELECT CS.Sec AS IdCurso, CS.NomCurso, TI.IdTercero AS DocTitular
,CONCAT(TI.Pnombre,'',TI.Snombre,'',TI.Papellido,'',TI.Sapellido) AS NomTitular
, GD.Grado, GD.Nombre AS NomGrado FROM ADCursos CS 
INNER JOIN ADTerceros TI ON CS.IdTitular = TI.IdTercero
INNER JOIN ADCursoGrado CG ON CS.Sec = CG.IdCurso 
INNER JOIN ADGrados GD ON CG.IdGrado = GD.Sec


SELECT  MC.Sec, MC.IdMateria, MR.NomMateria, TR.IdTercero,
CONCAT(TR.Pnombre,' ',TR.Snombre,' ',TR.Papellido,' ',TR.Sapellido) AS NomMaestro,
MT.IdCurso, CS.NomCurso
FROM ADMateriasCursoMaestro MC
INNER JOIN ADMaestroMatCurso MT ON MC.IdMaestroMatCurso = MT.Sec
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
INNER JOIN ADMaterias MR ON MC.IdMateria = MR.Sec
INNER JOIN ADTerceros TR ON MT.IdMaestro = TR.IdTercero
WHERE CS.Sec = 1 AND MT.IdMaestro = 1117521997

SELECT Sec, NomCurso FROM ADCursos
--CARGA MAESTROS CON 
SELECT ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NomMaestro 
FROM ADMatriculas MT
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
INNER JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero
WHERE MT.IdAñoLectivo = 1 AND CS.Sec = 1

--CARGAR CORSOS ASOCIADOS A UN MAESTRO
SELECT MT.IdCurso, CS.NomCurso
FROM ADMateriasCursoMaestro MC
INNER JOIN ADMaestroMatCurso MT ON MC.IdMaestroMatCurso = MT.Sec
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
WHERE CS.Sec = 1 AND MT.IdMaestro = 1117521997

SELECT MC.IdMateria, MR.NomMateria
FROM ADMateriasCursoMaestro MC
INNER JOIN ADMaestroMatCurso MT ON MC.IdMaestroMatCurso = MT.Sec
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
INNER JOIN ADMaterias MR ON MC.IdMateria = MR.Sec
INNER JOIN ADTerceros TR ON MT.IdMaestro = TR.IdTercero
WHERE CS.Sec = 1 AND MT.IdMaestro = 1117521997


SELECT * FROM ADAñoLectivo AL
INNER JOIN ADMatriculas MT ON AL.Sec = MT.IdAñoLectivo
INNER JOIN ADCursos CS ON 




SELECT * FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente

SELECT MT.Nombre AS NomMateria, DC.NomCompleto AS Docente, NT.IH, NT.AUS, NT.p1, NT.p2, 
NT.p3,NT.p4, (ISNULL(NT.p1,0) + ISNULL(NT.p2,0) + ISNULL(NT.p3,0))/3 AS Prom,
NT.competencias, DC1.NomCompleto AS Titular  FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente
WHERE NT.idestudiante = 2

SELECT NT.idestudiante, AVG(ISNULL(NT.p3,0)) AS Prom FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente
WHERE GR.IdGrado =0
GROUP BY NT.idestudiante



SELECT Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, 
GD.Nombre, GD.Descripcion,(SELECT NT.idestudiante, AVG(ISNULL(NT.p3,0)) AS Prom FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente
WHERE GR.IdGrado =0 AND NT.idestudiante = ET.Documento
GROUP BY NT.idestudiante)  FROM local.Estudiantes ET
INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado

WHERE GD.IdGrado = 0


SELECT Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, 
GD.Nombre, GD.Descripcion,(SELECT AVG(ISNULL(NT.p3,0)) AS Prom FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente
WHERE GR.IdGrado =0 AND NT.idestudiante = ET.Documento
GROUP BY NT.idestudiante)  FROM local.Estudiantes ET
INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado
WHERE GD.IdGrado = 0
GROUP BY Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, 
GD.Nombre, GD.Descripcion


SELECT ROW_NUMBER() OVER (ORDER BY Z.PromPeriodo DESC) AS Posicion 
,Z.Documento, Z.NomCompleto, Z.Sede, Z.Jornada, Z.Codigo, Z.Grupo,
Z.Nombre, Z.Descripcion, Z.PromPeriodo
FROM (SELECT Documento, NomCompleto, 
Sede, Jornada, Codigo, Grupo, GD.Nombre, GD.Descripcion,
(SELECT AVG(ISNULL(NT.p3,0)) AS Prom FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente
WHERE GR.IdGrado =0 AND NT.idestudiante = ET.Documento
GROUP BY NT.idestudiante) AS PromPeriodo FROM local.Estudiantes ET
INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado
INNER JOIN local.Notas NT ON ET.Documento = NT.idestudiante
WHERE GD.IdGrado = 0
GROUP BY Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, 
GD.Nombre, GD.Descripcion) AS Z



SELECT ROW_NUMBER() OVER (ORDER BY (SELECT AVG(ISNULL(NT.p3,0)) AS Prom FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente
WHERE GR.IdGrado =0 AND NT.idestudiante = ET.Documento
GROUP BY NT.idestudiante) DESC) AS Posicion, Documento, NomCompleto, 
Sede, Jornada, Codigo, Grupo, GD.Nombre, GD.Descripcion,
(SELECT AVG(ISNULL(NT.p3,0)) AS Prom FROM local.Notas NT
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente
WHERE GR.IdGrado =0 AND NT.idestudiante = ET.Documento
GROUP BY NT.idestudiante) AS PromPeriodo FROM local.Estudiantes ET
INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado
INNER JOIN local.Notas NT ON ET.Documento = NT.idestudiante
WHERE GD.IdGrado = 0
GROUP BY Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, 
GD.Nombre, GD.Descripcion


SELECT * FROM local.Notas



SELECT IdGrado, Descripcion FROM local.Grados

SELECT MT.Nombre AS NomMateria, DC.NomCompleto AS Docente, NT.IH, NT.AUS, 
NT.p1, NT.p2,  NT.p3,NT.p4, NT.competencias, DC1.NomCompleto AS Titular 
FROM local.Notas NT  
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado  
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria  
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente  
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente  WHERE NT.idestudiante = 2


SELECT NT.*, MT.NomMateria FROM ADNotas NT
INNER JOIN ADMaterias MT ON NT.SecMateria = MT.Sec
ORDER BY SecMateria
WHERE IdEstudiante = 11187645



SELECT ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NombreComp 
FROM ADMatriculas MT  
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec  
INNER JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero  

WHERE MT.IdAñoLectivo = 1 AND CS.Sec = 1

--Consulta las materias para ingresar la nota
SELECT MC.Sec, ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NombreComp , 
MC.IdMateria, MR.NomMateria, MS.Sec AS IdMatricula, 
(SELECT P1 FROM ADNotas WHERE IdEstudiante = ET.IdTercero AND SecMateria = MR.Sec AND SecCurso = CS.Sec AND IdMatricula = MS.Sec) AS P1, 
(SELECT P2 FROM ADNotas WHERE IdEstudiante = ET.IdTercero AND SecMateria = MR.Sec AND SecCurso = CS.Sec AND IdMatricula = MS.Sec) AS P2, 
(SELECT P3 FROM ADNotas WHERE IdEstudiante = ET.IdTercero AND SecMateria = MR.Sec AND SecCurso = CS.Sec AND IdMatricula = MS.Sec) AS P3, 
(SELECT P4 FROM ADNotas WHERE IdEstudiante = ET.IdTercero AND SecMateria = MR.Sec AND SecCurso = CS.Sec AND IdMatricula = MS.Sec) AS P4
FROM ADMateriasCursos MC  
INNER JOIN ADMaestroMatCurso MT ON MC. = MT.Sec  
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec  
INNER JOIN ADMaterias MR ON MC.IdMateria = MR.Sec  
INNER JOIN ADTerceros TR ON MT.IdMaestro = TR.IdTercero  
INNER JOIN ADMatriculas MS ON CS.Sec = MS.IdCurso
INNER JOIN ADTerceros ET ON MS.IdEstudiante = ET.IdTercero  
WHERE CS.Sec = 1 AND MS.IdAñoLectivo = 1 AND MT.IdMaestro = 1117521997 AND IdMateria = 1
ORDER BY NombreComp

SELECT MC.Sec, MR.NomMateria, MS.Sec AS IdMatricula  FROM ADNotas NT 
INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec  
INNER JOIN ADMatriculas MS ON NT.SecCurso = MS.IdCurso
INNER JOIN ADMateriasCursos MC ON NT.SecCurso = MC.IdCurso AND NT.SecMateria = MC.IdMateria AND NT.IdMatricula = MS.Sec
INNER JOIN ADCursos CS ON MC.IdCurso = CS.Sec 
WHERE CS.Sec = 1 AND MS.IdAñoLectivo = 1 AND MC.IdMaestro = 1117521997 AND MC.IdMateria = 1
ORDER BY NombreComp

truncate table ADNotas

SELECT P1 FROM ADNotas WHERE IdEstudiante = 1234543 AND SecMateria = 1 AND SecCurso = 1 AND IdMatricula = 1

--UPDATE NT
--SET NT.IdMatricula = MT.Sec --etc
--FROM ADNotas NT
--JOIN ADMatriculas MT
--    ON NT.IdEstudiante = MT.IdEstudiante

SELECT MR.Sec, MR.NomMateria, P1, P2, P3, P4, 
((P1+P2+P3+P4)/4) AS Prom 
FROM ADNotas NT 
INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec 
INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec
WHERE MT.IdEstudiante = 1234543


SELECT NT.Sec, ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NombreComp ,
MR.Sec AS IdMateria, MR.NomMateria, MT.Sec AS IdMatricula, P1, P2, P3, P4, 
((P1+P2+P3+P4)/4) AS Prom 
FROM ADNotas NT 
INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec 
INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec
INNER JOIN ADMateriasCursos MC ON NT.SecCurso = MC.IdCurso AND NT.SecMateria = MC.IdMateria AND NT.IdMatricula = MT.Sec
INNER JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero
WHERE NT.SecCurso = 1 AND MT.IdAñoLectivo = 1 AND MC.IdMaestro = 1117521997 AND MC.IdMateria = 1
WHERE MT.IdEstudiante = 1234543


SELECT ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NombreComp , MR.Sec AS IdMateria, MR.NomMateria, MT.Sec AS IdMatricula, 
P1, P2, P3, P4,  ((P1 + P2 + P3 + P4) / 4) AS Prom  
FROM ADNotas NT  
INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec  
INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec  
INNER JOIN ADMateriasCursos MC ON NT.SecCurso = MC.IdCurso AND NT.SecMateria = MC.IdMateria AND NT.IdMatricula = MT.Sec  
INNER JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero 
 WHERE NT.SecCurso = 1 AND MT.IdAñoLectivo = 1 AND MC.IdMaestro = 1117521997 AND MC.IdMateria = 1 ORDER BY NombreComp


SELECT ROW_NUMBER() OVER (ORDER BY Z.PromPeriodo DESC) AS Posicion  
, Z.Documento, Z.NomCompleto, Z.Sede, Z.Jornada, Z.Codigo, Z.Grupo, 
Z.Nombre, Z.Descripcion, Z.PromPeriodo 
FROM(SELECT Documento, NomCompleto, 
Sede, Jornada, Codigo, Grupo, GD.Nombre, GD.Descripcion, 
(SELECT AVG(ISNULL(NT.p3, 0)) AS Prom FROM local.Notas NT 
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado 
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria 
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente 
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente 
WHERE GR.IdGrado = 1 AND NT.idestudiante = ET.Documento 
GROUP BY NT.idestudiante) AS PromPeriodo FROM local.Estudiantes ET 
INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado 
INNER JOIN local.Notas NT ON ET.Documento = NT.idestudiante 
WHERE GD.IdGrado =1 
GROUP BY Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, 
GD.Nombre, GD.Descripcion) AS Z

SELECT * FROM ADTerceros ET 
INNER JOIN ADMatriculas MT ON ET.IdTercero = MT.IdEstudiante 
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
WHERE MT.IdCurso = 1 AND MT.IdAñoLectivo = 1


SELECT ROW_NUMBER() OVER (ORDER BY Z.Promedio DESC) AS Posicion  
, Z.IdTercero, Z.NombreComp, Z.NomCurso, Z.NomSede, Z.NomJornada, Z.Promedio
FROM(SELECT ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NombreComp ,
CS.NomCurso, SD.NomSede, JD.NomJornada
,(SELECT AVG(ISNULL(P1, 0)) AS Prom 
	FROM ADNotas NT 
	INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec 
	INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec
	WHERE MT.IdEstudiante = ET.IdTercero) AS Promedio 
FROM ADTerceros ET 
INNER JOIN ADMatriculas MT ON ET.IdTercero = MT.IdEstudiante 
INNER JOIN ADSedes SD ON MT.IdSede = SD.Sec
INNER JOIN ADJornadas JD ON MT.IdJornada = JD.Sec
INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
WHERE MT.IdCurso = 1 AND MT.IdAñoLectivo = 1) AS Z

SELECT MT.Nombre AS NomMateria, DC.NomCompleto AS Docente, NT.IH, NT.AUS, NT.p1, NT.p2, 
NT.p3,NT.p4, (ISNULL(NT.p1,0) + ISNULL(NT.p2,0) + ISNULL(NT.p3,0))/3 AS Prom,
NT.competencias, CONCAT(DC1.NomCompleto,DC1.PerfilAbv) AS Titular FROM local.Notas NT 
INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado 
INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria 
INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente 
INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente 
WHERE NT.idestudiante = 1234543

SELECT MT.NomMateria,MS.IH, NT.P1, NT.P2, NT.P3, NT.P4, 
 AVG(ISNULL(P1, 0)) AS Prom, MS.Desempeños, CS.IdTitular FROM ADNotas NT 
INNER JOIN ADMaterias MT ON NT.SecMateria = MT.Sec
INNER JOIN ADMatriculas MC ON NT.IdMatricula = MC.Sec
INNER JOIN ADMateriasCursos MS ON MC.IdCurso = MS.IdCurso AND MT.Sec = MS.IdMateria
INNER JOIN ADCursos CS ON MS.IdCurso = CS.Sec
INNER JOIN ADTerceros DC ON MS.IdMaestro = DC.IdTercero
--LEFT JOIN ADInasistencia IA ON IA.
WHERE NT.IdEstudiante = 1234543
GROUP BY MT.NomMateria,MS.IH, NT.P1, NT.P2, NT.P3, NT.P4, MS.Desempeños, CS.IdTitular

SELECT MT.NomMateria, CONCAT(DC.Pnombre,' ',DC.Snombre,' ',DC.Papellido,' ',DC.Sapellido) AS Docente
,MS.IH, IA.CantInasistencias AS AUS, NT.P1, NT.P2, NT.P3, NT.P4, MS.Desempeños
,CONCAT(TI.Pnombre,' ',TI.Snombre,' ',TI.Papellido,' ',TI.Sapellido) AS Titular
FROM ADNotas NT 
INNER JOIN ADMaterias MT ON NT.SecMateria = MT.Sec
INNER JOIN ADMatriculas MC ON NT.IdMatricula = MC.Sec
INNER JOIN ADMateriasCursos MS ON MC.IdCurso = MS.IdCurso AND MT.Sec = MS.IdMateria
INNER JOIN ADTerceros DC ON MS.IdMaestro = DC.IdTercero
INNER JOIN ADCursos CS ON MS.IdCurso = CS.Sec
INNER JOIN ADTerceros TI ON CS.IdTitular = TI.IdTercero
LEFT JOIN ADInasistencia IA ON NT.IdEstudiante = IA.IdEstudiante AND CS.Sec = IA.IdCurso AND MT.Sec = IA.IdMateria
WHERE NT.IdEstudiante = 1234543

SELECT ET.IdTercero, CONCAT(ET.Pnombre,' ',ET.Snombre,' ',ET.Papellido,' ',ET.Sapellido) AS NombreComp 
, MR.Sec AS IdMateria, MR.NomMateria, MT.Sec AS IdMatricula, P1, P2, P3, P4,  ((P1 + P2 + P3 + P4) / 4) AS Prom  
FROM ADNotas NT  
INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec  
INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec  
INNER JOIN ADMateriasCursos MC ON NT.SecCurso = MC.IdCurso AND NT.SecMateria = MC.IdMateria AND NT.IdMatricula = MT.Sec  
INNER JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero  
WHERE NT.SecCurso = 1 AND MT.IdAñoLectivo = 1 
AND MC.IdMaestro = 1117521997 AND MC.IdMateria = 1 ORDER BY NombreComp

SELECT *
FROM Table1 NT
RIGHT JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec  
INNER JOIN ADMateriasCursos MC ON NT.SecMatCurso = MC.IdCurso AND NT.IdMatricula = MT.Sec  
INNER JOIN ADMaterias MR ON MC.IdMateria = MR.Sec  
RIGHT JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero  
WHERE MC.IdCurso = 1 AND MT.IdAñoLectivo = 1 
AND MC.IdMaestro = 1117521997 AND MC.IdMateria = 1 ORDER BY NombreComp


SELECT AVG(ISNULL(P1, 0)) AS Prom 
FROM ADNotas NT 
INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec 
INNER JOIN ADMaterias MR ON NT.SecMateria = MR.Sec
WHERE MT.IdEstudiante = 1234543


SELECT CS.Sec, CS.NomCurso FROM ADCursos CS 
INNER JOIN ADMatriculas MT ON CS.Sec = MT.IdCurso AND MT.IdAñoLectivo = 1
GROUP BY CS.Sec, CS.NomCurso


SELECT * FROM ADCursos CS 
WHERE CS.Sec = (SELECT TOP 1 (IdCurso) FROM ADMatriculas MT  WHERE MT.IdAñoLectivo = 1)

SELECT Sec, Año FROM ADAñoLectivo


SELECT MT.Sec AS IdMateria, MT.NomMateria FROM ADMaterias MT  
INNER JOIN ADMateriasCursos MC ON MT.Sec = MC.IdMateria  
INNER JOIN ADCursos CS ON MC.IdCurso = CS.Sec 

SELECT MT.Sec AS IdMateria, MT.NomMateria FROM ADMaterias MT



SELECT MC.Sec, MC.IdCurso, CS.NomCurso, MC.IdMateria, MT.NomMateria, MC.IdMaestro,  
CONCAT(ET.Pnombre, ' ', ET.Snombre, ' ', ET.Papellido, ' ', ET.Sapellido) AS NombreComp,  
MC.Desempeños, MC.IH, CS.IdAñoLectivo, AL.Año 
FROM ADMaterias MT  
INNER JOIN ADMateriasCursos MC ON MT.Sec = MC.IdMateria  
INNER JOIN ADCursos CS ON MC.IdCurso = CS.Sec  
INNER JOIN ADAñoLectivo AL ON CS.IdAñoLectivo = AL.Sec  
INNER JOIN ADTerceros ET ON MC.IdMaestro = ET.IdTercero  WHERE MC.IdCurso = 2


