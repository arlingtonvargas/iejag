﻿namespace Colegio.Vistas
{
    partial class FrmGeneraCarnet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGeneraCarnet));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET3 = new dll.Common.ET.TituloColsBusquedaET();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.rgOpciones = new DevExpress.XtraEditors.RadioGroup();
            this.txtCurso = new dll.Controles.ucBusqueda();
            this.txtEstudiante = new dll.Controles.ucBusqueda();
            this.btnConsultar = new DevExpress.XtraEditors.SimpleButton();
            this.btnGenerar = new DevExpress.XtraEditors.SimpleButton();
            this.gcEstudiantes = new DevExpress.XtraGrid.GridControl();
            this.aDTercerosCarnetETListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvEstudiantes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSede = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdTercero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNombreComp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPnombre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnombre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPapellido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSapellido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDireccion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCorreo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSexo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaNacimiento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdLugarNacimiento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomLugarNacimiento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelefono = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaCrea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdUsuario = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomUsu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdRol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNomRol = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodDpto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPais = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoDoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoTercero = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFoto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRH = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgOpciones.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEstudiantes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDTercerosCarnetETListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEstudiantes)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.txtAño);
            this.groupControl1.Controls.Add(this.rgOpciones);
            this.groupControl1.Controls.Add(this.txtCurso);
            this.groupControl1.Controls.Add(this.txtEstudiante);
            this.groupControl1.Controls.Add(this.btnConsultar);
            this.groupControl1.Controls.Add(this.btnGenerar);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1142, 115);
            this.groupControl1.TabIndex = 9;
            this.groupControl1.Text = "Parametros";
            // 
            // txtAño
            // 
            this.txtAño.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAño.AnchoTextBox = 140;
            this.txtAño.AnchoTitulo = 90;
            this.txtAño.Enabled = false;
            this.txtAño.Location = new System.Drawing.Point(193, 26);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(849, 28);
            this.txtAño.TabIndex = 1;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtAño.ValorTextBox = "";
            // 
            // rgOpciones
            // 
            this.rgOpciones.EditValue = ((short)(0));
            this.rgOpciones.Location = new System.Drawing.Point(5, 28);
            this.rgOpciones.Name = "rgOpciones";
            this.rgOpciones.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgOpciones.Properties.Appearance.Options.UseBackColor = true;
            this.rgOpciones.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(0)), "Por Estudiante"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "Por Curso")});
            this.rgOpciones.Size = new System.Drawing.Size(185, 82);
            this.rgOpciones.TabIndex = 1;
            this.rgOpciones.SelectedIndexChanged += new System.EventHandler(this.rgOpciones_SelectedIndexChanged);
            // 
            // txtCurso
            // 
            this.txtCurso.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCurso.AnchoTextBox = 140;
            this.txtCurso.AnchoTitulo = 90;
            this.txtCurso.Enabled = false;
            this.txtCurso.Location = new System.Drawing.Point(193, 82);
            this.txtCurso.Margin = new System.Windows.Forms.Padding(0);
            this.txtCurso.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCurso.MensajeDeAyuda = null;
            this.txtCurso.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.PermiteSoloNumeros = false;
            this.txtCurso.Script = "";
            this.txtCurso.Size = new System.Drawing.Size(849, 28);
            this.txtCurso.TabIndex = 2;
            this.txtCurso.TextoTitulo = "Curso :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtCurso.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtCurso.ValorTextBox = "";
            // 
            // txtEstudiante
            // 
            this.txtEstudiante.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEstudiante.AnchoTextBox = 140;
            this.txtEstudiante.AnchoTitulo = 90;
            this.txtEstudiante.Location = new System.Drawing.Point(193, 54);
            this.txtEstudiante.Margin = new System.Windows.Forms.Padding(0);
            this.txtEstudiante.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtEstudiante.MensajeDeAyuda = null;
            this.txtEstudiante.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtEstudiante.Name = "txtEstudiante";
            this.txtEstudiante.PermiteSoloNumeros = false;
            this.txtEstudiante.Script = "";
            this.txtEstudiante.Size = new System.Drawing.Size(849, 28);
            this.txtEstudiante.TabIndex = 12;
            this.txtEstudiante.TextoTitulo = "Estudiante :";
            tituloColsBusquedaET3.Codigo = "Código";
            tituloColsBusquedaET3.Descripcion = "Descripción";
            this.txtEstudiante.TituloColsBusqueda = tituloColsBusquedaET3;
            this.txtEstudiante.ValorTextBox = "";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConsultar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultar.ImageOptions.Image")));
            this.btnConsultar.Location = new System.Drawing.Point(1045, 28);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(91, 38);
            this.btnConsultar.TabIndex = 4;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // btnGenerar
            // 
            this.btnGenerar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGenerar.ImageOptions.Image")));
            this.btnGenerar.Location = new System.Drawing.Point(1045, 68);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(91, 38);
            this.btnGenerar.TabIndex = 5;
            this.btnGenerar.Text = "Generar";
            this.btnGenerar.Click += new System.EventHandler(this.btnGenerar_Click);
            // 
            // gcEstudiantes
            // 
            this.gcEstudiantes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcEstudiantes.DataSource = this.aDTercerosCarnetETListBindingSource;
            this.gcEstudiantes.Location = new System.Drawing.Point(12, 133);
            this.gcEstudiantes.MainView = this.gvEstudiantes;
            this.gcEstudiantes.Name = "gcEstudiantes";
            this.gcEstudiantes.Size = new System.Drawing.Size(1142, 315);
            this.gcEstudiantes.TabIndex = 10;
            this.gcEstudiantes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvEstudiantes});
            // 
            // aDTercerosCarnetETListBindingSource
            // 
            this.aDTercerosCarnetETListBindingSource.DataSource = typeof(Colegio.ET.ADTercerosCarnetETList);
            // 
            // gvEstudiantes
            // 
            this.gvEstudiantes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSede,
            this.colGrado,
            this.colIdTercero,
            this.colNombreComp,
            this.colPnombre,
            this.colSnombre,
            this.colPapellido,
            this.colSapellido,
            this.colDireccion,
            this.colCorreo,
            this.colSexo,
            this.colFechaNacimiento,
            this.colIdLugarNacimiento,
            this.colNomLugarNacimiento,
            this.colTelefono,
            this.colFechaCrea,
            this.colIdUsuario,
            this.colNomUsu,
            this.colIdRol,
            this.colNomRol,
            this.colCodDpto,
            this.colPais,
            this.colTipoDoc,
            this.colTipoTercero,
            this.colFoto,
            this.colEPS,
            this.colRH});
            this.gvEstudiantes.GridControl = this.gcEstudiantes;
            this.gvEstudiantes.Name = "gvEstudiantes";
            this.gvEstudiantes.OptionsBehavior.Editable = false;
            // 
            // colSede
            // 
            this.colSede.Caption = "Sede";
            this.colSede.FieldName = "Sede";
            this.colSede.Name = "colSede";
            this.colSede.Visible = true;
            this.colSede.VisibleIndex = 3;
            this.colSede.Width = 187;
            // 
            // colGrado
            // 
            this.colGrado.Caption = "Grado";
            this.colGrado.FieldName = "Grado";
            this.colGrado.Name = "colGrado";
            this.colGrado.Visible = true;
            this.colGrado.VisibleIndex = 4;
            this.colGrado.Width = 145;
            // 
            // colIdTercero
            // 
            this.colIdTercero.Caption = "Documento";
            this.colIdTercero.FieldName = "IdTercero";
            this.colIdTercero.Name = "colIdTercero";
            this.colIdTercero.Visible = true;
            this.colIdTercero.VisibleIndex = 0;
            this.colIdTercero.Width = 117;
            // 
            // colNombreComp
            // 
            this.colNombreComp.Caption = "Nombres";
            this.colNombreComp.FieldName = "NombreComp";
            this.colNombreComp.Name = "colNombreComp";
            this.colNombreComp.Visible = true;
            this.colNombreComp.VisibleIndex = 1;
            this.colNombreComp.Width = 340;
            // 
            // colPnombre
            // 
            this.colPnombre.FieldName = "Pnombre";
            this.colPnombre.Name = "colPnombre";
            // 
            // colSnombre
            // 
            this.colSnombre.FieldName = "Snombre";
            this.colSnombre.Name = "colSnombre";
            // 
            // colPapellido
            // 
            this.colPapellido.FieldName = "Papellido";
            this.colPapellido.Name = "colPapellido";
            // 
            // colSapellido
            // 
            this.colSapellido.FieldName = "Sapellido";
            this.colSapellido.Name = "colSapellido";
            // 
            // colDireccion
            // 
            this.colDireccion.FieldName = "Direccion";
            this.colDireccion.Name = "colDireccion";
            // 
            // colCorreo
            // 
            this.colCorreo.FieldName = "Correo";
            this.colCorreo.Name = "colCorreo";
            // 
            // colSexo
            // 
            this.colSexo.FieldName = "Sexo";
            this.colSexo.Name = "colSexo";
            // 
            // colFechaNacimiento
            // 
            this.colFechaNacimiento.FieldName = "FechaNacimiento";
            this.colFechaNacimiento.Name = "colFechaNacimiento";
            // 
            // colIdLugarNacimiento
            // 
            this.colIdLugarNacimiento.FieldName = "IdLugarNacimiento";
            this.colIdLugarNacimiento.Name = "colIdLugarNacimiento";
            // 
            // colNomLugarNacimiento
            // 
            this.colNomLugarNacimiento.FieldName = "NomLugarNacimiento";
            this.colNomLugarNacimiento.Name = "colNomLugarNacimiento";
            // 
            // colTelefono
            // 
            this.colTelefono.FieldName = "Telefono";
            this.colTelefono.Name = "colTelefono";
            // 
            // colFechaCrea
            // 
            this.colFechaCrea.FieldName = "FechaCrea";
            this.colFechaCrea.Name = "colFechaCrea";
            // 
            // colIdUsuario
            // 
            this.colIdUsuario.FieldName = "IdUsuario";
            this.colIdUsuario.Name = "colIdUsuario";
            // 
            // colNomUsu
            // 
            this.colNomUsu.FieldName = "NomUsu";
            this.colNomUsu.Name = "colNomUsu";
            // 
            // colIdRol
            // 
            this.colIdRol.FieldName = "IdRol";
            this.colIdRol.Name = "colIdRol";
            // 
            // colNomRol
            // 
            this.colNomRol.FieldName = "NomRol";
            this.colNomRol.Name = "colNomRol";
            // 
            // colCodDpto
            // 
            this.colCodDpto.FieldName = "CodDpto";
            this.colCodDpto.Name = "colCodDpto";
            // 
            // colPais
            // 
            this.colPais.FieldName = "Pais";
            this.colPais.Name = "colPais";
            // 
            // colTipoDoc
            // 
            this.colTipoDoc.FieldName = "TipoDoc";
            this.colTipoDoc.Name = "colTipoDoc";
            // 
            // colTipoTercero
            // 
            this.colTipoTercero.FieldName = "TipoTercero";
            this.colTipoTercero.Name = "colTipoTercero";
            // 
            // colFoto
            // 
            this.colFoto.Caption = "Foto";
            this.colFoto.FieldName = "Foto";
            this.colFoto.Name = "colFoto";
            this.colFoto.Visible = true;
            this.colFoto.VisibleIndex = 2;
            this.colFoto.Width = 279;
            // 
            // colEPS
            // 
            this.colEPS.Caption = "EPS";
            this.colEPS.FieldName = "EPS";
            this.colEPS.Name = "colEPS";
            this.colEPS.Visible = true;
            this.colEPS.VisibleIndex = 5;
            // 
            // colRH
            // 
            this.colRH.Caption = "RH";
            this.colRH.FieldName = "RH";
            this.colRH.Name = "colRH";
            this.colRH.Visible = true;
            this.colRH.VisibleIndex = 6;
            // 
            // FrmGeneraCarnet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1166, 460);
            this.Controls.Add(this.gcEstudiantes);
            this.Controls.Add(this.groupControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmGeneraCarnet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Generar Carnet";
            this.Load += new System.EventHandler(this.FrmGeneraCarnet_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgOpciones.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEstudiantes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDTercerosCarnetETListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvEstudiantes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private dll.Controles.ucBusqueda txtAño;
        private dll.Controles.ucBusqueda txtCurso;
        private DevExpress.XtraEditors.SimpleButton btnConsultar;
        private DevExpress.XtraEditors.SimpleButton btnGenerar;
        private DevExpress.XtraEditors.RadioGroup rgOpciones;
        private dll.Controles.ucBusqueda txtEstudiante;
        private DevExpress.XtraGrid.GridControl gcEstudiantes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvEstudiantes;
        private System.Windows.Forms.BindingSource aDTercerosCarnetETListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSede;
        private DevExpress.XtraGrid.Columns.GridColumn colGrado;
        private DevExpress.XtraGrid.Columns.GridColumn colIdTercero;
        private DevExpress.XtraGrid.Columns.GridColumn colNombreComp;
        private DevExpress.XtraGrid.Columns.GridColumn colPnombre;
        private DevExpress.XtraGrid.Columns.GridColumn colSnombre;
        private DevExpress.XtraGrid.Columns.GridColumn colPapellido;
        private DevExpress.XtraGrid.Columns.GridColumn colSapellido;
        private DevExpress.XtraGrid.Columns.GridColumn colDireccion;
        private DevExpress.XtraGrid.Columns.GridColumn colCorreo;
        private DevExpress.XtraGrid.Columns.GridColumn colSexo;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaNacimiento;
        private DevExpress.XtraGrid.Columns.GridColumn colIdLugarNacimiento;
        private DevExpress.XtraGrid.Columns.GridColumn colNomLugarNacimiento;
        private DevExpress.XtraGrid.Columns.GridColumn colTelefono;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaCrea;
        private DevExpress.XtraGrid.Columns.GridColumn colIdUsuario;
        private DevExpress.XtraGrid.Columns.GridColumn colNomUsu;
        private DevExpress.XtraGrid.Columns.GridColumn colIdRol;
        private DevExpress.XtraGrid.Columns.GridColumn colNomRol;
        private DevExpress.XtraGrid.Columns.GridColumn colCodDpto;
        private DevExpress.XtraGrid.Columns.GridColumn colPais;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoDoc;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoTercero;
        private DevExpress.XtraGrid.Columns.GridColumn colFoto;
        private DevExpress.XtraGrid.Columns.GridColumn colEPS;
        private DevExpress.XtraGrid.Columns.GridColumn colRH;
    }
}