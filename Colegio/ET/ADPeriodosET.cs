﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADPeriodosET
    {
        public int IdPeriodo { get; set; }
        public string Nombre { get; set; }
    }   

    public class ListADPeriodosET : List<ADPeriodosET>
    {}
}
