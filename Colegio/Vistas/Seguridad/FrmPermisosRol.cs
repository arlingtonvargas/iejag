﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using dll.Common;
using Colegio.Clases;

namespace Colegio.Vistas.Seguridad
{
    public partial class FrmPermisosRol : FrmBase
    {
        ClPermisosRol clPermisosRol = new ClPermisosRol();
        ClRoles clRoles = new ClRoles();
        public FrmPermisosRol()
        {
            InitializeComponent();
        }

        private void FrmPermisosRol_Load(object sender, EventArgs e)
        {
            CargarRoles();
        }

        private void CargarRoles()
        {
            try
            {
                cbxRoles.Properties.DataSource = clRoles.GetRoles();
                cbxRoles.Properties.ValueMember = "Sec";
                cbxRoles.Properties.DisplayMember = "NomRol";
                cbxRoles.Properties.PopulateColumns();
                cbxRoles.Properties.ShowFooter = false;
                cbxRoles.Properties.ShowHeader = false;                
                cbxRoles.Properties.Columns[0].Visible = false;
                cbxRoles.Properties.Columns[2].Visible = false;
                cbxRoles.Properties.Columns[3].Visible = false;
                cbxRoles.Properties.Columns[4].Visible = false;
                cbxRoles.Properties.Columns[5].Visible = false;

            }
            catch (Exception)
            {
            }
        }

        private void CargarPermisosPorRol(int idRol)
        {
            lbcPermisos.DataSource = clPermisosRol.GetPermisosNotInRol(idRol);
            lbcPermisos.DisplayMember = "NomPermiso";
            lbcPermisos.ValueMember = "SecTransaccion";
            lbcPermisosRol.DataSource = clPermisosRol.GetPermisosByRol(idRol);
            lbcPermisosRol.DisplayMember = "NomPermiso";
            lbcPermisosRol.ValueMember = "SecTransaccion";
        }

        private void cbxRoles_EditValueChanged(object sender, EventArgs e)
        {
            if (cbxRoles.ItemIndex>-1)
            {
                CargarPermisosPorRol(Convert.ToInt32(cbxRoles.EditValue));
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                int idPermiso = Convert.ToInt32(lbcPermisos.SelectedValue);
                DataTable dt = (DataTable)lbcPermisosRol.DataSource;
                DataRow[] fila = dt.Select("SecTransaccion = " + idPermiso.ToString());
                if (fila.Length <= 0)
                {
                    DataTable dtPermisos = (DataTable)lbcPermisos.DataSource;
                    fila = dtPermisos.Select("SecTransaccion = " + idPermiso.ToString());
                    DataRow newFila = dt.NewRow();
                    newFila["SecTransaccion"] = fila[0]["SecTransaccion"];
                    newFila["NomPermiso"] = fila[0]["NomPermiso"];
                    newFila["DesPermiso"] = fila[0]["DesPermiso"];
                    dt.Rows.Add(newFila);
                    dt.AcceptChanges();
                    //lbcPermisosRol.DataSource = dt;
                    dtPermisos.Rows.Remove(fila[0]);
                    dtPermisos.AcceptChanges();
                }
            }
            catch (Exception ex)
            {}
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            try
            {
                int idPermiso = Convert.ToInt32(lbcPermisosRol.SelectedValue);
                DataTable dt = (DataTable)lbcPermisos.DataSource;
                DataRow[] fila = dt.Select("SecTransaccion = " + idPermiso.ToString());
                if (fila.Length <= 0)
                {
                    DataTable dtPermisosRol = (DataTable)lbcPermisosRol.DataSource;
                    fila = dtPermisosRol.Select("SecTransaccion = " + idPermiso.ToString());
                    DataRow newFila = dt.NewRow();
                    newFila["SecTransaccion"] = fila[0]["SecTransaccion"];
                    newFila["NomPermiso"] = fila[0]["NomPermiso"];
                    newFila["DesPermiso"] = fila[0]["DesPermiso"];
                    dt.Rows.Add(newFila);
                    dt.AcceptChanges();
                    //lbcPermisosRol.DataSource = dtPermisosRol;
                    dtPermisosRol.Rows.Remove(fila[0]);
                    dtPermisosRol.AcceptChanges();
                }

            }
            catch (Exception)
            { }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = (DataTable)lbcPermisosRol.DataSource;
                if (cbxRoles.ItemIndex < 0)
                    return;
                if (dt.Rows.Count>0)
                {
                    if (clPermisosRol.GuardarPermisosRol(Convert.ToInt32(cbxRoles.EditValue), dt))
                    {
                        FrmMensaje frm = new FrmMensaje();
                        frm.lblMensaje.Text = "Permisos asignados de manera correcta.";
                        frm.ShowDialog();
                    }
                }else
                {
                    if (clPermisosRol.EliminarPermisosRol(Convert.ToInt32(cbxRoles.EditValue), dt))
                    {
                        FrmMensaje frm = new FrmMensaje();
                        frm.lblMensaje.Text = "Permisos configurados de manera correcta.";
                        frm.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}