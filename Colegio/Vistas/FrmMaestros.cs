﻿using Colegio.Clases;
using Colegio.ET;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Colegio.Vistas
{
    public partial class FrmMaestros : XtraForm
    {
        ClTercero clMaestro = new ClTercero();
        public bool EsMaestro = true;
        public FrmMaestros()
        {
            InitializeComponent();
        }

        private void FrmMaestros_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenarGrilla();
            HabilitaBotones();
        }
        private void HabilitaBotones()
        {
            int rol = ClFunciones.UsuarioIngreso.IdRol;
            if (EsMaestro)
            {
                if (!ClSeguridad.ValidaPermisosLocal(rol, 1006)) btnNuevo.Enabled = false;
                if (!ClSeguridad.ValidaPermisosLocal(rol, 1007)) btnEditar.Enabled = false;
            }
            else
            {
                if (!ClSeguridad.ValidaPermisosLocal(rol, 1009)) btnNuevo.Enabled = false;
                if (!ClSeguridad.ValidaPermisosLocal(rol, 1010)) btnEditar.Enabled = false;
            }
           
        }
        private void CrearGrilla()
        {
            try
            {
                gvMaestros = GrillaDevExpress.CrearGrilla(false, true);
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("IdTercero", "Documento"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("NombreComp", "Nombre"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("TipoDoc", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Pnombre", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Snombre", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Papellido", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Sapellido", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Direccion", "Dirección"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Correo", "Correo"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Telefono", "Teléfono"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("FechaNacimiento", "Fecha Nacimiento"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("IdLugarNacimiento", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("NomLugarNacimiento", "Lugar de nacimiento"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("FechaCrea", "Creado"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("IdUsuario", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("NomUsu", "Usuario"));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("IdRol", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("CodDpto", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Pais", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("TipoTercero", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("Foto", "", visible: false));
                gvMaestros.Columns.Add(GrillaDevExpress.CrearColumna("NomRol", "Rol"));
                gvMaestros.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaestros.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaestros.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvMaestros.OptionsCustomization.AllowColumnResizing = true;
                gcMaestros.MainView = gvMaestros;
                this.gvMaestros.DoubleClick += new System.EventHandler(this.gvMaestros_DoubleClick);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void LlenarGrilla()
        {
            try
            {
                if (EsMaestro)
                {
                    gcMaestros.DataSource = clMaestro.GetTercerosTipoTer(1);
                }
                else
                {
                    gcMaestros.DataSource = clMaestro.GetTercerosTipoTer(3);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void gvMaestros_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;
            GridHitInfo info = view.CalcHitInfo(ea.Location);
            try
            {
                if (info.InRow || info.InRowCell)
                {
                    ET.ADTercerosET clMaestroET = ExtraerData(info.RowHandle);
                    if (clMaestro == null)
                    {
                        FrmMensaje f = new FrmMensaje();
                        f.lblMensaje.Text = "Lo sentimos, ha ocurrido un error.";
                        f.ShowDialog();
                        return;
                    }
                    FrmNuevoMaestro frm = new FrmNuevoMaestro(clMaestroET);
                    frm.EstaActualizando = true;
                    frm.EsMaestro = true;
                    if (clMaestroET.TipoTercero == 3)
                    {
                        frm.EsMaestro = false;
                        frm.lblTitulo.Text = "Administrativo";
                        frm.grcInfo.Text = "Información básica";
                    }
                    frm.txtDocumento.Focus();
                    frm.ShowDialog();
                    LlenarGrilla();
                }
            }
            catch (Exception ex)
            {
                
            }            
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmNuevoMaestro frm = new FrmNuevoMaestro(null);
            if (EsMaestro)
            {
                frm.EsMaestro = true;
                frm.ShowDialog();
                LlenarGrilla();
            }
            else
            {
                frm.lblTitulo.Text = "Administrativo";
                frm.grcInfo.Text = "Información básica";
                frm.EsMaestro = false;
                frm.ShowDialog();
            }            
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            int RowHandle = -1;
            if (true)
            {
                RowHandle = gvMaestros.FocusedRowHandle;
            }
            if (RowHandle <= -1)
                return;
            ET.ADTercerosET clMaestroET = ExtraerData(RowHandle);
            if (clMaestro == null)
            {
                FrmMensaje f = new FrmMensaje();
                f.lblMensaje.Text = "Lo sentimos, ha ocurrido un error.";
                f.ShowDialog();
                return;
            }
            FrmNuevoMaestro frm = new FrmNuevoMaestro(clMaestroET);
            frm.EstaActualizando = true;
            frm.txtDocumento.Focus();
            frm.ShowDialog();
            LlenarGrilla();
        }

        private ADTercerosET ExtraerData(int RowHandle)
        {
            try
            {
                ET.ADTercerosET clMaestroET = new ET.ADTercerosET();
                clMaestroET.IdTercero = Convert.ToInt64(gvMaestros.GetRowCellValue(RowHandle, "IdTercero"));
                clMaestroET.TipoDoc = Convert.ToInt32(gvMaestros.GetRowCellValue(RowHandle, "TipoDoc"));
                clMaestroET.Pnombre = gvMaestros.GetRowCellValue(RowHandle, "Pnombre").ToString();
                clMaestroET.Snombre = gvMaestros.GetRowCellValue(RowHandle, "Snombre").ToString();
                clMaestroET.Papellido = gvMaestros.GetRowCellValue(RowHandle, "Papellido").ToString();
                clMaestroET.Sapellido = gvMaestros.GetRowCellValue(RowHandle, "Sapellido").ToString();
                clMaestroET.Telefono = Convert.ToDouble(gvMaestros.GetRowCellValue(RowHandle, "Telefono"));
                clMaestroET.Direccion = gvMaestros.GetRowCellValue(RowHandle, "Direccion").ToString();
                clMaestroET.Correo = gvMaestros.GetRowCellValue(RowHandle, "Correo").ToString();
                clMaestroET.NomUsu = gvMaestros.GetRowCellValue(RowHandle, "NomUsu").ToString();
                clMaestroET.IdUsuario = Convert.ToInt32(gvMaestros.GetRowCellValue(RowHandle, "IdUsuario"));
                clMaestroET.IdRol = Convert.ToInt32(gvMaestros.GetRowCellValue(RowHandle, "IdRol"));
                clMaestroET.Sexo = Convert.ToInt32(gvMaestros.GetRowCellValue(RowHandle, "Sexo"));
                clMaestroET.FechaNacimiento = Convert.ToDateTime(gvMaestros.GetRowCellValue(RowHandle, "FechaNacimiento"));
                clMaestroET.IdLugarNacimiento = gvMaestros.GetRowCellValue(RowHandle, "IdLugarNacimiento").ToString();
                clMaestroET.CodDpto = gvMaestros.GetRowCellValue(RowHandle, "CodDpto").ToString();
                clMaestroET.Pais = gvMaestros.GetRowCellValue(RowHandle, "Pais").ToString();
                return clMaestroET;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarGrilla();
        }
    }
}
