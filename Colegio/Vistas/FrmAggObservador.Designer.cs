﻿namespace Colegio.Vistas
{
    partial class FrmAggObservador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        ///
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAggObservador));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET3 = new dll.Common.ET.TituloColsBusquedaET();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions1 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions2 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions3 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions windowsUIButtonImageOptions4 = new DevExpress.XtraBars.Docking2010.WindowsUIButtonImageOptions();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pcbFoto = new DevExpress.XtraEditors.PictureEdit();
            this.txtAño = new dll.Controles.ucBusqueda();
            this.gcObservaciones = new DevExpress.XtraGrid.GridControl();
            this.aDObservacionEstudianteListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvObservaciones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOSId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSIdEstudiente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSNomEstudiente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSIdAño = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSDesAño = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSFechaObservacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSObservacion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSFechaCrea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSMaestroReistra = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSNomMaestroReistra = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSMaestroReporta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSNomMaestroReporta = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOSIsPositive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtMaestro = new dll.Controles.ucBusqueda();
            this.txtObserv = new DevExpress.XtraEditors.MemoEdit();
            this.lblObservacion = new DevExpress.XtraEditors.LabelControl();
            this.txtEstudiante = new dll.Controles.ucBusqueda();
            this.lblFecha = new DevExpress.XtraEditors.LabelControl();
            this.dtpFecha = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkbtnOk = new DevExpress.XtraEditors.CheckButton();
            this.chkbtnNeg = new DevExpress.XtraEditors.CheckButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.windowsUIButtonPanelMain = new DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel();
            this.labelControl = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFoto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcObservaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDObservacionEstudianteListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObservaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObserv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.AllowCustomization = false;
            this.dataLayoutControl1.Controls.Add(this.tableLayoutPanel1);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 30);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1095, 471);
            this.dataLayoutControl1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 146F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel1.Controls.Add(this.pcbFoto, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtAño, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.gcObservaciones, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtMaestro, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtObserv, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblObservacion, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtEstudiante, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblFecha, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.dtpFecha, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.chkbtnOk, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.chkbtnNeg, 4, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1071, 447);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // pcbFoto
            // 
            this.pcbFoto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcbFoto.Location = new System.Drawing.Point(3, 3);
            this.pcbFoto.Name = "pcbFoto";
            this.pcbFoto.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.pcbFoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.tableLayoutPanel1.SetRowSpan(this.pcbFoto, 4);
            this.pcbFoto.Size = new System.Drawing.Size(140, 144);
            this.pcbFoto.TabIndex = 0;
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 50;
            this.txtAño.AnchoTitulo = 90;
            this.tableLayoutPanel1.SetColumnSpan(this.txtAño, 3);
            this.txtAño.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAño.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAño.Enabled = false;
            this.txtAño.Location = new System.Drawing.Point(732, 0);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(339, 28);
            this.txtAño.TabIndex = 2;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtAño.ValorTextBox = "";
            // 
            // gcObservaciones
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gcObservaciones, 5);
            this.gcObservaciones.DataSource = this.aDObservacionEstudianteListBindingSource;
            this.gcObservaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcObservaciones.Location = new System.Drawing.Point(3, 153);
            this.gcObservaciones.MainView = this.gvObservaciones;
            this.gcObservaciones.Name = "gcObservaciones";
            this.gcObservaciones.Size = new System.Drawing.Size(1065, 291);
            this.gcObservaciones.TabIndex = 2;
            this.gcObservaciones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvObservaciones});
            // 
            // aDObservacionEstudianteListBindingSource
            // 
            this.aDObservacionEstudianteListBindingSource.DataSource = typeof(Colegio.ET.ADObservacionEstudianteList);
            // 
            // gvObservaciones
            // 
            this.gvObservaciones.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOSId,
            this.colOSIdEstudiente,
            this.colOSNomEstudiente,
            this.colOSIdAño,
            this.colOSDesAño,
            this.colOSFechaObservacion,
            this.colOSObservacion,
            this.colOSFechaCrea,
            this.colOSMaestroReistra,
            this.colOSNomMaestroReistra,
            this.colOSMaestroReporta,
            this.colOSNomMaestroReporta,
            this.colOSIsPositive});
            this.gvObservaciones.GridControl = this.gcObservaciones;
            this.gvObservaciones.Name = "gvObservaciones";
            this.gvObservaciones.OptionsBehavior.Editable = false;
            this.gvObservaciones.OptionsView.ShowGroupPanel = false;
            // 
            // colOSId
            // 
            this.colOSId.FieldName = "OSId";
            this.colOSId.Name = "colOSId";
            // 
            // colOSIdEstudiente
            // 
            this.colOSIdEstudiente.Caption = "Id Estudiante";
            this.colOSIdEstudiente.FieldName = "OSIdEstudiente";
            this.colOSIdEstudiente.Name = "colOSIdEstudiente";
            this.colOSIdEstudiente.Visible = true;
            this.colOSIdEstudiente.VisibleIndex = 0;
            this.colOSIdEstudiente.Width = 120;
            // 
            // colOSNomEstudiente
            // 
            this.colOSNomEstudiente.Caption = "Nombre Estudiente";
            this.colOSNomEstudiente.FieldName = "OSNomEstudiente";
            this.colOSNomEstudiente.Name = "colOSNomEstudiente";
            this.colOSNomEstudiente.Visible = true;
            this.colOSNomEstudiente.VisibleIndex = 1;
            this.colOSNomEstudiente.Width = 225;
            // 
            // colOSIdAño
            // 
            this.colOSIdAño.FieldName = "OSIdAño";
            this.colOSIdAño.Name = "colOSIdAño";
            // 
            // colOSDesAño
            // 
            this.colOSDesAño.Caption = "Año";
            this.colOSDesAño.FieldName = "OSDesAño";
            this.colOSDesAño.Name = "colOSDesAño";
            this.colOSDesAño.Visible = true;
            this.colOSDesAño.VisibleIndex = 2;
            this.colOSDesAño.Width = 90;
            // 
            // colOSFechaObservacion
            // 
            this.colOSFechaObservacion.Caption = "Fecha";
            this.colOSFechaObservacion.FieldName = "OSFechaObservacion";
            this.colOSFechaObservacion.Name = "colOSFechaObservacion";
            this.colOSFechaObservacion.Visible = true;
            this.colOSFechaObservacion.VisibleIndex = 3;
            this.colOSFechaObservacion.Width = 155;
            // 
            // colOSObservacion
            // 
            this.colOSObservacion.Caption = "Observación";
            this.colOSObservacion.FieldName = "OSObservacion";
            this.colOSObservacion.Name = "colOSObservacion";
            this.colOSObservacion.Visible = true;
            this.colOSObservacion.VisibleIndex = 4;
            this.colOSObservacion.Width = 233;
            // 
            // colOSFechaCrea
            // 
            this.colOSFechaCrea.FieldName = "OSFechaCrea";
            this.colOSFechaCrea.Name = "colOSFechaCrea";
            // 
            // colOSMaestroReistra
            // 
            this.colOSMaestroReistra.Caption = "Id Maestro";
            this.colOSMaestroReistra.FieldName = "OSMaestroReistra";
            this.colOSMaestroReistra.Name = "colOSMaestroReistra";
            this.colOSMaestroReistra.Width = 102;
            // 
            // colOSNomMaestroReistra
            // 
            this.colOSNomMaestroReistra.Caption = "Nombre Maestro";
            this.colOSNomMaestroReistra.FieldName = "OSNomMaestroReistra";
            this.colOSNomMaestroReistra.Name = "colOSNomMaestroReistra";
            this.colOSNomMaestroReistra.Width = 134;
            // 
            // colOSMaestroReporta
            // 
            this.colOSMaestroReporta.Caption = "Id Maestro Reporta";
            this.colOSMaestroReporta.FieldName = "OSMaestroReporta";
            this.colOSMaestroReporta.Name = "colOSMaestroReporta";
            this.colOSMaestroReporta.Visible = true;
            this.colOSMaestroReporta.VisibleIndex = 5;
            this.colOSMaestroReporta.Width = 120;
            // 
            // colOSNomMaestroReporta
            // 
            this.colOSNomMaestroReporta.Caption = "Maestro Reporta";
            this.colOSNomMaestroReporta.FieldName = "OSNomMaestroReporta";
            this.colOSNomMaestroReporta.Name = "colOSNomMaestroReporta";
            this.colOSNomMaestroReporta.Visible = true;
            this.colOSNomMaestroReporta.VisibleIndex = 6;
            this.colOSNomMaestroReporta.Width = 157;
            // 
            // colOSIsPositive
            // 
            this.colOSIsPositive.Caption = "Positiva";
            this.colOSIsPositive.FieldName = "OSIsPositive";
            this.colOSIsPositive.Name = "colOSIsPositive";
            this.colOSIsPositive.Visible = true;
            this.colOSIsPositive.VisibleIndex = 7;
            // 
            // txtMaestro
            // 
            this.txtMaestro.AnchoTextBox = 100;
            this.txtMaestro.AnchoTitulo = 90;
            this.txtMaestro.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtMaestro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMaestro.Location = new System.Drawing.Point(146, 25);
            this.txtMaestro.Margin = new System.Windows.Forms.Padding(0);
            this.txtMaestro.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMaestro.MensajeDeAyuda = null;
            this.txtMaestro.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMaestro.Name = "txtMaestro";
            this.txtMaestro.PermiteSoloNumeros = false;
            this.txtMaestro.Script = "";
            this.txtMaestro.Size = new System.Drawing.Size(586, 28);
            this.txtMaestro.TabIndex = 3;
            this.txtMaestro.TextoTitulo = "Maestro :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtMaestro.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtMaestro.ValorTextBox = "";
            // 
            // txtObserv
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtObserv, 4);
            this.txtObserv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObserv.Location = new System.Drawing.Point(149, 83);
            this.txtObserv.Name = "txtObserv";
            this.txtObserv.Size = new System.Drawing.Size(919, 64);
            this.txtObserv.TabIndex = 5;
            // 
            // lblObservacion
            // 
            this.lblObservacion.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservacion.Appearance.Options.UseFont = true;
            this.lblObservacion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblObservacion.Location = new System.Drawing.Point(149, 53);
            this.lblObservacion.Name = "lblObservacion";
            this.lblObservacion.Size = new System.Drawing.Size(580, 24);
            this.lblObservacion.TabIndex = 7;
            this.lblObservacion.Text = "Descripción de Observación";
            // 
            // txtEstudiante
            // 
            this.txtEstudiante.AnchoTextBox = 100;
            this.txtEstudiante.AnchoTitulo = 90;
            this.txtEstudiante.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtEstudiante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEstudiante.Location = new System.Drawing.Point(146, 0);
            this.txtEstudiante.Margin = new System.Windows.Forms.Padding(0);
            this.txtEstudiante.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtEstudiante.MensajeDeAyuda = null;
            this.txtEstudiante.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtEstudiante.Name = "txtEstudiante";
            this.txtEstudiante.PermiteSoloNumeros = false;
            this.txtEstudiante.Script = "";
            this.txtEstudiante.Size = new System.Drawing.Size(586, 28);
            this.txtEstudiante.TabIndex = 1;
            this.txtEstudiante.TextoTitulo = "Estudiante :";
            tituloColsBusquedaET3.Codigo = "Código";
            tituloColsBusquedaET3.Descripcion = "Descripción";
            this.txtEstudiante.TituloColsBusqueda = tituloColsBusquedaET3;
            this.txtEstudiante.ValorTextBox = "";
            this.txtEstudiante.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtEstudiante_SaleControl);
            // 
            // lblFecha
            // 
            this.lblFecha.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Appearance.Options.UseFont = true;
            this.lblFecha.Appearance.Options.UseTextOptions = true;
            this.lblFecha.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblFecha.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFecha.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblFecha.Location = new System.Drawing.Point(735, 28);
            this.lblFecha.Margin = new System.Windows.Forms.Padding(3, 3, 3, 7);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(84, 15);
            this.lblFecha.TabIndex = 4;
            this.lblFecha.Text = "Fecha :";
            // 
            // dtpFecha
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.dtpFecha, 2);
            this.dtpFecha.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpFecha.EditValue = null;
            this.dtpFecha.Location = new System.Drawing.Point(825, 28);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.dtpFecha.Properties.Appearance.Options.UseFont = true;
            this.dtpFecha.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecha.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecha.Size = new System.Drawing.Size(243, 22);
            this.dtpFecha.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(735, 53);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(84, 24);
            this.labelControl1.TabIndex = 7;
            this.labelControl1.Text = "Obs. Tipo:";
            // 
            // chkbtnOk
            // 
            this.chkbtnOk.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.chkbtnOk.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("chkbtnOk.ImageOptions.Image")));
            this.chkbtnOk.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.chkbtnOk.Location = new System.Drawing.Point(825, 53);
            this.chkbtnOk.Name = "chkbtnOk";
            this.chkbtnOk.Size = new System.Drawing.Size(24, 23);
            this.chkbtnOk.TabIndex = 8;
            this.chkbtnOk.ToolTip = "Observación Positiva";
            this.chkbtnOk.CheckedChanged += new System.EventHandler(this.chkbtnOk_CheckedChanged);
            // 
            // chkbtnNeg
            // 
            this.chkbtnNeg.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.chkbtnNeg.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("chkbtnNeg.ImageOptions.Image")));
            this.chkbtnNeg.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.chkbtnNeg.Location = new System.Drawing.Point(856, 53);
            this.chkbtnNeg.Name = "chkbtnNeg";
            this.chkbtnNeg.Size = new System.Drawing.Size(24, 23);
            this.chkbtnNeg.TabIndex = 8;
            this.chkbtnNeg.ToolTip = "Observación Negativa";
            this.chkbtnNeg.CheckedChanged += new System.EventHandler(this.chkbtnNeg_CheckedChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1095, 471);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.tableLayoutPanel1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1075, 451);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // windowsUIButtonPanelMain
            // 
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.FontSizeDelta = -1;
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(130)))), ((int)(((byte)(130)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.Options.UseBackColor = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.Options.UseFont = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Hovered.Options.UseForeColor = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Normal.FontSizeDelta = -1;
            this.windowsUIButtonPanelMain.AppearanceButton.Normal.Options.UseFont = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.FontSizeDelta = -1;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(159)))), ((int)(((byte)(159)))), ((int)(((byte)(159)))));
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.Options.UseBackColor = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.Options.UseFont = true;
            this.windowsUIButtonPanelMain.AppearanceButton.Pressed.Options.UseForeColor = true;
            this.windowsUIButtonPanelMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            windowsUIButtonImageOptions1.ImageUri.Uri = "Save;Size32x32;GrayScaled";
            windowsUIButtonImageOptions2.ImageUri.Uri = "SaveAndClose;Size32x32;GrayScaled";
            windowsUIButtonImageOptions3.ImageUri.Uri = "Reset;Size32x32;GrayScaled";
            windowsUIButtonImageOptions4.ImageUri.Uri = "Edit/Delete;Size32x32;GrayScaled";
            this.windowsUIButtonPanelMain.Buttons.AddRange(new DevExpress.XtraEditors.ButtonPanel.IBaseButton[] {
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Guardar", true, windowsUIButtonImageOptions1, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Guardar y salir", true, windowsUIButtonImageOptions2, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Limpiar", true, windowsUIButtonImageOptions3, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false),
            new DevExpress.XtraBars.Docking2010.WindowsUIButton("Salir", true, windowsUIButtonImageOptions4, DevExpress.XtraBars.Docking2010.ButtonStyle.PushButton, "", -1, true, null, true, false, true, null, -1, false)});
            this.windowsUIButtonPanelMain.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.windowsUIButtonPanelMain.EnableImageTransparency = true;
            this.windowsUIButtonPanelMain.ForeColor = System.Drawing.Color.White;
            this.windowsUIButtonPanelMain.Location = new System.Drawing.Point(0, 501);
            this.windowsUIButtonPanelMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.windowsUIButtonPanelMain.MaximumSize = new System.Drawing.Size(0, 60);
            this.windowsUIButtonPanelMain.MinimumSize = new System.Drawing.Size(60, 60);
            this.windowsUIButtonPanelMain.Name = "windowsUIButtonPanelMain";
            this.windowsUIButtonPanelMain.Size = new System.Drawing.Size(1095, 60);
            this.windowsUIButtonPanelMain.TabIndex = 3;
            this.windowsUIButtonPanelMain.Text = "windowsUIButtonPanelMain";
            this.windowsUIButtonPanelMain.UseButtonBackgroundImages = false;
            this.windowsUIButtonPanelMain.ButtonClick += new DevExpress.XtraBars.Docking2010.ButtonEventHandler(this.windowsUIButtonPanelMain_ButtonClick);
            // 
            // labelControl
            // 
            this.labelControl.AllowHtmlString = true;
            this.labelControl.Appearance.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelControl.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(140)))), ((int)(((byte)(140)))));
            this.labelControl.Appearance.Options.UseFont = true;
            this.labelControl.Appearance.Options.UseForeColor = true;
            this.labelControl.Appearance.Options.UseTextOptions = true;
            this.labelControl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl.Location = new System.Drawing.Point(0, 0);
            this.labelControl.Name = "labelControl";
            this.labelControl.Padding = new System.Windows.Forms.Padding(10, 5, 0, 0);
            this.labelControl.Size = new System.Drawing.Size(1095, 30);
            this.labelControl.TabIndex = 1;
            this.labelControl.Text = "Agregar observación a estudiante";
            // 
            // FrmAggObservador
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1095, 561);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.labelControl);
            this.Controls.Add(this.windowsUIButtonPanelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmAggObservador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.FrmAggObservador_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFoto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcObservaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aDObservacionEstudianteListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvObservaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtObserv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecha.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraBars.Docking2010.WindowsUIButtonPanel windowsUIButtonPanelMain;
        private DevExpress.XtraEditors.LabelControl labelControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.PictureEdit pcbFoto;
        private dll.Controles.ucBusqueda txtAño;
        private DevExpress.XtraGrid.GridControl gcObservaciones;
        private DevExpress.XtraGrid.Views.Grid.GridView gvObservaciones;
        private dll.Controles.ucBusqueda txtMaestro;
        private DevExpress.XtraEditors.DateEdit dtpFecha;
        private DevExpress.XtraEditors.LabelControl lblFecha;
        private DevExpress.XtraEditors.MemoEdit txtObserv;
        private DevExpress.XtraEditors.LabelControl lblObservacion;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private System.Windows.Forms.BindingSource aDObservacionEstudianteListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOSId;
        private DevExpress.XtraGrid.Columns.GridColumn colOSIdEstudiente;
        private DevExpress.XtraGrid.Columns.GridColumn colOSNomEstudiente;
        private DevExpress.XtraGrid.Columns.GridColumn colOSIdAño;
        private DevExpress.XtraGrid.Columns.GridColumn colOSDesAño;
        private DevExpress.XtraGrid.Columns.GridColumn colOSFechaObservacion;
        private DevExpress.XtraGrid.Columns.GridColumn colOSObservacion;
        private DevExpress.XtraGrid.Columns.GridColumn colOSFechaCrea;
        private DevExpress.XtraGrid.Columns.GridColumn colOSMaestroReistra;
        private DevExpress.XtraGrid.Columns.GridColumn colOSNomMaestroReistra;
        private DevExpress.XtraGrid.Columns.GridColumn colOSMaestroReporta;
        private DevExpress.XtraGrid.Columns.GridColumn colOSNomMaestroReporta;
        private dll.Controles.ucBusqueda txtEstudiante;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckButton chkbtnOk;
        private DevExpress.XtraEditors.CheckButton chkbtnNeg;
        private DevExpress.XtraGrid.Columns.GridColumn colOSIsPositive;
    }

}