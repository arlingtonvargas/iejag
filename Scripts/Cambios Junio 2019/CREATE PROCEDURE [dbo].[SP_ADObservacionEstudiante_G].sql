
GO
-- =============================================
-- Author:		Arlington
-- Create date: 07-11-2018
-- Description:	Consulta las observaciones por id de estudiante.
-- =============================================
CREATE PROCEDURE [dbo].[SP_ADObservacionEstudiante_G]
	
	@pIdEstudiante	bigint

AS
BEGIN

DECLARE 
	@IdEstudiante bigint = @pIdEstudiante
	SET NOCOUNT ON;

	SELECT 
	OS.OSId
	,OS.OSIdEstudiente
	,CONCAT(EST.Papellido, ' ', EST.Sapellido, ' ', EST.Pnombre, ' ', EST.Snombre) AS OSNomEstudiente
	,OS.OSIdAño
	,AL.Año AS OSDesAño
	,OS.OSFechaObservacion
	,OS.OSObservacion
	,OS.OSFechaCrea
	,OS.OSMaestroReistra
	,CONCAT(MRG.Papellido, ' ', MRG.Sapellido, ' ', MRG.Pnombre, ' ', MRG.Snombre) AS OSNomMaestroReistra
	,OS.OSMaestroReporta
	,CONCAT(MRP.Papellido, ' ', MRP.Sapellido, ' ', MRP.Pnombre, ' ', MRP.Snombre) AS OSNomMaestroReporta	
	FROM ADObservacionEstudiante OS 
	INNER JOIN ADTerceros EST ON OS.OSIdEstudiente = EST.IdTercero
	INNER JOIN ADTerceros MRP ON OS.OSMaestroReporta = MRP.IdTercero
	INNER JOIN ADTerceros MRG ON OS.OSMaestroReistra = MRG.IdTercero
	INNER JOIN ADAñoLectivo AL ON OS.OSIdAño = AL.Sec
	WHERE OS.OSIdEstudiente = @IdEstudiante
END
