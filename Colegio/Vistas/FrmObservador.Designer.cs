﻿namespace Colegio.Vistas
{
    partial class FrmObservador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmObservador));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET3 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET4 = new dll.Common.ET.TituloColsBusquedaET();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtEstudiante = new dll.Controles.ucBusqueda();
            this.txtEdad = new dll.Controles.ucLabelTextBox();
            this.txtSexo = new dll.Controles.ucLabelTextBox();
            this.txtNac = new dll.Controles.ucLabelTextBox();
            this.txtOcupacion = new dll.Controles.ucLabelTextBox();
            this.txtMadre = new dll.Controles.ucBusqueda();
            this.txtPadre = new dll.Controles.ucBusqueda();
            this.txtOcuPadre = new dll.Controles.ucLabelTextBox();
            this.chkViveCon = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.txtDir = new dll.Controles.ucLabelTextBox();
            this.txtCorreo = new dll.Controles.ucLabelTextBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.rbtRelaciones = new DevExpress.XtraEditors.RadioGroup();
            this.txtPeso = new dll.Controles.ucLabelTextBox();
            this.txtTalla = new dll.Controles.ucLabelTextBox();
            this.txtTelPadre = new dll.Controles.ucLabelTextBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTelMadre = new dll.Controles.ucLabelTextBox();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txtEps = new dll.Controles.ucLabelTextBox();
            this.txtGrupoSanguineo = new dll.Controles.ucLabelTextBox();
            this.txtAlergias = new dll.Controles.ucLabelTextBox();
            this.txtEnferPadecidas = new dll.Controles.ucLabelTextBox();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.rbtIntercacion = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.rbtRendimiento = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.chkSeManifiesta = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.rbtMaltrato = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.txtMejoraP1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtFortaP1 = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.txtAyudaColegio = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txtLeGusMejorar = new dll.Controles.ucLabelTextBox();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.txtApoyoInterpersonal = new dll.Controles.ucLabelTextBox();
            this.txtApoyoActitudinal = new dll.Controles.ucLabelTextBox();
            this.txtApoyoAcade = new dll.Controles.ucLabelTextBox();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.txtDesRelaciones = new dll.Controles.ucLabelTextBox();
            this.txtDesDeport = new dll.Controles.ucLabelTextBox();
            this.txtDesArtis = new dll.Controles.ucLabelTextBox();
            this.txtDesAcademico = new dll.Controles.ucLabelTextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtMejoraP2 = new DevExpress.XtraEditors.MemoEdit();
            this.txtFortaP2 = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.txtMejoraP3 = new DevExpress.XtraEditors.MemoEdit();
            this.txtFortaP3 = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txtMejoraP4 = new DevExpress.XtraEditors.MemoEdit();
            this.txtFortaP4 = new DevExpress.XtraEditors.MemoEdit();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.txtAño = new dll.Controles.ucBusqueda();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkViveCon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtRelaciones.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbtIntercacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtRendimiento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeManifiesta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtMaltrato.Properties)).BeginInit();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtAyudaColegio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP4.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(965, 295);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "DATOS PERSONALES Y FAMILIARES";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 203F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 236F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199F));
            this.tableLayoutPanel1.Controls.Add(this.txtEstudiante, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtRelaciones, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtEdad, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtPeso, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl2, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtTalla, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtSexo, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtDir, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtCorreo, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtNac, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtMadre, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtTelPadre, 5, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.chkViveCon, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtOcupacion, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtOcuPadre, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtTelMadre, 5, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtPadre, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtAño, 4, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(961, 273);
            this.tableLayoutPanel1.TabIndex = 21;
            // 
            // txtEstudiante
            // 
            this.txtEstudiante.AnchoTextBox = 110;
            this.txtEstudiante.AnchoTitulo = 203;
            this.tableLayoutPanel1.SetColumnSpan(this.txtEstudiante, 4);
            this.txtEstudiante.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEstudiante.Location = new System.Drawing.Point(0, 0);
            this.txtEstudiante.Margin = new System.Windows.Forms.Padding(0);
            this.txtEstudiante.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtEstudiante.MensajeDeAyuda = null;
            this.txtEstudiante.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtEstudiante.Name = "txtEstudiante";
            this.txtEstudiante.PermiteSoloNumeros = false;
            this.txtEstudiante.Script = "";
            this.txtEstudiante.Size = new System.Drawing.Size(622, 28);
            this.txtEstudiante.TabIndex = 1;
            this.txtEstudiante.TextoTitulo = "Estudiante :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtEstudiante.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtEstudiante.ValorTextBox = "";
            // 
            // txtEdad
            // 
            this.txtEdad.AnchoTitulo = 200;
            this.tableLayoutPanel1.SetColumnSpan(this.txtEdad, 2);
            this.txtEdad.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEdad.Location = new System.Drawing.Point(3, 30);
            this.txtEdad.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtEdad.MensajeDeAyuda = null;
            this.txtEdad.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.PermiteSoloNumeros = true;
            this.txtEdad.Size = new System.Drawing.Size(255, 24);
            this.txtEdad.TabIndex = 2;
            this.txtEdad.TextoTitulo = "Edad :";
            this.txtEdad.ValorTextBox = "";
            // 
            // txtSexo
            // 
            this.txtSexo.AnchoTitulo = 77;
            this.tableLayoutPanel1.SetColumnSpan(this.txtSexo, 2);
            this.txtSexo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSexo.Location = new System.Drawing.Point(625, 30);
            this.txtSexo.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtSexo.MensajeDeAyuda = null;
            this.txtSexo.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.PermiteSoloNumeros = false;
            this.txtSexo.Size = new System.Drawing.Size(333, 24);
            this.txtSexo.TabIndex = 5;
            this.txtSexo.TextoTitulo = "Sexo :";
            this.txtSexo.ValorTextBox = "";
            // 
            // txtNac
            // 
            this.txtNac.AnchoTitulo = 200;
            this.tableLayoutPanel1.SetColumnSpan(this.txtNac, 6);
            this.txtNac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNac.Location = new System.Drawing.Point(3, 57);
            this.txtNac.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNac.MensajeDeAyuda = null;
            this.txtNac.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNac.Name = "txtNac";
            this.txtNac.PermiteSoloNumeros = false;
            this.txtNac.Size = new System.Drawing.Size(955, 24);
            this.txtNac.TabIndex = 6;
            this.txtNac.TextoTitulo = "Lugar y fecha Nac :";
            this.txtNac.ValorTextBox = "";
            // 
            // txtOcupacion
            // 
            this.txtOcupacion.AnchoTitulo = 200;
            this.tableLayoutPanel1.SetColumnSpan(this.txtOcupacion, 5);
            this.txtOcupacion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOcupacion.Location = new System.Drawing.Point(3, 111);
            this.txtOcupacion.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtOcupacion.MensajeDeAyuda = null;
            this.txtOcupacion.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtOcupacion.Name = "txtOcupacion";
            this.txtOcupacion.PermiteSoloNumeros = false;
            this.txtOcupacion.Size = new System.Drawing.Size(756, 24);
            this.txtOcupacion.TabIndex = 8;
            this.txtOcupacion.TextoTitulo = "Ocupación :";
            this.txtOcupacion.ValorTextBox = "";
            // 
            // txtMadre
            // 
            this.txtMadre.AnchoTextBox = 110;
            this.txtMadre.AnchoTitulo = 203;
            this.tableLayoutPanel1.SetColumnSpan(this.txtMadre, 6);
            this.txtMadre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMadre.Location = new System.Drawing.Point(0, 81);
            this.txtMadre.Margin = new System.Windows.Forms.Padding(0);
            this.txtMadre.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMadre.MensajeDeAyuda = null;
            this.txtMadre.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMadre.Name = "txtMadre";
            this.txtMadre.PermiteSoloNumeros = false;
            this.txtMadre.Script = "";
            this.txtMadre.Size = new System.Drawing.Size(961, 28);
            this.txtMadre.TabIndex = 7;
            this.txtMadre.TextoTitulo = "Madre :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtMadre.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtMadre.ValorTextBox = "";
            // 
            // txtPadre
            // 
            this.txtPadre.AnchoTextBox = 110;
            this.txtPadre.AnchoTitulo = 203;
            this.tableLayoutPanel1.SetColumnSpan(this.txtPadre, 6);
            this.txtPadre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPadre.Location = new System.Drawing.Point(0, 135);
            this.txtPadre.Margin = new System.Windows.Forms.Padding(0);
            this.txtPadre.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtPadre.MensajeDeAyuda = null;
            this.txtPadre.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtPadre.Name = "txtPadre";
            this.txtPadre.PermiteSoloNumeros = false;
            this.txtPadre.Script = "";
            this.txtPadre.Size = new System.Drawing.Size(961, 28);
            this.txtPadre.TabIndex = 10;
            this.txtPadre.TextoTitulo = "Padre :";
            tituloColsBusquedaET3.Codigo = "Código";
            tituloColsBusquedaET3.Descripcion = "Descripción";
            this.txtPadre.TituloColsBusqueda = tituloColsBusquedaET3;
            this.txtPadre.ValorTextBox = "";
            // 
            // txtOcuPadre
            // 
            this.txtOcuPadre.AnchoTitulo = 200;
            this.tableLayoutPanel1.SetColumnSpan(this.txtOcuPadre, 5);
            this.txtOcuPadre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOcuPadre.Location = new System.Drawing.Point(3, 165);
            this.txtOcuPadre.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtOcuPadre.MensajeDeAyuda = null;
            this.txtOcuPadre.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtOcuPadre.Name = "txtOcuPadre";
            this.txtOcuPadre.PermiteSoloNumeros = false;
            this.txtOcuPadre.Size = new System.Drawing.Size(756, 24);
            this.txtOcuPadre.TabIndex = 11;
            this.txtOcuPadre.TextoTitulo = "Ocupación :";
            this.txtOcuPadre.ValorTextBox = "";
            // 
            // chkViveCon
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.chkViveCon, 2);
            this.chkViveCon.ColumnWidth = 50;
            this.chkViveCon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkViveCon.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Padres"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Madre"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Padre"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Abuelos"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Otros")});
            this.chkViveCon.Location = new System.Drawing.Point(206, 192);
            this.chkViveCon.Name = "chkViveCon";
            this.tableLayoutPanel1.SetRowSpan(this.chkViveCon, 3);
            this.chkViveCon.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.chkViveCon.Size = new System.Drawing.Size(177, 78);
            this.chkViveCon.TabIndex = 13;
            // 
            // txtDir
            // 
            this.txtDir.AnchoTitulo = 235;
            this.tableLayoutPanel1.SetColumnSpan(this.txtDir, 3);
            this.txtDir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDir.Location = new System.Drawing.Point(386, 219);
            this.txtDir.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txtDir.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDir.MensajeDeAyuda = null;
            this.txtDir.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDir.Name = "txtDir";
            this.txtDir.PermiteSoloNumeros = false;
            this.txtDir.Size = new System.Drawing.Size(572, 24);
            this.txtDir.TabIndex = 15;
            this.txtDir.TextoTitulo = "Dirección :";
            this.txtDir.ValorTextBox = "";
            // 
            // txtCorreo
            // 
            this.txtCorreo.AnchoTitulo = 235;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCorreo, 3);
            this.txtCorreo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCorreo.Location = new System.Drawing.Point(386, 192);
            this.txtCorreo.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.txtCorreo.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCorreo.MensajeDeAyuda = null;
            this.txtCorreo.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.PermiteSoloNumeros = false;
            this.txtCorreo.Size = new System.Drawing.Size(572, 24);
            this.txtCorreo.TabIndex = 14;
            this.txtCorreo.TextoTitulo = "Correo electrónico :";
            this.txtCorreo.ValorTextBox = "";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseTextOptions = true;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(389, 246);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(230, 24);
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "Las relaciones en la familia son excelentes :";
            // 
            // rbtRelaciones
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.rbtRelaciones, 2);
            this.rbtRelaciones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtRelaciones.Location = new System.Drawing.Point(625, 246);
            this.rbtRelaciones.Name = "rbtRelaciones";
            this.rbtRelaciones.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Buenas"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Aceptables"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Dificiles")});
            this.rbtRelaciones.Size = new System.Drawing.Size(333, 24);
            this.rbtRelaciones.TabIndex = 16;
            // 
            // txtPeso
            // 
            this.txtPeso.AnchoTitulo = 75;
            this.txtPeso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPeso.Location = new System.Drawing.Point(264, 30);
            this.txtPeso.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPeso.MensajeDeAyuda = null;
            this.txtPeso.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.PermiteSoloNumeros = true;
            this.txtPeso.Size = new System.Drawing.Size(119, 24);
            this.txtPeso.TabIndex = 3;
            this.txtPeso.TextoTitulo = "Peso :";
            this.txtPeso.ValorTextBox = "";
            // 
            // txtTalla
            // 
            this.txtTalla.AnchoTitulo = 75;
            this.txtTalla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTalla.Location = new System.Drawing.Point(389, 30);
            this.txtTalla.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtTalla.MensajeDeAyuda = null;
            this.txtTalla.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtTalla.Name = "txtTalla";
            this.txtTalla.PermiteSoloNumeros = true;
            this.txtTalla.Size = new System.Drawing.Size(230, 24);
            this.txtTalla.TabIndex = 4;
            this.txtTalla.TextoTitulo = "Talla :";
            this.txtTalla.ValorTextBox = "";
            // 
            // txtTelPadre
            // 
            this.txtTelPadre.AnchoTitulo = 80;
            this.txtTelPadre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTelPadre.Location = new System.Drawing.Point(765, 165);
            this.txtTelPadre.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtTelPadre.MensajeDeAyuda = null;
            this.txtTelPadre.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtTelPadre.Name = "txtTelPadre";
            this.txtTelPadre.PermiteSoloNumeros = false;
            this.txtTelPadre.Size = new System.Drawing.Size(193, 24);
            this.txtTelPadre.TabIndex = 12;
            this.txtTelPadre.TextoTitulo = "Teléfono :";
            this.txtTelPadre.ValorTextBox = "";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Options.UseTextOptions = true;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(3, 192);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(197, 21);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "Vive con :";
            // 
            // txtTelMadre
            // 
            this.txtTelMadre.AnchoTitulo = 80;
            this.txtTelMadre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTelMadre.Location = new System.Drawing.Point(765, 111);
            this.txtTelMadre.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtTelMadre.MensajeDeAyuda = null;
            this.txtTelMadre.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtTelMadre.Name = "txtTelMadre";
            this.txtTelMadre.PermiteSoloNumeros = false;
            this.txtTelMadre.Size = new System.Drawing.Size(193, 24);
            this.txtTelMadre.TabIndex = 9;
            this.txtTelMadre.TextoTitulo = "Teléfono :";
            this.txtTelMadre.ValorTextBox = "";
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.txtEps);
            this.groupControl2.Controls.Add(this.txtGrupoSanguineo);
            this.groupControl2.Controls.Add(this.txtAlergias);
            this.groupControl2.Controls.Add(this.txtEnferPadecidas);
            this.groupControl2.Location = new System.Drawing.Point(12, 313);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(965, 130);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "CONDICIONES DE DESARROLLO FÍSICO Y SITUACIONES DE SALUD ";
            // 
            // txtEps
            // 
            this.txtEps.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEps.AnchoTitulo = 200;
            this.txtEps.Location = new System.Drawing.Point(4, 101);
            this.txtEps.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtEps.MensajeDeAyuda = null;
            this.txtEps.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtEps.Name = "txtEps";
            this.txtEps.PermiteSoloNumeros = false;
            this.txtEps.Size = new System.Drawing.Size(957, 24);
            this.txtEps.TabIndex = 4;
            this.txtEps.TextoTitulo = "EPS que lo atiende :";
            this.txtEps.ValorTextBox = "";
            // 
            // txtGrupoSanguineo
            // 
            this.txtGrupoSanguineo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGrupoSanguineo.AnchoTitulo = 200;
            this.txtGrupoSanguineo.Location = new System.Drawing.Point(4, 75);
            this.txtGrupoSanguineo.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtGrupoSanguineo.MensajeDeAyuda = null;
            this.txtGrupoSanguineo.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtGrupoSanguineo.Name = "txtGrupoSanguineo";
            this.txtGrupoSanguineo.PermiteSoloNumeros = false;
            this.txtGrupoSanguineo.Size = new System.Drawing.Size(957, 24);
            this.txtGrupoSanguineo.TabIndex = 3;
            this.txtGrupoSanguineo.TextoTitulo = "Grupo sanguíneo :";
            this.txtGrupoSanguineo.ValorTextBox = "";
            // 
            // txtAlergias
            // 
            this.txtAlergias.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAlergias.AnchoTitulo = 200;
            this.txtAlergias.Location = new System.Drawing.Point(4, 49);
            this.txtAlergias.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtAlergias.MensajeDeAyuda = null;
            this.txtAlergias.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtAlergias.Name = "txtAlergias";
            this.txtAlergias.PermiteSoloNumeros = false;
            this.txtAlergias.Size = new System.Drawing.Size(957, 24);
            this.txtAlergias.TabIndex = 2;
            this.txtAlergias.TextoTitulo = "Alergias a :";
            this.txtAlergias.ValorTextBox = "";
            // 
            // txtEnferPadecidas
            // 
            this.txtEnferPadecidas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEnferPadecidas.AnchoTitulo = 200;
            this.txtEnferPadecidas.Location = new System.Drawing.Point(4, 23);
            this.txtEnferPadecidas.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtEnferPadecidas.MensajeDeAyuda = null;
            this.txtEnferPadecidas.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtEnferPadecidas.Name = "txtEnferPadecidas";
            this.txtEnferPadecidas.PermiteSoloNumeros = false;
            this.txtEnferPadecidas.Size = new System.Drawing.Size(957, 24);
            this.txtEnferPadecidas.TabIndex = 1;
            this.txtEnferPadecidas.TextoTitulo = "Enfermedades padecidas :";
            this.txtEnferPadecidas.ValorTextBox = "";
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.tableLayoutPanel2);
            this.groupControl3.Location = new System.Drawing.Point(12, 449);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(965, 102);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "CONDICIONES PSICO-AFECTIVAS Y ACADÉMICAS";
            // 
            // rbtIntercacion
            // 
            this.rbtIntercacion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtIntercacion.Location = new System.Drawing.Point(658, 55);
            this.rbtIntercacion.Name = "rbtIntercacion";
            this.rbtIntercacion.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rbtIntercacion.Properties.Appearance.Options.UseBackColor = true;
            this.rbtIntercacion.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rbtIntercacion.Properties.Columns = 4;
            this.rbtIntercacion.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Superior"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Alto"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Básico"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Bajo")});
            this.rbtIntercacion.Size = new System.Drawing.Size(300, 22);
            this.rbtIntercacion.TabIndex = 4;
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseTextOptions = true;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl6.Location = new System.Drawing.Point(414, 55);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(238, 22);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "La interacción con los demás es superior :";
            // 
            // rbtRendimiento
            // 
            this.rbtRendimiento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtRendimiento.Location = new System.Drawing.Point(658, 29);
            this.rbtRendimiento.Name = "rbtRendimiento";
            this.rbtRendimiento.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rbtRendimiento.Properties.Appearance.Options.UseBackColor = true;
            this.rbtRendimiento.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rbtRendimiento.Properties.Columns = 4;
            this.rbtRendimiento.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Superior"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Alto"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Básico"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "Bajo")});
            this.rbtRendimiento.Size = new System.Drawing.Size(300, 20);
            this.rbtRendimiento.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseTextOptions = true;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl5.Location = new System.Drawing.Point(414, 29);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(238, 20);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "Su rendimiento ha sido superior :";
            // 
            // chkSeManifiesta
            // 
            this.chkSeManifiesta.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(236)))), ((int)(((byte)(239)))));
            this.chkSeManifiesta.Appearance.Options.UseBackColor = true;
            this.chkSeManifiesta.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.chkSeManifiesta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkSeManifiesta.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Comunicativo(a)"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Activo(a)"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Agresivo(a)"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Dependiente"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Independiente"),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Dinámico(a) "),
            new DevExpress.XtraEditors.Controls.CheckedListBoxItem(null, "Pasivo(a)")});
            this.chkSeManifiesta.Location = new System.Drawing.Point(206, 3);
            this.chkSeManifiesta.Name = "chkSeManifiesta";
            this.tableLayoutPanel2.SetRowSpan(this.chkSeManifiesta, 3);
            this.chkSeManifiesta.Size = new System.Drawing.Size(202, 74);
            this.chkSeManifiesta.TabIndex = 1;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseTextOptions = true;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl4.Location = new System.Drawing.Point(3, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(197, 20);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Se manifiesta :";
            // 
            // rbtMaltrato
            // 
            this.rbtMaltrato.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtMaltrato.Location = new System.Drawing.Point(658, 3);
            this.rbtMaltrato.Name = "rbtMaltrato";
            this.rbtMaltrato.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rbtMaltrato.Properties.Appearance.Options.UseBackColor = true;
            this.rbtMaltrato.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.rbtMaltrato.Properties.Columns = 4;
            this.rbtMaltrato.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "SI"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "NO")});
            this.rbtMaltrato.Size = new System.Drawing.Size(300, 20);
            this.rbtMaltrato.TabIndex = 2;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseTextOptions = true;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(414, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(238, 20);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Ha experimentado maltrato :";
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.btnGuardar);
            this.xtraScrollableControl1.Controls.Add(this.btnSalir);
            this.xtraScrollableControl1.Controls.Add(this.groupControl10);
            this.xtraScrollableControl1.Controls.Add(this.groupControl9);
            this.xtraScrollableControl1.Controls.Add(this.groupControl8);
            this.xtraScrollableControl1.Controls.Add(this.groupControl7);
            this.xtraScrollableControl1.Controls.Add(this.groupControl6);
            this.xtraScrollableControl1.Controls.Add(this.groupControl5);
            this.xtraScrollableControl1.Controls.Add(this.groupControl4);
            this.xtraScrollableControl1.Controls.Add(this.groupControl1);
            this.xtraScrollableControl1.Controls.Add(this.groupControl3);
            this.xtraScrollableControl1.Controls.Add(this.groupControl2);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.xtraScrollableControl1.Size = new System.Drawing.Size(1002, 749);
            this.xtraScrollableControl1.TabIndex = 3;
            // 
            // groupControl7
            // 
            this.groupControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl7.Controls.Add(this.tableLayoutPanel3);
            this.groupControl7.Location = new System.Drawing.Point(12, 963);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(966, 184);
            this.groupControl7.TabIndex = 7;
            this.groupControl7.Text = "Primer Periodo";
            // 
            // txtMejoraP1
            // 
            this.txtMejoraP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMejoraP1.Location = new System.Drawing.Point(484, 20);
            this.txtMejoraP1.Name = "txtMejoraP1";
            this.txtMejoraP1.Size = new System.Drawing.Size(475, 139);
            this.txtMejoraP1.TabIndex = 22;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(484, 3);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(103, 13);
            this.labelControl9.TabIndex = 21;
            this.labelControl9.Text = "Aspectos por mejorar";
            // 
            // txtFortaP1
            // 
            this.txtFortaP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFortaP1.Location = new System.Drawing.Point(3, 20);
            this.txtFortaP1.Name = "txtFortaP1";
            this.txtFortaP1.Size = new System.Drawing.Size(475, 139);
            this.txtFortaP1.TabIndex = 20;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(3, 3);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(50, 13);
            this.labelControl8.TabIndex = 19;
            this.labelControl8.Text = "Fortalezas";
            // 
            // groupControl6
            // 
            this.groupControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl6.Controls.Add(this.txtAyudaColegio);
            this.groupControl6.Controls.Add(this.labelControl7);
            this.groupControl6.Controls.Add(this.txtLeGusMejorar);
            this.groupControl6.Location = new System.Drawing.Point(12, 802);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(966, 155);
            this.groupControl6.TabIndex = 6;
            this.groupControl6.Text = "INFORMACIÓN GENERAL";
            // 
            // txtAyudaColegio
            // 
            this.txtAyudaColegio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAyudaColegio.Location = new System.Drawing.Point(207, 72);
            this.txtAyudaColegio.Name = "txtAyudaColegio";
            this.txtAyudaColegio.Size = new System.Drawing.Size(752, 75);
            this.txtAyudaColegio.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Appearance.Options.UseTextOptions = true;
            this.labelControl7.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.labelControl7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Location = new System.Drawing.Point(9, 50);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(736, 16);
            this.labelControl7.TabIndex = 19;
            this.labelControl7.Text = "Manera como desearía que le ayudara la I.E. JOSÉ ANTONIO GALÁN en su proyecto de " +
    "vida :";
            // 
            // txtLeGusMejorar
            // 
            this.txtLeGusMejorar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLeGusMejorar.AnchoTitulo = 200;
            this.txtLeGusMejorar.Location = new System.Drawing.Point(5, 23);
            this.txtLeGusMejorar.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtLeGusMejorar.MensajeDeAyuda = null;
            this.txtLeGusMejorar.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtLeGusMejorar.Name = "txtLeGusMejorar";
            this.txtLeGusMejorar.PermiteSoloNumeros = false;
            this.txtLeGusMejorar.Size = new System.Drawing.Size(956, 24);
            this.txtLeGusMejorar.TabIndex = 1;
            this.txtLeGusMejorar.TextoTitulo = "Aspectos en que le gustaría mejorar :";
            this.txtLeGusMejorar.ValorTextBox = "";
            // 
            // groupControl5
            // 
            this.groupControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl5.Controls.Add(this.txtApoyoInterpersonal);
            this.groupControl5.Controls.Add(this.txtApoyoActitudinal);
            this.groupControl5.Controls.Add(this.txtApoyoAcade);
            this.groupControl5.Location = new System.Drawing.Point(12, 692);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(966, 104);
            this.groupControl5.TabIndex = 5;
            this.groupControl5.Text = "ASPECTOS EN QUE NECESITA APOYO Y ORIENTACION ESPECIAL";
            // 
            // txtApoyoInterpersonal
            // 
            this.txtApoyoInterpersonal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApoyoInterpersonal.AnchoTitulo = 200;
            this.txtApoyoInterpersonal.Location = new System.Drawing.Point(5, 75);
            this.txtApoyoInterpersonal.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtApoyoInterpersonal.MensajeDeAyuda = null;
            this.txtApoyoInterpersonal.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtApoyoInterpersonal.Name = "txtApoyoInterpersonal";
            this.txtApoyoInterpersonal.PermiteSoloNumeros = false;
            this.txtApoyoInterpersonal.Size = new System.Drawing.Size(956, 24);
            this.txtApoyoInterpersonal.TabIndex = 3;
            this.txtApoyoInterpersonal.TextoTitulo = "En las relaciones interpersonales :";
            this.txtApoyoInterpersonal.ValorTextBox = "";
            // 
            // txtApoyoActitudinal
            // 
            this.txtApoyoActitudinal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApoyoActitudinal.AnchoTitulo = 200;
            this.txtApoyoActitudinal.Location = new System.Drawing.Point(5, 49);
            this.txtApoyoActitudinal.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtApoyoActitudinal.MensajeDeAyuda = null;
            this.txtApoyoActitudinal.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtApoyoActitudinal.Name = "txtApoyoActitudinal";
            this.txtApoyoActitudinal.PermiteSoloNumeros = false;
            this.txtApoyoActitudinal.Size = new System.Drawing.Size(956, 24);
            this.txtApoyoActitudinal.TabIndex = 2;
            this.txtApoyoActitudinal.TextoTitulo = "En lo actitudinal :";
            this.txtApoyoActitudinal.ValorTextBox = "";
            // 
            // txtApoyoAcade
            // 
            this.txtApoyoAcade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApoyoAcade.AnchoTitulo = 200;
            this.txtApoyoAcade.Location = new System.Drawing.Point(5, 23);
            this.txtApoyoAcade.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtApoyoAcade.MensajeDeAyuda = null;
            this.txtApoyoAcade.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtApoyoAcade.Name = "txtApoyoAcade";
            this.txtApoyoAcade.PermiteSoloNumeros = false;
            this.txtApoyoAcade.Size = new System.Drawing.Size(956, 24);
            this.txtApoyoAcade.TabIndex = 1;
            this.txtApoyoAcade.TextoTitulo = "En lo académico :";
            this.txtApoyoAcade.ValorTextBox = "";
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.txtDesRelaciones);
            this.groupControl4.Controls.Add(this.txtDesDeport);
            this.groupControl4.Controls.Add(this.txtDesArtis);
            this.groupControl4.Controls.Add(this.txtDesAcademico);
            this.groupControl4.Location = new System.Drawing.Point(13, 557);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(965, 129);
            this.groupControl4.TabIndex = 4;
            this.groupControl4.Text = "ASPECTOS EN QUE SE DESTACA";
            // 
            // txtDesRelaciones
            // 
            this.txtDesRelaciones.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDesRelaciones.AnchoTitulo = 200;
            this.txtDesRelaciones.Location = new System.Drawing.Point(4, 101);
            this.txtDesRelaciones.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDesRelaciones.MensajeDeAyuda = null;
            this.txtDesRelaciones.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDesRelaciones.Name = "txtDesRelaciones";
            this.txtDesRelaciones.PermiteSoloNumeros = false;
            this.txtDesRelaciones.Size = new System.Drawing.Size(956, 24);
            this.txtDesRelaciones.TabIndex = 4;
            this.txtDesRelaciones.TextoTitulo = "En las relaciones interpersonales :";
            this.txtDesRelaciones.ValorTextBox = "";
            // 
            // txtDesDeport
            // 
            this.txtDesDeport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDesDeport.AnchoTitulo = 200;
            this.txtDesDeport.Location = new System.Drawing.Point(4, 75);
            this.txtDesDeport.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDesDeport.MensajeDeAyuda = null;
            this.txtDesDeport.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDesDeport.Name = "txtDesDeport";
            this.txtDesDeport.PermiteSoloNumeros = false;
            this.txtDesDeport.Size = new System.Drawing.Size(956, 24);
            this.txtDesDeport.TabIndex = 3;
            this.txtDesDeport.TextoTitulo = "En lo deportivo :";
            this.txtDesDeport.ValorTextBox = "";
            // 
            // txtDesArtis
            // 
            this.txtDesArtis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDesArtis.AnchoTitulo = 200;
            this.txtDesArtis.Location = new System.Drawing.Point(4, 49);
            this.txtDesArtis.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDesArtis.MensajeDeAyuda = null;
            this.txtDesArtis.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDesArtis.Name = "txtDesArtis";
            this.txtDesArtis.PermiteSoloNumeros = false;
            this.txtDesArtis.Size = new System.Drawing.Size(956, 24);
            this.txtDesArtis.TabIndex = 2;
            this.txtDesArtis.TextoTitulo = "En lo artístico :";
            this.txtDesArtis.ValorTextBox = "";
            // 
            // txtDesAcademico
            // 
            this.txtDesAcademico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDesAcademico.AnchoTitulo = 200;
            this.txtDesAcademico.Location = new System.Drawing.Point(4, 23);
            this.txtDesAcademico.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDesAcademico.MensajeDeAyuda = null;
            this.txtDesAcademico.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDesAcademico.Name = "txtDesAcademico";
            this.txtDesAcademico.PermiteSoloNumeros = false;
            this.txtDesAcademico.Size = new System.Drawing.Size(956, 24);
            this.txtDesAcademico.TabIndex = 1;
            this.txtDesAcademico.TextoTitulo = "En lo académico :";
            this.txtDesAcademico.ValorTextBox = "";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 203F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 208F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 244F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.rbtIntercacion, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelControl4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.rbtRendimiento, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelControl6, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.rbtMaltrato, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.chkSeManifiesta, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl5, 2, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(961, 80);
            this.tableLayoutPanel2.TabIndex = 26;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.labelControl8, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelControl9, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtMejoraP1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtFortaP1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(962, 162);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // groupControl8
            // 
            this.groupControl8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl8.Controls.Add(this.tableLayoutPanel4);
            this.groupControl8.Location = new System.Drawing.Point(12, 1153);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(966, 184);
            this.groupControl8.TabIndex = 8;
            this.groupControl8.Text = "Primer Periodo";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.labelControl10, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelControl11, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtMejoraP2, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtFortaP2, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(962, 162);
            this.tableLayoutPanel4.TabIndex = 23;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(3, 3);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(50, 13);
            this.labelControl10.TabIndex = 19;
            this.labelControl10.Text = "Fortalezas";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(484, 3);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(103, 13);
            this.labelControl11.TabIndex = 21;
            this.labelControl11.Text = "Aspectos por mejorar";
            // 
            // txtMejoraP2
            // 
            this.txtMejoraP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMejoraP2.Location = new System.Drawing.Point(484, 20);
            this.txtMejoraP2.Name = "txtMejoraP2";
            this.txtMejoraP2.Size = new System.Drawing.Size(475, 139);
            this.txtMejoraP2.TabIndex = 22;
            // 
            // txtFortaP2
            // 
            this.txtFortaP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFortaP2.Location = new System.Drawing.Point(3, 20);
            this.txtFortaP2.Name = "txtFortaP2";
            this.txtFortaP2.Size = new System.Drawing.Size(475, 139);
            this.txtFortaP2.TabIndex = 20;
            // 
            // groupControl9
            // 
            this.groupControl9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl9.Controls.Add(this.tableLayoutPanel5);
            this.groupControl9.Location = new System.Drawing.Point(12, 1343);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(966, 184);
            this.groupControl9.TabIndex = 9;
            this.groupControl9.Text = "Primer Periodo";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.labelControl12, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.labelControl13, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtMejoraP3, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtFortaP3, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(962, 162);
            this.tableLayoutPanel5.TabIndex = 23;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(3, 3);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(50, 13);
            this.labelControl12.TabIndex = 19;
            this.labelControl12.Text = "Fortalezas";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(484, 3);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(103, 13);
            this.labelControl13.TabIndex = 21;
            this.labelControl13.Text = "Aspectos por mejorar";
            // 
            // txtMejoraP3
            // 
            this.txtMejoraP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMejoraP3.Location = new System.Drawing.Point(484, 20);
            this.txtMejoraP3.Name = "txtMejoraP3";
            this.txtMejoraP3.Size = new System.Drawing.Size(475, 139);
            this.txtMejoraP3.TabIndex = 22;
            // 
            // txtFortaP3
            // 
            this.txtFortaP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFortaP3.Location = new System.Drawing.Point(3, 20);
            this.txtFortaP3.Name = "txtFortaP3";
            this.txtFortaP3.Size = new System.Drawing.Size(475, 139);
            this.txtFortaP3.TabIndex = 20;
            // 
            // groupControl10
            // 
            this.groupControl10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl10.Controls.Add(this.tableLayoutPanel6);
            this.groupControl10.Location = new System.Drawing.Point(12, 1533);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(966, 184);
            this.groupControl10.TabIndex = 10;
            this.groupControl10.Text = "Primer Periodo";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.labelControl14, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.labelControl15, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtMejoraP4, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.txtFortaP4, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(962, 162);
            this.tableLayoutPanel6.TabIndex = 23;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(3, 3);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(50, 13);
            this.labelControl14.TabIndex = 19;
            this.labelControl14.Text = "Fortalezas";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(484, 3);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(103, 13);
            this.labelControl15.TabIndex = 21;
            this.labelControl15.Text = "Aspectos por mejorar";
            // 
            // txtMejoraP4
            // 
            this.txtMejoraP4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMejoraP4.Location = new System.Drawing.Point(484, 20);
            this.txtMejoraP4.Name = "txtMejoraP4";
            this.txtMejoraP4.Size = new System.Drawing.Size(475, 139);
            this.txtMejoraP4.TabIndex = 22;
            // 
            // txtFortaP4
            // 
            this.txtFortaP4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFortaP4.Location = new System.Drawing.Point(3, 20);
            this.txtFortaP4.Name = "txtFortaP4";
            this.txtFortaP4.Size = new System.Drawing.Size(475, 139);
            this.txtFortaP4.TabIndex = 20;
            // 
            // btnSalir
            // 
            this.btnSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalir.Location = new System.Drawing.Point(892, 1725);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(86, 34);
            this.btnSalir.TabIndex = 12;
            this.btnSalir.Text = "Salir";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Location = new System.Drawing.Point(800, 1725);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(86, 34);
            this.btnGuardar.TabIndex = 11;
            this.btnGuardar.Text = "Guardar";
            // 
            // txtAño
            // 
            this.txtAño.AnchoTextBox = 50;
            this.txtAño.AnchoTitulo = 80;
            this.tableLayoutPanel1.SetColumnSpan(this.txtAño, 2);
            this.txtAño.Location = new System.Drawing.Point(622, 0);
            this.txtAño.Margin = new System.Windows.Forms.Padding(0);
            this.txtAño.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAño.MensajeDeAyuda = null;
            this.txtAño.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAño.Name = "txtAño";
            this.txtAño.PermiteSoloNumeros = false;
            this.txtAño.Script = "";
            this.txtAño.Size = new System.Drawing.Size(339, 28);
            this.txtAño.TabIndex = 1;
            this.txtAño.TextoTitulo = "Año lectivo :";
            tituloColsBusquedaET4.Codigo = "Código";
            tituloColsBusquedaET4.Descripcion = "Descripción";
            this.txtAño.TituloColsBusqueda = tituloColsBusquedaET4;
            this.txtAño.ValorTextBox = "";
            // 
            // FrmObservador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1002, 749);
            this.Controls.Add(this.xtraScrollableControl1);
            this.Name = "FrmObservador";
            this.Text = "Observador";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkViveCon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtRelaciones.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbtIntercacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtRendimiento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeManifiesta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtMaltrato.Properties)).EndInit();
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtAyudaColegio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMejoraP4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFortaP4.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private dll.Controles.ucLabelTextBox txtPeso;
        private dll.Controles.ucLabelTextBox txtTalla;
        private dll.Controles.ucLabelTextBox txtOcupacion;
        private dll.Controles.ucLabelTextBox txtNac;
        private dll.Controles.ucLabelTextBox txtSexo;
        private dll.Controles.ucLabelTextBox txtEdad;
        private dll.Controles.ucBusqueda txtEstudiante;
        private dll.Controles.ucLabelTextBox txtTelMadre;
        private dll.Controles.ucLabelTextBox txtOcuPadre;
        private dll.Controles.ucBusqueda txtPadre;
        private dll.Controles.ucBusqueda txtMadre;
        private dll.Controles.ucLabelTextBox txtTelPadre;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckedListBoxControl chkViveCon;
        private dll.Controles.ucLabelTextBox txtCorreo;
        private dll.Controles.ucLabelTextBox txtDir;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.RadioGroup rbtRelaciones;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private dll.Controles.ucLabelTextBox txtEps;
        private dll.Controles.ucLabelTextBox txtGrupoSanguineo;
        private dll.Controles.ucLabelTextBox txtAlergias;
        private dll.Controles.ucLabelTextBox txtEnferPadecidas;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.RadioGroup rbtIntercacion;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.RadioGroup rbtRendimiento;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.CheckedListBoxControl chkSeManifiesta;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.RadioGroup rbtMaltrato;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private DevExpress.XtraEditors.MemoEdit txtMejoraP1;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.MemoEdit txtFortaP1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.MemoEdit txtAyudaColegio;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private dll.Controles.ucLabelTextBox txtLeGusMejorar;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private dll.Controles.ucLabelTextBox txtApoyoInterpersonal;
        private dll.Controles.ucLabelTextBox txtApoyoActitudinal;
        private dll.Controles.ucLabelTextBox txtApoyoAcade;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private dll.Controles.ucLabelTextBox txtDesRelaciones;
        private dll.Controles.ucLabelTextBox txtDesDeport;
        private dll.Controles.ucLabelTextBox txtDesArtis;
        private dll.Controles.ucLabelTextBox txtDesAcademico;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.MemoEdit txtMejoraP4;
        private DevExpress.XtraEditors.MemoEdit txtFortaP4;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.MemoEdit txtMejoraP3;
        private DevExpress.XtraEditors.MemoEdit txtFortaP3;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.MemoEdit txtMejoraP2;
        private DevExpress.XtraEditors.MemoEdit txtFortaP2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private dll.Controles.ucBusqueda txtAño;
    }
}