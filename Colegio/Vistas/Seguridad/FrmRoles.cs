﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using dll.Common;
using Colegio.Clases;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using dll.Common.Class;

namespace Colegio.Vistas.Seguridad
{
    public partial class FrmRoles : FrmBase
    {
        private ClRoles clRoles =  new ClRoles();
        private bool _EstaActualizando = false;
        private int IdRol = 0;
        public bool EstaActualizando
        {
            get
            {
                return _EstaActualizando;
            }
            set
            {
                _EstaActualizando = value;               
            }
        }
        public FrmRoles()
        {
            InitializeComponent();
        }

        private void FrmRoles_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenarGrilla();
            txtNomRol.Focus();
        }

        private void CrearGrilla()
        {
            try
            {
                gvRoles = GrillaDevExpress.CrearGrilla(false, true);
                gvRoles.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho:20));
                gvRoles.Columns.Add(GrillaDevExpress.CrearColumna("NomRol", "Nombre del rol"));
                gvRoles.Columns.Add(GrillaDevExpress.CrearColumna("FechaCrea", "", visible: false));
                gvRoles.Columns.Add(GrillaDevExpress.CrearColumna("FechaMod", "", visible: false));
                gvRoles.Columns.Add(GrillaDevExpress.CrearColumna("Anulado", "", visible: false));
                gvRoles.Columns.Add(GrillaDevExpress.CrearColumna("UsuCrea", "", visible:false));
                gvRoles.Appearance.ViewCaption.Font = new Font("Segoe UI", 9.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvRoles.Appearance.FocusedRow.Font = new Font("Segoe UI", 9.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvRoles.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvRoles.OptionsCustomization.AllowColumnResizing = true;
                gcRoles.MainView = gvRoles;
                this.gvRoles.DoubleClick += new System.EventHandler(this.gvRoles_DoubleClick);
            }
            catch (Exception)
            {

                throw;
            }
        }


        private void LlenarGrilla()
        {
            try
            {
                gcRoles.DataSource = clRoles.GetRoles();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void LimpiarCampos()
        {
            try
            {
                txtNomRol.ValorTextBox = "";
                IdRol = 0;
                txtNomRol.Enabled = true;
                EstaActualizando = false;
                txtNomRol.Focus();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private bool GuardarRol(string NomRol, int UsuCrea)
        {
            try
            {
               if(clRoles.InsertRol(NomRol, UsuCrea))
                {
                    return true;
                }else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        private bool ActualizoRol(int idRol, string NomRol)
        {
            try
            {
                if (clRoles.UpdateRol(idRol, NomRol))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        private void ucBtnGuardar_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                FrmMensaje frm = new FrmMensaje();
                if (txtNomRol.ValorTextBox == "")
                {
                    frm.lblMensaje.Text = "Por favor digite el nombre del rol que desea insertar.";
                    frm.ShowDialog();
                    txtNomRol.Focus();
                    return;
                }
                if (!EstaActualizando)
                {                    
                    if (GuardarRol(txtNomRol.ValorTextBox, ClFunciones.UsuarioIngreso.IdUsuario))
                    {
                        frm.lblMensaje.Text = "Rol insertado de manera correcta.";
                        frm.ShowDialog();
                        txtNomRol.ValorTextBox = "";
                        txtNomRol.Focus();
                    }
                }
                else
                {
                    if (ActualizoRol(IdRol, txtNomRol.ValorTextBox))
                    {
                        frm.lblMensaje.Text = "Rol actualizado de manera correcta.";
                        frm.ShowDialog();
                        txtNomRol.ValorTextBox = "";
                        txtNomRol.Focus();
                    }
                }
                LimpiarCampos();
                LlenarGrilla();
            }
            catch (Exception)
            {
            }
        }

        private void ucBtnEliminar_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                if (EstaActualizando)
                {
                    FrmMensaje frmMsj = new FrmMensaje();
                    if (clRoles.EstaEnUsuario(IdRol))
                    {
                        frmMsj.lblMensaje.Text = "El rol seleccionado se encuentra asociado a uno o mas usuarios, no es posible eliminar.";
                    }
                    FrmConfirmacion frm = new FrmConfirmacion();
                    frm.lblMensaje.Text = "Seguro que desea eliminar el rol seleccionado.";
                    frm.ShowDialog();
                    if (frm.Confirmo)
                    {
                        if (clRoles.DeleteRol(IdRol))
                        {
                            frmMsj.lblMensaje.Text = "Rol eliminado de manera correcta.";
                            frmMsj.ShowDialog();
                            LlenarGrilla();
                            LimpiarCampos();
                        } 
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void ucBtnLimpiar_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            LimpiarCampos();
        }

        private void ucBtnCancelar_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvRoles_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                EstaActualizando = false;
                int numFila = ClFuncionesdll.DobleClicSoreFila(sender, e);
                if (numFila > -1)
                {
                    IdRol = Convert.ToInt32(gvRoles.GetRowCellValue(numFila, "Sec"));
                    string sql = "SELECT * FROM SGUsuarios US INNER JOIN SGRoles RL ON US.IdRol = RL.Sec WHERE IdRol = "+ IdRol.ToString();
                    DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                    txtNomRol.Enabled = true;
                    ucBtnEliminar.Enabled = true;
                    lblMensaje.Text = "";
                    txtNomRol.ValorTextBox = Convert.ToString(gvRoles.GetRowCellValue(numFila, "NomRol"));
                    txtNomRol.Focus();
                    EstaActualizando = true;
                    if (dt.Rows.Count>0)
                    {
                        ucBtnEliminar.Enabled = false;
                        txtNomRol.Enabled = false;
                        lblMensaje.Text = "Existen usuarios con el rol seleccionado, imposible cambiar el nombre del rol.";
                    }
                    
                }                
            }
            catch (Exception)
            {
            }
        }
    }
}