﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Colegio.Clases;

namespace Colegio.Vistas.Reports
{
    public partial class FrmRptNotas : DevExpress.XtraEditors.XtraForm
    {
        ClNotas clNotas = new ClNotas();
        ClAñoLectivo clAño = new ClAñoLectivo();
        ClCursos clCursos = new ClCursos();

        public FrmRptNotas()
        {
            InitializeComponent();
        }

        private void FrmRptNotas_Load(object sender, EventArgs e)
        {
            LlenarCamposBusq();
            LlenarCbxPeriodos();
            txtCurso.Focus();
            txtCurso.Select();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            listADNotasVistaETBindingSource.Clear();
            if (txtAño.ValorTextBox == "" || txtCurso.ValorTextBox == "" || lkePeriodo.lkeDatos.EditValue == null) return;
            int idPeriodo = Convert.ToInt32(lkePeriodo.lkeDatos.EditValue);
            int idCurso = Convert.ToInt32(txtCurso.ValorTextBox);
      
            listADNotasVistaETBindingSource.DataSource = clNotas.GetNotasByCursoPeriodo(idPeriodo, idCurso);
            if (listADNotasVistaETBindingSource.Count <= 0) ClFunciones.msgError("No se encontraron datos.");

            pgEstudiantes.CollapseAll();
        }

        private void LlenarCamposBusq()
        {
            try
            {
                //string sql = "SELECT Sec, Año FROM ADAñoLectivo";
                DataTable dt = clAño.GetAñosLectivos(); //Clases.ClFunciones.Consultar(sql, Clases.ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.DataTable = dt;
                    txtAño.ValorTextBox = ClFunciones.wADDatosInstitucion[0].IdAñoActual.ToString();

                    txtCurso.DataTable = new DataTable();
                    if (txtAño.ValorTextBox != "")
                    {
                        dt = clCursos.GetCursosByAño(Convert.ToInt32(txtAño.ValorTextBox));
                        if (dt.Rows.Count > 0)
                        {
                            dt.Columns[0].ColumnName = "Codigo";
                            dt.Columns[1].ColumnName = "Descripcion";
                        }
                        txtCurso.DataTable = dt;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LlenarCbxPeriodos()
        {
            try
            {
                Clases.ClPeriodos cl = new Clases.ClPeriodos();
                lkePeriodo.lkeDatos.Properties.DataSource = cl.GetPeriodos();
                lkePeriodo.lkeDatos.Properties.ValueMember = "IdPeriodo";
                lkePeriodo.lkeDatos.Properties.DisplayMember = "Nombre";
                lkePeriodo.lkeDatos.Properties.PopulateColumns();
                lkePeriodo.lkeDatos.Properties.Columns[0].Visible = false;
                lkePeriodo.lkeDatos.EditValue = ClFunciones.wADDatosInstitucion[0].PeriodoActual;
                lkePeriodo.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void pgEstudiantes_CustomSummary(object sender, DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventArgs e)
        {

           
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (listADNotasVistaETBindingSource.Count <= 0) return;
            if (tabPane1.SelectedPage == tabNavigationPage1)
            {
                pgEstudiantes.OptionsPrint.PageSettings.Landscape = true;
                pgEstudiantes.OptionsPrint.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Legal;
                pgEstudiantes.ShowPrintPreview();
            }
            else if (tabPane1.SelectedPage == tabNavigationPage2)
            {
                chartControl1.OptionsPrint.SizeMode = DevExpress.XtraCharts.Printing.PrintSizeMode.Stretch;
                
                chartControl1.ShowPrintPreview();

            }
            
        }
    }
}