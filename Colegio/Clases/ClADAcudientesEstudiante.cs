﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Colegio.Clases
{
    public class ClADAcudientesEstudiante
    {
        public List<ADAcudientesEstudianteET> GetAcudientesEstudiante(long IdEstudiante)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList();
                listaParam.Add(new ParametersSpET() { NombreParametro = "@pIdEstudiante", Valor = IdEstudiante });
                List<ADAcudientesEstudianteET> res = ClFunciones.DataReaderMapToListConSP<ADAcudientesEstudianteET>("SP_ADAcudientesEstudiante_G", listaParam);
                return res;
            }
            catch (Exception ex)
            {
                return new List<ADAcudientesEstudianteET>();
            }
        }

        public ADAcudientesEstudianteET InsertAcudientesEstudiante(ADAcudientesEstudianteET Acudiente)
        {
            try
            {                
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_ADAcudientesEstudiante_I", con);
                        comando.CommandType = CommandType.StoredProcedure;                      
                        comando.Parameters.AddWithValue("@pIdEstudiante",  Acudiente.IdEstudiante);
                        comando.Parameters.AddWithValue("@pIdAcudiente",  Acudiente.IdAcudiente);
                        comando.Parameters.AddWithValue("@pParentesco",  Acudiente.Parentesco);
                        comando.Parameters.AddWithValue("@pNombres",  Acudiente.Nombres);
                        comando.Parameters.AddWithValue("@pApellidos",  Acudiente.Apellidos);
                        comando.Parameters.AddWithValue("@pTelefono",  Acudiente.Telefono);
                        comando.Parameters.AddWithValue("@pDireccion",  Acudiente.Direccion);
                        comando.Parameters.AddWithValue("@pEmail",  Acudiente.Email);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        string Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return null;
                        }
                    }
                    scope.Complete();
                }
                return Acudiente;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ADAcudientesEstudianteET UpdateAcudientesEstudiante(ADAcudientesEstudianteET Acudiente)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_ADAcudientesEstudiante_U", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pIdEstudiante", Acudiente.IdEstudiante);
                        comando.Parameters.AddWithValue("@pIdAcudiente", Acudiente.IdAcudiente);
                        comando.Parameters.AddWithValue("@pParentesco", Acudiente.Parentesco);
                        comando.Parameters.AddWithValue("@pNombres", Acudiente.Nombres);
                        comando.Parameters.AddWithValue("@pApellidos", Acudiente.Apellidos);
                        comando.Parameters.AddWithValue("@pTelefono", Acudiente.Telefono);
                        comando.Parameters.AddWithValue("@pDireccion", Acudiente.Direccion);
                        comando.Parameters.AddWithValue("@pEmail", Acudiente.Email);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        string Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return null;
                        }
                    }
                    scope.Complete();
                }
                return Acudiente;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public bool DeleteAcudientesEstudiante(ADAcudientesEstudianteET Acudiente)
        {
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        SqlCommand comando = new SqlCommand("SP_ADAcudientesEstudiante_D", con);
                        comando.CommandType = CommandType.StoredProcedure;
                        comando.Parameters.AddWithValue("@pIdEstudiante", Acudiente.IdEstudiante);
                        comando.Parameters.AddWithValue("@pParentesco", Acudiente.Parentesco);
                        comando.Parameters.Add("@Inserto", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
                        comando.ExecuteNonQuery();
                        string Inserto = comando.Parameters["@Inserto"].Value.ToString();
                        if (Inserto == "NO")
                        {
                            return false;
                        }
                    }
                    scope.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
