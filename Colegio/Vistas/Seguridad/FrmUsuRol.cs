﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using dll.Common;
using Colegio.Clases;

namespace Colegio.Vistas.Seguridad
{
    public partial class FrmUsuRol : FrmBase
    {
        ClUsuarios clUsuarios = new ClUsuarios();
        ClRoles clRoles = new ClRoles();
        ClUsuariosRoles clUsuariosRoles = new ClUsuariosRoles();
        public FrmUsuRol()
        {
            InitializeComponent();
        }   

        private void FrmUsuRol_Load(object sender, EventArgs e)
        {
            CargarUsuarios();
            CargarRoles();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (cbxUsuarios.ItemIndex>-1 && cbxRoles.ItemIndex > -1)
            {
                if (clUsuariosRoles.GuardarRolUsuario(Convert.ToInt32(cbxRoles.EditValue), Convert.ToInt32(cbxUsuarios.EditValue)))
                {
                    FrmMensaje frm = new FrmMensaje();
                    frm.lblMensaje.Text = "Permisos asignados de manera correcta.";
                    frm.ShowDialog();
                }else
                {
                    FrmMensaje frm = new FrmMensaje();
                    frm.lblMensaje.Text = "Lo sentimos, ha ocurrido un error, por favor vuela a intentarlo.";
                    frm.ShowDialog();
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CargarUsuarios()
        {
            cbxUsuarios.Properties.DataSource = clUsuarios.GetUsuarios();
            cbxUsuarios.Properties.ValueMember = "IdUsuario";
            cbxUsuarios.Properties.DisplayMember = "NomUsu";
            cbxUsuarios.Properties.PopulateColumns();
            cbxUsuarios.Properties.ShowFooter = false;
            cbxUsuarios.Properties.ShowHeader = false;
            cbxUsuarios.Properties.Columns[0].Visible = false;
            cbxUsuarios.Properties.Columns[2].Visible = false;
            cbxUsuarios.Properties.Columns[3].Visible = false;
            cbxUsuarios.Properties.Columns[4].Visible = false;
            cbxUsuarios.Properties.Columns[5].Visible = false;
            cbxUsuarios.Properties.Columns[6].Visible = false;

        }

        private void CargarRoles()
        {
            cbxRoles.Properties.DataSource = clRoles.GetRoles();
            cbxRoles.Properties.ValueMember = "Sec";
            cbxRoles.Properties.DisplayMember = "NomRol";
            cbxRoles.Properties.PopulateColumns();
            cbxRoles.Properties.ShowFooter = false;
            cbxRoles.Properties.ShowHeader = false;
            cbxRoles.Properties.Columns[0].Visible = false;
            cbxRoles.Properties.Columns[2].Visible = false;
            cbxRoles.Properties.Columns[3].Visible = false;
            cbxRoles.Properties.Columns[4].Visible = false;
            cbxRoles.Properties.Columns[5].Visible = false;
        }
    }
}