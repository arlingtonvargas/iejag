
GO
-- =============================================
-- Author:		Arlington
-- Create date: 02-06-2019
-- Description:	Inserta observacion por de estudiante.
-- =============================================
CREATE PROCEDURE [dbo].[SP_ADObservacionEstudiante_I]
	
	 @pOSIdEstudiente bigint
	,@pOSIdAño int
	,@pOSFechaObservacion datetime
	,@pOSObservacion varchar(max)
	,@pOSMaestroReistra bigint
	,@pOSMaestroReporta bigint

AS
BEGIN

	SET NOCOUNT ON;

INSERT INTO [dbo].[ADObservacionEstudiante]
    ([OSIdEstudiente]
    ,[OSIdAño]
    ,[OSFechaObservacion]
    ,[OSObservacion]
    ,[OSFechaCrea]
    ,[OSMaestroReistra]
    ,[OSMaestroReporta])
VALUES
    (@pOSIdEstudiente
    ,@pOSIdAño
    ,@pOSFechaObservacion
    ,@pOSObservacion
    ,GETDATE()
    ,@pOSMaestroReistra
    ,@pOSMaestroReporta)
	
END
