﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClAñoLectivo
    {
        public DataTable GetAñosLectivos()
        {
            try
            {
                string sql = "SELECT AL.Sec, AL.Año, AL.FechaDesde, AL.FechaHasta FROM ADAñoLectivo AL";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ADAñoLectivoET GetAñoLectivo(int idAñoLectivo)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList();
                listaParam.Add(new ParametersSpET() { NombreParametro = "@pSec", Valor = idAñoLectivo });
                List<ADAñoLectivoET> AñoLectivo = ClFunciones.DataReaderMapToListConSP<ADAñoLectivoET>("SP_AñoLectivoById_G", listaParam);
                return (AñoLectivo.Count > 0) ? AñoLectivo[0] : new ADAñoLectivoET();
            }
            catch (Exception)
            {
                return new ADAñoLectivoET();
            }
        }

        public ADAñoLectivoET GetAñoLectivoVigente(int idAñoLectivo)
        {
            try
            {
                ParametersSpETList listaParam = new ParametersSpETList();
                listaParam.Add(new ParametersSpET() { NombreParametro = "@pSec", Valor = idAñoLectivo });
                List<ADAñoLectivoET> AñoLectivo = ClFunciones.DataReaderMapToListConSP<ADAñoLectivoET>("SP_AñoLectivoVigenteById_G", listaParam);
                return (AñoLectivo.Count > 0) ? AñoLectivo[0] : new ADAñoLectivoET();
            }
            catch (Exception)
            {
                return new ADAñoLectivoET();
            }
        }
    }
}
