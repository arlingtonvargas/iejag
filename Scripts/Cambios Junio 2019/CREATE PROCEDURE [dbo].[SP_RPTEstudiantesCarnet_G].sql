USE [BDIEJAG]
GO
/****** Object:  StoredProcedure [dbo].[SP_RPTEstudiantesCarnet_G]    Script Date: 03/06/2019 12:14:49 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 03-06-2019
-- Description:	Consulta los estudiantes para imprimir carnet.
-- =============================================
ALTER PROCEDURE [dbo].[SP_RPTEstudiantesCarnet_G]
	
	@IdTercero BIGINT = NULL
	,@IdAñoLectivo	INT
	,@SecCurso	INT = NULL

AS
BEGIN

DECLARE 
	@pIdTercero bigint = @IdTercero
	,@pIdAñoLectivo bigint = @IdAñoLectivo
	,@pSecCurso int = @SecCurso
	

	DECLARE @SQL NVARCHAR(MAX) = 'SELECT M.IdTercero 
	,CONCAT(RTRIM(LTRIM(M.Papellido)), '' '', RTRIM(LTRIM(M.Sapellido)), '' '', RTRIM(LTRIM(M.Pnombre)), '' '', RTRIM(LTRIM(M.Snombre))) AS NombreComp
	,M.TipoDoc
	,CONCAT(RTRIM(LTRIM(M.Pnombre)), '' '', RTRIM(LTRIM(M.Snombre))) AS Pnombre
	,M.Snombre
	,CONCAT(RTRIM(LTRIM(M.Papellido)), '' '', RTRIM(LTRIM(M.Sapellido))) AS Papellido
	,M.Sapellido
	,M.Correo
	,M.Direccion
	,M.Sexo
	,M.FechaNacimiento
	,M.LugarNaciento AS IdLugarNacimiento
	,M.Telefono
	,M.FechaCrea
	,M.IdUsuario
	,M.TipoTercero
	,M.Foto
	,SD.NomSede
	,GD.Nombre AS NomGrado
	FROM ADTerceros M
	INNER JOIN ADMatriculas MT ON M.IdTercero = MT.IdEstudiante
	INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
	INNER JOIN ADGrados GD ON CS.IdGrado = GD.Sec
	INNER JOIN ADSedes SD ON MT.IdSede = SD.Sec
	WHERE M.TipoTercero = 2 
	AND MT.IdAñoLectivo = ' + LTRIM(STR(@pIdAñoLectivo,10));
	
	IF(@pSecCurso IS NOT NULL OR @pSecCurso <> '')
		 SET @SQL += ' AND CS.Sec = ' + LTRIM(STR(@pSecCurso,10));

	IF(@pIdTercero IS NOT NULL OR @pIdTercero <> '')
		 SET @SQL += ' AND M.IdTercero = ' + LTRIM(STR(@pIdTercero,20));
	
	 EXECUTE sp_executesql @SQL;

END
