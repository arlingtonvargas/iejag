﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Colegio.Clases;
using System.Globalization;

namespace Colegio.Reportes
{
    public partial class RPTCarnet : DevExpress.XtraReports.UI.XtraReport
    {
        public RPTCarnet()
        {
            InitializeComponent();
            lblNomRector.Text = ClFunciones.wADDatosInstitucion[0].NombreRector;
            ClAñoLectivo clAñoLectivo = new ClAñoLectivo();
            ET.ADAñoLectivoET aDAñoLectivoET = clAñoLectivo.GetAñoLectivo(ClFunciones.wADDatosInstitucion[0].IdAñoActual);
            lblAño.Text = "Válido hasta " + aDAñoLectivoET.FechaHasta.ToShortDateString();
        }

        private void lblNombres_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                XRLabel label = (XRLabel)sender;
                string txt = label.Text;                
                if (txt != "")
                {
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    txt = textInfo.ToTitleCase(txt.ToLower());
                }
                label.Text = txt;

            }
            catch (Exception ex)
            {

            }            
        }
    }
}
