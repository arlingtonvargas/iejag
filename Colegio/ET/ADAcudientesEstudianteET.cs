﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADAcudientesEstudianteET
    {
        public long IdEstudiante { get; set; }
        public long IdAcudiente { get; set; }
        public int Parentesco { get; set; }
        public string NomParentesco { get; set; }        
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public long Telefono { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
    }

    public class ADAcudientesEstudianteList : List<ADAcudientesEstudianteET>
    { }
}
