﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class SGUsuariosET
    {
        public int IdUsuario { get; set; }
        public string NomUsu { get; set; }
        public int UsuCrea { get; set; }
        public DateTime FechaCrea     { get; set; }
        public DateTime FechaMod { get; set; }
        public int? UsuMod { get; set; }
        public string password { get; set; }
        public DateTime FechaUltIngreso { get; set; }
        public bool  Anulado { get; set; }
        public int IdRol { get; set; }
        public long IdTercero { get; set; }
    }
}
