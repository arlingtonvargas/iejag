﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public   class AsignaturaNotasET
    {
        public string NomAsig { get; set; }
        public string NomDoc { get; set; }
        public string IH { get; set; }
        public string AUS { get; set; }
        public string P1 { get; set; }
        public string P2 { get; set; }
        public string P3 { get; set; }
        public string P4 { get; set; }
        public string PROM { get; set; }
        public string Desempeños { get; set; }
        public string Observaciones { get; set; }
        public string Periodo { get; set; }
    }
}
