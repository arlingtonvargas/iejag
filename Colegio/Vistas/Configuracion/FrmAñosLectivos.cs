﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Colegio.Clases;

namespace Colegio.Vistas.Configuracion
{
    public partial class FrmAñosLectivos : dll.Common.FrmBase
    {
        int SecAño = -1;
        bool EstaActualizando = false;
        public FrmAñosLectivos()
        {
            InitializeComponent();
        }

        private void FrmAñosLectivos_Load(object sender, EventArgs e)
        {
            LlenaCombo();
            CrearGrilla();
            LlenaGrilla();
            cbxAño.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (cbxAño.lkeDatos.EditValue == null)
            {
                return;
            }
            if (ClFunciones.ExisteDato("ADAñoLectivo", "Año", cbxAño.lkeDatos.EditValue.ToString(), ClConexion.clConexion.Conexion))
            {
                ClFunciones.msgError("Ya existe el año "+ cbxAño.lkeDatos.EditValue.ToString());
                return;
            }
            if (!EstaActualizando) SecAño = ClFunciones.TraeSiguienteSecuencial("ADAñoLectivo", "Sec", ClConexion.clConexion.Conexion);
            if (SecAño > 0)
            {
                if (GuardarMat(SecAño, cbxAño.lkeDatos.EditValue.ToString()))
                {                   
                    ClFunciones.msgExitoso("Año creado de manera correcta.");
                    LlenaGrilla();
                    LimpiarCampos();
                }
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            cbxAño.lkeDatos.EditValue = null;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void LlenaCombo()
        {
            List<string> Años = new List<string>()
            {
                "2001", "2002", "2003",
                "2004", "2005", "2006",
                "2007", "2008", "2009",
                "2010", "2011", "2012",
                "2013", "2014", "2015",
                "2016", "2017", "2018",
                "2019", "2020", "2021",
                "2022", "2023", "2024",
                "2025", "2026", "2027",
                "2028", "2029", "2030"
            };
            cbxAño.lkeDatos.Properties.DataSource = Años;
        }

        private void CrearGrilla()
        {
            try
            {
                gvMaterias = GrillaDevExpress.CrearGrilla(false, true);
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho: 20));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("Año", "Año"));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("FechaDesde", "Fecha Desde"));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("FechaHasta", "Fecha Hasta"));
                gvMaterias.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvMaterias.OptionsCustomization.AllowColumnResizing = true;
                gcMaterias.MainView = gvMaterias;
                this.gvMaterias.DoubleClick += new System.EventHandler(this.gvMaterias_DoubleClick);

            }
            catch (Exception)
            {

            }
        }

        private bool GuardarMat(int secAño, string Año)
        {
            try
            {
                string sql = string.Format("set dateformat dmy; INSERT INTO [dbo].[ADAñoLectivo]  " +
                    " ([Sec],[Año],[FechaDesde],[FechaHasta]) VALUES ({0},'{1}','{2:dd/MM/yyyy}', '{3:dd/MM/yyyy}')",
                    secAño, Año, new DateTime(Convert.ToInt32(Año),01,01), new DateTime(Convert.ToInt32(Año), 12, 31));
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) != "si") return false;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        private void LlenaGrilla()
        {
            try
            {
                string sql = "SELECT Sec, Año, FechaDesde, FechaHasta FROM ADAñoLectivo";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                gcMaterias.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LimpiarCampos()
        {
            cbxAño.lkeDatos.EditValue = null;
            SecAño = -1;
            EstaActualizando = false;
            cbxAño.Focus();
        }

        private void CargarMateria(int numFila)
        {
            try
            {
                SecAño = Convert.ToInt32(gvMaterias.GetRowCellValue(numFila, "Sec"));
                cbxAño.lkeDatos.EditValue = gvMaterias.GetRowCellValue(numFila, "NomMateria").ToString();
                EstaActualizando = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gvMaterias_DoubleClick(object sender, EventArgs e)
        {
            //int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
            //if (numFila > -1)
            //{
            //    CargarMateria(numFila);
            //}
        }

    }
}