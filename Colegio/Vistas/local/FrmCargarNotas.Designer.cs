﻿namespace Colegio.Vistas.local
{
    partial class FrmCargarNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcNotas = new DevExpress.XtraGrid.GridControl();
            this.gvNotas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnCargar = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcNotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNotas)).BeginInit();
            this.SuspendLayout();
            // 
            // gcNotas
            // 
            this.gcNotas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcNotas.Location = new System.Drawing.Point(12, 53);
            this.gcNotas.MainView = this.gvNotas;
            this.gcNotas.Name = "gcNotas";
            this.gcNotas.Size = new System.Drawing.Size(776, 385);
            this.gcNotas.TabIndex = 0;
            this.gcNotas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvNotas});
            // 
            // gvNotas
            // 
            this.gvNotas.GridControl = this.gcNotas;
            this.gvNotas.Name = "gvNotas";
            // 
            // btnCargar
            // 
            this.btnCargar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCargar.Location = new System.Drawing.Point(516, 12);
            this.btnCargar.Name = "btnCargar";
            this.btnCargar.Size = new System.Drawing.Size(133, 35);
            this.btnCargar.TabIndex = 1;
            this.btnCargar.Text = "Cargar excel de notas";
            this.btnCargar.Click += new System.EventHandler(this.btnCargar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGuardar.Location = new System.Drawing.Point(655, 12);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(133, 35);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.Text = "Iniciar insercion";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // FrmCargarNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnCargar);
            this.Controls.Add(this.gcNotas);
            this.Name = "FrmCargarNotas";
            this.Text = "Cargar Notas";
            this.Load += new System.EventHandler(this.FrmCargarNotas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcNotas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNotas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcNotas;
        private DevExpress.XtraGrid.Views.Grid.GridView gvNotas;
        private DevExpress.XtraEditors.SimpleButton btnCargar;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
    }
}