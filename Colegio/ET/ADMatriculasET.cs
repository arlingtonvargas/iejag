﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADMatriculasET
    {
        public int Sec { get; set; }
        public long IdEstudiante { get; set; }
        public int IdCurso { get; set; }
        public int IdAñoLectivo { get; set; }
        public DateTime FechaCrea { get; set; }
        public int UsuCrea { get; set; }
        public int IdSede { get; set; }
        public int IdJornada { get; set; }
        public bool Internado { get; set; }
        public bool Cancelada { get; set; }
    }

    public class ADMatriculasETVista
    {
        public int IdMatricula  { get; set; }
        public DateTime FechaCrea { get; set; }
        public long IdTercero { get; set; }
        public string NombreCompleto { get; set; }
        public string UsuCrea { get; set; }
        public int IdGrado { get; set; }
        public string NomCurso { get; set; }
        public string AñoLectivo { get; set; }
        public bool Internado { get; set; }
        public bool Cancelada { get; set; }
    }
}
