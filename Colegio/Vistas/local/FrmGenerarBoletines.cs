﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;

namespace Colegio.Vistas.local
{
    public partial class FrmGenerarBoletines : Form
    {
        public FrmGenerarBoletines()
        {
            InitializeComponent();
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {
            GenerarBoletines();
        }

        private void GenerarBoletines()
        {
            try
            {
                //string sql = "SELECT Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, " +
                //" GD.Nombre, GD.Descripcion FROM local.Estudiantes ET" +
                //" INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado WHERE GD.IdGrado = " + lkeCursos.EditValue.ToString();
                //string sql = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY ET.Documento) AS Posicion, Documento, NomCompleto,  " +
                //" Sede, Jornada, Codigo, Grupo, GD.Nombre, GD.Descripcion, " +
                //" (SELECT AVG(ISNULL(NT.p3, 0)) AS Prom FROM local.Notas NT " +
                //" INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado " +
                //" INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria " +
                //" INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente " +
                //" INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente " +
                //" WHERE GR.IdGrado = {0} AND NT.idestudiante = ET.Documento " +
                //" GROUP BY NT.idestudiante) AS PromPeriodo FROM local.Estudiantes ET " +
                //" INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado " +
                //" INNER JOIN local.Notas NT ON ET.Documento = NT.idestudiante " +
                //" WHERE GD.IdGrado = {0} " +
                //" GROUP BY Documento, NomCompleto, Sede, Jornada, Codigo, Grupo,  " +
                //" GD.Nombre, GD.Descripcion", lkeCursos.EditValue);
                string sql = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY Z.PromPeriodo DESC) AS Posicion  " +
                " , Z.Documento, Z.NomCompleto, Z.Sede, Z.Jornada, Z.Codigo, Z.Grupo, " +
                " Z.Nombre, Z.Descripcion, Z.PromPeriodo " +
                " FROM(SELECT Documento, NomCompleto, " +
                " Sede, Jornada, Codigo, Grupo, GD.Nombre, GD.Descripcion, " +
                " (SELECT AVG(ISNULL(NT.p3, 0)) AS Prom FROM local.Notas NT " +
                " INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado " +
                " INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria " +
                " INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente " +
                " INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente " +
                " WHERE GR.IdGrado = {0} AND NT.idestudiante = ET.Documento " +
                " GROUP BY NT.idestudiante) AS PromPeriodo FROM local.Estudiantes ET " +
                " INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado " +
                " INNER JOIN local.Notas NT ON ET.Documento = NT.idestudiante " +
                " WHERE GD.IdGrado = {0} " +
                " GROUP BY Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, " +
                " GD.Nombre, GD.Descripcion) AS Z", lkeCursos.EditValue);
                List<double> promedios = new List<double>();
                DataTable dt = (DataTable)gridControl1.DataSource; //ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);

                if (dt.Rows.Count>0)
                {
                    sql = "SELECT MT.Nombre AS NomMateria, DC.NomCompleto AS Docente, NT.IH, NT.AUS, NT.p1, NT.p2, " +
                        " NT.p3,NT.p4, (ISNULL(NT.p1,0) + ISNULL(NT.p2,0) + ISNULL(NT.p3,0))/3 AS Prom," +
                        " NT.competencias, CONCAT(DC1.NomCompleto,DC1.PerfilAbv) AS Titular FROM local.Notas NT " +
                        " INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado " +
                        " INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria " +
                        " INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente " +
                        " INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente " +
                        " WHERE NT.idestudiante = " + dt.Rows[0]["Documento"].ToString();
                    DataTable dtMat = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);

                    Reportes.RPTBoletin RPT = new Reportes.RPTBoletin();
                    object prom = dtMat.Compute("Sum(p3)/" + dtMat.Rows.Count, "");
                    RPT.DetailReport.DataSource = dtMat;
                    RPT.lblAsignatura.DataBindings.Add("Text", null, "NomMateria", "     {0}");
                    RPT.lblNomDocente.DataBindings.Add("Text", null, "Docente", "Docente: {0}");
                    RPT.lblIH.DataBindings.Add("Text", null, "IH");
                    RPT.lblAUS.DataBindings.Add("Text", null, "AUS");
                    RPT.lblP1.DataBindings.Add("Text", null, "P1", "{0:N1}");
                    RPT.lblP2.DataBindings.Add("Text", null, "P2", "{0:N1}");
                    RPT.lblP3.DataBindings.Add("Text", null, "P3", "{0:N1}");
                    RPT.lblP4.DataBindings.Add("Text", null, "P4", "{0:N1}");
                    RPT.lblProm.DataBindings.Add("Text", null, "Prom", "{0:N1}");
                    RPT.lblDesempeños.DataBindings.Add("Text", null, "competencias");

                    RPT.lblSede.Text = dt.Rows[0]["Sede"].ToString();
                    RPT.lblJornada.Text = dt.Rows[0]["Jornada"].ToString();
                    RPT.lblPosGrupo.Text = dt.Rows[0]["Posicion"].ToString(); ;
                    RPT.lblGrupo.Text = dt.Rows[0]["Descripcion"].ToString();
                    RPT.lblPeriodo.Text = "3";
                    RPT.lblDocumento.Text = dt.Rows[0]["Documento"].ToString();
                    RPT.lblNomEst.Text = dt.Rows[0]["NomCompleto"].ToString();
                    RPT.lblCodigo.Text = dt.Rows[0]["Codigo"].ToString();
                    RPT.lblNivelDesemp.Text = Desempeño(Convert.ToDouble(prom));//MANDAR PROM
                    //RPT.lblObserv.Text = "";// dt.Rows[0]["Observaciones"].ToString();
                    RPT.lblPromGen.Text = Convert.ToDouble(prom).ToString("N1");
                    RPT.lblTitularGrupo.Text = dtMat.Rows[0]["Titular"].ToString();
                    RPT.CreateDocument();
                    for (int i = 1; i < dt.Rows.Count; i++)
                    {
                        DataRow item = dt.Rows[i];
                        sql = "SELECT MT.Nombre AS NomMateria, DC.NomCompleto AS Docente, NT.IH, NT.AUS, NT.p1, NT.p2, " +
                        " NT.p3,NT.p4, (ISNULL(NT.p1,0) + ISNULL(NT.p2,0) + ISNULL(NT.p3,0))/3 AS Prom, " +
                        " NT.competencias, CONCAT(DC1.NomCompleto,DC1.PerfilAbv) AS Titular FROM local.Notas NT " +
                        " INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado " +
                        " INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria " +
                        " INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente " +
                        " INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente " +
                        " WHERE NT.idestudiante = " + item["Documento"].ToString();
                        dtMat = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                        if (dtMat.Rows.Count>0)
                        {
                            Reportes.RPTBoletin RPT2 = new Reportes.RPTBoletin();
                            prom = dtMat.Compute("Sum(p3)/" + dtMat.Rows.Count, "");
                            RPT2.DetailReport.DataSource = dtMat;
                            RPT2.lblAsignatura.DataBindings.Add("Text", null, "NomMateria", "     {0}");
                            RPT2.lblNomDocente.DataBindings.Add("Text", null, "Docente", "Docente: {0}");
                            RPT2.lblIH.DataBindings.Add("Text", null, "IH");
                            RPT2.lblAUS.DataBindings.Add("Text", null, "AUS");
                            RPT2.lblP1.DataBindings.Add("Text", null, "P1", "{0:N1}");
                            RPT2.lblP2.DataBindings.Add("Text", null, "P2", "{0:N1}");
                            RPT2.lblP3.DataBindings.Add("Text", null, "P3", "{0:N1}");
                            RPT2.lblP4.DataBindings.Add("Text", null, "P4", "{0:N1}");
                            RPT2.lblProm.DataBindings.Add("Text", null, "Prom", "{0:N1}");
                            RPT2.lblDesempeños.DataBindings.Add("Text", null, "competencias");

                            RPT2.lblSede.Text = item["Sede"].ToString();
                            RPT2.lblJornada.Text = item["Jornada"].ToString();
                            RPT2.lblPosGrupo.Text = item["Posicion"].ToString();
                            RPT2.lblGrupo.Text = item["Descripcion"].ToString();
                            RPT2.lblPeriodo.Text = "3";
                            RPT2.lblDocumento.Text = item["Documento"].ToString();
                            RPT2.lblNomEst.Text = item["NomCompleto"].ToString();
                            RPT2.lblCodigo.Text = item["Codigo"].ToString();
                            RPT2.lblNivelDesemp.Text = Desempeño(Convert.ToDouble(prom));//MANDAR PROM
                            //RPT2.lblObserv.Text = "";// dt.Rows[0]["Observaciones"].ToString();
                            RPT2.lblPromGen.Text = Convert.ToDouble(prom).ToString("N1");
                            RPT2.lblTitularGrupo.Text = dtMat.Rows[0]["Titular"].ToString();
                            RPT2.CreateDocument();

                            RPT.Pages.AddRange(RPT2.Pages);

                            RPT.PrintingSystem.ContinuousPageNumbering = true;
                        }                       
                    }

                    FrmVistaPrevia report = new FrmVistaPrevia();
                    report.document.DocumentSource = RPT;
                    report.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string Desempeño(double prom)
        {
            try
            {
                if (prom>=0 && prom <= 2.9)
                {
                    return "Bajo";
                }
                else if (prom >= 3 && prom <= 3.9)
                {
                    return "Básico";

                }
                else if (prom >= 4 && prom <= 4.5)
                {

                    return "Alto";
                }
                else if (prom >= 4.6 && prom <= 5)
                {
                    return "Superior";

                }else
                {
                    return "";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "";

            }
        }

        private void CargarCursos()
        {
            try
            {
                string sql = "SELECT IdGrado, Descripcion FROM local.Grados";

                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);

                lkeCursos.Properties.DataSource = dt;
                lkeCursos.Properties.ValueMember = "IdGrado";
                lkeCursos.Properties.DisplayMember = "Descripcion";
                lkeCursos.Properties.PopulateColumns();
                lkeCursos.Properties.Columns[0].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmGenerarBoletines_Load(object sender, EventArgs e)
        {
            CargarCursos();
        }

        private void lkeCursos_EditValueChanged(object sender, EventArgs e)
        {
            string sql = string.Format("SELECT ROW_NUMBER() OVER (ORDER BY Z.PromPeriodo DESC) AS Posicion  " +
              " , Z.Documento, Z.NomCompleto, Z.Sede, Z.Jornada, Z.Codigo, Z.Grupo, " +
              " Z.Nombre, Z.Descripcion, Z.PromPeriodo " +
              " FROM(SELECT Documento, NomCompleto, " +
              " Sede, Jornada, Codigo, Grupo, GD.Nombre, GD.Descripcion, " +
              " (SELECT AVG(ISNULL(NT.p3, 0)) AS Prom FROM local.Notas NT " +
              " INNER JOIN local.Grados GR ON NT.idgrado = GR.IdGrado " +
              " INNER JOIN local.Materias MT ON NT.idmateria = MT.IdMateria " +
              " INNER JOIN local.Docentes DC ON NT.idMaestro = DC.IdDocente " +
              " INNER JOIN local.Docentes DC1 ON NT.idtitular = DC1.IdDocente " +
              " WHERE GR.IdGrado = {0} AND NT.idestudiante = ET.Documento " +
              " GROUP BY NT.idestudiante) AS PromPeriodo FROM local.Estudiantes ET " +
              " INNER JOIN local.Grados GD ON ET.Grupo = GD.IdGrado " +
              " INNER JOIN local.Notas NT ON ET.Documento = NT.idestudiante " +
              " WHERE GD.IdGrado = {0} " +
              " GROUP BY Documento, NomCompleto, Sede, Jornada, Codigo, Grupo, " +
              " GD.Nombre, GD.Descripcion) AS Z", lkeCursos.EditValue);
            DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
            gridControl1.DataSource = dt;
        }
    }
}
