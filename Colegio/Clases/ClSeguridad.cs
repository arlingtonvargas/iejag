﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClSeguridad
    {
        public static void CrearTransacciones()
        {
            RegistraTansaccion(1001, "Crear roles.", "");
            RegistraTansaccion(1002, "Asignar roles a usuario.", "");
            RegistraTansaccion(1003, "Asignar permisos por rol.", "");
            RegistraTansaccion(1004, "Ver lista de usuarios.", "");
            RegistraTansaccion(1005, "Ver lista de Maestros.", "");
            RegistraTansaccion(1006, "Crear Maestros.", "");
            RegistraTansaccion(1007, "Editar Maestros.", "");
            RegistraTansaccion(1008, "Ver lista de Administrativos.", "");
            RegistraTansaccion(1009, "Crear Administrativos.", "");
            RegistraTansaccion(1010, "Editar Administrativos.", "");
            RegistraTansaccion(1011, "Ver lista de Estudiantes.", "");
            RegistraTansaccion(1012, "Crear Estudiantes.", "");
            RegistraTansaccion(1013, "Editar Estudiantes.", "");
            RegistraTansaccion(1014, "Ver lista de matriculas.", "");
            RegistraTansaccion(1015, "Crear matricula.", "");
            RegistraTansaccion(1016, "Crear Curso.", "");
            RegistraTansaccion(1017, "Agregar materias a curso.", "");
            RegistraTansaccion(1018, "Reportar notas.", "");
            RegistraTansaccion(1019, "Gerenerar Boletines", "");
            RegistraTansaccion(1020, "Ver reporte de notas por estudiante", "");
            RegistraTansaccion(1021, "Crear Materias", "");
            RegistraTansaccion(1022, "Crear Sedes", "");
            RegistraTansaccion(1023, "Crear Años lectivos", "");
            RegistraTansaccion(1024, "Crear Jornadas", "");
            RegistraTansaccion(1025, "Ver la configuración general", "");
            RegistraTansaccion(1026, "Guardar el periodo actual", "");
            RegistraTansaccion(1027, "Permite registrar notas por todos los maestros", "");
            RegistraTansaccion(1028, "Generar carnets", "");
        }

        public static List<SGPermisosET> CargarPermisos()
        {
            try
            {
                string sql = "SELECT SecTransaccion, NomPermiso, DesPermiso, PR.IdRol FROM SGPermisos PS " +
                " INNER JOIN SGPermisosRoles PR ON PS.SecTransaccion = PR.IdPermiso";
                List <SGPermisosET> res = ClFunciones.DataReaderMapToList<SGPermisosET>(sql);
                return res;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static void RegistraTansaccion(int numTransaccion, string nomPermiso, string DesTransaccion)
        {
            try
            {
                string sql = "";
                if (ClFunciones.ExisteDato("SGPermisos", "SecTransaccion", numTransaccion.ToString(), ClConexion.clConexion.Conexion))
                {
                    sql = string.Format("UPDATE SGPermisos SET NomPermiso='{0}', DesPermiso = '{1}' WHERE SecTransaccion= {2}", nomPermiso, DesTransaccion, numTransaccion);
                }
                else
                {
                    sql = string.Format("INSERT INTO SGPermisos VALUES({0},'{1}', '{2}')", numTransaccion, nomPermiso, DesTransaccion);
                }
                if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) == "") { ClFunciones.msgError("Error registrando la transaccion " + numTransaccion.ToString()); }
            }
            catch (Exception ex)
            {
                return;
            }
            
        }

        public static bool ValidaPermisosDB(int idRol, int idPermiso)
        {
            try
            {
                string sql = string.Format("SELECT PS.Sec, PS.NomPermiso FROM SGPermisos PS " +
                " INNER JOIN SGPermisosRoles PR ON PS.Sec = PR.IdPermiso "+
                " WHERE PR.IdRol = {0} AND PS.Sec ={1}", idRol, idPermiso);
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool ValidaPermisosLocal(int idRol, int idPermiso)
        {
            try
            {
                if (ClFunciones.ListaPermisos.Where(x => x.SecTransaccion == idPermiso && x.IdRol == idRol).ToList().Count > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
