USE [BDIEJAG]
GO

ALTER TABLE [dbo].[ADDatosInstitucion] DROP CONSTRAINT [FK_ADDatosInstitucion_ADAñoLectivo]
GO

/****** Object:  Table [dbo].[ADDatosInstitucion]    Script Date: 03/06/2019 1:14:04 p.m. ******/
DROP TABLE [dbo].[ADDatosInstitucion]
GO

/****** Object:  Table [dbo].[ADDatosInstitucion]    Script Date: 03/06/2019 1:14:04 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ADDatosInstitucion](
	[Sec] [int] IDENTITY(1,1) NOT NULL,
	[Nit] [varchar](20) NOT NULL,
	[NombreCompleto] [varchar](1000) NOT NULL,
	[NombreCorto] [varchar](500) NOT NULL,
	[PeriodoActual] [int] NOT NULL,
	[CodDANA] [varchar](100) NOT NULL,
	[NombreRector] [varchar](500) NOT NULL,
	[DocRector] [bigint] NOT NULL,
	[IdAñoActual] [int] NOT NULL,
 CONSTRAINT [PK_ADDatosInstitucion] PRIMARY KEY CLUSTERED 
(
	[Sec] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[ADDatosInstitucion]  WITH CHECK ADD  CONSTRAINT [FK_ADDatosInstitucion_ADAñoLectivo] FOREIGN KEY([IdAñoActual])
REFERENCES [dbo].[ADAñoLectivo] ([Sec])
GO

ALTER TABLE [dbo].[ADDatosInstitucion] CHECK CONSTRAINT [FK_ADDatosInstitucion_ADAñoLectivo]
GO


