﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Colegio.ET;

namespace Colegio.Clases
{
    public class ClMaterias
    {
        //public List<ADMateriasETVista> GetMateriasVista()
        //{
        //    try
        //    {
        //        string sql = "SELECT MT.Sec AS IdMateria, MT.NomMateria FROM ADMaterias MT " +
        //        " INNER JOIN ADMateriasCursos MC ON MT.Sec = MC.IdMateria " +
        //        " INNER JOIN ADCursos CS ON MC.IdCurso = CS.Sec ";
        //        List<ADMateriasETVista> res = ClFunciones.DataReaderMapToList<ADMateriasETVista>(sql);
        //        return res;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        //public List<ADMateriasETVista> GetMaterias()
        //{
        //    try
        //    {
        //        string sql = "SELECT MT.Sec AS IdMateria, MT.NomMateria FROM ADMaterias MT";
        //        List<ADMateriasETVista> res = ClFunciones.DataReaderMapToList<ADMateriasETVista>(sql);
        //        return res;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        public DataTable GetMateriasDT()
        {
            try
            {
                string sql = "SELECT MT.Sec AS IdMateria, MT.NomMateria FROM ADMaterias MT";
                return ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);                
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<ADMateriasETVista> GetMateriasVistaByCurso(int idCurso)
        {
            try
            {
                string sql = "SELECT MC.Sec, MC.IdCurso, CS.NomCurso, MC.IdMateria, MT.NomMateria, MC.IdMaestro, " +
                " CONCAT(ET.Pnombre, ' ', ET.Snombre, ' ', ET.Papellido, ' ', ET.Sapellido) AS NomMaestro, " +
                " MC.Desempeños, MC.IH, CS.IdAñoLectivo, AL.Año FROM ADMaterias MT " +
                " INNER JOIN ADMateriasCursos MC ON MT.Sec = MC.IdMateria " +
                " INNER JOIN ADCursos CS ON MC.IdCurso = CS.Sec " +
                " INNER JOIN ADAñoLectivo AL ON CS.IdAñoLectivo = AL.Sec " +
                " INNER JOIN ADTerceros ET ON MC.IdMaestro = ET.IdTercero " +
                " WHERE MC.IdCurso = " + idCurso.ToString();
                List<ADMateriasETVista> res = ClFunciones.DataReaderMapToList<ADMateriasETVista>(sql);
                return res;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable GetMateriasVistaNoInCurso(int idCurso)
        {
            try
            {
                string sql = string.Format("SELECT MT.Sec AS IdMateria, MT.NomMateria " +
                " FROM ADMaterias MT" +
                " WHERE MT.Sec NOT IN(SELECT IdMateria FROM ADMateriasCursos WHERE IdCurso = {0})", idCurso);
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable GetMateriasVistaByCursoDT(int idCurso)
        {
            try
            {
                string sql = "SELECT MT.Sec AS IdMateria, MT.NomMateria FROM ADMaterias MT " +
                " INNER JOIN ADMateriasCursos MC ON MT.Sec = MC.IdMateria " +
                " INNER JOIN ADCursos CS ON MC.IdCurso = CS.Sec " +
                " WHERE CS.Sec = " + idCurso.ToString();
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
