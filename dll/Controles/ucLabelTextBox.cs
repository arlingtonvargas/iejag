﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace dll.Controles
{
    public partial class ucLabelTextBox : DevExpress.XtraEditors.XtraUserControl
    {
        private bool _PermiteSoloNumeros;
        private string _MensajeDeAyuda;
        private int _AnchoTitulo = 100;
        private string _ValorTextBox = "";
        private string _TextoTitulo = "Titulo";

        public ucLabelTextBox()
        {
            InitializeComponent();
        }


        [Category("Propiedades propias")]
        [Description("Determina si el campo solo recibe numeros.")]
        public bool PermiteSoloNumeros
        {
            get
            {
                return _PermiteSoloNumeros;
            }
            set
            {
                _PermiteSoloNumeros = value;
            }
        }

        [Category("Propiedades propias")]
        [Description("Mensaje de ayuda.")]
        public string MensajeDeAyuda
        {
            get
            {
                return _MensajeDeAyuda;
            }
            set
            {
                _MensajeDeAyuda = value;
            }
        }

        [Category("Propiedades propias")]
        [Description("Ancho del titulo.")]
        public int AnchoTitulo
        {
            get
            {
                return _AnchoTitulo;
            }
            set
            {
                _AnchoTitulo = value;
                tableLayoutPanel1.ColumnStyles[0].Width = AnchoTitulo;

            }
        }     

        [Category("Propiedades propias")]
        [Description("Valor del TextBox.")]
        public string ValorTextBox
        {
            get
            {
                return _ValorTextBox;
            }
            set
            {
                _ValorTextBox = value;
                txtCampo.Text = ValorTextBox;
            }
        }

        [Category("Propiedades propias")]
        [Description("Texto del titulo.")]
        public string TextoTitulo
        {
            get
            {
                return _TextoTitulo;
            }
            set
            {
                _TextoTitulo = value;
                lblTitulo.Text = TextoTitulo;
            }
        }

        private void txtCampo_Enter(object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt != null)
                {
                    txt.BackColor = dll.Common.Class.ClFuncionesdll.ColorFondoControles;
                    txt.SelectionStart = 0;
                    txt.SelectionLength = txt.Text.Length;
                    txt.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                    lblTitulo.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                    dll.Common.Class.ClFuncionesdll.BarStaticItemPrincipal.Caption = this.MensajeDeAyuda;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtCampo_Leave(object sender, EventArgs e)
        {
            try
            {
                TextEdit txt = (TextEdit)sender;
                if (txt != null)
                {
                    txt.BackColor = Color.White;
                    txt.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular);
                    lblTitulo.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular);
                    ValorTextBox = txt.Text;
                    //Txt.BorderStyle = BorderStyle.FixedSingle;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtCampo_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {                
                if (e.KeyChar == (char)Keys.Enter)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
                else if (e.KeyChar == (char)Keys.Escape)
                {
                    e.Handled = true;
                    SendKeys.Send("+{TAB}");
                }

                if (PermiteSoloNumeros)
                {
                    if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
                    {
                        e.Handled = true;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void txtCampo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Up)
                {
                    e.Handled = true;
                    SendKeys.Send("+{TAB}");
                }
                else if (e.KeyCode == Keys.Down)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

      
    }
}
