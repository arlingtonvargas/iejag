﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class SGPermisosET
    {
        public int SecTransaccion { get; set; }
        public string NomPermiso { get; set; }
        public string DesPermiso { get; set; }
        public int IdRol { get; set; }
    }
}
