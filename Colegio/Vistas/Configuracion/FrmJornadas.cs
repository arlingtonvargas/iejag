﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Colegio.Clases;


namespace Colegio.Vistas.Configuracion
{
    public partial class FrmJornadas : dll.Common.FrmBase
    {
        int SecJornada = -1;
        bool EstaActualizando = false;
        public FrmJornadas()
        {
            InitializeComponent();
        }

        private void FrmJornadas_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            LlenaGrilla();
            txtNomMateria.Focus();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtNomMateria.ValorTextBox == "")
            {
                ClFunciones.msgError("Debe digitar el nombre de la jornada.");
                txtNomMateria.Focus();
                return;
            }
            if (!EstaActualizando) SecJornada = ClFunciones.TraeSiguienteSecuencial("ADJornadas", "Sec", ClConexion.clConexion.Conexion);
            if (SecJornada > 0)
            {
                if (GuardarMat(SecJornada, txtNomMateria.ValorTextBox))
                {
                    if (!EstaActualizando)
                    {
                        ClFunciones.msgExitoso("Jornada creada de manera correcta.");

                    }
                    else
                    {
                        ClFunciones.msgExitoso("Jornada actualizada de manera correcta.");
                    }
                    LlenaGrilla();
                    LimpiarCampos();
                }
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CrearGrilla()
        {
            try
            {
                gvMaterias = GrillaDevExpress.CrearGrilla(false, true);
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "Id", ancho: 20));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("NomJornada", "Jornada"));
                gvMaterias.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvMaterias.OptionsCustomization.AllowColumnResizing = true;
                gcMaterias.MainView = gvMaterias;
                this.gvMaterias.DoubleClick += new System.EventHandler(this.gvMaterias_DoubleClick);

            }
            catch (Exception)
            {

            }
        }

        private bool GuardarMat(int secMat, string nomMat)
        {
            try
            {
                string sql = "UPDATE [dbo].[ADJornadas] SET [NomJornada] = '" + nomMat + "' WHERE Sec = " + secMat.ToString();
                if (EstaActualizando)
                {
                    if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) != "si") return false;

                }
                else
                {
                    sql = "INSERT INTO [dbo].[ADJornadas]  ([Sec],[NomJornada]) VALUES (" + secMat.ToString() + ",'" + nomMat + "')";
                    if (ClFunciones.EjecutaComando(sql, ClConexion.clConexion.Conexion) != "si") return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        private void LlenaGrilla()
        {
            try
            {
                string sql = "SELECT Sec, NomJornada FROM ADJornadas";
                DataTable dt = ClFunciones.Consultar(sql, ClConexion.clConexion.Conexion);
                gcMaterias.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LimpiarCampos()
        {
            txtNomMateria.ValorTextBox = "";
            SecJornada = -1;
            EstaActualizando = false;
            txtNomMateria.Focus();
        }

        private void CargarMateria(int numFila)
        {
            try
            {
                SecJornada = Convert.ToInt32(gvMaterias.GetRowCellValue(numFila, "Sec"));
                txtNomMateria.ValorTextBox = gvMaterias.GetRowCellValue(numFila, "NomJornada").ToString();
                EstaActualizando = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gvMaterias_DoubleClick(object sender, EventArgs e)
        {
            int numFila = dll.Common.Class.ClFuncionesdll.DobleClicSoreFila(sender, e);
            if (numFila > -1)
            {
                CargarMateria(numFila);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
        }
    }
}