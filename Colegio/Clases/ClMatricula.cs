﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Colegio.ET;

namespace Colegio.Clases
{
    public class ClMatricula
    {
        public List<ADMatriculasETVista> GetMatriculas()
        {
            try
            {
                string sql = "SELECT MT.Sec AS IdMatricula, MT.FechaCrea, ET.IdTercero,  " +
                " CONCAT(Papellido, ' ', Sapellido, ' ', Pnombre, ' ', Snombre) AS NombreCompleto," +
                " US.NomUsu AS UsuCrea, CS.IdGrado, CS.NomCurso, AL.Sec AS SecAñoLectivo, AL.Año AS AñoLectivo, MT.Internado, MT.Cancelada" +
                " FROM ADMatriculas MT" +
                " INNER JOIN ADTerceros ET ON MT.IdEstudiante = ET.IdTercero" +
                " INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec" +
                " INNER JOIN ADAñoLectivo AL ON MT.IdAñoLectivo = AL.Sec" +
                " INNER JOIN SGUsuarios US ON MT.UsuCrea = US.IdUsuario";
                List<ADMatriculasETVista> matriculas = ClFunciones.DataReaderMapToList<ADMatriculasETVista>(sql);
                return matriculas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertMatricula(ADMatriculasET pParam, SqlConnection con)
        {
            try
            {
                int Sec = ClFunciones.TraeSiguienteSecuencial("ADMatriculas", "Sec", con);
                if (Sec > 0)
                {
                    string sql = string.Format("INSERT INTO ADMatriculas (Sec, IdEstudiante, IdCurso, " +
                    " IdAñoLectivo, FechaCrea, UsuCrea, IdSede, IdJornada, Internado) " +
                    " VALUES ({0}, {1}, {2},{3}, '{4:dd/MM/yyyy HH:mm:ss}',{5}, {6}, {7}, '{8}')",
                    Sec, pParam.IdEstudiante, pParam.IdCurso, pParam.IdAñoLectivo, pParam.FechaCrea,
                    pParam.UsuCrea, pParam.IdSede, pParam.IdJornada, pParam.Internado);
                    string res = ClFunciones.EjecutaComando(sql, con);
                    if (res == "si")
                    {
                        return true;
                    }
                    else { return false; }
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                ClRegistraLogs.RegistrarError(new ET.SGLogs()
                {
                    FechaReg = DateTime.Now,
                    MensajeError = ex.Message,
                    NomArchivo = "Matriculas",
                    NomError = "Exception",
                    NomFunction = "InsertMatricula",
                    NomModulo = "Matricula",
                    NumLinea = 57,
                    Sec = 0
                });
                return false;
            }
        }
        public bool EstudianteEstaMatricualdo(ADMatriculasET pParam, SqlConnection con)
        {
            try
            {

                //string sql = string.Format("SELECT * FROM ADMatriculas MT WHERE  MT.IdEstudiante = {0} AND MT.IdAñoLectivo = {1}",
                //                    pParam.IdEstudiante, pParam.IdAñoLectivo);
                ParametersSpETList ListParam = new ParametersSpETList()
                {
                    new ParametersSpET(){ NombreParametro = "@IdEstudiante", Valor = pParam.IdEstudiante },
                    new ParametersSpET(){ NombreParametro = "@IdAñoLectivo", Valor = pParam.IdAñoLectivo }
                };
                DataTable dt = ClFunciones.ConsultarConSP("SP_EstudianteMatriculado_G", ListParam, ClConexion.clConexion.Conexion);
                if (dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                    //sql = string.Format("SET DATEFORMAT DMY; " +
                    //" SELECT* FROM ADMatriculas MT" +
                    //" INNER JOIN ADAñoLectivo AL ON MT.IdAñoLectivo = AL.Sec" +
                    //" WHERE MT.IdEstudiante = {0} AND FechaHasta > '{1:dd/MM/yyyy}'", pParam.IdEstudiante, DateTime.Now);
                    //dt = ClFunciones.Consultar(sql, con);
                    //if (dt.Rows.Count > 0)
                    //{
                    //    return true;
                    //}
                    //else
                    //{
                    //    return false;
                    //}
                }
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public bool CancelaMatricula(int idMatricula, SqlConnection con)
        {
            try
            {
                string sql = "UPDATE ADMatriculas SET Cancelada = 1 WHERE Sec = " + idMatricula.ToString();
                string res = ClFunciones.EjecutaComando(sql, con);
                if (res == "si")return true;
                return false;
            }
            catch (Exception ex)
            {
                ClRegistraLogs.RegistrarError(new ET.SGLogs()
                {
                    FechaReg = DateTime.Now,
                    MensajeError = ex.Message,
                    NomArchivo = "Matriculas",
                    NomError = "Exception",
                    NomFunction = "Cancelando matricula",
                    NomModulo = "Matricula",
                    NumLinea = 57,
                    Sec = 0
                });
                return false;
            }
        }


    }
}
