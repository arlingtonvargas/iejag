
GO
-- =============================================
-- Author:		Arlington
-- Create date: 02-06-2019
-- Description:	Inserta observacion por de estudiante.
-- =============================================
-- Author:		Arlington
-- Create date: 16-06-2019
-- Description:	Se agrga el campo [OSIsPositive]
-- =============================================
ALTER PROCEDURE [dbo].[SP_ADObservacionEstudiante_I]
	
	 @pOSIdEstudiente bigint
	,@pOSIdAño int
	,@pOSFechaObservacion datetime
	,@pOSObservacion varchar(max)
	,@pOSMaestroReistra bigint
	,@pOSMaestroReporta bigint
	,@pOSIsPositive bit

AS
BEGIN

	SET NOCOUNT ON;

INSERT INTO [dbo].[ADObservacionEstudiante]
    ([OSIdEstudiente]
    ,[OSIdAño]
    ,[OSFechaObservacion]
    ,[OSObservacion]
    ,[OSFechaCrea]
    ,[OSMaestroReistra]
    ,[OSMaestroReporta]
    ,[OSIsPositive])
VALUES
    (@pOSIdEstudiente
    ,@pOSIdAño
    ,@pOSFechaObservacion
    ,@pOSObservacion
    ,GETDATE()
    ,@pOSMaestroReistra
    ,@pOSMaestroReporta
    ,@pOSIsPositive)
	
END
