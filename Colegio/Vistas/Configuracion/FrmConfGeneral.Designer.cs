﻿namespace Colegio.Vistas.Configuracion
{
    partial class FrmConfGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConfGeneral));
            this.txtNit = new dll.Controles.ucLabelTextBox();
            this.txtNomComp = new dll.Controles.ucLabelTextBox();
            this.txtNomCorto = new dll.Controles.ucLabelTextBox();
            this.cbxPeriodo = new dll.Controles.ucLabelCombo();
            this.btnEditNit = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveNit = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveNom = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditNom = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveNomCorto = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditNomCorto = new DevExpress.XtraEditors.SimpleButton();
            this.btnSavePeriodo = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditPeriodo = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnSaveRepNotas = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditRepNotas = new DevExpress.XtraEditors.SimpleButton();
            this.cbxReporteNotas = new dll.Controles.ucLabelCombo();
            this.txtDocRector = new dll.Controles.ucLabelTextBox();
            this.btnSaveDocRec = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditDocRec = new DevExpress.XtraEditors.SimpleButton();
            this.txtNomRector = new dll.Controles.ucLabelTextBox();
            this.btnSaveNomRec = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditNomRec = new DevExpress.XtraEditors.SimpleButton();
            this.txtCodDANA = new dll.Controles.ucLabelTextBox();
            this.btnSaveCodDANA = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditCodDANA = new DevExpress.XtraEditors.SimpleButton();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnGuardarTodo = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveAño = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditAño = new DevExpress.XtraEditors.SimpleButton();
            this.cbxAñoActual = new dll.Controles.ucLabelCombo();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNit
            // 
            this.txtNit.AnchoTitulo = 120;
            this.txtNit.Location = new System.Drawing.Point(5, 26);
            this.txtNit.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNit.MensajeDeAyuda = null;
            this.txtNit.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNit.Name = "txtNit";
            this.txtNit.PermiteSoloNumeros = false;
            this.txtNit.Size = new System.Drawing.Size(455, 24);
            this.txtNit.TabIndex = 0;
            this.txtNit.TextoTitulo = "Nit :";
            this.txtNit.ValorTextBox = "";
            // 
            // txtNomComp
            // 
            this.txtNomComp.AnchoTitulo = 120;
            this.txtNomComp.Location = new System.Drawing.Point(5, 54);
            this.txtNomComp.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNomComp.MensajeDeAyuda = null;
            this.txtNomComp.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNomComp.Name = "txtNomComp";
            this.txtNomComp.PermiteSoloNumeros = false;
            this.txtNomComp.Size = new System.Drawing.Size(455, 24);
            this.txtNomComp.TabIndex = 1;
            this.txtNomComp.TextoTitulo = "Nombre Completo :";
            this.txtNomComp.ValorTextBox = "";
            // 
            // txtNomCorto
            // 
            this.txtNomCorto.AnchoTitulo = 120;
            this.txtNomCorto.Location = new System.Drawing.Point(5, 83);
            this.txtNomCorto.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNomCorto.MensajeDeAyuda = null;
            this.txtNomCorto.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNomCorto.Name = "txtNomCorto";
            this.txtNomCorto.PermiteSoloNumeros = false;
            this.txtNomCorto.Size = new System.Drawing.Size(455, 24);
            this.txtNomCorto.TabIndex = 2;
            this.txtNomCorto.TextoTitulo = "Nombre Corto :";
            this.txtNomCorto.ValorTextBox = "";
            // 
            // cbxPeriodo
            // 
            this.cbxPeriodo.AnchoTitulo = 120;
            this.cbxPeriodo.Location = new System.Drawing.Point(5, 227);
            this.cbxPeriodo.MaximumSize = new System.Drawing.Size(3000, 28);
            this.cbxPeriodo.MensajeDeAyuda = null;
            this.cbxPeriodo.MinimumSize = new System.Drawing.Size(50, 28);
            this.cbxPeriodo.Name = "cbxPeriodo";
            this.cbxPeriodo.Size = new System.Drawing.Size(457, 28);
            this.cbxPeriodo.TabIndex = 3;
            this.cbxPeriodo.TextoTitulo = "Periodo Actual :";
            // 
            // btnEditNit
            // 
            this.btnEditNit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditNit.ImageOptions.Image")));
            this.btnEditNit.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditNit.Location = new System.Drawing.Point(466, 27);
            this.btnEditNit.Name = "btnEditNit";
            this.btnEditNit.Size = new System.Drawing.Size(34, 21);
            this.btnEditNit.TabIndex = 4;
            this.btnEditNit.Click += new System.EventHandler(this.btnEditNit_Click);
            // 
            // btnSaveNit
            // 
            this.btnSaveNit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveNit.ImageOptions.Image")));
            this.btnSaveNit.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveNit.Location = new System.Drawing.Point(506, 27);
            this.btnSaveNit.Name = "btnSaveNit";
            this.btnSaveNit.Size = new System.Drawing.Size(34, 21);
            this.btnSaveNit.TabIndex = 5;
            this.btnSaveNit.Click += new System.EventHandler(this.btnSaveNit_Click);
            // 
            // btnSaveNom
            // 
            this.btnSaveNom.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveNom.ImageOptions.Image")));
            this.btnSaveNom.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveNom.Location = new System.Drawing.Point(506, 56);
            this.btnSaveNom.Name = "btnSaveNom";
            this.btnSaveNom.Size = new System.Drawing.Size(34, 21);
            this.btnSaveNom.TabIndex = 7;
            this.btnSaveNom.Click += new System.EventHandler(this.btnSaveNom_Click);
            // 
            // btnEditNom
            // 
            this.btnEditNom.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditNom.ImageOptions.Image")));
            this.btnEditNom.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditNom.Location = new System.Drawing.Point(466, 56);
            this.btnEditNom.Name = "btnEditNom";
            this.btnEditNom.Size = new System.Drawing.Size(34, 21);
            this.btnEditNom.TabIndex = 6;
            this.btnEditNom.Click += new System.EventHandler(this.btnEditNom_Click);
            // 
            // btnSaveNomCorto
            // 
            this.btnSaveNomCorto.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveNomCorto.ImageOptions.Image")));
            this.btnSaveNomCorto.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveNomCorto.Location = new System.Drawing.Point(506, 84);
            this.btnSaveNomCorto.Name = "btnSaveNomCorto";
            this.btnSaveNomCorto.Size = new System.Drawing.Size(34, 21);
            this.btnSaveNomCorto.TabIndex = 9;
            this.btnSaveNomCorto.Click += new System.EventHandler(this.btnSaveNomCorto_Click);
            // 
            // btnEditNomCorto
            // 
            this.btnEditNomCorto.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditNomCorto.ImageOptions.Image")));
            this.btnEditNomCorto.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditNomCorto.Location = new System.Drawing.Point(466, 84);
            this.btnEditNomCorto.Name = "btnEditNomCorto";
            this.btnEditNomCorto.Size = new System.Drawing.Size(34, 21);
            this.btnEditNomCorto.TabIndex = 8;
            this.btnEditNomCorto.Click += new System.EventHandler(this.btnEditNomCorto_Click);
            // 
            // btnSavePeriodo
            // 
            this.btnSavePeriodo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSavePeriodo.ImageOptions.Image")));
            this.btnSavePeriodo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSavePeriodo.Location = new System.Drawing.Point(506, 231);
            this.btnSavePeriodo.Name = "btnSavePeriodo";
            this.btnSavePeriodo.Size = new System.Drawing.Size(34, 21);
            this.btnSavePeriodo.TabIndex = 11;
            this.btnSavePeriodo.Click += new System.EventHandler(this.btnSavePeriodo_Click);
            // 
            // btnEditPeriodo
            // 
            this.btnEditPeriodo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditPeriodo.ImageOptions.Image")));
            this.btnEditPeriodo.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditPeriodo.Location = new System.Drawing.Point(466, 231);
            this.btnEditPeriodo.Name = "btnEditPeriodo";
            this.btnEditPeriodo.Size = new System.Drawing.Size(34, 21);
            this.btnEditPeriodo.TabIndex = 10;
            this.btnEditPeriodo.Click += new System.EventHandler(this.btnEditPeriodo_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnSaveRepNotas);
            this.groupControl1.Controls.Add(this.btnEditRepNotas);
            this.groupControl1.Controls.Add(this.cbxReporteNotas);
            this.groupControl1.Controls.Add(this.txtDocRector);
            this.groupControl1.Controls.Add(this.btnSaveDocRec);
            this.groupControl1.Controls.Add(this.btnEditDocRec);
            this.groupControl1.Controls.Add(this.txtNomRector);
            this.groupControl1.Controls.Add(this.btnSaveNomRec);
            this.groupControl1.Controls.Add(this.btnEditNomRec);
            this.groupControl1.Controls.Add(this.txtCodDANA);
            this.groupControl1.Controls.Add(this.btnSaveCodDANA);
            this.groupControl1.Controls.Add(this.btnEditCodDANA);
            this.groupControl1.Controls.Add(this.txtNit);
            this.groupControl1.Controls.Add(this.btnSavePeriodo);
            this.groupControl1.Controls.Add(this.txtNomComp);
            this.groupControl1.Controls.Add(this.btnEditPeriodo);
            this.groupControl1.Controls.Add(this.txtNomCorto);
            this.groupControl1.Controls.Add(this.btnSaveNomCorto);
            this.groupControl1.Controls.Add(this.cbxPeriodo);
            this.groupControl1.Controls.Add(this.btnEditNomCorto);
            this.groupControl1.Controls.Add(this.btnEditNit);
            this.groupControl1.Controls.Add(this.btnSaveNom);
            this.groupControl1.Controls.Add(this.btnSaveNit);
            this.groupControl1.Controls.Add(this.btnEditNom);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(554, 290);
            this.groupControl1.TabIndex = 12;
            this.groupControl1.Text = "Opciones de configuración";
            // 
            // btnSaveRepNotas
            // 
            this.btnSaveRepNotas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveRepNotas.ImageOptions.Image")));
            this.btnSaveRepNotas.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveRepNotas.Location = new System.Drawing.Point(506, 260);
            this.btnSaveRepNotas.Name = "btnSaveRepNotas";
            this.btnSaveRepNotas.Size = new System.Drawing.Size(34, 21);
            this.btnSaveRepNotas.TabIndex = 23;
            this.btnSaveRepNotas.Click += new System.EventHandler(this.btnSaveRepNotas_Click);
            // 
            // btnEditRepNotas
            // 
            this.btnEditRepNotas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditRepNotas.ImageOptions.Image")));
            this.btnEditRepNotas.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditRepNotas.Location = new System.Drawing.Point(466, 260);
            this.btnEditRepNotas.Name = "btnEditRepNotas";
            this.btnEditRepNotas.Size = new System.Drawing.Size(34, 21);
            this.btnEditRepNotas.TabIndex = 22;
            this.btnEditRepNotas.Click += new System.EventHandler(this.btnEditRepNotas_Click);
            // 
            // cbxReporteNotas
            // 
            this.cbxReporteNotas.AnchoTitulo = 120;
            this.cbxReporteNotas.Location = new System.Drawing.Point(5, 256);
            this.cbxReporteNotas.MaximumSize = new System.Drawing.Size(3000, 28);
            this.cbxReporteNotas.MensajeDeAyuda = null;
            this.cbxReporteNotas.MinimumSize = new System.Drawing.Size(50, 28);
            this.cbxReporteNotas.Name = "cbxReporteNotas";
            this.cbxReporteNotas.Size = new System.Drawing.Size(457, 28);
            this.cbxReporteNotas.TabIndex = 21;
            this.cbxReporteNotas.TextoTitulo = "Reporte de notas :";
            // 
            // txtDocRector
            // 
            this.txtDocRector.AnchoTitulo = 120;
            this.txtDocRector.Location = new System.Drawing.Point(5, 171);
            this.txtDocRector.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDocRector.MensajeDeAyuda = null;
            this.txtDocRector.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDocRector.Name = "txtDocRector";
            this.txtDocRector.PermiteSoloNumeros = false;
            this.txtDocRector.Size = new System.Drawing.Size(455, 24);
            this.txtDocRector.TabIndex = 18;
            this.txtDocRector.TextoTitulo = "Doc Rector :";
            this.txtDocRector.ValorTextBox = "";
            // 
            // btnSaveDocRec
            // 
            this.btnSaveDocRec.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveDocRec.ImageOptions.Image")));
            this.btnSaveDocRec.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveDocRec.Location = new System.Drawing.Point(506, 172);
            this.btnSaveDocRec.Name = "btnSaveDocRec";
            this.btnSaveDocRec.Size = new System.Drawing.Size(34, 21);
            this.btnSaveDocRec.TabIndex = 20;
            this.btnSaveDocRec.Click += new System.EventHandler(this.btnSaveDocRec_Click);
            // 
            // btnEditDocRec
            // 
            this.btnEditDocRec.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditDocRec.ImageOptions.Image")));
            this.btnEditDocRec.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditDocRec.Location = new System.Drawing.Point(466, 172);
            this.btnEditDocRec.Name = "btnEditDocRec";
            this.btnEditDocRec.Size = new System.Drawing.Size(34, 21);
            this.btnEditDocRec.TabIndex = 19;
            this.btnEditDocRec.Click += new System.EventHandler(this.btnEditDocRec_Click);
            // 
            // txtNomRector
            // 
            this.txtNomRector.AnchoTitulo = 120;
            this.txtNomRector.Location = new System.Drawing.Point(5, 141);
            this.txtNomRector.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtNomRector.MensajeDeAyuda = null;
            this.txtNomRector.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtNomRector.Name = "txtNomRector";
            this.txtNomRector.PermiteSoloNumeros = false;
            this.txtNomRector.Size = new System.Drawing.Size(455, 24);
            this.txtNomRector.TabIndex = 15;
            this.txtNomRector.TextoTitulo = "Nombre Rector :";
            this.txtNomRector.ValorTextBox = "";
            // 
            // btnSaveNomRec
            // 
            this.btnSaveNomRec.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveNomRec.ImageOptions.Image")));
            this.btnSaveNomRec.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveNomRec.Location = new System.Drawing.Point(506, 142);
            this.btnSaveNomRec.Name = "btnSaveNomRec";
            this.btnSaveNomRec.Size = new System.Drawing.Size(34, 21);
            this.btnSaveNomRec.TabIndex = 17;
            this.btnSaveNomRec.Click += new System.EventHandler(this.btnSaveNomRec_Click);
            // 
            // btnEditNomRec
            // 
            this.btnEditNomRec.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditNomRec.ImageOptions.Image")));
            this.btnEditNomRec.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditNomRec.Location = new System.Drawing.Point(466, 142);
            this.btnEditNomRec.Name = "btnEditNomRec";
            this.btnEditNomRec.Size = new System.Drawing.Size(34, 21);
            this.btnEditNomRec.TabIndex = 16;
            this.btnEditNomRec.Click += new System.EventHandler(this.btnEditNomRec_Click);
            // 
            // txtCodDANA
            // 
            this.txtCodDANA.AnchoTitulo = 120;
            this.txtCodDANA.Location = new System.Drawing.Point(5, 111);
            this.txtCodDANA.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCodDANA.MensajeDeAyuda = null;
            this.txtCodDANA.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCodDANA.Name = "txtCodDANA";
            this.txtCodDANA.PermiteSoloNumeros = false;
            this.txtCodDANA.Size = new System.Drawing.Size(455, 24);
            this.txtCodDANA.TabIndex = 12;
            this.txtCodDANA.TextoTitulo = "Código DANA :";
            this.txtCodDANA.ValorTextBox = "";
            // 
            // btnSaveCodDANA
            // 
            this.btnSaveCodDANA.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveCodDANA.ImageOptions.Image")));
            this.btnSaveCodDANA.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveCodDANA.Location = new System.Drawing.Point(506, 112);
            this.btnSaveCodDANA.Name = "btnSaveCodDANA";
            this.btnSaveCodDANA.Size = new System.Drawing.Size(34, 21);
            this.btnSaveCodDANA.TabIndex = 14;
            this.btnSaveCodDANA.Click += new System.EventHandler(this.btnSaveCodDANA_Click);
            // 
            // btnEditCodDANA
            // 
            this.btnEditCodDANA.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditCodDANA.ImageOptions.Image")));
            this.btnEditCodDANA.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditCodDANA.Location = new System.Drawing.Point(466, 112);
            this.btnEditCodDANA.Name = "btnEditCodDANA";
            this.btnEditCodDANA.Size = new System.Drawing.Size(34, 21);
            this.btnEditCodDANA.TabIndex = 13;
            this.btnEditCodDANA.Click += new System.EventHandler(this.btnEditCodDANA_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(478, 314);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(88, 26);
            this.btnSalir.TabIndex = 12;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardarTodo
            // 
            this.btnGuardarTodo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarTodo.ImageOptions.Image")));
            this.btnGuardarTodo.Location = new System.Drawing.Point(377, 314);
            this.btnGuardarTodo.Name = "btnGuardarTodo";
            this.btnGuardarTodo.Size = new System.Drawing.Size(95, 26);
            this.btnGuardarTodo.TabIndex = 13;
            this.btnGuardarTodo.Text = "Guardar todo";
            this.btnGuardarTodo.Click += new System.EventHandler(this.btnGuardarTodo_Click);
            // 
            // btnSaveAño
            // 
            this.btnSaveAño.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveAño.ImageOptions.Image")));
            this.btnSaveAño.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnSaveAño.Location = new System.Drawing.Point(518, 214);
            this.btnSaveAño.Name = "btnSaveAño";
            this.btnSaveAño.Size = new System.Drawing.Size(34, 21);
            this.btnSaveAño.TabIndex = 16;
            this.btnSaveAño.Click += new System.EventHandler(this.btnSaveAño_Click);
            // 
            // btnEditAño
            // 
            this.btnEditAño.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditAño.ImageOptions.Image")));
            this.btnEditAño.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnEditAño.Location = new System.Drawing.Point(478, 214);
            this.btnEditAño.Name = "btnEditAño";
            this.btnEditAño.Size = new System.Drawing.Size(34, 21);
            this.btnEditAño.TabIndex = 15;
            this.btnEditAño.Click += new System.EventHandler(this.btnEditAño_Click);
            // 
            // cbxAñoActual
            // 
            this.cbxAñoActual.AnchoTitulo = 120;
            this.cbxAñoActual.Location = new System.Drawing.Point(17, 210);
            this.cbxAñoActual.MaximumSize = new System.Drawing.Size(3000, 28);
            this.cbxAñoActual.MensajeDeAyuda = null;
            this.cbxAñoActual.MinimumSize = new System.Drawing.Size(50, 28);
            this.cbxAñoActual.Name = "cbxAñoActual";
            this.cbxAñoActual.Size = new System.Drawing.Size(457, 28);
            this.cbxAñoActual.TabIndex = 14;
            this.cbxAñoActual.TextoTitulo = "Año Lectivo :";
            // 
            // FrmConfGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 352);
            this.Controls.Add(this.btnSaveAño);
            this.Controls.Add(this.btnEditAño);
            this.Controls.Add(this.cbxAñoActual);
            this.Controls.Add(this.btnGuardarTodo);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.groupControl1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConfGeneral";
            this.Text = "Configuración General";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmConfGeneral_FormClosed);
            this.Load += new System.EventHandler(this.FrmConfGeneral_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private dll.Controles.ucLabelTextBox txtNit;
        private dll.Controles.ucLabelTextBox txtNomComp;
        private dll.Controles.ucLabelTextBox txtNomCorto;
        private dll.Controles.ucLabelCombo cbxPeriodo;
        private DevExpress.XtraEditors.SimpleButton btnEditNit;
        private DevExpress.XtraEditors.SimpleButton btnSaveNit;
        private DevExpress.XtraEditors.SimpleButton btnSaveNom;
        private DevExpress.XtraEditors.SimpleButton btnEditNom;
        private DevExpress.XtraEditors.SimpleButton btnSaveNomCorto;
        private DevExpress.XtraEditors.SimpleButton btnEditNomCorto;
        private DevExpress.XtraEditors.SimpleButton btnSavePeriodo;
        private DevExpress.XtraEditors.SimpleButton btnEditPeriodo;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.SimpleButton btnGuardarTodo;
        private dll.Controles.ucLabelTextBox txtCodDANA;
        private DevExpress.XtraEditors.SimpleButton btnSaveCodDANA;
        private DevExpress.XtraEditors.SimpleButton btnEditCodDANA;
        private dll.Controles.ucLabelTextBox txtDocRector;
        private DevExpress.XtraEditors.SimpleButton btnSaveDocRec;
        private DevExpress.XtraEditors.SimpleButton btnEditDocRec;
        private dll.Controles.ucLabelTextBox txtNomRector;
        private DevExpress.XtraEditors.SimpleButton btnSaveNomRec;
        private DevExpress.XtraEditors.SimpleButton btnEditNomRec;
        private DevExpress.XtraEditors.SimpleButton btnSaveAño;
        private DevExpress.XtraEditors.SimpleButton btnEditAño;
        private dll.Controles.ucLabelCombo cbxAñoActual;
        private DevExpress.XtraEditors.SimpleButton btnSaveRepNotas;
        private DevExpress.XtraEditors.SimpleButton btnEditRepNotas;
        private dll.Controles.ucLabelCombo cbxReporteNotas;
    }
}