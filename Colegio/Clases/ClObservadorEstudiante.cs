﻿using Colegio.ET;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.Clases
{
    public class ClObservadorEstudiante
    {
        public ADObservacionEstudianteList GetObservacionEstudiante(long pIdEstudiante)
        {
            try
            {
                ADObservacionEstudianteList wADObservacionEstudianteList = new ADObservacionEstudianteList();
                ADObservacionEstudianteET wADObservacionEstudianteET = null;
                using (SqlConnection cnn = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "[SP_ADObservacionEstudiante_G]";
                        cmd.Parameters.AddWithValue("@pIdEstudiante", pIdEstudiante);
                        cnn.Open();
                        SqlDataReader rd = cmd.ExecuteReader();
                        while (rd.Read())
                        {
                            wADObservacionEstudianteET = new ADObservacionEstudianteET();
                            wADObservacionEstudianteET.OSId = Convert.ToInt32(rd["OSId"]);
                            wADObservacionEstudianteET.OSIdEstudiente  = Convert.ToUInt32(rd["OSIdEstudiente"]);
                            wADObservacionEstudianteET.OSNomEstudiente = Convert.ToString(rd["OSNomEstudiente"]);
                            wADObservacionEstudianteET.OSIdAño = Convert.ToInt32(rd["OSIdAño"]);
                            wADObservacionEstudianteET.OSDesAño = Convert.ToString(rd["OSDesAño"]);
                            wADObservacionEstudianteET.OSFechaObservacion  = Convert.ToDateTime(rd["OSFechaObservacion"]);
                            wADObservacionEstudianteET.OSObservacion  = Convert.ToString(rd["OSObservacion"]);
                            wADObservacionEstudianteET.OSFechaCrea = Convert.ToDateTime(rd["OSFechaCrea"]);
                            wADObservacionEstudianteET.OSMaestroReistra = Convert.ToUInt32(rd["OSMaestroReistra"]);
                            wADObservacionEstudianteET.OSNomMaestroReistra = Convert.ToString(rd["OSNomMaestroReistra"]);
                            wADObservacionEstudianteET.OSMaestroReporta = Convert.ToUInt32(rd["OSMaestroReporta"]);
                            wADObservacionEstudianteET.OSNomMaestroReporta = Convert.ToString(rd["OSNomMaestroReporta"]);
                            wADObservacionEstudianteET.OSIsPositive = Convert.ToBoolean(rd["OSIsPositive"]);
                            wADObservacionEstudianteList.Add(wADObservacionEstudianteET);
                        }
                    }
                    return wADObservacionEstudianteList;
                }
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
                return null;
            }
        }

        public ADObservacionEstudianteET InsertObservacion(ADObservacionEstudianteET pADObservacionEstudiante)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "[SP_ADObservacionEstudiante_I]";

                        cmd.Parameters.AddWithValue("@pOSIdEstudiente", pADObservacionEstudiante.OSIdEstudiente);
                        cmd.Parameters.AddWithValue("@pOSIdAño", pADObservacionEstudiante.OSIdAño);
                        cmd.Parameters.AddWithValue("@pOSFechaObservacion", pADObservacionEstudiante.OSFechaObservacion);
                        cmd.Parameters.AddWithValue("@pOSObservacion", pADObservacionEstudiante.OSObservacion);
                        cmd.Parameters.AddWithValue("@pOSMaestroReistra", pADObservacionEstudiante.OSMaestroReistra);
                        cmd.Parameters.AddWithValue("@pOSMaestroReporta", pADObservacionEstudiante.OSMaestroReporta);
                        cmd.Parameters.AddWithValue("@pOSIsPositive", pADObservacionEstudiante.OSIsPositive);
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                        return pADObservacionEstudiante;
                    }
                }                
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
                return null;
            }
        }



    }
}
