﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using dll.Common;

namespace Colegio.Vistas
{
    public partial class FrmConfirmacion : FrmBase
    {
        public bool Confirmo { get; set; }
        public FrmConfirmacion()
        {
            InitializeComponent();
        }

        private void FrmConfirmacion_Load(object sender, EventArgs e)
        {

        }

        private void ucBtnAceptarAV1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            this.Confirmo = true;
            this.Close();
        }

        private void ucBtnCancelarAV1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            this.Confirmo = false;
            this.Close();
        }
    }
}