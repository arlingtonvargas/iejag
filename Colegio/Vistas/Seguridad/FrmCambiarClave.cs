﻿using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;

namespace Colegio.Vistas.Seguridad
{
    public partial class FrmCambiarClave : FrmBase
    {
        ET.SGUsuariosET usuarioActual = new ET.SGUsuariosET();
        ClUsuarios clUsuario = new ClUsuarios();
        public FrmCambiarClave(ET.SGUsuariosET _usuarioActual)
        {
            InitializeComponent();
            usuarioActual = _usuarioActual;
        }

        private void lblCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmCambiarClave_Load(object sender, EventArgs e)
        {
            txtIdUsu.ValorTextBox = usuarioActual.IdUsuario.ToString();
            txtUsu.ValorTextBox = usuarioActual.NomUsu;
            txtPswActual.txtCampo.Properties.PasswordChar = '*';
            txtPswNew.txtCampo.Properties.PasswordChar = '*';
            txtPswNewConfirm.txtCampo.Properties.PasswordChar = '*';
            txtPswNew.txtCampo.Properties.MaxLength = 40;
            txtPswNewConfirm.txtCampo.Properties.MaxLength = 40;
        }

        private void btnGuardar_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            FrmMensaje frm = new FrmMensaje();
            try
            {

                if (txtPswActual.ValorTextBox == "")
                {
                    frm.lblMensaje.Text = "Debe digitar la contraseña actual.";
                    frm.ShowDialog();
                    txtPswActual.Focus();
                }
                else if (txtPswNew.ValorTextBox == "")
                {
                    frm.lblMensaje.Text = "Debe digitar la contraseña nueva.";
                    frm.ShowDialog();
                    txtPswNew.Focus();
                }
                else if (txtPswNewConfirm.ValorTextBox == "")
                {
                    frm.lblMensaje.Text = "Debe digitar la confirmación de contraseña nueva.";
                    frm.ShowDialog();
                    txtPswNewConfirm.Focus();
                }
                else if (ClFunciones.Encrypt(txtPswActual.ValorTextBox) != usuarioActual.password)
                {
                    frm.lblMensaje.Text = "La contraseña actual no coincide con la digitada.";
                    frm.ShowDialog();
                    txtPswActual.Focus();
                }
                else if (txtPswNew.ValorTextBox != txtPswNewConfirm.ValorTextBox)
                {
                    frm.lblMensaje.Text = "Las contraseñas nuevas no coincide.";
                    frm.ShowDialog();
                    txtPswActual.Focus();
                } else
                {
                    ET.SGUsuariosET pParam = new ET.SGUsuariosET();
                    pParam.password = ClFunciones.Encrypt(txtPswNew.ValorTextBox);
                    pParam.IdUsuario = ClFunciones.UsuarioIngreso.IdUsuario;
                    bool res = clUsuario.UpdatePswUsuario(pParam, ClConexion.clConexion.Conexion);
                    FrmMensaje f = new FrmMensaje();
                    if (res)
                    {
                        f.lblMensaje.Text = "Contraseña actualizada de manera correcta.";
                        f.ShowDialog();
                        ClFunciones.UsuarioIngreso.password = txtPswNew.ValorTextBox;
                        this.Close();
                    }
                    else
                    {
                        f.lblMensaje.Text = "Lo sentimos, ha ocurrido un error.";
                        f.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void btnCancelar_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
