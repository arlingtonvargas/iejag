USE [BDIEJAG]
GO
/****** Object:  StoredProcedure [dbo].[SP_EstudiantesCurso_G]    Script Date: 08/12/2018 11:30:28 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 24-10-2017
-- Description:	Consulta los estudiantes de un curso con la posicion de acuerdo a sus notas
-- =============================================
ALTER PROCEDURE [dbo].[SP_EstudiantesCurso_G]
	@pIdCurso INT,
	@pIdAñoLectivo INT,
	@pIdPeriodo VARCHAR(1)
AS
BEGIN
	DECLARE 
	@IdCurso INT = @pIdCurso,
	@IdAñoLectivo INT = @pIdAñoLectivo,
	@IdPeriodo VARCHAR(1) = @pIdPeriodo,
	@Sql NVARCHAR(MAX)
	SET @Sql = '
		SET NOCOUNT ON;
		SELECT ROW_NUMBER() OVER (ORDER BY Z.Promedio DESC) AS Posicion  
		, Z.Documento, Z.NomCompleto, Z.Curso, Z.Sede, Z.Jornada, Z.Promedio
		FROM(SELECT ET.IdTercero AS Documento, CONCAT(ET.Papellido,'' '',ET.Sapellido,'' '',ET.Pnombre,'' '',ET.Snombre) AS NomCompleto ,
		CS.NomCurso AS Curso, SD.NomSede AS Sede, JD.NomJornada AS Jornada
		,(SELECT AVG(ISNULL(P' + @IdPeriodo + ', 0)) AS Prom 
			FROM ADNotas NT 
			INNER JOIN ADMatriculas MT ON NT.IdMatricula = MT.Sec 
			INNER JOIN ADMateriasCursos MC ON MT.IdCurso = MC.IdCurso			
			INNER JOIN ADMaterias MR ON MC.IdMateria = MR.Sec
			WHERE MT.IdEstudiante = ET.IdTercero) AS Promedio 
		FROM ADTerceros ET 
		INNER JOIN ADMatriculas MT ON ET.IdTercero = MT.IdEstudiante 
		INNER JOIN ADSedes SD ON MT.IdSede = SD.Sec
		INNER JOIN ADJornadas JD ON MT.IdJornada = JD.Sec
		INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
		WHERE MT.IdCurso = @IdCurso AND MT.IdAñoLectivo = @IdAñoLectivo AND MT.Cancelada = 0) AS Z
	'
	EXECUTE sp_executesql @Sql, N'@IdCurso INT, @IdAñoLectivo INT', @IdCurso = @IdCurso, @IdAñoLectivo = @IdAñoLectivo

	END
