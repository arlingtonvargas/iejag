﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colegio.ET
{
    public class ADDatosInstitucionET
    {
        public int Sec { get; set; }
        public string Nit { get; set; }
        public string NombreCompleto { get; set; }
        public string NombreCorto { get; set; }
        public int PeriodoActual { get; set; }
        public string CodDANA { get; set; }
        public string NombreRector { get; set; }
        public long DocRector { get; set; }
        public int IdAñoActual { get; set; }
        public bool ReportarNotas { get; set; }

    }
}
