﻿namespace Colegio.Vistas
{
    partial class FrmNuevoMaestro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmNuevoMaestro));
            this.lblTitulo = new System.Windows.Forms.Label();
            this.dtpFecNacMaestro = new DevExpress.XtraEditors.DateEdit();
            this.grcInfo = new DevExpress.XtraEditors.GroupControl();
            this.txtDir = new dll.Controles.ucLabelTextBox();
            this.txtPape = new dll.Controles.ucLabelTextBox();
            this.txtUsu = new dll.Controles.ucLabelTextBox();
            this.cbxTipoDoc = new DevExpress.XtraEditors.LookUpEdit();
            this.lblTipoDoc = new dll.Controles.ucLabelAV();
            this.cbxRoles = new DevExpress.XtraEditors.LookUpEdit();
            this.txtPnom = new dll.Controles.ucLabelTextBox();
            this.txtSape = new dll.Controles.ucLabelTextBox();
            this.cbxSexoMaestro = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtTel = new dll.Controles.ucLabelTextBox();
            this.lblSexMaestro = new dll.Controles.ucLabelAV();
            this.txtSnom = new dll.Controles.ucLabelTextBox();
            this.txtDocumento = new dll.Controles.ucLabelTextBox();
            this.grcNacMaestro = new DevExpress.XtraEditors.GroupControl();
            this.cbxMuni = new DevExpress.XtraEditors.LookUpEdit();
            this.cbxPais = new DevExpress.XtraEditors.LookUpEdit();
            this.cbxDpto = new DevExpress.XtraEditors.LookUpEdit();
            this.lblMuni = new dll.Controles.ucLabelAV();
            this.lblDpto = new dll.Controles.ucLabelAV();
            this.lblFecNacMaestro = new dll.Controles.ucLabelAV();
            this.lblPais = new dll.Controles.ucLabelAV();
            this.lblRolMaestro = new dll.Controles.ucLabelAV();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancelar = new dll.Controles.ucBtnCancelarAV();
            this.btnGuardar = new dll.Controles.ucBtnGuardarAV();
            this.ucBtnAceptarAV1 = new dll.Controles.ucBtnAceptarAV();
            this.txtCorreo = new dll.Controles.ucLabelTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNacMaestro.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNacMaestro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).BeginInit();
            this.grcInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxTipoDoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxRoles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSexoMaestro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcNacMaestro)).BeginInit();
            this.grcNacMaestro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbxMuni.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPais.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDpto.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(115)))), ((int)(((byte)(199)))));
            this.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lblTitulo.Size = new System.Drawing.Size(596, 32);
            this.lblTitulo.TabIndex = 17;
            this.lblTitulo.Text = "Maestro";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpFecNacMaestro
            // 
            this.dtpFecNacMaestro.EditValue = null;
            this.dtpFecNacMaestro.Location = new System.Drawing.Point(127, 29);
            this.dtpFecNacMaestro.Name = "dtpFecNacMaestro";
            this.dtpFecNacMaestro.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFecNacMaestro.Properties.Appearance.Options.UseFont = true;
            this.dtpFecNacMaestro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecNacMaestro.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpFecNacMaestro.Size = new System.Drawing.Size(152, 22);
            this.dtpFecNacMaestro.TabIndex = 1;
            this.dtpFecNacMaestro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxRol_KeyPress);
            // 
            // grcInfo
            // 
            this.grcInfo.Controls.Add(this.txtCorreo);
            this.grcInfo.Controls.Add(this.txtDir);
            this.grcInfo.Controls.Add(this.txtPape);
            this.grcInfo.Controls.Add(this.txtUsu);
            this.grcInfo.Controls.Add(this.cbxTipoDoc);
            this.grcInfo.Controls.Add(this.lblTipoDoc);
            this.grcInfo.Controls.Add(this.cbxRoles);
            this.grcInfo.Controls.Add(this.txtPnom);
            this.grcInfo.Controls.Add(this.txtSape);
            this.grcInfo.Controls.Add(this.cbxSexoMaestro);
            this.grcInfo.Controls.Add(this.txtTel);
            this.grcInfo.Controls.Add(this.lblSexMaestro);
            this.grcInfo.Controls.Add(this.txtSnom);
            this.grcInfo.Controls.Add(this.txtDocumento);
            this.grcInfo.Controls.Add(this.grcNacMaestro);
            this.grcInfo.Controls.Add(this.lblRolMaestro);
            this.grcInfo.Location = new System.Drawing.Point(21, 49);
            this.grcInfo.Name = "grcInfo";
            this.grcInfo.Size = new System.Drawing.Size(554, 315);
            this.grcInfo.TabIndex = 1;
            this.grcInfo.Text = "Información básica del maestro";
            // 
            // txtDir
            // 
            this.txtDir.AnchoTitulo = 114;
            this.txtDir.Location = new System.Drawing.Point(4, 191);
            this.txtDir.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDir.MensajeDeAyuda = null;
            this.txtDir.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDir.Name = "txtDir";
            this.txtDir.PermiteSoloNumeros = false;
            this.txtDir.Size = new System.Drawing.Size(545, 24);
            this.txtDir.TabIndex = 12;
            this.txtDir.TextoTitulo = "Dirección :";
            this.txtDir.ValorTextBox = "";
            // 
            // txtPape
            // 
            this.txtPape.AnchoTitulo = 114;
            this.txtPape.Location = new System.Drawing.Point(4, 80);
            this.txtPape.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPape.MensajeDeAyuda = null;
            this.txtPape.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPape.Name = "txtPape";
            this.txtPape.PermiteSoloNumeros = false;
            this.txtPape.Size = new System.Drawing.Size(272, 24);
            this.txtPape.TabIndex = 5;
            this.txtPape.TextoTitulo = "Primer Apellido :";
            this.txtPape.ValorTextBox = "";
            this.txtPape.Leave += new System.EventHandler(this.txtPape_Leave);
            // 
            // txtUsu
            // 
            this.txtUsu.AnchoTitulo = 114;
            this.txtUsu.Enabled = false;
            this.txtUsu.Location = new System.Drawing.Point(4, 134);
            this.txtUsu.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtUsu.MensajeDeAyuda = null;
            this.txtUsu.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtUsu.Name = "txtUsu";
            this.txtUsu.PermiteSoloNumeros = false;
            this.txtUsu.Size = new System.Drawing.Size(272, 24);
            this.txtUsu.TabIndex = 9;
            this.txtUsu.TextoTitulo = "Usuario :";
            this.txtUsu.ValorTextBox = "";
            // 
            // cbxTipoDoc
            // 
            this.cbxTipoDoc.Location = new System.Drawing.Point(122, 28);
            this.cbxTipoDoc.Name = "cbxTipoDoc";
            this.cbxTipoDoc.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxTipoDoc.Properties.Appearance.Options.UseFont = true;
            this.cbxTipoDoc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxTipoDoc.Properties.NullText = "Seleccione";
            this.cbxTipoDoc.Properties.NullValuePrompt = "Seleccione";
            this.cbxTipoDoc.Size = new System.Drawing.Size(152, 22);
            this.cbxTipoDoc.TabIndex = 1;
            this.cbxTipoDoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxRol_KeyPress);
            // 
            // lblTipoDoc
            // 
            this.lblTipoDoc.Appearance.Options.UseTextOptions = true;
            this.lblTipoDoc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblTipoDoc.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblTipoDoc.Location = new System.Drawing.Point(5, 28);
            this.lblTipoDoc.Name = "lblTipoDoc";
            this.lblTipoDoc.Size = new System.Drawing.Size(111, 22);
            this.lblTipoDoc.TabIndex = 63;
            this.lblTipoDoc.Text = "Tipo doc :";
            // 
            // cbxRoles
            // 
            this.cbxRoles.Location = new System.Drawing.Point(394, 135);
            this.cbxRoles.Name = "cbxRoles";
            this.cbxRoles.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxRoles.Properties.Appearance.Options.UseFont = true;
            this.cbxRoles.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxRoles.Properties.NullText = "Seleccione";
            this.cbxRoles.Properties.NullValuePrompt = "Seleccione";
            this.cbxRoles.Size = new System.Drawing.Size(154, 22);
            this.cbxRoles.TabIndex = 10;
            this.cbxRoles.Enter += new System.EventHandler(this.cbxRol_Enter);
            this.cbxRoles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxRol_KeyPress);
            // 
            // txtPnom
            // 
            this.txtPnom.AnchoTitulo = 114;
            this.txtPnom.Location = new System.Drawing.Point(4, 53);
            this.txtPnom.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtPnom.MensajeDeAyuda = null;
            this.txtPnom.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtPnom.Name = "txtPnom";
            this.txtPnom.PermiteSoloNumeros = false;
            this.txtPnom.Size = new System.Drawing.Size(272, 24);
            this.txtPnom.TabIndex = 3;
            this.txtPnom.TextoTitulo = "Primer Nombre :";
            this.txtPnom.ValorTextBox = "";
            this.txtPnom.Leave += new System.EventHandler(this.txtPnom_Leave);
            // 
            // txtSape
            // 
            this.txtSape.AnchoTitulo = 114;
            this.txtSape.Location = new System.Drawing.Point(277, 80);
            this.txtSape.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtSape.MensajeDeAyuda = null;
            this.txtSape.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtSape.Name = "txtSape";
            this.txtSape.PermiteSoloNumeros = false;
            this.txtSape.Size = new System.Drawing.Size(272, 24);
            this.txtSape.TabIndex = 6;
            this.txtSape.TextoTitulo = "Segundo Apellido :";
            this.txtSape.ValorTextBox = "";
            // 
            // cbxSexoMaestro
            // 
            this.cbxSexoMaestro.Location = new System.Drawing.Point(122, 108);
            this.cbxSexoMaestro.Name = "cbxSexoMaestro";
            this.cbxSexoMaestro.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxSexoMaestro.Properties.Appearance.Options.UseFont = true;
            this.cbxSexoMaestro.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxSexoMaestro.Properties.DropDownRows = 3;
            this.cbxSexoMaestro.Properties.Items.AddRange(new object[] {
            "Seleccione",
            "Masculino\t",
            "Femenino"});
            this.cbxSexoMaestro.Properties.NullText = "Seleccione";
            this.cbxSexoMaestro.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxSexoMaestro.Size = new System.Drawing.Size(152, 22);
            this.cbxSexoMaestro.TabIndex = 7;
            this.cbxSexoMaestro.Enter += new System.EventHandler(this.cbxSexoMaestro_Enter);
            this.cbxSexoMaestro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxRol_KeyPress);
            // 
            // txtTel
            // 
            this.txtTel.AnchoTitulo = 114;
            this.txtTel.Location = new System.Drawing.Point(277, 107);
            this.txtTel.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtTel.MensajeDeAyuda = null;
            this.txtTel.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtTel.Name = "txtTel";
            this.txtTel.PermiteSoloNumeros = true;
            this.txtTel.Size = new System.Drawing.Size(272, 24);
            this.txtTel.TabIndex = 8;
            this.txtTel.TextoTitulo = "Telefono :";
            this.txtTel.ValorTextBox = "";
            // 
            // lblSexMaestro
            // 
            this.lblSexMaestro.Appearance.Options.UseTextOptions = true;
            this.lblSexMaestro.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblSexMaestro.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblSexMaestro.Location = new System.Drawing.Point(16, 107);
            this.lblSexMaestro.Name = "lblSexMaestro";
            this.lblSexMaestro.Size = new System.Drawing.Size(100, 23);
            this.lblSexMaestro.TabIndex = 59;
            this.lblSexMaestro.Text = "Sexo :";
            // 
            // txtSnom
            // 
            this.txtSnom.AnchoTitulo = 114;
            this.txtSnom.Location = new System.Drawing.Point(277, 53);
            this.txtSnom.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtSnom.MensajeDeAyuda = null;
            this.txtSnom.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtSnom.Name = "txtSnom";
            this.txtSnom.PermiteSoloNumeros = false;
            this.txtSnom.Size = new System.Drawing.Size(272, 24);
            this.txtSnom.TabIndex = 4;
            this.txtSnom.TextoTitulo = "Segundo Nombre :";
            this.txtSnom.ValorTextBox = "";
            // 
            // txtDocumento
            // 
            this.txtDocumento.AnchoTitulo = 114;
            this.txtDocumento.Location = new System.Drawing.Point(277, 26);
            this.txtDocumento.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtDocumento.MensajeDeAyuda = null;
            this.txtDocumento.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.PermiteSoloNumeros = true;
            this.txtDocumento.Size = new System.Drawing.Size(272, 24);
            this.txtDocumento.TabIndex = 2;
            this.txtDocumento.TextoTitulo = "Documento :";
            this.txtDocumento.ValorTextBox = "";
            // 
            // grcNacMaestro
            // 
            this.grcNacMaestro.Controls.Add(this.cbxMuni);
            this.grcNacMaestro.Controls.Add(this.cbxPais);
            this.grcNacMaestro.Controls.Add(this.cbxDpto);
            this.grcNacMaestro.Controls.Add(this.lblMuni);
            this.grcNacMaestro.Controls.Add(this.lblDpto);
            this.grcNacMaestro.Controls.Add(this.lblFecNacMaestro);
            this.grcNacMaestro.Controls.Add(this.lblPais);
            this.grcNacMaestro.Controls.Add(this.dtpFecNacMaestro);
            this.grcNacMaestro.Location = new System.Drawing.Point(1, 225);
            this.grcNacMaestro.Name = "grcNacMaestro";
            this.grcNacMaestro.Size = new System.Drawing.Size(552, 89);
            this.grcNacMaestro.TabIndex = 13;
            this.grcNacMaestro.Text = "Lugar y fecha de nacimiento";
            // 
            // cbxMuni
            // 
            this.cbxMuni.Location = new System.Drawing.Point(395, 56);
            this.cbxMuni.Name = "cbxMuni";
            this.cbxMuni.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxMuni.Properties.Appearance.Options.UseFont = true;
            this.cbxMuni.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxMuni.Properties.NullText = "Seleccione";
            this.cbxMuni.Properties.NullValuePrompt = "Seleccione";
            this.cbxMuni.Size = new System.Drawing.Size(152, 22);
            this.cbxMuni.TabIndex = 4;
            this.cbxMuni.Enter += new System.EventHandler(this.cbxRol_Enter);
            this.cbxMuni.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxRol_KeyPress);
            this.cbxMuni.Leave += new System.EventHandler(this.cbxMuni_Leave);
            // 
            // cbxPais
            // 
            this.cbxPais.Location = new System.Drawing.Point(395, 29);
            this.cbxPais.Name = "cbxPais";
            this.cbxPais.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxPais.Properties.Appearance.Options.UseFont = true;
            this.cbxPais.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxPais.Properties.NullText = "Seleccione";
            this.cbxPais.Properties.NullValuePrompt = "Seleccione";
            this.cbxPais.Size = new System.Drawing.Size(152, 22);
            this.cbxPais.TabIndex = 2;
            this.cbxPais.EditValueChanged += new System.EventHandler(this.cbxPais_EditValueChanged);
            this.cbxPais.Enter += new System.EventHandler(this.cbxRol_Enter);
            this.cbxPais.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxRol_KeyPress);
            // 
            // cbxDpto
            // 
            this.cbxDpto.Location = new System.Drawing.Point(127, 56);
            this.cbxDpto.Name = "cbxDpto";
            this.cbxDpto.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cbxDpto.Properties.Appearance.Options.UseFont = true;
            this.cbxDpto.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxDpto.Properties.NullText = "Seleccione";
            this.cbxDpto.Properties.NullValuePrompt = "Seleccione";
            this.cbxDpto.Size = new System.Drawing.Size(152, 22);
            this.cbxDpto.TabIndex = 3;
            this.cbxDpto.EditValueChanged += new System.EventHandler(this.cbxDpto_EditValueChanged);
            this.cbxDpto.Enter += new System.EventHandler(this.cbxRol_Enter);
            this.cbxDpto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbxRol_KeyPress);
            // 
            // lblMuni
            // 
            this.lblMuni.Appearance.Options.UseTextOptions = true;
            this.lblMuni.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblMuni.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblMuni.Location = new System.Drawing.Point(289, 56);
            this.lblMuni.Name = "lblMuni";
            this.lblMuni.Size = new System.Drawing.Size(100, 23);
            this.lblMuni.TabIndex = 61;
            this.lblMuni.Text = "Municipio :";
            // 
            // lblDpto
            // 
            this.lblDpto.Appearance.Options.UseTextOptions = true;
            this.lblDpto.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblDpto.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDpto.Location = new System.Drawing.Point(21, 56);
            this.lblDpto.Name = "lblDpto";
            this.lblDpto.Size = new System.Drawing.Size(100, 23);
            this.lblDpto.TabIndex = 65;
            this.lblDpto.Text = "Departamento :";
            // 
            // lblFecNacMaestro
            // 
            this.lblFecNacMaestro.Appearance.Options.UseTextOptions = true;
            this.lblFecNacMaestro.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblFecNacMaestro.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblFecNacMaestro.Location = new System.Drawing.Point(21, 29);
            this.lblFecNacMaestro.Name = "lblFecNacMaestro";
            this.lblFecNacMaestro.Size = new System.Drawing.Size(100, 23);
            this.lblFecNacMaestro.TabIndex = 57;
            this.lblFecNacMaestro.Text = "Fecha Nac :";
            // 
            // lblPais
            // 
            this.lblPais.Appearance.Options.UseTextOptions = true;
            this.lblPais.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblPais.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblPais.Location = new System.Drawing.Point(289, 28);
            this.lblPais.Name = "lblPais";
            this.lblPais.Size = new System.Drawing.Size(100, 23);
            this.lblPais.TabIndex = 63;
            this.lblPais.Text = "Pais :";
            // 
            // lblRolMaestro
            // 
            this.lblRolMaestro.Appearance.Options.UseTextOptions = true;
            this.lblRolMaestro.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lblRolMaestro.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblRolMaestro.Location = new System.Drawing.Point(288, 134);
            this.lblRolMaestro.Name = "lblRolMaestro";
            this.lblRolMaestro.Size = new System.Drawing.Size(100, 20);
            this.lblRolMaestro.TabIndex = 58;
            this.lblRolMaestro.Text = "Rol inicial :";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnCancelar);
            this.panel1.Controls.Add(this.btnGuardar);
            this.panel1.Location = new System.Drawing.Point(0, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(596, 422);
            this.panel1.TabIndex = 1;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(449, 347);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(125, 50);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.OnUserControlButtonClicked += new dll.Controles.ucBtnCancelarAV.ButtonClickedEventHandler(this.ucBtnCancelarAV1_OnUserControlButtonClicked);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(319, 347);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(125, 50);
            this.btnGuardar.TabIndex = 2;
            this.btnGuardar.OnUserControlButtonClicked += new dll.Controles.ucBtnGuardarAV.ButtonClickedEventHandler(this.btnGuardar_OnUserControlButtonClicked);

            // 
            // ucBtnAceptarAV1
            // 
            this.ucBtnAceptarAV1.Location = new System.Drawing.Point(191, 275);
            this.ucBtnAceptarAV1.Name = "ucBtnAceptarAV1";
            this.ucBtnAceptarAV1.Size = new System.Drawing.Size(125, 50);
            this.ucBtnAceptarAV1.TabIndex = 43;
            // 
            // txtCorreo
            // 
            this.txtCorreo.AnchoTitulo = 114;
            this.txtCorreo.Location = new System.Drawing.Point(4, 163);
            this.txtCorreo.MaximumSize = new System.Drawing.Size(3000, 24);
            this.txtCorreo.MensajeDeAyuda = null;
            this.txtCorreo.MinimumSize = new System.Drawing.Size(110, 24);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.PermiteSoloNumeros = false;
            this.txtCorreo.Size = new System.Drawing.Size(545, 24);
            this.txtCorreo.TabIndex = 11;
            this.txtCorreo.TextoTitulo = "Correo :";
            this.txtCorreo.ValorTextBox = "";
            // 
            // FrmNuevoMaestro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 453);
            this.Controls.Add(this.grcInfo);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmNuevoMaestro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Maestro";
            this.Load += new System.EventHandler(this.FrmNuevoMaestro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNacMaestro.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpFecNacMaestro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcInfo)).EndInit();
            this.grcInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbxTipoDoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxRoles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxSexoMaestro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcNacMaestro)).EndInit();
            this.grcNacMaestro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbxMuni.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxPais.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxDpto.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lblTitulo;
        public DevExpress.XtraEditors.DateEdit dtpFecNacMaestro;
        private dll.Controles.ucBtnAceptarAV ucBtnAceptarAV1;
        private dll.Controles.ucLabelAV lblFecNacMaestro;
        private dll.Controles.ucLabelAV lblRolMaestro;
        private dll.Controles.ucLabelAV lblSexMaestro;
        private dll.Controles.ucBtnGuardarAV btnGuardar;
        private dll.Controles.ucBtnCancelarAV btnCancelar;
        private DevExpress.XtraEditors.GroupControl grcNacMaestro;
        private dll.Controles.ucLabelAV lblMuni;
        private dll.Controles.ucLabelAV lblDpto;
        private dll.Controles.ucLabelAV lblPais;
        private System.Windows.Forms.Panel panel1;
        public DevExpress.XtraEditors.ComboBoxEdit cbxSexoMaestro;
        public DevExpress.XtraEditors.LookUpEdit cbxDpto;
        public DevExpress.XtraEditors.LookUpEdit cbxMuni;
        public DevExpress.XtraEditors.LookUpEdit cbxPais;
        public DevExpress.XtraEditors.LookUpEdit cbxRoles;
        public DevExpress.XtraEditors.GroupControl grcInfo;
        private dll.Controles.ucLabelAV lblTipoDoc;
        public DevExpress.XtraEditors.LookUpEdit cbxTipoDoc;
        private dll.Controles.ucLabelTextBox txtPnom;
        private dll.Controles.ucLabelTextBox txtSnom;
        private dll.Controles.ucLabelTextBox txtSape;
        private dll.Controles.ucLabelTextBox txtTel;
        private dll.Controles.ucLabelTextBox txtUsu;
        private dll.Controles.ucLabelTextBox txtDir;
        private dll.Controles.ucLabelTextBox txtPape;
        public dll.Controles.ucLabelTextBox txtDocumento;
        private dll.Controles.ucLabelTextBox txtCorreo;
    }
}