﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;

namespace  Colegio.Vistas
{
    public partial class FrmLogin : Form
    {
        bool ingresaTxtUser = false;
        bool ingresaTxtClave = false;
        bool ingresaServer = false;
        ClUsuarios clUsuarios = new ClUsuarios();
        ClADDatosInstitucion clADDatosInstitucion = new ClADDatosInstitucion();
        //Clases.Funciones f = new Clases.Funciones();
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            try
            {
                if (ClFunciones.listaServidores.Count > 0) { txtServer.Text = ClFunciones.listaServidores[ClFunciones.listaServidores.Count - 1]; }
                lblHora.Text = DateTime.Now.ToString("hh:mm tt");
                lblFecha.Text = DateTime.Now.ToLongDateString();
                lblVersion.Text = "Sistema de Gestón V" + Application.ProductVersion;
                lblTituloLogin.Text = "Institución Educativa Jose Antonio Galán | " + DateTime.Now.Year.ToString();

            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (!ingresaTxtUser)
            {
                ingresaTxtUser = true;
                LimpiaTxtMarcaAgua(ref txtUsuario);
            }

        }

        private void LimpiaTxtMarcaAgua(ref TextBox txt)
        {
            txt.Font = new Font("Segoe UI", txt.Font.Size, FontStyle.Regular);
            txt.ForeColor = Color.Black;
            txt.BackColor = Color.LightCyan;
            if(txt.Name == "txtClave")
            {
                txt.PasswordChar = '*';
            }
            if (txt.Name != "txtServer" && txt.Text!="Servidor")
            {                
                txt.Text = "";
            }
            if (txt.Name == "txtServer" && txt.Text == "Servidor")
            {
                txt.Text = "";
            }
        }
        private void FormatTxtMarcaAgua(ref TextBox txt)
        {
            txt.Font = new Font("Segoe UI", txt.Font.Size, FontStyle.Italic);
            txt.ForeColor = SystemColors.ScrollBar;
            txt.BackColor = Color.White;
            if (txt.Name == "txtUsuario")
            {
                txt.Text = "Usuario";
            }
            else if (txt.Name=="txtClave")
            {
                txt.Text = "Contraseña";
                txt.PasswordChar = '\0';
            }else if(txt.Name=="txtServer")
            {
                txt.Text = "Servidor";
            }

        }
        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
            {
                FormatTxtMarcaAgua(ref txtUsuario);
                ingresaTxtUser = false;
            }else { txtUsuario.BackColor = Color.White; }
        }

        private void txtClave_Enter(object sender, EventArgs e)
        {
            if (!ingresaTxtClave)
            {
                ingresaTxtClave = true;
                LimpiaTxtMarcaAgua(ref txtClave);
            }
        }

        private void txtClave_Leave(object sender, EventArgs e)
        {
            if (txtClave.Text == "")
            {
                FormatTxtMarcaAgua(ref txtClave);
                ingresaTxtClave = false;
            }else { txtClave.BackColor = Color.White; }
        }

        private void btnIngreso_Click(object sender, EventArgs e)
        {
            FrmMensaje frmMsj = new FrmMensaje();
            try
            {                   
                if (ValidaCampos())
                {

                    if (Clases.ClConexion.clConexion.Ingreso(txtServer.Text))
                    {
                        ClFunciones.UsuarioIngreso = clUsuarios.Ingreso(txtUsuario.Text, txtClave.Text);
                        if (ClFunciones.UsuarioIngreso != null)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(dll.Common.FrmWait)); 
                            Clases.ClConexion.clConexion.EstaConectado = true;
                            Clases.ClFunciones.NomUsu = txtUsuario.Text;
                            ClSeguridad.CrearTransacciones();
                            ClFunciones.ListaPermisos = ClSeguridad.CargarPermisos();
                            ClFunciones.wADDatosInstitucion = clADDatosInstitucion.GetDatosInstitucion();
                            FrmPrincipal frmPrinc = new FrmPrincipal();
                            ClFunciones.formPrinci = frmPrinc;
                            frmPrinc.Show();
                            txtClave.Text = "";
                            txtUsuario.Text = "";
                            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
                            this.Hide();
                        }
                        else
                        {
                            //ClFunciones.msgErrorFrm("Bienvenido al sistema...", "", true);
                            //Clases.ClFunciones.f.EstaConectado = true;
                            //Clases.ClFunciones.NomUsu = txtUsuario.Text;
                            //FrmPrincipal frmPrinc = new FrmPrincipal();
                            //frmPrinc.btnEstudiantes.Enabled = false;
                            //frmPrinc.Show();
                            //ClFunciones.formLogin = this;
                            //this.Hide();
                            ClFunciones.msgError("Usuario o Contraseña Incorrectos!!", "", true);
                            Clases.ClConexion.clConexion.EstaConectado = false;
                            txtClave.Text = "";
                            txtClave.Focus();
                            Clases.ClConexion.clConexion.DesconectaBD();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }
        private bool ValidaCampos()
        {
            
            if (txtServer.Text == "" || txtServer.Text == "Servidor")
            {
                ClFunciones.msgError("Lo sentimos, al parecer el campo servidor se encuentra vacio!");                
                txtServer.Focus();
                return false;
            }
            else if (txtUsuario.Text == "" || txtUsuario.Text == "Usuario")
            {
                ClFunciones.msgError("Lo sentimos, al parecer el nombre de usuario se encuentra vacio!");
                txtUsuario.Focus();
                return false;
            }
            else if (txtClave.Text == "" || txtClave.Text == "Contraseña")
            {
                ClFunciones.msgError("Lo sentimos, al parecer la contraseña se encuentra vacia!");
                txtClave.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Clases.ClFunciones.AvanzaConEnter(e);
        }

        private void txtClave_KeyPress(object sender, KeyPressEventArgs e)
        {
            Clases.ClFunciones.AvanzaConEnter(e);
        }

        private void txtServer_Enter(object sender, EventArgs e)
        {
            if (!ingresaServer)
            {
                ingresaServer = true;
                LimpiaTxtMarcaAgua(ref txtServer);
            }
        }

        private void txtServer_Leave(object sender, EventArgs e)
        {
            if (txtServer.Text == "")
            {
                FormatTxtMarcaAgua(ref txtServer);
                ingresaServer = false;
            }else { txtServer.BackColor = Color.White; }
        }

        private void txtServer_KeyPress(object sender, KeyPressEventArgs e)
        {
            ClFunciones.AvanzaConEnter(e);
        }

        private void btnVerServ_Click(object sender, EventArgs e)
        {
            try
            {
                Form f = new FrmServidores(ref this.txtServer);
                f.ShowDialog();
                txtServer.Focus();
            }
            catch (Exception ex)
            {
                ClFunciones.msgError(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {           
            System.Windows.Forms.Application.Exit();
        }
    }
}
