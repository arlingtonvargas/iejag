USE [BDIEJAG]
GO
/****** Object:  StoredProcedure [dbo].[SP_NotasEstudiantesMateria_G]    Script Date: 08/12/2018 11:30:36 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Arlington
-- Create date: 04-11-2018
-- Description:	Trae las notas de los estudiantes de determinado curso en determinado año y de determinada materia
-- =============================================
ALTER PROCEDURE [dbo].[SP_NotasEstudiantesMateria_G]

	@pIdAñoLectivo INT,
	@pIdCurso INT,
	@pIdMateria INT,
	@pIdMaestro BIGINT		

AS
BEGIN
	
	DECLARE 
	@IdAñoLectivo INT = @pIdAñoLectivo, 
	@IdCurso INT = @pIdCurso,
	@IdMaestro INT = @pIdMaestro,
	@IdMateria BIGINT = @pIdMateria

	SET NOCOUNT ON;

		SELECT MC.Sec AS SecMatCurso, ET.IdTercero, CONCAT(ET.Papellido,' ',ET.Sapellido,' ',ET.Pnombre,' ',ET.Snombre) AS NombreComp 
		, MR.Sec AS IdMateria, MR.NomMateria, MT.Sec AS IdMatricula, P1, P2, P3, P4,  ((P1 + P2 + P3 + P4) / 4) AS Prom   
		FROM ADTerceros ET 
		INNER JOIN ADMatriculas MT ON ET.IdTercero = MT.IdEstudiante AND MT.IdAñoLectivo = @IdAñoLectivo
		INNER JOIN ADCursos CS ON MT.IdCurso = CS.Sec
		INNER JOIN ADMateriasCursos MC ON CS.Sec = MC.IdCurso
		INNER JOIN ADMaterias MR ON MC.IdMateria = MR.Sec
		LEFT JOIN ADNotas NT ON MC.Sec = NT.SecMatCurso AND NT.IdEstudiante = ET.IdTercero AND NT.IdMatricula = MT.Sec
		WHERE CS.Sec = @IdCurso AND MR.Sec = @IdMateria AND MC.IdMaestro = @IdMaestro AND MT.Cancelada = 0 ORDER BY NombreComp
END
