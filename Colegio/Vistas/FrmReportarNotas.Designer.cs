﻿namespace Colegio.Vistas
{
    partial class FrmReportarNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReportarNotas));
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET1 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET2 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET3 = new dll.Common.ET.TituloColsBusquedaET();
            dll.Common.ET.TituloColsBusquedaET tituloColsBusquedaET4 = new dll.Common.ET.TituloColsBusquedaET();
            this.txtCurso = new dll.Controles.ucBusqueda();
            this.txtMaestro = new dll.Controles.ucBusqueda();
            this.txtAñoLectivo = new dll.Controles.ucBusqueda();
            this.gcMaterias = new DevExpress.XtraGrid.GridControl();
            this.gvMaterias = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnBuscar = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lkePeriodo = new dll.Controles.ucLabelCombo();
            this.btnSalir = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.txtMateria = new dll.Controles.ucBusqueda();
            this.btnGuardar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtDesempeño = new DevExpress.XtraEditors.MemoEdit();
            this.lblDesempeño = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcMaterias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaterias)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesempeño.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCurso
            // 
            this.txtCurso.AnchoTextBox = 110;
            this.txtCurso.AnchoTitulo = 80;
            this.txtCurso.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCurso.Location = new System.Drawing.Point(0, 27);
            this.txtCurso.Margin = new System.Windows.Forms.Padding(0);
            this.txtCurso.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtCurso.MensajeDeAyuda = null;
            this.txtCurso.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtCurso.Name = "txtCurso";
            this.txtCurso.PermiteSoloNumeros = false;
            this.txtCurso.Script = "";
            this.txtCurso.Size = new System.Drawing.Size(327, 28);
            this.txtCurso.TabIndex = 3;
            this.txtCurso.TextoTitulo = "Curso :";
            tituloColsBusquedaET1.Codigo = "Código";
            tituloColsBusquedaET1.Descripcion = "Descripción";
            this.txtCurso.TituloColsBusqueda = tituloColsBusquedaET1;
            this.txtCurso.ValorTextBox = "";
            this.txtCurso.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtCurso_SaleControl);
            // 
            // txtMaestro
            // 
            this.txtMaestro.AnchoTextBox = 110;
            this.txtMaestro.AnchoTitulo = 80;
            this.txtMaestro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMaestro.Location = new System.Drawing.Point(0, 0);
            this.txtMaestro.Margin = new System.Windows.Forms.Padding(0);
            this.txtMaestro.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMaestro.MensajeDeAyuda = null;
            this.txtMaestro.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMaestro.Name = "txtMaestro";
            this.txtMaestro.PermiteSoloNumeros = false;
            this.txtMaestro.Script = "";
            this.txtMaestro.Size = new System.Drawing.Size(327, 28);
            this.txtMaestro.TabIndex = 1;
            this.txtMaestro.TextoTitulo = "Maestro :";
            tituloColsBusquedaET2.Codigo = "Código";
            tituloColsBusquedaET2.Descripcion = "Descripción";
            this.txtMaestro.TituloColsBusqueda = tituloColsBusquedaET2;
            this.txtMaestro.ValorTextBox = "";
            // 
            // txtAñoLectivo
            // 
            this.txtAñoLectivo.AnchoTextBox = 40;
            this.txtAñoLectivo.AnchoTitulo = 80;
            this.txtAñoLectivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAñoLectivo.Enabled = false;
            this.txtAñoLectivo.Location = new System.Drawing.Point(327, 0);
            this.txtAñoLectivo.Margin = new System.Windows.Forms.Padding(0);
            this.txtAñoLectivo.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtAñoLectivo.MensajeDeAyuda = null;
            this.txtAñoLectivo.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtAñoLectivo.Name = "txtAñoLectivo";
            this.txtAñoLectivo.PermiteSoloNumeros = false;
            this.txtAñoLectivo.Script = "";
            this.txtAñoLectivo.Size = new System.Drawing.Size(327, 28);
            this.txtAñoLectivo.TabIndex = 2;
            this.txtAñoLectivo.TextoTitulo = "Año Lectivo :";
            tituloColsBusquedaET3.Codigo = "Código";
            tituloColsBusquedaET3.Descripcion = "Descripción";
            this.txtAñoLectivo.TituloColsBusqueda = tituloColsBusquedaET3;
            this.txtAñoLectivo.ValorTextBox = "";
            this.txtAñoLectivo.SaleControl += new dll.Controles.ucBusqueda.EventHandler(this.txtAñoLectivo_SaleControl);
            // 
            // gcMaterias
            // 
            this.gcMaterias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcMaterias.Location = new System.Drawing.Point(9, 95);
            this.gcMaterias.MainView = this.gvMaterias;
            this.gcMaterias.Name = "gcMaterias";
            this.gcMaterias.Size = new System.Drawing.Size(1098, 393);
            this.gcMaterias.TabIndex = 5;
            this.gcMaterias.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMaterias});
            // 
            // gvMaterias
            // 
            this.gvMaterias.GridControl = this.gcMaterias;
            this.gvMaterias.Name = "gvMaterias";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBuscar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.ImageOptions.Image")));
            this.btnBuscar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(897, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 21);
            this.btnBuscar.TabIndex = 5;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Controls.Add(this.lkePeriodo, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnSalir, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnRefresh, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCurso, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtMaestro, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnBuscar, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtAñoLectivo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtMateria, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnGuardar, 4, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1094, 55);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lkePeriodo
            // 
            this.lkePeriodo.AnchoTitulo = 70;
            this.lkePeriodo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lkePeriodo.Enabled = false;
            this.lkePeriodo.Location = new System.Drawing.Point(657, 0);
            this.lkePeriodo.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.lkePeriodo.MaximumSize = new System.Drawing.Size(3000, 28);
            this.lkePeriodo.MensajeDeAyuda = null;
            this.lkePeriodo.MinimumSize = new System.Drawing.Size(50, 28);
            this.lkePeriodo.Name = "lkePeriodo";
            this.lkePeriodo.Size = new System.Drawing.Size(234, 28);
            this.lkePeriodo.TabIndex = 6;
            this.lkePeriodo.TextoTitulo = "Periodo :";
            // 
            // btnSalir
            // 
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSalir.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.ImageOptions.Image")));
            this.btnSalir.Location = new System.Drawing.Point(997, 30);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(94, 22);
            this.btnSalir.TabIndex = 10;
            this.btnSalir.Text = "Salir";
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnRefresh.ImageOptions.Image")));
            this.btnRefresh.Location = new System.Drawing.Point(897, 30);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(94, 22);
            this.btnRefresh.TabIndex = 8;
            this.btnRefresh.Text = "Refrescar";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtMateria
            // 
            this.txtMateria.AnchoTextBox = 40;
            this.txtMateria.AnchoTitulo = 80;
            this.tableLayoutPanel1.SetColumnSpan(this.txtMateria, 2);
            this.txtMateria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMateria.Location = new System.Drawing.Point(327, 27);
            this.txtMateria.Margin = new System.Windows.Forms.Padding(0);
            this.txtMateria.MaximumSize = new System.Drawing.Size(3000, 28);
            this.txtMateria.MensajeDeAyuda = null;
            this.txtMateria.MinimumSize = new System.Drawing.Size(200, 28);
            this.txtMateria.Name = "txtMateria";
            this.txtMateria.PermiteSoloNumeros = false;
            this.txtMateria.Script = "";
            this.txtMateria.Size = new System.Drawing.Size(567, 28);
            this.txtMateria.TabIndex = 4;
            this.txtMateria.TextoTitulo = "Materia :";
            tituloColsBusquedaET4.Codigo = "Código";
            tituloColsBusquedaET4.Descripcion = "Descripción";
            this.txtMateria.TituloColsBusqueda = tituloColsBusquedaET4;
            this.txtMateria.ValorTextBox = "";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGuardar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.ImageOptions.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(997, 3);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(94, 21);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.tableLayoutPanel1);
            this.groupControl1.Location = new System.Drawing.Point(9, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1098, 77);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Datos de las notas";
            // 
            // txtDesempeño
            // 
            this.txtDesempeño.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDesempeño.Location = new System.Drawing.Point(96, 494);
            this.txtDesempeño.Name = "txtDesempeño";
            this.txtDesempeño.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDesempeño.Properties.Appearance.Options.UseFont = true;
            this.txtDesempeño.Size = new System.Drawing.Size(1011, 44);
            this.txtDesempeño.TabIndex = 6;
            // 
            // lblDesempeño
            // 
            this.lblDesempeño.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDesempeño.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblDesempeño.Appearance.Options.UseFont = true;
            this.lblDesempeño.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblDesempeño.Location = new System.Drawing.Point(9, 496);
            this.lblDesempeño.Name = "lblDesempeño";
            this.lblDesempeño.Size = new System.Drawing.Size(81, 20);
            this.lblDesempeño.TabIndex = 7;
            this.lblDesempeño.Text = "Desempeños";
            // 
            // FrmReportarNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 550);
            this.Controls.Add(this.lblDesempeño);
            this.Controls.Add(this.txtDesempeño);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.gcMaterias);
            this.Name = "FrmReportarNotas";
            this.Text = "Reportar Notas";
            this.Load += new System.EventHandler(this.FrmReportarNotas_Load);
            this.SizeChanged += new System.EventHandler(this.FrmReportarNotas_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.gcMaterias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMaterias)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDesempeño.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private dll.Controles.ucBusqueda txtCurso;
        private dll.Controles.ucBusqueda txtMaestro;
        private dll.Controles.ucBusqueda txtAñoLectivo;
        private DevExpress.XtraGrid.GridControl gcMaterias;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMaterias;
        private DevExpress.XtraEditors.SimpleButton btnBuscar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.SimpleButton btnGuardar;
        private DevExpress.XtraEditors.SimpleButton btnSalir;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private dll.Controles.ucBusqueda txtMateria;
        private dll.Controles.ucLabelCombo lkePeriodo;
        private DevExpress.XtraEditors.MemoEdit txtDesempeño;
        private DevExpress.XtraEditors.LabelControl lblDesempeño;
    }
}