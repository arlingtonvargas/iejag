﻿using Colegio.ET;
using dll.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Colegio.Clases;
using dll.Common.ET;

namespace Colegio.Vistas
{
    public partial class FrmAggMateriasCurso : FrmBase
    {
        ClAñoLectivo clAñoLectivo = new ClAñoLectivo();        
        ClMaterias clMateria = new ClMaterias();
        ClTercero clTercero = new ClTercero();
        ClCursos clCurso = new ClCursos();
        bool EstaActualizando = false;
        int SecMatCurso = -1;
        public FrmAggMateriasCurso()
        {
            InitializeComponent();
        }

        private void FrmAggMateriasCurso_Load(object sender, EventArgs e)
        {            
            LlenaControles();
            CrearGrilla();
            txtCurso.Focus();
        }

        private void CrearGrilla()
        {
            try
            {
                gvMaterias = GrillaDevExpress.CrearGrilla(false, true);
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("Sec", "", visible: false));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("IdCurso", "Id Curso", visible: false));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("NomCurso", "Curso", visible: false));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("IdMateria", "Id Materia", ancho: 50));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("NomMateria", "Materia", ancho: 100));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("IdMaestro", "Doc Maestro", ancho: 70));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("NomMaestro", "Maestro"));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("Desempeños", "Desempeños", ancho: 130));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("IH", "IH", ancho: 30));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("IdAñoLectivo", "Id Año", ancho: 70));
                gvMaterias.Columns.Add(GrillaDevExpress.CrearColumna("Año", "Año lectivo", ancho: 40));
                gvMaterias.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvMaterias.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvMaterias.OptionsCustomization.AllowColumnResizing = true;
                gcMaterias.MainView = gvMaterias;
                this.gvMaterias.DoubleClick += new System.EventHandler(this.gvMaterias_DoubleClick);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void gvMaterias_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                txtAño.ValorTextBox = gvMaterias.GetFocusedRowCellValue("IdAñoLectivo").ToString();
                CargarCrusos(Convert.ToInt32(txtAño.ValorTextBox));
                txtCurso.ValorTextBox = gvMaterias.GetFocusedRowCellValue("IdCurso").ToString();
                CargarMateriasInCurso(Convert.ToInt32(txtCurso.ValorTextBox));
                txtMaterias.ValorTextBox = gvMaterias.GetFocusedRowCellValue("IdMateria").ToString();
                txtMaestro.ValorTextBox = gvMaterias.GetFocusedRowCellValue("IdMaestro").ToString();
                seIH.Value = Convert.ToInt32(gvMaterias.GetFocusedRowCellValue("IH"));
                txtDesempeños.Text = (gvMaterias.GetFocusedRowCellValue("Desempeños")!=null) ? gvMaterias.GetFocusedRowCellValue("Desempeños").ToString(): "";
                SecMatCurso = Convert.ToInt32(gvMaterias.GetFocusedRowCellValue("Sec"));
                txtAño.Enabled = false;
                txtCurso.Enabled = false;
                txtMaterias.Enabled = false;

                EstaActualizando = true;
                txtMaestro.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void LlenaControles()
        {
            try
            {
                DataTable dt = clAñoLectivo.GetAñosLectivos();
                if (dt.Rows.Count > 0)
                {
                    TituloColsBusquedaET TituloColsBusqueda = new TituloColsBusquedaET() { Codigo = "Id año", Descripcion = "Descripción" };
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtAño.TituloColsBusqueda = TituloColsBusqueda;
                    txtAño.DataTable = dt;
                }               
                dt = clTercero.GetTercerosTipoTer(1);
                if (dt.Rows.Count > 0)
                {
                    TituloColsBusquedaET TituloColsBusqueda = new TituloColsBusquedaET() { Codigo = "Documento", Descripcion = "Nombre completo" };
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtMaestro.TituloColsBusqueda = TituloColsBusqueda;
                    txtMaestro.DataTable = dt;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private bool ValidaCampos()
        {
            try
            {
                if (txtAño.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el año lectivo.");
                    txtAño.Focus();
                    return false;
                }
                else if (txtCurso.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el curso.");
                    txtCurso.Focus();
                    return false;
                }
                else if (txtMaterias.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar la materia.");
                    txtMaterias.Focus();
                    return false;
                }
                else if (txtMaestro.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe seleccionar el maestro.");
                    txtMaestro.Focus();
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        private void LlenaGrilla(int idCurso)
        {
            try
            {
               gcMaterias.DataSource = clMateria.GetMateriasVistaByCurso(idCurso);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void LimpiarCampos()
        {
            try
            {
                txtAño.ValorTextBox = "";
                txtCurso.ValorTextBox = "";
                txtMaterias.ValorTextBox = "";
                txtMaestro.ValorTextBox = "";
                txtDesempeños.Text = "";
                txtAño.Enabled = true;
                txtCurso.Enabled = true;
                txtMaterias.Enabled = true;
                seIH.Value = 0;
                EstaActualizando = false;
                txtAño.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void CargarCrusos(int idAño)
        {
            try
            {
              DataTable dt =  clCurso.GetCursosByAño(idAño);
                if (dt.Rows.Count>0)
                {
                    TituloColsBusquedaET TituloColsBusqueda = new TituloColsBusquedaET() { Codigo = "Id", Descripcion = "Curso" };
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtCurso.TituloColsBusqueda = TituloColsBusqueda;
                    txtCurso.DataTable = dt;
                }else
                {
                    txtCurso.DataTable = new DataTable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void CargarMateriasNoInCurso(int idCurso)
        {
            try
            {
                DataTable dt = clMateria.GetMateriasVistaNoInCurso(idCurso);
                if (dt.Rows.Count > 0)
                {
                    TituloColsBusquedaET TituloColsBusqueda = new TituloColsBusquedaET() { Codigo = "Id", Descripcion = "Materia" };
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtMaterias.TituloColsBusqueda = TituloColsBusqueda;
                    txtMaterias.DataTable = dt;
                }
                else
                {
                    txtMaterias.DataTable = new DataTable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void CargarMateriasInCurso(int idCurso)
        {
            try
            {
                DataTable dt = clMateria.GetMateriasVistaByCursoDT(idCurso);
                if (dt.Rows.Count > 0)
                {
                    TituloColsBusquedaET TituloColsBusqueda = new TituloColsBusquedaET() { Codigo = "Id", Descripcion = "Materia" };
                    dt.Columns[0].ColumnName = "Codigo";
                    dt.Columns[1].ColumnName = "Descripcion";
                    txtMaterias.TituloColsBusqueda = TituloColsBusqueda;
                    txtMaterias.DataTable = dt;
                }
                else
                {
                    txtMaterias.DataTable = new DataTable();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidaCampos()) return;
                int idCurso = Convert.ToInt32(txtCurso.ValorTextBox);
                ADMateriasCursosET ADMateriaCurso = new ADMateriasCursosET()
                {
                    Sec = SecMatCurso,
                    IdCurso = idCurso,
                    IdMaestro = Convert.ToInt64(txtMaestro.ValorTextBox),
                    IdMateria = Convert.ToInt32(txtMaterias.ValorTextBox),
                    IH = Convert.ToInt32(seIH.Value),
                    Desempeños = txtDesempeños.Text

                };
                if (!EstaActualizando)
                {
                    if (clCurso.InsertMateriasCurso(ADMateriaCurso))
                    {
                        ClFunciones.msgExitoso("Materia agregada correctamente.");
                        txtMaterias.ValorTextBox = "";
                        txtMaestro.ValorTextBox = "";
                        seIH.Value = 0;
                        txtDesempeños.Text = "";
                        LlenaGrilla(idCurso);
                        CargarMateriasNoInCurso(idCurso);
                        txtMaterias.Focus();
                    }
                    else
                    {
                        ClFunciones.msgError("Ha ocurrido un error.");
                        return;
                    }
                }
                else
                {
                    if (clCurso.UpdateMateriasCurso(ADMateriaCurso))
                    {
                        ClFunciones.msgExitoso("Materia actualizada correctamente.");
                        txtMaterias.ValorTextBox = "";
                        txtMaestro.ValorTextBox = "";
                        seIH.Value = 0;
                        txtDesempeños.Text = "";
                        LlenaGrilla(idCurso);
                        CargarMateriasNoInCurso(idCurso);
                        EstaActualizando = false;
                        txtAño.Enabled = true;
                        txtCurso.Enabled = true;
                        txtMaterias.Enabled = true;
                        txtMaterias.Focus();
                    }
                    else
                    {
                        ClFunciones.msgError("Ha ocurrido un error.");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void seIH_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ClFunciones.AvanzaConEnter(e);
            }
        }

        private void txtAño_SaleControl(object sender, EventArgs e)
        {
            if (txtAño.ValorTextBox == "")
            {
                txtCurso.DataTable = new DataTable();
                txtCurso.ValorTextBox = "";
            }else
            {
                CargarCrusos(Convert.ToInt32(txtAño.ValorTextBox));
            }
        }

        private void txtCurso_SaleControl(object sender, EventArgs e)
        {
            
            if (txtCurso.ValorTextBox == "")
            {
                gcMaterias.DataSource = new DataTable();
            }
            else
            {
                int idCurso = Convert.ToInt32(txtCurso.ValorTextBox);
                CargarMateriasNoInCurso(idCurso);
                LlenaGrilla(idCurso);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenaControles();
        }
    }
}
