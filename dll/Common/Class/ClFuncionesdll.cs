﻿using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace dll.Common.Class
{
    public class ClFuncionesdll
    {
        public static BarStaticItem _BarStaticItemPrincipal = new BarStaticItem();
        public static BarStaticItem BarStaticItemPrincipal
        {
            get
            {
                return _BarStaticItemPrincipal;
            }
            set
            {
                _BarStaticItemPrincipal = value;
            }
        }

        public static System.Drawing.Color _ColorFondoControles  = System.Drawing.Color.LightCyan;
        public static System.Drawing.Color ColorFondoControles
        {
            get
            {
                return _ColorFondoControles;
            }
            set
            {
                _ColorFondoControles = value;
            }
        }

        public static string CrearFiltro(DataTable tabla, string texto)
        {
            try
            {
                texto = texto.Replace(' ', ';');
                string[] cadenas = texto.Split(';');
                string filtro = "";
                for (int i = 0; i < cadenas.Length; i++)
                {
                    if (cadenas[i] != "" && cadenas[i] != " ")
                    {
                        if (i > 0)
                            filtro += " and ";
                        filtro += " ( ";
                        for (int o = 0; o < tabla.Columns.Count; o++)
                        {
                            filtro += " CONVERT(" + tabla.Columns[o].ColumnName + ", System.String) like '%" + cadenas[i] + "%' ";
                            if (o < tabla.Columns.Count - 1)
                                filtro += " or ";
                        }
                        filtro += " ) ";
                    }
                }
                return filtro;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static int DobleClicSoreFila(object sender, EventArgs e)
        {
            try
            {
                DXMouseEventArgs ea = e as DXMouseEventArgs;
                GridView view = sender as GridView;
                GridHitInfo info = view.CalcHitInfo(ea.Location);
                if (info.InRow || info.InRowCell)
                {
                    return info.RowHandle;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
public class GrillaDevExpress
{
    public static GridView CrearGrilla(bool mostrarTitulo, bool activarFiltro, string tituloGrilla = "")
    {
        GridView Vista = new GridView();
        Vista.OptionsView.ShowViewCaption = mostrarTitulo;
        if (mostrarTitulo)
            Vista.ViewCaption = tituloGrilla;

        Vista.OptionsView.ColumnAutoWidth = true;
        Vista.ActiveFilterEnabled = activarFiltro;
        Vista.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
        Vista.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
        Vista.OptionsBehavior.Editable = true;
        Vista.OptionsSelection.EnableAppearanceFocusedRow = true;
        // Opciones de Manejo x Usuario Final                                                                                                                                            
        Vista.OptionsCustomization.AllowRowSizing = false;
        Vista.OptionsCustomization.AllowColumnResizing = false;
        Vista.OptionsCustomization.AllowColumnMoving = false;
        Vista.OptionsCustomization.AllowFilter = true;
        Vista.OptionsCustomization.AllowSort = true;
        //Opciones de Vista del Grid                                                                                                                                                    
        Vista.OptionsView.ShowGroupPanel = false;
        Vista.OptionsView.ShowAutoFilterRow = true;
        Vista.OptionsView.ColumnAutoWidth = true;
        Vista.OptionsView.ShowFooter = false;
        //'Panel Inferior de Filtro                                                                                                                                                       
        Vista.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
        Vista.OptionsMenu.ShowAutoFilterRowItem = false;
        Vista.OptionsMenu.ShowGroupSortSummaryItems = false;
        Vista.OptionsMenu.EnableColumnMenu = true;
        Vista.OptionsMenu.EnableGroupPanelMenu = false;
        Vista.OptionsMenu.ShowGroupSortSummaryItems = false;
        // ' Manejo de Colores   
        Vista.Appearance.HeaderPanel.Font = new System.Drawing.Font("Segoe UI", 10.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
        Vista.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Maroon;
        Vista.Appearance.FocusedRow.Options.UseTextOptions = false;
        Vista.Appearance.Row.Font = new System.Drawing.Font("Segoe UI", 9.5f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (Byte)0);
        return Vista;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="nombre">Nombre de la columna del datasource</param>
    /// <param name="titulo">Titulo del encabezado de columna</param>
    /// <returns></returns>
    public static GridColumn CrearColumna(string nombre, string titulo, FormatType tipo = FormatType.None, string formato = "", bool visible = true, bool autoAncho = false, int ancho = 100, bool soloLectura = true, bool permiteEditar = false, bool permiteFoco = false, HorzAlignment alineacionEncabezado = HorzAlignment.Center, HorzAlignment alineacion = HorzAlignment.Default)
    {
        GridColumn Columna = new GridColumn();
        Columna.Name = Columna.FieldName = nombre;
        Columna.Caption = titulo;
        Columna.DisplayFormat.FormatType = tipo;
        Columna.DisplayFormat.FormatString = formato;
        Columna.Visible = visible;
        Columna.OptionsColumn.ReadOnly = soloLectura;
        Columna.OptionsColumn.AllowEdit = permiteEditar;
        Columna.OptionsColumn.AllowFocus = permiteFoco;
        Columna.AppearanceCell.Options.UseTextOptions = true;
        Columna.AppearanceCell.TextOptions.HAlignment = alineacion;
        Columna.AppearanceHeader.Options.UseTextOptions = true;
        Columna.AppearanceHeader.TextOptions.HAlignment = alineacionEncabezado;
        if (autoAncho)
            Columna.BestFit();
        else
        {
            Columna.OptionsColumn.FixedWidth = true;
            Columna.Width = ancho;
        }
        return Columna;
    }
}
