﻿using Colegio.Clases;
using dll.Common;
using Colegio.ET;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;

using DevExpress.XtraEditors.Camera;

namespace Colegio.Vistas
{
    public partial class FrmEstudiante : FrmBase
    {
        ClUsuarios clUsuarios = new ClUsuarios();
        ClMuniDptoPais clMuniDptoPais = new ClMuniDptoPais();
        ClTercero clTercero = new ClTercero();
        ADTercerosET EstudianteActual = new ADTercerosET();

        bool EstaActualizando = false;
        public FrmEstudiante()
        {
            InitializeComponent();
        }

        private void lblDirMaestro_Load(object sender, EventArgs e)
        {

        }

        private void FrmEstudiante_Load(object sender, EventArgs e)
        {
            CrearGrilla();
            cbxTipoDoc.Focus();
            LlenarCbxTipoDoc();
            LlenarCbxPaises();
            LlenarGrilla();
            HabilitaBotones();
            btnCam.Visible = false;
        }
        private void HabilitaBotones()
        {
            int rol = ClFunciones.UsuarioIngreso.IdRol;           
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1012)) btnGuardar.Enabled = false;
            if (!ClSeguridad.ValidaPermisosLocal(rol, 1013)) btnGuardar.Enabled = false;           

        }

        private void LlenarCbxPaises()
        {
            cbxPais.Properties.DataSource = clMuniDptoPais.GetPaises();
            cbxPais.Properties.ValueMember = "CodPais";
            cbxPais.Properties.DisplayMember = "NomPais";
            cbxPais.Properties.PopulateColumns();
            cbxPais.Properties.ShowFooter = false;
            cbxPais.Properties.ShowHeader = false;
            cbxPais.Properties.Columns[0].Visible = false;
            cbxPais.Properties.Columns[2].Visible = false;
        }

        private void LlenarCbxDpto(string idPais)
        {
            cbxDpto.Properties.DataSource = clMuniDptoPais.GetDptosByPais(idPais);
            cbxDpto.Properties.ValueMember = "CodDpto";
            cbxDpto.Properties.DisplayMember = "NomDpto";
            cbxDpto.Properties.PopulateColumns();
            cbxDpto.Properties.ShowFooter = false;
            cbxDpto.Properties.ShowHeader = false;
            cbxDpto.Properties.Columns[0].Visible = false;
            cbxDpto.Properties.Columns[1].Visible = false;
        }

        private void LlenarCbxMunis(string idDpto)
        {
            cbxMuni.Properties.DataSource = clMuniDptoPais.GetMuniByDpto(idDpto);
            cbxMuni.Properties.ValueMember = "IdMunicipio";
            cbxMuni.Properties.DisplayMember = "NombreMunicipio";
            cbxMuni.Properties.PopulateColumns();
            cbxMuni.Properties.ShowFooter = false;
            cbxMuni.Properties.ShowHeader = false;
            cbxMuni.Properties.Columns[0].Visible = false;
            cbxMuni.Properties.Columns[1].Visible = false;
        }

        private void LlenarCbxTipoDoc()
        {
            DataTable dt = clTercero.GetTiposDocumentos();
            DataTable dtNew = new DataTable();
            dtNew.Columns.Add("TipoDoc", typeof(String));
            dtNew.Columns.Add("Nombre", typeof(String));
            foreach (DataRow item in dt.Rows)
            {
                DataRow newFila = dtNew.NewRow();
                newFila["TipoDoc"] = item["TipoDoc"].ToString();
                newFila["Nombre"] = item["Nombre"].ToString();
                dtNew.Rows.Add(newFila);
                dtNew.AcceptChanges();
            }
            cbxTipoDoc.Properties.DataSource = dtNew;
            cbxTipoDoc.Properties.ValueMember = "TipoDoc";
            cbxTipoDoc.Properties.DisplayMember = "Nombre";
            cbxTipoDoc.Properties.PopulateColumns();
            cbxTipoDoc.Properties.ShowFooter = false;
            cbxTipoDoc.Properties.ShowHeader = false;
            cbxTipoDoc.Properties.Columns[0].Visible = false;
        }

        private void CrearGrilla()
        {
            try
            {
                gvEstudiantes = GrillaDevExpress.CrearGrilla(false, true);
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("IdTercero", "Id"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("NombreComp", "Nombre"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Pnombre", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Snombre", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Papellido", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Sapellido", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Direccion", "Dirección"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Correo", "Correo"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Telefono", "Teléfono"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("FechaNacimiento", "Fecha Nacimiento"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("IdLugarNacimiento", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("NomLugarNacimiento", "Lugar de nacimiento"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("FechaCrea", "Creado"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("IdUsuario", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("NomUsu", "Usuario"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("IdRol", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("CodDpto", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Pais", "", visible: false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("Foto", "Foto"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("NomRol", "Rol", visible:false));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("RH", "RH"));
                gvEstudiantes.Columns.Add(GrillaDevExpress.CrearColumna("EPS", "EPS"));
                gvEstudiantes.Appearance.ViewCaption.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvEstudiantes.Appearance.FocusedRow.Font = new Font("Segoe UI", 8.5f, FontStyle.Bold, GraphicsUnit.Point, (Byte)0);
                gvEstudiantes.Appearance.FocusedRow.BackColor = Color.LightGreen;
                gvEstudiantes.OptionsCustomization.AllowColumnResizing = true;
                gcEstudiantes.MainView = gvEstudiantes;
                this.gvEstudiantes.DoubleClick += new System.EventHandler(this.gvEstudiantes_DoubleClick);

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void gvEstudiantes_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                int RowHandle = -1;
                if (true)
                {
                    RowHandle = gvEstudiantes.FocusedRowHandle;
                }
                if (RowHandle <= -1)
                    return;
                EstudianteActual = ExtraerData(RowHandle);
                txtDocumento.ValorTextBox = EstudianteActual.IdTercero.ToString();
                txtPnom.ValorTextBox = EstudianteActual.Pnombre;
                txtSnom.ValorTextBox = EstudianteActual.Snombre;
                txtPape.ValorTextBox = EstudianteActual.Papellido;
                txtSape.ValorTextBox = EstudianteActual.Sapellido;
                txtTel.ValorTextBox = EstudianteActual.Telefono.ToString();
                txtDir.ValorTextBox = EstudianteActual.Direccion;
                txtCorreo.ValorTextBox = EstudianteActual.Correo;
                txtUsu.ValorTextBox = EstudianteActual.NomUsu;
                txtRh.ValorTextBox = EstudianteActual.RH;
                txtEps.ValorTextBox = EstudianteActual.EPS;
                cbxTipoDoc.EditValue = EstudianteActual.TipoDoc.ToString();
                cbxSexo.SelectedIndex = EstudianteActual.Sexo;
                dtpFecNac.DateTime = EstudianteActual.FechaNacimiento;
                cbxPais.EditValue = EstudianteActual.Pais.ToString();
                LlenarCbxDpto(EstudianteActual.Pais);
                cbxDpto.EditValue = EstudianteActual.CodDpto.ToString();
                LlenarCbxMunis(EstudianteActual.CodDpto);
                cbxMuni.EditValue = EstudianteActual.IdLugarNacimiento.ToString();
                pcbFoto.Image = EstudianteActual.Foto;
                txtDocumento.Enabled = false;
                btnAcudiente.Enabled = true;
                EstaActualizando = true;
                cbxTipoDoc.Focus();
            }
            catch (Exception ex)
            {
            }         
        }

        private void LlenarGrilla()
        {
            try
            {
                gcEstudiantes.DataSource = clTercero.GetTercerosTipoTer(2);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private ADTercerosET ExtraerData(int RowHandle)
        {
            try
            {
                ET.ADTercerosET clMaestroET = new ET.ADTercerosET();
                clMaestroET.IdTercero = Convert.ToInt64(gvEstudiantes.GetRowCellValue(RowHandle, "IdTercero"));
                clMaestroET.TipoDoc = Convert.ToInt32(gvEstudiantes.GetRowCellValue(RowHandle, "TipoDoc"));
                clMaestroET.Pnombre = gvEstudiantes.GetRowCellValue(RowHandle, "Pnombre").ToString();
                clMaestroET.Snombre = gvEstudiantes.GetRowCellValue(RowHandle, "Snombre").ToString();
                clMaestroET.Papellido = gvEstudiantes.GetRowCellValue(RowHandle, "Papellido").ToString();
                clMaestroET.Sapellido = gvEstudiantes.GetRowCellValue(RowHandle, "Sapellido").ToString();
                clMaestroET.Telefono = gvEstudiantes.GetRowCellValue(RowHandle, "Telefono")!= DBNull.Value ? Convert.ToDouble(gvEstudiantes.GetRowCellValue(RowHandle, "Telefono")):0;
                clMaestroET.Direccion = gvEstudiantes.GetRowCellValue(RowHandle, "Direccion").ToString();
                clMaestroET.Correo = gvEstudiantes.GetRowCellValue(RowHandle, "Correo").ToString();
                clMaestroET.NomUsu = gvEstudiantes.GetRowCellValue(RowHandle, "NomUsu").ToString();
                clMaestroET.RH = gvEstudiantes.GetRowCellValue(RowHandle, "RH").ToString();
                clMaestroET.EPS = gvEstudiantes.GetRowCellValue(RowHandle, "EPS").ToString();
                clMaestroET.IdUsuario = Convert.ToInt32(gvEstudiantes.GetRowCellValue(RowHandle, "IdUsuario"));
                clMaestroET.IdRol = Convert.ToInt32(gvEstudiantes.GetRowCellValue(RowHandle, "IdRol"));
                clMaestroET.Sexo = Convert.ToInt32(gvEstudiantes.GetRowCellValue(RowHandle, "Sexo"));
                clMaestroET.FechaNacimiento = Convert.ToDateTime(gvEstudiantes.GetRowCellValue(RowHandle, "FechaNacimiento"));
                clMaestroET.IdLugarNacimiento = gvEstudiantes.GetRowCellValue(RowHandle, "IdLugarNacimiento").ToString();
                clMaestroET.CodDpto = gvEstudiantes.GetRowCellValue(RowHandle, "CodDpto").ToString();
                clMaestroET.Pais = gvEstudiantes.GetRowCellValue(RowHandle, "Pais").ToString();
                byte[] imgByte = (gvEstudiantes.GetRowCellValue(RowHandle, "Foto") != DBNull.Value) ? (byte[])(gvEstudiantes.GetRowCellValue(RowHandle, "Foto")) : null;
                clMaestroET.Foto = ClFunciones.byteArrayToImage(imgByte);
                return clMaestroET;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
                return null;
            }
        }


        #region[Validacion]

        private bool ValidaCampos()
        {
            try
            {
                DateTime fechaNull = new DateTime();
                if (cbxTipoDoc.ItemIndex <=-1)
                {
                    ClFunciones.msgError("Debe seleccionar el tipo de documento del estudiante.");                    
                    cbxTipoDoc.Focus();
                    return false;
                }else if (txtDocumento.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el documento del estudiante.");
                    txtDocumento.Focus();
                    return false;
                }
                else if (txtPnom.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el primer nombre del estudiante.");
                    txtPnom.Focus();
                    return false;
                }
                else if (txtPape.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el primer apellido del estudiante.");
                    txtPape.Focus();
                    return false;
                }
                else if (txtTel.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el teléfono del estudiante.");
                    txtTel.Focus();
                    return false;
                }
                //else if (cbxRoles.ItemIndex <= -1)
                //{
                //    frm.lblMensaje.Text = "Debe seleccionar el rol inical del maestro.";
                //    frm.ShowDialog();
                //    cbxRoles.Focus();
                //    return false;
                //}
                else if (cbxSexo.SelectedIndex <= 0)
                {
                    ClFunciones.msgError("Debe seleccionar el sexo del estudiante.");
                    cbxSexo.Focus();
                    return false;
                }
                else if (dtpFecNac.DateTime.Date == fechaNull)
                {
                    ClFunciones.msgError("Debe seleccionar la fecha de nacimiento del estudiante.");
                    dtpFecNac.Focus();
                    return false;
                }
                else if (cbxMuni.ItemIndex <= -1)
                {
                    ClFunciones.msgError("Debe seleccionar el lugar de nacimiento del estudiante.");
                    cbxMuni.Focus();
                    return false;
                }
                else if (txtRh.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar el RH del estudiante.");
                    txtRh.Focus();
                    return false;
                }
                else if (txtEps.ValorTextBox == "")
                {
                    ClFunciones.msgError("Debe digitar la EPS del estudiante.");
                    txtEps.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                 MessageBox.Show("Error : " + ex.Message);
                return false;
            }
        }

        #endregion


        #region[LimpiarCampos]
        private void LimpiarCampos()
        {
            txtDocumento.ValorTextBox = "";
            txtPnom.ValorTextBox = "";
            txtSnom.ValorTextBox = "";
            txtPape.ValorTextBox = "";
            txtSape.ValorTextBox = "";
            txtTel.ValorTextBox = "";
            txtUsu.ValorTextBox = "";
            txtDir.ValorTextBox = "";
            txtRh.ValorTextBox = "";
            txtEps.ValorTextBox = "";
            cbxSexo.SelectedIndex = 0;
            dtpFecNac.DateTime = DateTime.Now;
            cbxTipoDoc.EditValue = null;
            cbxPais.EditValue = null;
            cbxDpto.EditValue = null;
            cbxMuni.EditValue = null;
            EstaActualizando = false;
            pcbFoto.Image = null;
            txtDocumento.Enabled = true;
            btnAcudiente.Enabled = false;
            cbxTipoDoc.Focus();
        }

        #endregion

        private void cbxPais_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                cbxDpto.Properties.DataSource = new DataTable();
                cbxMuni.Properties.DataSource = new DataTable();
                if (cbxPais.ItemIndex > 0)
                {
                    LlenarCbxDpto(cbxPais.EditValue.ToString());
                    //cbxDpto.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void cbxDpto_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                cbxMuni.Properties.DataSource = new DataTable();
                if (cbxDpto.ItemIndex > 0)
                {
                    LlenarCbxMunis(cbxDpto.EditValue.ToString());
                    //cbxDpto.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void cbxTipoDoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                SendKeys.Send("{TAB}");
            }
            else if (e.KeyChar == (char)Keys.Escape)
            {
                e.Handled = true;
                SendKeys.Send("+{TAB}");
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidaCampos())
                {
                    bool inserto = true;
                    FrmMensaje frm = new FrmMensaje();
                    try
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                            {
                                con.Open();
                                if (EstaActualizando)
                                {
                                   
                                    bool res = clTercero.UpdateTercero(Convert.ToInt64(txtDocumento.ValorTextBox)
                                                                        , Convert.ToInt32(cbxTipoDoc.EditValue)
                                                                        , txtPnom.ValorTextBox
                                                                        , txtSnom.ValorTextBox
                                                                        , txtPape.ValorTextBox
                                                                        , txtSape.ValorTextBox
                                                                        , txtDir.ValorTextBox
                                                                        , txtCorreo.ValorTextBox
                                                                        , Convert.ToDouble(txtTel.ValorTextBox)
                                                                        , DateTime.Now
                                                                        , ClFunciones.UsuarioIngreso.IdUsuario
                                                                        , cbxSexo.SelectedIndex
                                                                        , dtpFecNac.DateTime
                                                                        , cbxMuni.EditValue.ToString()
                                                                        , pcbFoto.Image
                                                                        , txtRh.ValorTextBox
                                                                        , txtEps.ValorTextBox
                                                                        , con);

                                    if (res)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        inserto = false;
                                    }
                                    
                                }
                                else
                                {
                                    string psw = txtDocumento.ValorTextBox.Substring(txtDocumento.ValorTextBox.Length - 4, 4);
                                    int idNewUsu = clUsuarios.CrearUsuario(txtUsu.ValorTextBox
                                                                        , ClFunciones.UsuarioIngreso.IdUsuario
                                                                        , DateTime.Now, psw
                                                                        , 1, con);
                                    if (idNewUsu > 0)
                                    {
                                        bool res = clTercero.InsertTercero(Convert.ToInt64(txtDocumento.ValorTextBox)
                                                                        , Convert.ToInt32(cbxTipoDoc.EditValue)
                                                                        , txtPnom.ValorTextBox
                                                                        , txtSnom.ValorTextBox
                                                                        , txtPape.ValorTextBox
                                                                        , txtSape.ValorTextBox
                                                                        , txtDir.ValorTextBox
                                                                        , txtCorreo.ValorTextBox
                                                                        , Convert.ToDouble(txtTel.ValorTextBox)
                                                                        , DateTime.Now
                                                                        , ClFunciones.UsuarioIngreso.IdUsuario
                                                                        , idNewUsu
                                                                        , cbxSexo.SelectedIndex
                                                                        , dtpFecNac.DateTime
                                                                        , cbxMuni.EditValue.ToString()
                                                                        , 2
                                                                        , pcbFoto.Image
                                                                        , txtRh.ValorTextBox
                                                                        , txtEps.ValorTextBox
                                                                        , con);
                                        if (res)
                                        {
                                            scope.Complete();
                                        }
                                        else
                                        {
                                            inserto = false;
                                        }
                                    }
                                    else
                                    {
                                        inserto = false;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error : " + ex.Message);
                    }

                    if (inserto)
                    {
                        frm.lblMensaje.Text = string.Format("Estudiante creado de manera correcta, el usuario es {0} y la contraseña provisional son los 4 digitos finales del id.", txtUsu.Text);
                        if (EstaActualizando)
                        {
                            frm.lblMensaje.Text = "Estudiante actualizado de manera correcta.";                            
                        }
                        frm.ShowDialog();
                        LlenarGrilla();
                        LimpiarCampos();
                        //this.Close();
                        //LimpiarCampos();
                    }
                    else
                    {
                        frm.lblMensaje.Text = "Lo sentimos, ocurrio un error, por favor intentelo nuevamente.";
                        frm.ShowDialog();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void CrearNomUsu()
        {
            string nomUsu = clUsuarios.CreaNomUsuario(txtPnom.ValorTextBox, txtPape.ValorTextBox, ClConexion.clConexion.Conexion);
            if (nomUsu.Length > 20)
            {
                nomUsu = nomUsu.Substring(0, 20);
            }
            txtUsu.ValorTextBox = nomUsu;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarCampos();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddFoto_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Seleccionar fotografía";
                dlg.Filter = "Archivo de imagen (*.jpg, *.jpeg, *.jpe, *.png) | *.jpg; *.jpeg; *.jpe; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    pcbFoto.Image = new Bitmap(dlg.FileName);
                }
            }
        }

        private void btnQuitaFoto_Click(object sender, EventArgs e)
        {
            pcbFoto.Image = null;
        }

        private void cbxDpto_Enter(object sender, EventArgs e)
        {
            LookUpEdit lkp = sender as LookUpEdit;
            lkp.ShowPopup();
        }

        private void cbxSexo_Enter(object sender, EventArgs e)
        {
            ComboBoxEdit cbx = sender as ComboBoxEdit;
            cbx.ShowPopup();
        }

        private void txtPnom_Leave(object sender, EventArgs e)
        {
            if (EstaActualizando)
                return;
            txtUsu.ValorTextBox = "";
            if (txtPnom.ValorTextBox.Length > 0 && txtPape.ValorTextBox.Length > 0)
            {
                CrearNomUsu();
            }
        }

        private void txtPape_Leave(object sender, EventArgs e)
        {
            if (EstaActualizando)
                return;
            txtUsu.ValorTextBox = "";
            if (txtPnom.ValorTextBox.Length > 0 && txtPape.ValorTextBox.Length > 0)
            {
                CrearNomUsu();
            }
        }

        private void CargarEstExcel()
        {
            try
            {

                DataSet dsPatent = new DataSet();
                DataTable dt = new DataTable();
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.Filter = "xlsx Files (*.xlsx)|*.xlsx|All Files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string filename = openFileDialog1.FileName;
                    dt = ExcelDataBaseHelper.OpenFile(filename, "Hoja1");
                    if (dt.Rows.Count <= 0)
                    {
                        return;
                    }
                }


                using (TransactionScope scope = new TransactionScope())
                {
                    using (SqlConnection con = new SqlConnection(ClConexion.clConexion.CadenaConexion))
                    {
                        con.Open();
                        foreach (DataRow fila in dt.Rows)
                        {
                            string psw = fila["IdTercero"].ToString().Substring(fila["IdTercero"].ToString().Length - 4, 4);
                            string usu = clUsuarios.CreaNomUsuario(fila["Pnombre"].ToString(), fila["Papellido"].ToString(), con);
                            int idNewUsu = clUsuarios.CrearUsuario(usu
                                                                , ClFunciones.UsuarioIngreso.IdUsuario
                                                                , DateTime.Now, psw
                                                                , 1, con);
                            if (idNewUsu > 0)
                            {
                                bool res = clTercero.InsertTercero(Convert.ToInt64(fila["IdTercero"])
                                                                , Convert.ToInt32(fila["TipoDoc"])
                                                                , fila["Pnombre"].ToString()
                                                                , fila["Snombre"].ToString()
                                                                , fila["Papellido"].ToString()
                                                                , fila["Sapellido"].ToString()
                                                                , fila["Direccion"].ToString()
                                                                , fila["Correo"].ToString()
                                                                , Convert.ToInt64(fila["Telefono"])
                                                                , DateTime.Now
                                                                , ClFunciones.UsuarioIngreso.IdUsuario
                                                                , idNewUsu
                                                                , cbxSexo.SelectedIndex
                                                                , Convert.ToDateTime(fila["FechaNacimiento"])
                                                                , ""
                                                                , 2
                                                                , pcbFoto.Image
                                                                , fila["RH"].ToString()
                                                                , fila["EPS"].ToString()
                                                                , con);
                                if (!res)
                                {
                                    return;
                                }

                            }
                            else
                            {
                                return;
                            }

                        }
                        scope.Complete();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LlenarGrilla();         
            //CargarEstExcel();
        }

        private void btnAcudiente_Click(object sender, EventArgs e)
        {
            FrmAcudientes frm = new FrmAcudientes(EstudianteActual);
            frm.ShowDialog();
        }

        private void cbxMuni_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void btnCarnet_Click(object sender, EventArgs e)
        {
            if (!EstaActualizando) return;
            
        }

        private void btnCam_Click(object sender, EventArgs e)
        {
            TakePictureDialog d = new TakePictureDialog();
            if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                pcbFoto.Image = new Bitmap(d.Image);
            }
        }
    }
}
