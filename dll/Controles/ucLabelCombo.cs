﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace dll.Controles
{
    public partial class ucLabelCombo : DevExpress.XtraEditors.XtraUserControl
    {
        public delegate void EventHandler(object sender, EventArgs e);
        public event EventHandler SaleControl;
        private string _MensajeDeAyuda;
        private int _AnchoTitulo = 100;
        private int _AnchoTextBox = 200;
        private string _ValorTextBox = "";
        private string _TextoTitulo = "Titulo";
        public ucLabelCombo()
        {
            InitializeComponent();
        }

        [Category("Propiedades propias")]
        [Description("Mensaje de ayuda.")]
        public string MensajeDeAyuda
        {
            get
            {
                return _MensajeDeAyuda;
            }
            set
            {
                _MensajeDeAyuda = value;
            }
        }

        [Category("Propiedades propias")]
        [Description("Ancho del titulo.")]
        public int AnchoTitulo
        {
            get
            {
                return _AnchoTitulo;
            }
            set
            {
                _AnchoTitulo = value;
                tableLayoutPanel1.ColumnStyles[0].Width = AnchoTitulo;

            }
        }

        [Category("Propiedades propias")]
        [Description("Texto del titulo.")]
        public string TextoTitulo
        {
            get
            {
                return _TextoTitulo;
            }
            set
            {
                _TextoTitulo = value;
                lblTitulo.Text = TextoTitulo;
            }
        }



        private void lkeDatos_Enter(object sender, EventArgs e)
        {
            try
            {
                LookUpEdit lke = (LookUpEdit)sender;
                if (lke != null)
                {
                    lke.BackColor = Color.LightCyan;
                    lke.SelectionStart = 0;
                    lke.SelectionLength = lke.Text.Length;
                    lke.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                    lblTitulo.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
                    dll.Common.Class.ClFuncionesdll.BarStaticItemPrincipal.Caption = this.MensajeDeAyuda;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lkeDatos_Leave(object sender, EventArgs e)
        {
            try
            {
                LookUpEdit lke = (LookUpEdit)sender;
                if (lke != null)
                {
                    lke.BackColor = Color.White;
                    lke.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular);
                    lblTitulo.Font = new Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular);
                    dll.Common.Class.ClFuncionesdll.BarStaticItemPrincipal.Caption = "";
                }

                if (SaleControl != null)
                    SaleControl(this, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lkeDatos_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Enter)
                {
                    e.Handled = true;
                    SendKeys.Send("{TAB}");
                }
                else if (e.KeyChar == (char)Keys.Escape)
                {
                    e.Handled = true;
                    SendKeys.Send("+{TAB}");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lkeDatos_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                //if (e.KeyCode == Keys.Up)
                //{
                //    e.Handled = true;
                //    SendKeys.Send("+{TAB}");
                //}
                //else if (e.KeyCode == Keys.Down)
                //{
                //    e.Handled = true;
                //    SendKeys.Send("{TAB}");
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
